// if you checked "fancy-settings" in extensionizr.com, uncomment this lines

// var settings = new Store("settings", {
//     "sample_setting": "This is how you use Store.js to remember values"
// });


//example of using a message handler from the inject scripts
 // Set up the context menus
 


/* Register a listener for the `onClicked` event */
/*chrome.contextMenus.onClicked.addListener(function(info, tab) {
    if (tab) {
         
        var code = [
            'var d = document.createElement("div");',
            'd.setAttribute("style", "'
                + 'background-color: red; '
                + 'width: 100px; '
                + 'height: 100px; '
                + 'position: fixed; '
                + 'top: 70px; '
                + 'left: 30px; '
                + 'z-index: 9999; '
                + '");',
            'document.body.appendChild(d);'
        ].join("\n");
 
        chrome.tabs.executeScript(tab.id, { 
            code: code });
        // changeBgkColour() here:
        chrome.tabs.insertCSS(tab.id, {code: "body{border:1px solid red}"});
        //chrome.tabs.insertCSS(tab.id, {file: "src/bg/context.css"});
    }
});*/

// A generic onclick callback function.
function genericOnClick(info, tab) {
  //alert("item " + info.menuItemId + " was clicked");
   
   //alert(info.menuItemId+' - '+JSON.stringify(info)  );
   
  //alert( info.menuItemId+' - '+typeof info.menuItemId );
  //clic droit sur ?
  //Image
    if (typeof info.linkUrl != 'undefined') { 
         addVazotsara(info.linkUrl,tab.id);
    }
    //Page
     else {    
        //alert('clic page : '+ info.pageUrl+' - '+typeof info.linkUrl );
        addVazotsara(info.pageUrl,tab.id);
    }
 

}

function addVazotsara(link,tabId)
{
    /*var code = [
            'var d = document.createElement("div");',
            'd.setAttribute("style", "'
                + 'background-color: red; '
                + 'width: 100px; '
                + 'height: 100px; '
                + 'position: fixed; '
                + 'top: 70px; '
                + 'left: 30px; '
                + 'z-index: 9999; '
                + '");',
            'document.body.appendChild(d);'
        ].join("\n");*/

    var code = [
        "if(null != document.querySelector('.vztiframeyt') ) {",
        "document.querySelector('.vztiframeyt').remove();",
        "}",
        "var divvztiframeyt = document.createElement('div');",
        "divvztiframeyt.className = 'vztiframeyt';",
        "var clearfix = document.createElement('div');",
        "clearfix.className = 'vztvghead clearfix';",
        "var texte_left = document.createElement('span');",
        "texte_left.className = 'vzttextleft yt watch-title-cdontainer floatleft';",
        "texte_left.textContent = 'Ajouter sur Vazotsara';",
        "var btnclose = document.createElement('a');",
        "btnclose.setAttribute('href','javascript:;');",
        "btnclose.className = 'vztbtnclose floatright yt-uix-button   yt-uix-sessionlink yt-uix-button-default yt-uix-button-size-default';",
        "btnclose.innerHTML = '<span class=\"yt-uix-button-content\">Fermer</span>'",
        "btnclose.onclick = function(){",
        "document.querySelector('.vztiframeyt').style.display = 'none';",
        "}",
        "var vztiframeyt = document.createElement('iframe');",
        "vztiframeyt.setAttribute('src','//www.vazotsara.com/backend/addOrEdit?url='+encodeURIComponent('"+link+"'));",
        "clearfix.appendChild(texte_left);",
        "clearfix.appendChild(btnclose);",
        "divvztiframeyt.appendChild(clearfix);",
        "divvztiframeyt.appendChild(vztiframeyt);",
        "if(document.body != null){",
        "document.body.appendChild(divvztiframeyt);",
        "}" 
         
    ].join("\n");

     
        chrome.tabs.executeScript(tabId, {  code: code });
}

// Create one test item for each context type.
var contexts = ["page","link","image"];
for (var i = 0; i < contexts.length; i++) {
  var context = contexts[i];
  var title = "Ajouter/Modifier sur Vazotsara";
  var id = chrome.contextMenus.create({"title": title, "contexts":[context], "onclick": genericOnClick}); 
}