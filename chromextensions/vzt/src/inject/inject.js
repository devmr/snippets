'use strict';

var ytid = 'UCvgLVxn_1Wf7IUZq0rctkSA';
var vazt = 'Vzt::ext *- ';
var txt_button = 'Modifier les infos';

function scrollTo(element, to, duration) {
    if (duration < 0) return;
    var difference = to - element.scrollTop;
     
    var perTick = difference / duration * 10;

    setTimeout(function() {
        element.scrollTop = element.scrollTop + perTick;
        if (element.scrollTop === to) return;
        scrollTo(element, to, duration - 10);
    }, 10);
}

window.addEventListener("load", function() {
   console.log(vazt+'loaded');
   var alldata = document.querySelectorAll(".yt-user-info");
   if(  alldata.length > 0 ) {
        [].forEach.call(alldata, function(el) {

            var a = el.querySelector("a");
            if( ytid == a.getAttribute('data-ytid') ) {
                console.log(vazt+'On peut modifier');
                //Ajouter un bouton apres .watch7-creator-bar-nav-buttons
                var ul_add = document.getElementById('watch7-creator-bar-nav-buttons');
                 
                //Ajouter <li>
                var li_add_to_ul = document.createElement('li');
                li_add_to_ul.className = 'creator-bar-item';

                //Ajouter <a>
                var a_add_to_li = document.createElement('a');
                a_add_to_li.className = 'yt-uix-button   yt-uix-sessionlink yt-uix-button-default yt-uix-button-size-default';
                a_add_to_li.innerHTML = '<span class="yt-uix-button-content">'+txt_button+'</span>';

                li_add_to_ul.appendChild(a_add_to_li);
                ul_add.appendChild(li_add_to_ul);

                a_add_to_li.onclick = function(){
                    //Créer IFRAME
                    if(null == document.querySelector('.vztiframeyt') ) {
                        var divvztiframeyt = document.createElement('div');
                        divvztiframeyt.className = 'vztiframeyt';

                        //clearfix
                        var clearfix = document.createElement('div');
                        clearfix.className = 'vztvghead clearfix';

                        //Texte à gauche
                        var texte_left = document.createElement('span');
                        texte_left.className = 'vzttextleft yt watch-title-cdontainer floatleft';
                        texte_left.textContent = document.getElementById('eow-title').textContent;

                        //Ajouter bouton fermeture
                        var btnclose = document.createElement('a');
                        btnclose.className = 'vztbtnclose floatright yt-uix-button   yt-uix-sessionlink yt-uix-button-default yt-uix-button-size-default';
                        btnclose.innerHTML = '<span class="yt-uix-button-content">Fermer</span>';

                        btnclose.onclick = function(){
                            document.querySelector('.vztiframeyt').style.display = 'none';
                        }


                        var vztiframeyt = document.createElement('iframe');
                        vztiframeyt.setAttribute('src','//www.vazotsara.com/backend/editvideos?url='+encodeURIComponent(document.location));

                        clearfix.appendChild(texte_left);
                        clearfix.appendChild(btnclose);

                        divvztiframeyt.appendChild(clearfix);
                        divvztiframeyt.appendChild(vztiframeyt);

                        if(document.body != null){ 
                            document.body.appendChild(divvztiframeyt);
                        }
                    } else {
                         document.querySelector('.vztiframeyt').style.display = 'block';
                         scrollTo(document.body, document.querySelector('.vztiframeyt').offsetTop, 600);
                    }
                }

            }

        });
   }
});