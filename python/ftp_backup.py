#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      rabehasy
#
# Created:     23/10/2013
# Copyright:   (c) rabehasy 2013
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python
from hurry.filesize import size

import smtplib
import os
import ftplib
#import zipfile
#import tarfile
import shutil
import sys
import progressbar
import datetime
import time, math
'''
def download(pathsrc, pathdst):
    print(pathsrc)
    print(pathdst)
    lenpathsrc = len(pathsrc)
    def _download(pathsrc):
        l = ftp.nlst(pathsrc)
        for i in l:
            try:
                ftp.size(i)
                print (i)
                ftp.retrbinary('RETR '+i, open(pathdst+os.sep+i[lenpathsrc:], 'wb').write)
            except:
                try: os.makedirs(pathdst+os.sep+os.path.dirname(i[lenpathsrc:]))
                except: pass
                _download(i)
    _download(pathsrc)
'''

'''
def zipdir(path, zip):
    for root, dirs, files in os.walk(path):
        for file in files:
            zip.write(os.path.join(root, file))

def make_tarfile(output_filename, source_dir):
    with tarfile.open(output_filename, "w:gz") as tar:
        tar.add(source_dir, arcname=os.path.basename(source_dir))
'''

''' Vider la memoire de Python '''
def clearall():
    all = [var for var in globals() if var[0] != "_"]
    for var in all:
        del globals()[var]
        
def writelog(file, content ) :
    f = open(dossier_sauvegarde+os.sep+file, 'a')
    f.write(content+"\n");
    f.close()
    
def email(to,objet,message,headers=""):
    
    gmail_sender = 'madatsara@gmail.com'
    gmail_passwd = '4m60h1tr4'
     
       
    server = smtplib.SMTP('smtp.gmail.com', 587)
    print( server.ehlo() );
    print( server.starttls() );
    login = server.login(gmail_sender, gmail_passwd);
    print(login);
    headers = ['From: %s' % gmail_sender,
           'Subject: %s' % objet,
           'To: %s' % to,
           "MIME-Version: 1.0",
           "Content-Type: text/plain"];
    headers = "\r\n".join(headers);
     
    
    
    BODY = headers + "\r\n\r\n" + message ;
     
     
    try:
        server.sendmail( gmail_sender, [to], BODY )
        print('email sent');
    except IOError as e:
        print ("I/O error({0}): {1}"+format(e.errno, e.strerror))
    except ValueError:
        print (str(ValueError))
    except:
        print ("Unexpected error:", sys.exc_info()[0])
        raise
    server.quit()

def make_file_list( ls, value='FILE' ):
    if value == "SIZE":
        number = 4;
    elif value == "FILE" or value == "":
        number = -1;
    elif value == "DROITS":
        number = 0;
    elif value == "OWNER":
        number = 2;
    elif value == "GROUP":
        number = 3;
    return [ x.split()[number] for x in ls ]

def storeline( l ):
    LINES.append( l )

def getdossierbackup() :
    dossier_courant = str( os.getcwd() ) ;
    dossier_sauvegarde = input("Dossier de sauvegarde :"  )
    if( not dossier_sauvegarde ) :
        print('Le backup sera sauvegardé dans ',dossier_courant);
        ok = input("Êtes-vous OK ? (Y/n) :")
        if( ok.strip().lower() == 'n' ) :
            dossier_sauvegarde = getdossierbackup()
        elif( ok.strip() == '' ) :
            dossier_sauvegarde = dossier_courant
    print('OK - Le backup sera sauvegardé dans ',dossier_sauvegarde);
    return dossier_sauvegarde

        
    
'''''
'End Functions
'''


now = datetime.datetime.now()
tlog =  [];
tlogf = []
start_time = time.time()
#start_datetime = str(datetime.date.today().strftime("%d-%m-%Y_%H%M%S"));
start_datetime = str(now.strftime("%d-%m-%Y"));
filetar_name = 'backup_'+start_datetime;
LINES = [];
number = -1;

TO = 'rabehasy@gmail.com'
SUBJECT = 'Madatsara - FTP Backup Python '+start_datetime

ftphost = '176.31.188.215';
ftphost_ks = '91.121.132.77';
ftpusername = 'backupftp';
ftppassword = '4nt54k4v1r0';
dossier_courant = str( os.getcwd() ) ;
#dossier_sauvegarde = input("Dossier de sauvegarde :"  )
dossier_sauvegarde = getdossierbackup();

tlog.append('Debut du script : '+now.strftime("%H:%M:%S") );

tlogf.append('Debut du script : '+now.strftime("%H:%M:%S") );


    
folder = dossier_sauvegarde+os.sep+'backups';
folder_parent = dossier_sauvegarde+"\\boxsync"+os.sep;
file = folder_parent+filetar_name+'.tar.gz';





#Si le fichier backup gz existe 
if( os.path.exists( file ) ) :
    from datetime import datetime
    s1 = now.strftime("%d-%m-%Y %H:%M:%S")
    s2 = datetime.fromtimestamp( math.floor(os.path.getmtime(file)) ).strftime("%d-%m-%Y %H:%M:%S")
    FMT = '%d-%m-%Y %H:%M:%S'
    tdelta = abs( datetime.strptime(s1, FMT) - datetime.strptime(s2, FMT) )
    deltadays =  int( tdelta.days )
    #Si moins de 1 jour ne rien faire
    if( deltadays < 1 ) :
        print('Arret du script');
        writelog('logbkfile.txt', "\n".join(tlogf) )
        raise SystemExit

if( os.path.exists(folder) ) :
    shutil.rmtree(folder);
    print('Suppression du dossier backup');
    tlog.append( 'Suppression du dossier backups sur le DD : '+folder);

 
          
print('Creation du dossier backups');
tlog.append( 'Creation du dossier backups sur le DD : '+folder);            
os.mkdir(folder);
os.mkdir(folder+os.sep+'vps');
tlog.append( 'Creation du dossier vps sur le DD : '+folder+os.sep+'vps');
os.mkdir(folder+os.sep+'kimsufi');
tlog.append( 'Creation du dossier kimsufi sur le DD : '+folder+os.sep+'kimsufi');

ftp = ftplib.FTP('')
try:
    ftp.connect(ftphost, 21) #176.31.188.215 : VPS
    ftp.login(ftpusername, ftppassword)
except:
    print("Unexpected error:", sys.exc_info()[0])
    email(TO,SUBJECT,"Erreur de connexion FTP : "+ftphost+' - '+ftpusername+' / '+ftppassword);
    raise
else:
    print( ftp.getwelcome() )
tlog.append('VPS - Msg de bienvenue FTP : '+ftp.getwelcome());            
#l = ftp.nlst('/')
ftp.cwd('/');
lines = ftp.retrlines('LIST',storeline);


files_size = make_file_list( LINES, 'SIZE' );
files_name = make_file_list( LINES, 'FILE' );

nbfile_ftp = len(files_size) 
tlog.append('VPS - Nb de fichier sur FTP : '+str( len(files_size) ) );
tlog.append('VPS - Debut recup fichiers FTP '+datetime.datetime.now().strftime("%H:%M:%S") );
i=0;
for fn in files_name:
    #if fn == "madatsara_files.tar.gz" :
    #    continue
    filesize = int( files_size[i] );
    filesize_format = size( filesize );
    progress = progressbar.AnimatedProgressBar(end=filesize)
    localfile = folder+os.sep+'vps'+os.sep+fn;    
    print("Téléchargement de "+fn+" - ("+filesize_format+") vers "+localfile);
    #tlog.append("Téléchargement de "+fn+" - ("+filesize_format+") vers "+localfile );
    tlog.append('VPS - Debut Telechargement '+fn+' - '+datetime.datetime.now().strftime("%H:%M:%S") );
 
    with open(localfile, 'wb') as f:
        def callback(chunk):
            f.write(chunk)
            progress + len(chunk)
            progress.show_progress()
            print("\n");

        ftp.retrbinary('RETR /'+fn, callback );
        tlog.append('VPS - Fin Telechargement '+fn+' - '+datetime.datetime.now().strftime("%H:%M:%S") );
    i+=1;
#Suppression des fichiers dans ftp
tlog.append('VPS - Suppression des fichiers dans ftp' );
for fn in files_name:
    ftp.delete('/'+fn);
ftp.close()

LINES = []

### KIMSUFI
ftp = ftplib.FTP('')
try:
    ftp.connect(ftphost_ks, 21) #176.31.188.215 : VPS
    ftp.login(ftpusername, ftppassword)
except:
    print("Unexpected error:", sys.exc_info()[0])
    email(TO,SUBJECT,"Erreur de connexion FTP : "+ftphost_ks+' - '+ftpusername+' / '+ftppassword);
    raise
else:
    print( ftp.getwelcome() )
tlog.append('KIMSUFI - Msg de bienvenue FTP : '+ftp.getwelcome());            
#l = ftp.nlst('/')
ftp.cwd('/');
lines = ftp.retrlines('LIST',storeline);


files_size = make_file_list( LINES, 'SIZE' );
files_name = make_file_list( LINES, 'FILE' );

nbfile_ftp = nbfile_ftp+len(files_size)

tlog.append('KIMSUFI - Nb de fichier sur FTP : '+str( len(files_size) ) );
tlog.append('KIMSUFI/VPS - Nb de fichier sur FTP : '+str( nbfile_ftp ) );
tlog.append('KIMSUFI - Debut recup fichiers FTP '+datetime.datetime.now().strftime("%H:%M:%S") );
#Si aucun fichier dans ftp - supprimer les dossiers
if( nbfile_ftp == 0 ) :
    print( 'Aucun fichier dans le serveur FTP' )
    if( os.path.exists(folder) ) :
        print( 'Suppression du dossier '+folder )
        shutil.rmtree(folder);
        ftp.close()
    writelog('logbkfile.txt', "\n".join(tlogf) )
    raise SystemExit
i=0;
for fn in files_name:
    #if fn == "madatsara_files.tar.gz" :
    #    continue
    filesize = int( files_size[i] );
    filesize_format = size( filesize );
    progress = progressbar.AnimatedProgressBar(end=filesize)
    localfile = folder+os.sep+'kimsufi'+os.sep+fn;    
    print("Téléchargement de "+fn+" - ("+filesize_format+") vers "+localfile);
    #tlog.append("Téléchargement de "+fn+" - ("+filesize_format+") vers "+localfile );
    tlog.append('KIMSUFI - Debut Telechargement '+fn+' - '+datetime.datetime.now().strftime("%H:%M:%S") );
 
    with open(localfile, 'wb') as f:
        def callback(chunk):
            f.write(chunk)
            progress + len(chunk)
            progress.show_progress()
            print("\n");

        ftp.retrbinary('RETR /'+fn, callback );
        tlog.append('KIMSUFI - Fin Telechargement '+fn+' - '+datetime.datetime.now().strftime("%H:%M:%S") );
    i+=1;
#Suppression des fichiers dans ftp
tlog.append('KIMSUFI - Suppression des fichiers dans ftp' );
for fn in files_name:
    ftp.delete('/'+fn);
ftp.close()

print('Compression tar en cours...'+filetar_name+'.tar.gz - '+now.strftime("%H:%M:%S"))
tlog.append('Compression tar en cours...'+folder_parent+filetar_name+'.tar.gz - '+now.strftime("%H:%M:%S"));            
shutil.make_archive(folder_parent+filetar_name, 'gztar',folder,folder);

print('Compression tar terminee');
tlog.append('Compression tar terminee - '+datetime.datetime.now().strftime("%H:%M:%S"));


 
#parcourir le répertoire en cours et afficher les fichiers dedans
#print(os.getcwd())
#print(sys.path)
dirl = os.listdir( folder+os.sep )
t = 0;
for data in dirl:     
    taille = os.path.getsize(folder+os.sep+data);
    t+=taille;
    #print(data+' '+str( taille ));
    
print('Taille du dossier backup '+size(t)); 
tlog.append('Taille du dossier backup '+size(t));
            
#Suppression du dossier
print('Suppression du dossier '+folder);
tlog.append('Suppression du dossier '+folder);            
try:
    shutil.rmtree(folder);
except OSError as errorsuppr:
    tlog.append('Erreur suppression du dossier ',folder,' - ',errorsuppr );
    pass
endtime = time.time() - start_time;



    
print('Temps d\'execution '+str(endtime) ); 
tlog.append('Temps d\'execution du script : '+str(endtime) );  
tlog.append('Fin du script : '+datetime.datetime.now().strftime("%H:%M:%S"));

tlogf.append('Fin du script : '+datetime.datetime.now().strftime("%H:%M:%S"));
writelog('logbkfile.txt', "\n".join(tlogf) )




#print(TEXT);
'''
f = open(os.getcwd()+os.sep+'testfile.txt', 'w')
f.write("\n".join(tlog));
f.close()
    
message = open(os.getcwd()+os.sep+'testfile.txt', 'r').read()
'''
TEXT = "\n".join(tlog);

email(TO,SUBJECT,TEXT);

clearall();
sys.exit();
 
'''
print('Compression en cours...')
zip = zipfile.ZipFile('C:\pythondata\Python.zip', 'w')
zipdir('C:\pythondata\data\\', zip)
zip.close()
print('Compression terminÃ©e')



#Compression tar avec shutil
#shutil.make_archive('C:\pythondata\Python2', 'gztar','C:\pythondata\data')

#print('Compression tar terminÃ©e')
#Suppression du dossier
#shutil.rmtree('C:\pythondata\data\\')
'''
