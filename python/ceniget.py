#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      rabehasy
#
# Created:     23/10/2013
# Copyright:   (c) rabehasy 2013
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python
import urllib.request, re, datetime , smtplib
from xml.dom.minidom import parseString
from bs4 import BeautifulSoup
import pymysql, sys

try:
   conn = pymysql.connect(host='176.31.188.215', port=3306, user='l7u73728f9fun', passwd='JgC+8(Eo', db='compareprogram')
except :
   print('erreur de connexion')
   raise SystemExit
try:
   cur = conn.cursor()
   #cur.execute("SELECT COUNT(*) from mg_region ")
   #result = cur.fetchone()
   #countregion = result[0] ;

   #sql = "SELECT libelle FROM mg_region LIMIT 10"
   #cur.execute(sql)
    
except :
   print('erreur de requete',str(sql))
   raise SystemExit   
#print(cur.description)
#print(countresult)
#for row in cur:
#   print(row)
#cur.close()
#conn.close()
#raise SystemExit


######## Fonctions ##############
def email(to,objet,message,headers=""):
    
    gmail_sender = 'madatsara@gmail.com'
    gmail_passwd = '4m60h1tr4'
     
       
    server = smtplib.SMTP('smtp.gmail.com', 587)
    print( server.ehlo() );
    print( server.starttls() );
    login = server.login(gmail_sender, gmail_passwd);
    print(login);
    headers = ['From: %s' % gmail_sender,
           'Subject: %s' % objet,
           'To: %s' % to,
           "MIME-Version: 1.0",
           "Content-Type: text/plain"];
    headers = "\r\n".join(headers);
     
    
    
    BODY = headers + "\r\n\r\n" + message ;
     
     
    try:
        server.sendmail( gmail_sender, [to], BODY )
        print('email sent');
    except IOError as e:
        print ("I/O error({0}): {1}"+format(e.errno, e.strerror))
    except ValueError:
        print (str(ValueError))
    except:
        print ("Unexpected error:", sys.exc_info()[0])
        raise
    server.quit()

def addslashes(s):
    l = ["\\", '"', "'", "\0", ]
    for i in l:
        if i in s:
            s = s.replace(i, '\\'+i)
    return s
def getText(nodelist):
    rc = []
    for node in nodelist:
        if node.nodeType == node.TEXT_NODE:
            rc.append(node.data)
    return ''.join(rc)
def handleTok(tokenlist):
    texts = ""
    for token in tokenlist:
        texts += "\n"+ getText(token.childNodes)
    return texts.strip()
def getID(liste):
    domain = re.search("[0-9]*", liste)
    return domain.group()
def getTitre(liste):
    s = re.sub('[0-9]*','',liste).strip();
    return s[1:]
def get_tab(url):
    tab_option =  [];
    response = urllib.request.urlopen(url)
    data = response.read()
    soup = BeautifulSoup(data)
    result = soup.find_all( 'option') 
    for option in result:
        if( option.get_text()!="" ) :
            tab_option.append( option.get_text() )
    return tab_option
def exist_inbdd( valuename, tablename,cenitid,retour="nb" ) :
    sql = "SELECT COUNT(*) from "+tablename+" WHERE `cenitid` = '"+cenitid+"' AND `libelle` = '"+addslashes(valuename)+"'"
    cur.execute(sql)
    result = cur.fetchone()
    nb = result[0];
    if(retour == "sql"):
        return sql
    return nb;
def db_insert(tablename,valuename,valueid,valuesuppl="") :
    sql = "INSERT INTO "+tablename+" SET `libelle` = '"+addslashes(valuename)+"', cenitid = '"+valueid+"'"
    if( valuesuppl != ""):
        sql = sql+","+valuesuppl;
    try:
        cur.execute(sql)
    except :
        print('erreur requete'+sql)
    conn.commit()
    return sql;
def db_update(tablename,valuename,valueid,valuesuppl="") :
    sql = "UPDATE "+tablename+" SET cenitid = '"+valueid+"'"
    if( valuesuppl != ""):
        sql = sql+","+valuesuppl;
    sql = sql+" WHERE `libelle` = '"+addslashes(valuename)+"'"
    cur.execute(sql)
    conn.commit()
    return sql;
######## Fonctions ##############
now = datetime.datetime.now()
start_datetime = str(now.strftime("%d-%m-%Y"));
TO = 'rabehasy@gmail.com'
SUBJECT = 'Madatsara - Python CENIget.py '+start_datetime
sql =  [];
tlog =  [];
countresult = 0;
tlog.append('Debut du script : '+now.strftime("%H:%M:%S") );

'''
response = urllib.request.urlopen("http://www.cenit-madagascar.mg/res/charge_fokontany_.php?id_commune=510215")
data = response.read()
soup = BeautifulSoup(data)
result = soup.find_all( 'option') 
for option in result:
    print(getID( option.get_text() ) )
raise SystemExit
'''


#Prendre les select globaux
response = urllib.request.urlopen('http://www.cenit-madagascar.mg/res/Res_par_bv_.php')
s = response.read()
soup = BeautifulSoup(s)
result = soup.find('td', attrs={'width': '45%'}).find_all( 'option')

for link in result:
    liste = link.get_text();
    #print( 'ID:#'+getID(liste)+'# - titre : #'+getTitre(liste)+'#'+str(  getID(liste).isdigit() )  );
    
    regioname = getTitre(liste)
    regionid = getID(liste)
    if( regioname != "" ) :
        print( 'Région : '+regioname+' (ID:'+regionid+') - Exist in BDD : '+str( exist_inbdd(regioname,'mg_region',regionid) ) )

        #Verif si la valeur n'existe pas en BDD -> insert
        print( exist_inbdd(regioname,'mg_region',regionid, "sql" ) )
        if( exist_inbdd(regioname,'mg_region',regionid ) == 0 ) :
            #Insert
            print( db_insert('mg_region',regioname,regionid) )
        else:
            #Update
            print( db_update('mg_region',regioname,regionid) )
            
    #Parse District dans les regions
    if( getID(liste).isdigit() ):
        tab = get_tab('http://www.cenit-madagascar.mg/res/charge_dist_.php?id_region='+getID(liste))

        
        for liste in tab :
            #print( 'ID:'+getID(liste)+'# - District : #'+getTitre(liste)+'#'  );
            
            districtname = getTitre(liste)
            districtid = getID(liste)
            print( 'Région '+regioname+' > District : '+districtname+' (ID:'+districtid+')' );
            

            #Verif si la valeur n'existe pas en BDD -> insert
            print( exist_inbdd(districtname,'mg_district',districtid, "sql" ) )
            if( exist_inbdd(districtname,'mg_district',districtid ) == 0 ) :
                #Insert
                print( db_insert('mg_district',districtname,districtid,"`cenitrid` = '"+regionid+"'") )
            else:
                print( db_update('mg_district',districtname,districtid,"`cenitrid` = '"+regionid+"'") )
                
            #Parse Communes dans les districts
            if( getID(liste).isdigit() ):
                tab = get_tab('http://www.cenit-madagascar.mg/res/charge_commune_.php?id_district='+getID(liste))
                
                for liste in tab :
                    #print( 'ID:'+getID(liste)+'# - Commune : #'+getTitre(liste)+'#'  );
                    
                    communename = getTitre(liste)
                    communeid = getID(liste)
                    print( 'Région '+addslashes(regioname)+' > District : '+districtname+' > Commune : '+communename+' (ID:'+communeid+')' );

                    #Verif si la valeur n'existe pas en BDD -> insert
                    print( exist_inbdd(communename,'mg_commune',communeid, "sql" ) )
                    if( exist_inbdd(communename,'mg_commune',communeid ) == 0 ) :
                        #Insert
                        print( db_insert('mg_commune',communename,communeid,"`cenitrid` = '"+regionid+"', `cenitdid` = '"+districtid+"'") )
                    else:
                        print( db_update('mg_commune',communename,communeid,"`cenitrid` = '"+regionid+"', `cenitdid` = '"+districtid+"'") )
                    
                    #Parse Fokontany dans les communes
                    if( getID(liste).isdigit() ):
                        tab = get_tab('http://www.cenit-madagascar.mg/res/charge_fokontany_.php?id_commune='+getID(liste))
                        for liste in tab :
                            fokontanyname = getTitre(liste)
                            fokontanyid = getID(liste)
                            print( 'Région '+regioname+' > District '+districtname+' > Commune '+communename+' > Fokontany : '+fokontanyname+' (ID:'+fokontanyid+')' );

                            #Verif si la valeur n'existe pas en BDD -> insert
                            print( exist_inbdd(fokontanyname,'mg_fokontany',fokontanyid, "sql" ) )
                            if( exist_inbdd(fokontanyname,'mg_fokontany',fokontanyid ) == 0 ) :
                                #Insert
                                print( db_insert('mg_fokontany',fokontanyname,fokontanyid,"`cenitrid` = '"+regionid+"', `cenitdid` = '"+districtid+"', `cenitcid` = '"+communeid+"'") )
                            else:
                                print( db_update('mg_fokontany',fokontanyname,fokontanyid,"`cenitrid` = '"+regionid+"', `cenitdid` = '"+districtid+"', `cenitcid` = '"+communeid+"'") )

                            #Parse Centre de vote dans les FKT
                            if( getID(liste).isdigit() ):
                                tab = get_tab('http://www.cenit-madagascar.mg/res/charge_cv_.php?id_fkt='+getID(liste) )
                                for liste in tab :
                                    
                                    cvname = getTitre(liste)
                                    cvid = getID(liste)
    
                                    print( 'Région '+regioname+' > District '+districtname+' > Commune '+communename+' > Fokontany : '+fokontanyname+' > CV : '+cvname+' (ID:'+cvid+')' );

                                    #Verif si la valeur n'existe pas en BDD -> insert
                                    print( exist_inbdd(cvname,'mg_cv',cvid, "sql" ) )
                                    if( exist_inbdd(cvname,'mg_cv',cvid ) == 0 ) :
                                        #Insert
                                        print( db_insert('mg_cv',cvname,cvid,"`cenitrid` = '"+regionid+"', `cenitdid` = '"+districtid+"', `cenitcid` = '"+communeid+"', `cenitfid` = '"+fokontanyid+"'") )
                                    else:
                                        print( db_update('mg_cv',cvname,cvid,"`cenitrid` = '"+regionid+"', `cenitdid` = '"+districtid+"', `cenitcid` = '"+communeid+"', `cenitfid` = '"+fokontanyid+"'") )
                                
                                    #Parse BV dans les Centre de vote
                                    if( getID(liste).isdigit() ):
                                        tab = get_tab('http://www.cenit-madagascar.mg/res/charge_bv_.php?id_cv='+getID(liste) )
                                        for liste in tab :
                                            
                                            bvname = getTitre(liste)
                                            bvid = getID(liste)

                                            print( 'Région '+regioname+' > District '+districtname+' > Commune '+communename+' > Fokontany : '+fokontanyname+' > CV : '+cvname+' > BV : '+bvname+' (ID:'+bvid+')' );
                                            #Verif si la valeur n'existe pas en BDD -> insert
                                            print( exist_inbdd(bvname,'mg_bv',bvid, "sql" ) )
                                            if( exist_inbdd(bvname,'mg_bv',bvid ) == 0 ) :
                                                #Insert
                                                print( db_insert('mg_bv',bvname,bvid,"`cenitrid` = '"+regionid+"', `cenitdid` = '"+districtid+"', `cenitcid` = '"+communeid+"', `cenitfid` = '"+fokontanyid+"', `cenitcvid` = '"+cvid+"'") )
                                            else:
                                                print( db_update( 'mg_bv',bvname,bvid,"`cenitrid` = '"+regionid+"', `cenitdid` = '"+districtid+"', `cenitcid` = '"+communeid+"', `cenitfid` = '"+fokontanyid+"', `cenitcvid` = '"+cvid+"'" ) )
    
                                print("-------------")
                        print("-------------")
                print("-------------")
        print("-------------")
#raise SystemExit
cur.close()
conn.close()
tlog.append('Fin du script : '+datetime.datetime.now().strftime("%H:%M:%S"));


TEXT = "\n".join(tlog);
email(TO,SUBJECT,TEXT);
