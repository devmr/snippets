#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      rabehasy
#
# Created:     23/10/2013
# Copyright:   (c) rabehasy 2013
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python
import urllib.request, re, datetime , smtplib
from xml.dom.minidom import parseString
from bs4 import BeautifulSoup
import pymysql, sys

try:
   conn = pymysql.connect(host='176.31.188.215', port=3306, user='l7u73728f9fun', passwd='JgC+8(Eo', db='compareprogram', charset='utf8')
except :
   print('erreur de connexion')
   raise SystemExit
try:
   cur = conn.cursor()
   #cur.execute("SELECT COUNT(*) from mg_region ")
   #result = cur.fetchone()
   #countregion = result[0] ;

   #sql = "SELECT libelle FROM mg_region LIMIT 10"
   #cur.execute(sql)
    
except :
   print('erreur de requete',str(sql))
   raise SystemExit   
#print(cur.description)
#print(countresult)
#for row in cur:
#   print(row)
#cur.close()
#conn.close()
#raise SystemExit


######## Fonctions ##############
def email(to,objet,message,headers=""):
    
    gmail_sender = 'madatsara@gmail.com'
    gmail_passwd = '4m60h1tr4'
     
       
    server = smtplib.SMTP('smtp.gmail.com', 587)
    print( server.ehlo() );
    print( server.starttls() );
    login = server.login(gmail_sender, gmail_passwd);
    print(login);
    headers = ['From: %s' % gmail_sender,
           'Subject: %s' % objet,
           'To: %s' % to,
           "MIME-Version: 1.0",
           "Content-Type: text/plain"];
    headers = "\r\n".join(headers);
     
    
    
    BODY = headers + "\r\n\r\n" + message ;
     
     
    try:
        server.sendmail( gmail_sender, [to], BODY )
        print('email sent');
    except IOError as e:
        print ("I/O error({0}): {1}"+format(e.errno, e.strerror))
    except ValueError:
        print (str(ValueError))
    except:
        print ("Unexpected error:", sys.exc_info()[0])
        raise
    server.quit()

def addslashes(s):
    l = ["\\", '"', "'", "\0", ]
    for i in l:
        if i in s:
            s = s.replace(i, '\\'+i)
    return s
def getText(nodelist):
    rc = []
    for node in nodelist:
        if node.nodeType == node.TEXT_NODE:
            rc.append(node.data)
    return ''.join(rc)
def handleTok(tokenlist):
    texts = ""
    for token in tokenlist:
        texts += "\n"+ getText(token.childNodes)
    return texts.strip()
def getID(liste):
    domain = re.search("[0-9]*", liste)
    return domain.group()
def getTitre(liste):
    s = re.sub('[0-9]*','',liste).strip();
    return s[1:]
def get_tab(url):
    tab_option =  [];
    response = urllib.request.urlopen(url)
    data = response.read()
    soup = BeautifulSoup(data)
    result = soup.find_all( 'option') 
    for option in result:
        if( option.get_text()!="" ) :
            tab_option.append( option.get_text() )
    return tab_option
def exist_inbdd( valuename, tablename,cenitid,retour="nb" ) :
    sql = "SELECT COUNT(*) from "+tablename+" WHERE `cenitid` = '"+cenitid+"' AND `libelle` = '"+addslashes(valuename)+"'"
    cur.execute(sql)
    result = cur.fetchone()
    nb = result[0];
    if(retour == "sql"):
        return sql
    return nb;
def db_insert(tablename,valuename,valueid,valuesuppl="") :
    sql = "INSERT INTO "+tablename+" SET `libelle` = '"+addslashes(valuename)+"', cenitid = '"+valueid+"'"
    if( valuesuppl != ""):
        sql = sql+","+valuesuppl;
    try:
        cur.execute(sql)
    except :
        print('erreur requete'+sql)
    conn.commit()
    return sql;
def db_update(tablename,valuename,valueid,valuesuppl="") :
    sql = "UPDATE "+tablename+" SET cenitid = '"+valueid+"'"
    if( valuesuppl != ""):
        sql = sql+","+valuesuppl;
    sql = sql+" WHERE `libelle` = '"+addslashes(valuename)+"'"
    cur.execute(sql)
    conn.commit()
    return sql;
######## Fonctions ##############
now = datetime.datetime.now()
start_datetime = str(now.strftime("%d-%m-%Y"));
TO = 'rabehasy@gmail.com'
SUBJECT = 'Madatsara - Python CENIget_fokontany.py '+start_datetime
sql =  [];
tlog =  [];
countresult = 0;
tlog.append('Debut du script : '+now.strftime("%H:%M:%S") );

'''
response = urllib.request.urlopen("http://www.cenit-madagascar.mg/res/charge_fokontany_.php?id_commune=510215")
data = response.read()
soup = BeautifulSoup(data)
result = soup.find_all( 'option') 
for option in result:
    print(getID( option.get_text() ) )
raise SystemExit
'''

sql = "SELECT cenitid,cenitrid,cenitdid from `mg_commune` WHERE `done` = 0";
cur.execute(sql)
result = cur.fetchall()
'''
row = result[0]
communeid = row[0]
regionid = row[1]
districtid = row[2]
'''
for row in result :
   #print( row[1] )
   communeid = row[0]
   regionid = row[1]
   districtid = row[2]
   if( communeid.isdigit() > 0 ) :
      tab = get_tab('http://www.cenit-madagascar.mg/res/charge_fokontany_.php?id_commune='+communeid )
      for liste in tab :
         fokontanyname = getTitre(liste)
         fokontanyid = getID(liste)
         print( 'Fokontany : '+fokontanyname+' (ID:'+fokontanyid+')' );

         #Verif si la valeur n'existe pas en BDD -> insert
         print( exist_inbdd(fokontanyname,'mg_fokontany',fokontanyid, "sql" ) )
         if( exist_inbdd(fokontanyname,'mg_fokontany',fokontanyid ) == 0 ) :
            #Insert
            print( db_insert('mg_fokontany',fokontanyname,fokontanyid,"`cenitrid` = '"+regionid+"', `cenitdid` = '"+districtid+"', `cenitcid` = '"+communeid+"'") )
         else:
            print( db_update('mg_fokontany',fokontanyname,fokontanyid,"`cenitrid` = '"+regionid+"', `cenitdid` = '"+districtid+"', `cenitcid` = '"+communeid+"'") )
      #Update table mg_commune
      sql = "UPDATE `mg_commune` SET `done` = 1 WHERE `cenitid` = '"+communeid+"'"
      cur.execute(sql)
      conn.commit()

#raise SystemExit

cur.close()
conn.close()
tlog.append('Fin du script : '+datetime.datetime.now().strftime("%H:%M:%S"));


TEXT = "\n".join(tlog);
email(TO,SUBJECT,TEXT);
