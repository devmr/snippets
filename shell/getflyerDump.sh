### Colors
GREEN='\033[1;32m'
RED='\033[1;31m'
YEL='\033[1;33m'
NC='\033[0m' # No Color

### Nom de la BDD
bname=madatsara.sql
filename="${bname%.*}"
extension="${bname##*.}"
datetime=$(date +"%d%m%Y-%H%M%S")
concat=$filename"_"$datetime"."$extension

### -------------- ###
### Functions ###
### --------------- ###
### Error Handler ###
gotoError() {
        echo -e "Erreur lors de l'execution : \"${RED}$1${NC}\""
}

### Error Handler ###
goInviteDossier() {
    echo -e "Dossier ${YEL}$path${NC} inexistant - le créer? [o|n]"
    read create

    if [ ! -d $path ] && [ ! -z "$create"  ] && [ "o" != "$create" ]
    then
        echo -e "Reponse attendue: ${YEL}o${NC}"
        goInviteDossier
        exit 1
    fi

    if [ ! -d $path ] && [ ! -z "$create"  ] && [ "o" == "$create" ]
    then
        createFolder $path 
    fi
}

### Creation dossier ####
createFolder() {
    if [ -z $1 ]; then
        gotoError "creation dossier"
        exit 1
    fi

    mkdir $1  > /dev/null 2>&1
    if [ $? -eq 0 ]
    then
            echo -e "Dossier ${YEL}$1${NC} créé correctement"
    else
            gotoError "Erreur création Dossier ${YEL}$1${NC}"
    exit 1
    fi
}

echo "Taper le Chemin vers madatsara.com: (exemple: /f/data/sites/madatsara.com/) "
read path

if [ -z "$path"  ]
then
    path=/e/data/sites/madatsara.com/
    exit 1
fi
echo -e "Chemin saisi : ${YEL}$path${NC}"

if [ ! -d $path ]
then
    goInviteDossier
fi
 


nbFilesServer=$(ssh -t web@91.121.132.77 -p2222 'ls -l /home/web/madatsara.com/www/web/assets/flyers/ | wc -l')
echo -e "Nb fichiers sur le serveur : ${YEL}$nbFilesServer${NC}";
 
###
# 
###
cd $path
if [ -f "$path$bname" ]
then
    echo -e "Fichier existe Dump ${YEL}$path$bname${NC} to ${YEL}"$path"dumpDb/"$concat"${NC}" 
    mv $path$bname $path"dumpDb/"$concat
fi
ssh -t web@91.121.132.77 -p2222 'mysqldump -umadatsara -p!4xntmj60! madatsara --add-drop-table > madatsara.sql'
lftp -c "set sftp:auto-confirm yes; open -p 2222 -u web,4xntmj60 sftp://91.121.132.77; get madatsara.sql; echo \"Fichier madatsara.sql telecharge\"; quit"
###
# Import in DB
###
# mysqladmin version  >/dev/null 2>&1
# if [ $? -eq 0 ]
# then
  echo -e "Mysql OK - Import de ${YEL}$path$bname${NC} dans la BDD mts_w4"
#  mysql -uroot mts_devw4 < $path$bname
docker exec -i madatsara_mysql mysql -uroot -psecret madatsara < $path$bname 
# fi
# echo "Mysql non démarré"

###
# 
###
echo "Check Nb fichiers dans web/assets/flyers/ avant download ...";
nbFilesBefore=$(ls -l $path/www/web/assets/flyers/ | wc -l)
nbFilesRestant=$(($nbFilesServer-$nbFilesBefore));
echo -e "Nb fichiers dans web/assets/flyers/ avant download : ${YEL}$nbFilesBefore${NC}";
echo -e "Nb fichiers à récupérer : ${YEL}$nbFilesRestant${NC}";
cd $path/www/web/assets/flyers/
###
# 
###
echo "Check Nb fichiers dans web/assets/flyers/ après download ...";
lftp -c "set sftp:auto-confirm yes; open -p 2222 -u web,4xntmj60 sftp://91.121.132.77; mirror /home/web/madatsara.com/www/web/assets/flyers .; mirror /home/web/madatsara.com/www/web/assets/artistes_orig ../artistes_orig; quit"
nbFilesAfter=$(ls -l $path/www/web/assets/flyers/ | wc -l)
echo -e "Nb fichiers dans web/assets/flyers/ après download : ${YEL}$nbFilesAfter${NC}";
difference=$(($nbFilesAfter-$nbFilesBefore));
echo -e "Soit : \"${YEL}$difference${NC}\" fichiers ajoutés";

