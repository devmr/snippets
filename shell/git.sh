env=$1

gohelp () {
    echo -e "Utilisation : \e[33m\e[1m./git.sh dev \e[39m\e[0m en DEV ou \e[33m\e[1m ./git.sh prod \e[39m\e[0m en PROD"
    echo -e "Message commit \e[33m\e[1mobligatoire \e[39men\e[0m DEV"
}

if [ -z "$env" ]
then
    echo "Environnement vide "
    gohelp
exit 1
fi
if [ "dev" == "$env" ]
    then
    echo "Taper le message du commit:"
    read msgcommit
fi


if [  "dev" == "$env" ] && [ -z "$msgcommit"  ]
then
    echo "Impossible de continuer - Message commit vide"
    gohelp
    exit 1
fi

## ----- GIT DEV ----- ###
if [ "dev" == "$env" ]
    then
    git pull kimsufi devmts    
    git add .
    git commit -am "$msgcommit"
    git push kimsufi devmts
    git push gitlab devmts

    exit 1
fi

## ----- GIT PROD ----- ###
if [ "prod" == "$env" ]
    then
    
    git checkout mastersf3
    git pull kimsufi mastersf3
    git merge devmts
    git push kimsufi mastersf3
    git push gitlab mastersf3
    git checkout devmts
    
    exit 1
fi

## ----- GIT PROD ----- ###
if [ "oprod" == "$env" ]
    then
    
    git checkout master
    git pull kimsufi master
    git merge devmts
    git push kimsufi master
    git push gitlab master
    git checkout devmts
    
    exit 1
fi


