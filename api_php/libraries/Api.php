<?php

/** 
 * Classe utile au fonctionnement général
 * 
 * Cette classe contient des méthodes utiles au fonctionnement générale du site
 * 
 * @author Miary R. <rabehasy@gmail.com>
 * @version 1.0 
 * 
 */
require_once __DIR__.'/Config.php';
function exception_handler($myException)
{
    header('Content-Type: application/json; charset=utf8');
    echo json_encode( array('code' => 'ERR', 'data' => $myException->getMessage() ) );
}
 
set_exception_handler('exception_handler');

class Api
{
    /**
     * Méthode en cours : GET/POST/PUT/DELETE
     * 
     *      
     */
    protected $method = '';
    
    /**
     * Tableau enregistrant les params de l'URL en cours
     * 
     *      
     */
    protected $args = Array();
    
    /**
     * Controllers autorisés
     * 
     *      
     */
    protected $controllerclass_allowed = Array(
        'usersController'
        ,'indexController'
    );
    
    /**
     * Methodes autorisés
     * 
     *      
     */
    protected $method_list = Array(
        'DELETE' => 'DELETE'
        ,'POST' => 'POST'
        ,'GET' => 'GET'
        ,'PUT' => 'PUT'
    );
    
    /**
     * Models autorisés
     * 
     *      
     */
    protected $modelclass_allowed = Array(
        'userModel'
    );
    
    /**
     * Initialise la classe
     * 
     * Permet l'accès de l'éxtérieur + sortie en JSON
     * 
     * @param string $request Requete URL
     * 
     * @return void Ne retourne rien
     * 
     */
    public function __construct() 
    {
        spl_autoload_register( array($this,'autoload'));
        
        
        $this->args = explode('/',$_SERVER['REQUEST_URI']) ;
        
         
        $this->method = $_SERVER['REQUEST_METHOD'];
        //echo $this->method;
        if ($this->method == 'POST' && array_key_exists('HTTP_X_HTTP_METHOD', $_SERVER)) {
            if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'DELETE') {
                $this->method = 'DELETE';
            } else if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'PUT') {
                $this->method = 'PUT';
            } else {
                throw new Exception("Unexpected Header");
            }
        }
        if( !isset( $this->method_list[$this->method]) ) {
                throw new Exception("methode invalide");
        }
        
    }
    
    /**
     * Autoload
     * 
     * @param string $class Nom de la classe
     * 
     * @return void Ne retourne rien
     * 
     */
    public function autoload($class)
    {
        if ( strstr($class, 'Controller') ) {
             if ( ! isset(array_flip( $this->controllerclass_allowed )[$class]) ) { 
                 throw new Exception('Classe *'.$class.'* non autorisée.'); 
             }
             if ( !file_exists(__DIR__.'/../application/Controller/'.$class.'.php' ) ) { 
                 throw new Exception('Fichier de la Classe *'.$class.'* inexistante.'); 
             }
                require  __DIR__.'/Controller.php';
                require  __DIR__.'/../application/Controller/' . $class . '.php';
        }
        elseif (strstr($class, 'Model') && isset(array_flip( $this->modelclass_allowed )[$class])) {
            require_once  __DIR__.'/Model.php';
            require  __DIR__.'/../application/Model/' . $class . '.php';
        }
        elseif (strstr($class, 'View') && isset(array_flip( $this->viewclass_allowed )[$class])) {
            require  __DIR__.'/../application/View/' . $class . '.php';
        }
    }
    
    /**
     * Execute la classe principale
     * 
     * Parse l'URl en cours et appelle le controller et la méthode demandée
     * 
     * @return void Ne retourne rien
     * 
     */
    public  function processAPI() 
    {
        
        $nb_args = count($this->args);

        //Le controller correspond au premier chemin dans l'URL -> exemple : GET /user/
        $controller = ($this->args[1]!=''?filter_var($this->args[1], FILTER_SANITIZE_STRING).'Controller':Config::$defaultController);
        
        //L'Identifiant correspond au 2 chemin dans l'URL -> exemple : GET /user/1
        $id = ($nb_args>2 && $this->args[2]!=='' ?filter_var($this->args[2], FILTER_VALIDATE_INT):0);
        
        //La fonction du controller correspond au 3 chemin dans l'URL -> exemple : GET /user/1/method
        $method_name = ($nb_args>3 && $this->args[3]!=='' ?filter_var($this->args[3], FILTER_SANITIZE_STRING).'Action':'defaultAction');
        
        //Si la classe correspondante et la méthode n'existent pas - renvoyer les valeurs par defaut dans le fichier config
        if (!is_string($controller) || !class_exists($controller)) {
            $controller = Config::$defaultController; 
            if ( !class_exists($controller) ) {
                throw new Exception ('Classe '.$controller.' inexistante' );
            }
        }
        
        //Msg erreur si methode non trouvé
        if (  !method_exists($controller, $method_name) ) { 
             Controller::errormethod($method_name, $controller);
             return false;
        }
        
        //Appel le controller demandé et la methode
        $instance = new $controller($this->args, $this->method);
        $instance->$method_name($this->args);
        return true;
    }
}
