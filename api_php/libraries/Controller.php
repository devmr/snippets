<?php
/**
 * Controller general
 *
 * @author Miary
 */
class Controller
{
    /**
     * 
     * Modifier les entetes pour permettre les acces de l'exterieur
     * 
     * @return void Ne retourne rien
     * 
     */
    public function __construct()
    {
        header("Access-Control-Allow-Orgin: *");
        header("Access-Control-Allow-Methods: *");
    }
    
    /**
     * 
     * Modifier les entetes pour permettre les acces de l'exterieur
     * 
     * @param $data array - contient les données  à afficher
     * @param $code String - Code de sortie : OK|ERR
     * 
     * @return String - JSON
     * 
     */
    public static function printJson($data = array(), $code = 'OK' )
    {
        header('Content-Type: application/json; charset=utf8');
        return json_encode( array('code' => $code, 'data' => $data ) );
    }
    
    /**
     * 
     * Affiche un message d'erreur général
     * 
     * @param $data array - contient les données  à afficher
     * @param $code String - Code de sortie : OK|ERR
     * 
     * @return String - JSON
     * 
     */
    public static function errormethod($method_name, $class_name){
        echo self::printJson('Méthode *'.$method_name.'* inexistante dans la classe **'.$class_name.'**','ERR');
    }
}
