<?php

/**
 * Model general relatif à user
 *
 * @author Miary
 */
class userModel extends Model
{
    /**
    * Récupere les infos d'un user ou de tous les users
     * 
     * @param $uid int - L'identifiant de l'user ou 0 -> tous les users
     * @return array - toutes les lignes du tableau
    */
    public static function get($uid=0)
    {
       $db = Database::getInstance();
       $sql = "SELECT `uid` AS `user_id`,`name`, `email` FROM `user`";
       if( $uid>0 ) {
           $sql .= ' WHERE ';
           $sql .= 'uid=:uid';
       }
       //fixer une limite si $uid vide
       if ( $uid == 0 ) { $sql .= ' LIMIT :limit'; }
       
       $stmt = $db->prepare($sql);
       if ( $uid>0 ) { $stmt->bindParam(':uid', $uid, PDO::PARAM_INT); }
       if ( $uid == 0 ) { $stmt->bindParam(':limit', Config::$defaultLimit, PDO::PARAM_INT); }
       $stmt->execute();
       return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    
    /**
    * Récupere les songs d'un user  
     * 
     * @param $uid int - L'identifiant de l'user ou 0 -> tous les users
     * 
     * @return array - toutes les lignes du tableau
    */
    public static function getSong($uid=0)
    {
       $db = Database::getInstance();
       $sql = "SELECT s.`uid` as `song_id`, s.`title` ,s.`duration` FROM `song` s INNER JOIN `song_byuser` AS b ON b.`song_id` = s.`uid` INNER JOIN `user` AS u ON u.`uid` = b.`user_id` ";
       if( $uid>0 ) {
           $sql .= ' WHERE ';
           $sql .= 'u.`uid` =:uid';
       }
       //fixer une limite si $uid vide
       if ( $uid == 0 ) { $sql .= ' LIMIT :limit'; }
       
       $stmt = $db->prepare($sql);
       if ( $uid>0 ) { $stmt->bindParam(':uid', $uid, PDO::PARAM_INT); }
       
       //Limit dans le fichier de config /libraries/Config.php
       if ( $uid == 0 ) { $stmt->bindParam(':limit', Config::$defaultLimit, PDO::PARAM_INT); }
       $stmt->execute();
       return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    
    /**
    * Récupere les favoris d'un user  
     * 
     * @param $uid int - L'identifiant de l'user ou 0 -> tous les users
     * @param $favorite_uid int - L'identifiant du favori ou 0 -> tous les favoris
     * 
     * @return array - toutes les lignes du tableau
    */
    public static function getFavoritesong($uid=0, $favorite_uid = 0)
    {
       $db = Database::getInstance();
       $sql = "SELECT s.`uid` AS `song_id`, s.`title`, s.`duration`, f.`uid` as `favoriuid`, f.`title` as `favorititle` 
           FROM `song` s
INNER JOIN favoritemuzik_byuser fbu ON fbu.song_id = s.uid
INNER JOIN favoritemuzik f ON f.uid = fbu.favorite_id";
       
       $tabwhere[] = ' WHERE 1=1 ';
       if ($uid > 0) {
           $tabwhere[] = 'f.`owner_userid` =:uid';
       }
       if ($favorite_uid > 0) {
           $tabwhere[] = 'f.`uid` =:fuid';
       }
       $sql .= implode(' AND ',$tabwhere );
       
       //fixer une limite si $uid vide
       if ( $uid == 0 ) { $sql .= ' LIMIT :limit'; }
       
       $stmt = $db->prepare($sql);
       if ( $uid>0 ) { $stmt->bindParam(':uid', $uid, PDO::PARAM_INT); }
       if ($favorite_uid > 0) { $stmt->bindParam(':fuid', $favorite_uid, PDO::PARAM_INT); }
       
       //Limit dans le fichier de config /libraries/Config.php
       if ( $uid == 0 ) { $stmt->bindParam(':limit', Config::$defaultLimit, PDO::PARAM_INT); }
       $stmt->execute();
       return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    
    /**
    * Supprime une musique d'un favori  
     * 
     * @param $favorite_id int - L'identifiant du favori
     * @param $ids array - Les identifiants des songs
     * 
     * @return array - toutes les lignes du tableau
    */    
    public static function deletefavorisongs( $favorite_id, $ids )
    {
        $db = Database::getInstance();
        $stmt = $db->prepare("DELETE FROM `favoritemuzik_byuser` WHERE `favorite_id` = :favorite_id AND `song_id` IN (".implode(',',$ids).")");
        if ( $favorite_id>0 ) { $stmt->bindParam(':favorite_id', $favorite_id, PDO::PARAM_INT); }
        $stmt->execute();
        return true;
    }
    
    /**
    * Ajoute une musique dans un favori  
     * 
     * @param $favorite_id int - L'identifiant du favori
     * @param $ids array - Les identifiants des songs
     * 
     * @return array - toutes les lignes du tableau
    */    
    public static function addfavorisongs( $favorite_id, $ids )
    {
        $db = Database::getInstance();
        $tab = array();
        $nb = count($ids);
        for( $i = 1; $i <= $nb; $i++ ){
            $tab[] = '(:favorite_id, :sond_id_'.$i.')';
        }
        
        $stmt = $db->prepare("INSERT INTO `favoritemuzik_byuser` VALUES ".implode(',',$tab )." ON DUPLICATE KEY UPDATE `favorite_id` = VALUES(`favorite_id`), `song_id` = VALUES(`song_id`)");
        if ( $favorite_id>0 ) { $stmt->bindParam(':favorite_id', $favorite_id, PDO::PARAM_INT); }
        
        $i = 1;
        foreach( $ids as $row ){
            $stmt->bindParam(':sond_id_'.$i, $row, PDO::PARAM_INT );
        }
        $stmt->execute();
        return true;
    }
    
}
