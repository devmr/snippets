<?php

/**
 * Controller responsable de l'affichage des users, des song et de ses favorites
 *
 * @author Miary
 */
class usersController extends Controller
{
    protected $user_uid = 0, 
            $favorite_uid = 0,
            $codejson = 'OK',
            $method = 'GET';
    
    public function __construct($args = array(), $method = 'GET')
    {
         $this->method = $method; 
         if ( isset($args[2]) && filter_var($args[2],FILTER_VALIDATE_INT) ) { 
             $this->user_uid = (int) $args[2]; 
         }
         if ( isset($args[4]) && filter_var($args[4],FILTER_VALIDATE_INT) ) { 
             $this->favorite_uid = (int) $args[4]; 
         }
    }
    
    /**
    * Declenché si aucune méthode n'a été trouvé dans l'appel
    *
    */
    public function defaultAction($args = array()){
        
        //Si on y arrive depuis une autre methode que GET -> erreur
        if ( $this->method !== 'GET' ) {
            echo $this->printJson( 'Méthode non autorisée.', 'ERR' );
            return;
        }
        
        $tabresult = userModel::get($this->user_uid);
        $nb = count($tabresult);
        
        //Si resultat vide de la BDD
        if ( $nb <= 0 ) { 
            $this->codejson = 'ERR'; 
            $tabresult = 'Aucun user trouvé avec cet identifiant.' ;
        } 
        echo $this->printJson($tabresult, $this->codejson );
        
    }
    
    /**
    * Affiche les songs d'un user
     * 
     * Eexemple d'appel : GET /users/1/songs 
    *
    */
    public function songsAction($args = array()){
        //Si on y arrive depuis une autre methode que GET -> erreur
        if ( $this->method !== 'GET' ) {
            echo $this->printJson( 'Méthode non autorisée.', 'ERR' );
            return;
        }
        
        $tabresult = userModel::get($this->user_uid);
        $nb = count($tabresult);
        
        //Si resultat vide de la BDD
        if ( $nb <= 0 ) { 
            $this->codejson = 'ERR'; 
            $tabresult = 'Aucun user trouvé avec cet identifiant.' ;
            echo $this->printJson($tabresult, $this->codejson );
            return;
        }
        
        $tabresult = userModel::getSong($this->user_uid);
        $nb = count($tabresult);
        
        if( $nb <= 0 ) { 
            $this->codejson = 'ERR'; 
            $tabresult = array( 'Aucun song appartenant à cet user.' );
        }
        echo $this->printJson($tabresult, $this->codejson );
        
    }
    
    /**
     * Afficher les musiques favoris d'un utilisateur
     * 
     * Eexemple d'appel : GET /users/1/favorisongs ou GET /users/1/favorisongs/1 pour afficher un favori en particulier
     */
    public function favorisongsAction(){
        
        //Rediriger vers une autre action si appelé depuis method PUT ou DELETE
        if ( $this->method === 'PUT' ) {
            return $this->addfavorisongsAction();
        } elseif ($this->method === 'DELETE') {
            return $this->deletefavorisongsAction($this->favorite_uid);
        }
         
        
        $tabresult = userModel::get($this->user_uid);
        $nb = count($tabresult);
        if ( $nb <= 0 ) { 
            $this->codejson = 'ERR'; 
            $tabresult = 'Aucun user trouvé avec cet identifiant.' ;
            echo $this->printJson($tabresult, $this->codejson );
            return;
        }
        
        $tabresult = userModel::getFavoritesong($this->user_uid, $this->favorite_uid);
        $nb = count($tabresult);
        if( $nb <= 0 ) { 
            $this->codejson = 'ERR'; 
            $tabresult = array( 'Identifiant incorrect ou favori non trouvé.' );
            echo $this->printJson($tabresult, $this->codejson );
            return;
        }
        $arr = array();
        foreach($tabresult as $key => $item)
        {
           if (isset($item['favoriuid'])) $arr[$item['favoriuid']][$key] = $item;
        }
        
        echo $this->printJson($arr, $this->codejson );
        
    }
    
    /**
     * Ajouter des musiques dans un favori
     * 
     * Exemple appel  -> curl -H "Accept: application/json" -H "Content-Type: application/json; charset=UTF-8" -X PUT /users/1/favorisongs/1 -d '{"song":[1,2,3]}' 
     */
    public function updatefavorisongsAction()
    {
        $data = file_get_contents("php://input");
        
        //traitement si non vide
        if (is_string($data) ) {
            
            $data = json_decode($data,true);
            
            //si Json mal formé
            if ( json_last_error()>0 ) {
                echo $this->printJson( 'Erreur réception data : '.json_last_error_msg(), 'ERR' );
                return;
            }
            
            //Si array non existant
            if ( !isset($data['song']) ) {
                echo $this->printJson( 'Erreur réception data : paramètre array[song] attendu' , 'ERR' );
                return;
            }
            
            //valider et retenir uniquement les types int
            $result = filter_var($data['song'], FILTER_VALIDATE_INT, array(
                'flags'   => FILTER_REQUIRE_ARRAY 
            ));
            
            //Grouper et séparer les valeurs non numériQUES
            $ids = array_filter( $result )  ;
            
            //Ajout dans la table favoritemuzik_byuser
            userModel::addfavorisongs($this->favorite_uid, $ids);
            echo $this->printJson( 'Les musiques ont bien été retirées.' , 'OK' );
            
            
        }
    }
    
    /**
     * Supprimer des musiques dans un favori
     * 
     * Exemple appel  -> curl -H "Accept: application/json" -H "Content-Type: application/json; charset=UTF-8" -X DELETE /users/1/favorisongs/1 -d '{"song":[1,2,3]}' 
     */
    public function addfavorisongsAction()
    {
        $data = file_get_contents("php://input");
        
        //traitement si non vide
        if (is_string($data) ) {
            
            $data = json_decode($data,true);
            
            //si Json mal formé
            if ( json_last_error()>0 ) {
                echo $this->printJson( 'Erreur réception data : '.json_last_error_msg(), 'ERR' );
                return;
            }
            
            //Si array non existant
            if ( !isset($data['song']) ) {
                echo $this->printJson( 'Erreur réception data : paramètre array[song] attendu' , 'ERR' );
                return;
            }
             
            //valider et retenir uniquement les types int
            $result = filter_var($data['song'], FILTER_VALIDATE_INT, array(
                'flags'   => FILTER_REQUIRE_ARRAY 
            ));
            
            //Grouper et séparer les valeurs non numériQUES
            $ids = array_filter( $result )  ;
            
            //Delete dans la table favoritemuzik_byuser
            userModel::addfavorisongs($this->favorite_uid, $ids);
            echo $this->printJson( 'Les musiques ont bien été ajoutées.' , 'OK' );
        }
        
    }
}
