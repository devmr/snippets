<?php

/**
 * Controller par defaut si aucun paramètre passé dans l'URL
 *
 * @author Miary
 */
class indexController extends Controller
{
    
    /**
     * Affiche l'aide à l'utilisation de l'API
     * 
    */
    public function defaultAction()
    {
        $html = <<<EOT
             <h1>Utilisation de l'API</h1>
             <h2>Afficher tous les user</h2>
                 <pre>GET /users</pre>
            <h2>Afficher un user</h2>
        <pre>GET /users/:id*</pre>
        <h2>Afficher un song d'un user</h2>
        <pre>GET /users/:id*/songs</pre>
        <h2>Afficher les favoris d'un user</h2>
        <pre>GET /users/:id*/favorisongs</pre>
        <h2>Ajouter une ou plusieurs songs dans un favori</h2>
        <pre>PUT /users/:id*/favorisongs/:fid*</pre>
        <p>Exemple d'appel : </p>
        <pre>curl -H "Accept: application/json" -H "Content-Type: application/json; charset=UTF-8" -X PUT /users/1/favorisongs/1 -d '{"song":[1,2,3]}' </pre>
        <h2>Supprimer une ou plusieurs songs d'un favori</h2>
        <pre>DELETE /users/:id*/favorisongs/:fid*</pre>
        <p>Exemple d'appel : </p>
        <pre>curl -H "Accept: application/json" -H "Content-Type: application/json; charset=UTF-8" -X DELETE /users/1/favorisongs/1 -d '{"song":[1,2,3]}' </pre>
        <hr /><p><em><strong>:id*</strong> = identifiant de l'user</em></p>
        <p><em><strong>:fid*</strong> = identifiant du favori</em></p>
EOT;
        echo $html;      
    }
}
