<?php
/** 
 * API en REST avec sortie en JSON
 * 
 * @author Miary R. <rabehasy@gmail.com>
 * @version 1.0 
 * 
 */

require(__DIR__.'/libraries/Api.php' );
$api = new Api();
$api->processAPI();

 
