<?php
/**
 * Returns HTML for a date element formatted as a single date.
 */
 
/**
 * Add body classes if certain regions have content.
 */
function sanitize_file_name( $filename ) {
	$filename_raw = $filename;
	$special_chars = array("?", "[", "]", "/", "\\", "=", "<", ">", ":", ";", ",", "'", "\"", "&", "$", "#", "*", "(", ")", "|", "~", "`", "!", "{", "}", chr(0));
	$filename = str_replace($special_chars, '', $filename);
	$filename = preg_replace('/[\s-]+/', '-', $filename);
	$filename = trim($filename, '.-_');

	return $filename;
}
function terresens_preprocess_html(&$variables) {
    $variables['classes_array'][] = 'extranet';
  if (!empty($variables['page']['featured'])) {
    $variables['classes_array'][] = 'featured';
  }

  if (!empty($variables['page']['triptych_first'])
    || !empty($variables['page']['triptych_middle'])
    || !empty($variables['page']['triptych_last'])) {
    $variables['classes_array'][] = 'triptych';
  }

  if (!empty($variables['page']['footer_firstcolumn'])
    || !empty($variables['page']['footer_secondcolumn'])
    || !empty($variables['page']['footer_thirdcolumn'])
    || !empty($variables['page']['footer_fourthcolumn'])
	|| !empty($variables['page']['footer_fifthcolumn'])) {
    $variables['classes_array'][] = 'footer-columns';
  }

  $path = drupal_get_path_alias($_GET['q']);
  $aliases = explode('/', $path);
  $classplus = '';
  foreach($aliases as $alias) {
      switch( drupal_clean_css_identifier($alias) ) {
          case 'programme' :
          case 'programmeterresens' :
              $classplus = 'colonnelarge';
          break;
          case 'incweb' :
              $classplus = 'popin';
          break;
      }
    
  }
  
  $data = drupal_get_query_parameters(null, array());
  if( isset($data['popin']) && $data['popin'] == 1 ) $classplus = 'popin';
  $variables['classes_array'][] = $classplus;
  
  global $user;
  if (user_is_logged_in() == TRUE) {
      
        if (user_access('administer site configuration')) {
            $variables['classes_array'][] = 'user_admin';
        }
        else $variables['classes_array'][] = 'user_loggedin';
  }
  
  // Add conditional stylesheets for IE
  drupal_add_css(path_to_theme() . '/css/ie.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'lte IE 7', '!IE' => FALSE), 'preprocess' => FALSE));
  drupal_add_css(path_to_theme() . '/css/ie6.css', array('group' => CSS_THEME, 'browsers' => array('IE' => 'IE 6', '!IE' => FALSE), 'preprocess' => FALSE));
}

/**
 * Override or insert variables into the page template for HTML output.
 */
function terresens_process_html(&$variables) {
  // Hook into color.module.
  if (module_exists('color')) {
   // _color_html_alter($variables);
  }
}

/**
 * Override or insert variables into the page template.
 */
function terresens_process_page(&$variables) {
  // Hook into color.module.
  if (module_exists('color')) {
   // _color_page_alter($variables);
  }
  // Always print the site name and slogan, but if they are toggled off, we'll
  // just hide them visually.
  $variables['hide_site_name']   = theme_get_setting('toggle_name') ? FALSE : TRUE;
  $variables['hide_site_slogan'] = theme_get_setting('toggle_slogan') ? FALSE : TRUE;
  if ($variables['hide_site_name']) {
    // If toggle_name is FALSE, the site_name will be empty, so we rebuild it.
    $variables['site_name'] = filter_xss_admin(variable_get('site_name', 'Drupal'));
  }
  if ($variables['hide_site_slogan']) {
    // If toggle_site_slogan is FALSE, the site_slogan will be empty, so we rebuild it.
    $variables['site_slogan'] = filter_xss_admin(variable_get('site_slogan', ''));
  }
  // Since the title and the shortcut link are both block level elements,
  // positioning them next to each other is much simpler with a wrapper div.
  if (!empty($variables['title_suffix']['add_or_remove_shortcut']) && $variables['title']) {
    // Add a wrapper div using the title_prefix and title_suffix render elements.
    $variables['title_prefix']['shortcut_wrapper'] = array(
      '#markup' => '<div class="shortcut-wrapper clearfix">',
      '#weight' => 100,
    );
    $variables['title_suffix']['shortcut_wrapper'] = array(
      '#markup' => '</div>',
      '#weight' => -99,
    );
    // Make sure the shortcut link is the first item in title_suffix.
    $variables['title_suffix']['add_or_remove_shortcut']['#weight'] = -100;
  }
}

/**
 * Implements hook_preprocess_maintenance_page().
 */
function terresens_preprocess_maintenance_page(&$variables) {
  // By default, site_name is set to Drupal if no db connection is available
  // or during site installation. Setting site_name to an empty string makes
  // the site and update pages look cleaner.
  // @see template_preprocess_maintenance_page
  if (!$variables['db_is_active']) {
    $variables['site_name'] = '';
  }
  drupal_add_css(drupal_get_path('theme', 'terresens') . '/css/maintenance-page.css');
}

function terresens_preprocess_page(&$vars, $hook) {
  if (isset($vars['node'])) {
    $vars['theme_hook_suggestions'][] = 'page__'. $vars['node']->type;
  }
}

/**
 * Override or insert variables into the maintenance page template.
 */
function terresens_process_maintenance_page(&$variables) {
  // Always print the site name and slogan, but if they are toggled off, we'll
  // just hide them visually.
  $variables['hide_site_name']   = theme_get_setting('toggle_name') ? FALSE : TRUE;
  $variables['hide_site_slogan'] = theme_get_setting('toggle_slogan') ? FALSE : TRUE;
  if ($variables['hide_site_name']) {
    // If toggle_name is FALSE, the site_name will be empty, so we rebuild it.
    $variables['site_name'] = filter_xss_admin(variable_get('site_name', 'Drupal'));
  }
  if ($variables['hide_site_slogan']) {
    // If toggle_site_slogan is FALSE, the site_slogan will be empty, so we rebuild it.
    $variables['site_slogan'] = filter_xss_admin(variable_get('site_slogan', ''));
  }
}

/**
 * Override or insert variables into the node template.
 */
function terresens_preprocess_node(&$variables) {
  if ($variables['view_mode'] == 'full' && node_is_page($variables['node'])) {
    $variables['classes_array'][] = 'node-full';
  }
}

/**
 * Override or insert variables into the block template.
 */
function terresens_preprocess_block(&$variables) {
  // In the header region visually hide block titles.
  if ($variables['block']->region == 'header') {
    $variables['title_attributes_array']['class'][] = 'element-invisible';
  }
}

/**
 * Implements theme_menu_tree().
 */
function terresens_menu_tree($variables) {
  return '<ul class="menu clearfix">' . $variables['tree'] . '</ul>';
}




/**
 * Implements theme_field__field_type().
 */
function terresens_field__taxonomy_term_reference($variables) {
  $output = '';

  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    $output .= '<h3 class="field-label">' . $variables['label'] . ': </h3>';
  }

  // Render the items.
  $output .= ($variables['element']['#label_display'] == 'inline') ? '<ul class="links inline">' : '<ul class="links">';
  foreach ($variables['items'] as $delta => $item) {
    $output .= '<li class="taxonomy-term-reference-' . $delta . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</li>';
  }
  $output .= '</ul>';

  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . (!in_array('clearfix', $variables['classes_array']) ? ' clearfix' : '') . '"' . $variables['attributes'] .'>' . $output . '</div>';

  return $output;
}

function  terresens_main_header_links($variables) { 
 $type = 'main-menu';
  $links = $variables['links'];
  $attributes = array();
  $heading = '';
  global $language_url;
  $output = '';
 $class2 = '';

  if (count($links) > 0) { 
    $output = '';

    // Treat the heading first if it is present to prepend it to the
    // list of links.
    if (!empty($heading)) {
      if (is_string($heading)) {
        // Prepare the array that will be used when the passed heading
        // is a string.
        $heading = array(
          'text' => $heading,
          // Set the default level of the heading.
          'level' => 'h2',
        );
      }
      $output .= '<' . $heading['level'];
      if (!empty($heading['class'])) {
        $output .= drupal_attributes(array('class' => $heading['class']));
      }
      $output .= '>' . check_plain($heading['text']) . '</' . $heading['level'] . '>';
    }

    $output .= '<ul class="sf-menu">';

    $num_links = count($links);
    $i = 1;

    foreach ($links as $key => $link) {
      $class = '';

      // Add first, last and active classes to the list of links to help out themers.
      if ($i == 1) {
        //$class = 'current';
      }
      if ($i == $num_links) {
        $class = '';
      }
      if (isset($link['href']) && ($link['href'] == $_GET['q'] || ($link['href'] == '<front>' && drupal_is_front_page()))
          && (empty($link['language']) || $link['language']->language == $language_url->language)) {
        $class = 'current';
      }
   $path = $_GET['q'];
  
         $plid = load_parent_menu( $path); 
       $p_link = load_parent_link($plid); 
      if (isset($link['href'])) { if($p_link == $link['href'] ) {  $class = 'active'; }
        // Pass in $link as $options, they share the same keys.
        $output .= '<li class="'.@$class.'"><a href="'.url($link['href']).'" >'.$link['title'].'</a>
                        '.@load_second_level_menu(str_replace('menu-', '', $key), $type, $i).'
                   </li>';
      }
      elseif (!empty($link['title'])) {
        // Some links are actually not links, but we wrap these in <span> for adding title and class attributes.
        if (empty($link['html'])) {
          $link['title'] = check_plain($link['title']);
        }
        $span_attributes = '';
        if (isset($link['attributes'])) {
          //$span_attributes = drupal_attributes($link['attributes']);
        }
       // $output .= '<li id="'.$class2.'"><a  href="'.url($link['href']).'" title="'.$link['title'].'"><span>'.$link['title'].'</span><b>&nbsp;</b></a></li>';
      }

      $i++;
      $output .= "</li>";
    }

    $output .= '</ul> <div class="clear"></div>';
  }

  
  return $output;
}

function load_second_level_menu($mlid, $type, $weigth) {
 $output = '';
 $output2 = '';
 $class_nav = '';
 $class_nav_content = '';
 
 
 
 $parent_id = $mlid;
 if($parent_id) {
  $query = db_select('menu_links', 'ml');
  $query->fields('ml', array('link_path','link_title','mlid'));
  $query->condition('ml.plid', $parent_id);
  $query->condition('ml.hidden', 0);
  $query->condition('ml.menu_name', $type);
  $query->orderBy('weight', 'ASC');
  $list = $query->execute();
  if($list->rowCount() > 0) {
  
  
  $output =' <ul style="visibility: hidden;">';
  $k = 1;
   foreach ($list as $row) {
   $class ='';
   if (isset($row->link_path) && ($row->link_path == $_GET['q'] || ($row->link_path == '<front>' && drupal_is_front_page()))
          && (empty($link['language']) || $link['language']->language == $language_url->language)) {
        $class = 'active';
            }
      
    $output .= '<li class="'.$class.'"><a href="'.url($row->link_path).'"><span>'.$row->link_title.'</span></a></li>
    ';
   }
   $output .= '</ul>';
  }
 }
 return $output;
}
function load_parent_link() {
 $output = '';
 global $language;
 global $base_url;
 $selPath = $_GET['q'];
      $output .='<select class="menu-moblie" name="select" onchange="linkTo(this.value)">';
  $query = db_select('menu_links', 'ml');
  $query->fields('ml', array('link_path','mlid','link_title'));
  $query->condition('ml.menu_name', 'main-menu');
  $query->condition('ml.plid', '');
  $query->condition('ml.hidden', 0);
  $query->orderBy('weight', 'DESC');
  $list = $query->execute();
  if($list->rowCount() > 0) { 
    foreach ($list as $row) {
     //print "<pre>"; print_r($row); die;
    $plid = $row->mlid;
    $linkPath = url($row->link_path);
    $linkTitle = $row->link_title;
    if($selPath == $row->link_path) { //print $selPath; die; 
    $selTab = ' selected="selected" '; }
     $parent = load_parent($plid);
     if($parent) {
    $output .=  '<option value="'.$linkPath.'"'.@$selTab.'>'.$linkTitle.' </option>'.@$parent;
    }
    $selTab = '';
         }
     }
  $output .= '</select>';  

 return $output; 
 }
 
 function load_parent($plid) {
 $output = '';
 global $language;
 global $base_url;
 $selPat = $_GET['q'];
 if($plid) {
  $query = db_select('menu_links', 'ml');
  $query->fields('ml', array('link_path','plid','mlid','link_title' ));
  $query->condition('ml.menu_name', 'main-menu');
  $query->condition('ml.plid', $plid);
  $query->condition('ml.hidden', 0);
  $list = $query->execute();
  $output .= '<optgroup>';
                   
  if($list->rowCount() > 0) { 
    foreach ($list as $row) {
      if($selPat == $row->link_path) { //print $selPath; die; 
    $selTa = ' selected="selected" '; }
   $plid1 = $row->plid;
   //$lan = $row->language; //print $lan; die;
   $title = '<option value="'.url($row->link_path).'"'.@$selTa.'>'.$row->link_title.'</option>';
   
   $output .= $title;
   $selTa = '';
          }
    
     }
}
  $output .= '</optgroup>';
 return $output; 
 }
 function terresens_theme() {
  $items = array();
  // create custom user-login.tpl.php
  $items['user_login'] = array(
  'render element' => 'form',
  'path' => drupal_get_path('theme', 'terresens') . '/templates',
  'template' => 'user-login',
  'preprocess functions' => array(
  'yourthemename_preprocess_user_login'
  ),
 );
return $items;
}
function number_format2( $number ,  $decimals = 0 ,  $dec_point = '.' ,  $thousands_sep = ',' ){

	$retnum = str_replace(' ', '', $number);
	if (is_numeric($number)){
		if (strpos($number, '.') !== false) $decimals = 2;
		$retnum = number_format($retnum, $decimals, $dec_point, $thousands_sep);
                $retnum = str_replace(" ", "&nbsp;", $retnum);
	}
	return $retnum;
}
/**
 * Returns field values as actual entities where possible,
 * also allows selection of individual items to be returned
 */
function field_fetch_field_values($entity_type, $entity, $field_name, $get_delta = NULL, $get_key = NULL) {
  $values = array();
  if (isset($entity->$field_name) && !empty($entity->$field_name)) {
    foreach (field_get_items($entity_type, $entity, $field_name) as $delta => $item) {
      $value = $item;
      $keys = array_keys($item);
      if (count($keys)==1) {
        $key = $keys[0];
        switch ($key) {
          case 'nid':
            $value = array_shift(entity_load('node', array($item[$key])));
            break;
          case 'uid':
            $value = array_shift(entity_load('user', array($item[$key])));
            break;
          case 'tid':
            $value = array_shift(entity_load('taxonomy_term', array($item[$key])));
            break;
          case 'vid':
            $value = array_shift(entity_load('taxonomy_vocabulary', array($item[$key])));
            break;
          case 'value':
            $value = $item['value'];
            break;
        }
      }
      else {
        if ($get_key && isset($item[$get_key])) {
          $value = $item[$get_key];
        }
        elseif (array_key_exists('value', $item)) {
          $value = isset($item['safe_value']) ? $item['safe_value'] : $item['value'];
        }
      }
      $values[$delta] = $value;
    }
  }
  if (is_numeric($get_delta)) {
    return isset($values[$get_delta]) ? $values[$get_delta] : NULL;
  }
  return $values;
}