<?php
define('DRUPAL_ROOT', getcwd().'/../../../../../../../');
require_once DRUPAL_ROOT . 'includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL); 
$data = drupal_get_query_parameters(null, array()); 
if ( !defined('BASE_URL') )
define('BASE_URL', (base_path()!='/'?base_path():''));
define('TEMPLATEPATH', drupal_get_path('theme',$GLOBALS['theme'] ) );
$pid = $data['pid'];
if ($pid>-1){
	$sql="SELECT * FROM programmes WHERE actif=1 and id = :pid ";
        $results = db_query( $sql, array(':pid' => $pid ) );
        $programme = $results->fetchObject();
} 
$zipname =  $pid.'.zip';
$zipname = sanitize_file_name($programme->nom) . '.zip';
$prog_dir =  '../../../../../../../wp-content/documents.ori/'.sanitize_file_name($programme->nom);

$prog_root_dir =  $prog_dir;
file_unmanaged_delete_recursive('tmp/'.$pid.'/');
//echo $zipname;

$prog_dir = '/wp-content/documents/'.sanitize_file_name($programme->nom);
$prog_root_dir = DRUPAL_ROOT.$prog_dir;
$doc_reservation = array();
drupal_mkdir(dirname(__FILE__).'/tmp/'.$pid ,0777); 
if (is_dir($prog_root_dir.'/Documents-de-reservation/')){
	$d = dir($prog_root_dir.'/Documents-de-reservation/');
        drupal_mkdir(dirname(__FILE__).'/tmp/'.$pid.'/documentsdereservation',0777);
	while (false !== ($entry = $d->read())) {
		if (($entry==".")||($entry=="..")||(strpos(trim($entry),'.')==0)) continue;
	   	$doc_reservation[] = $prog_root_dir.'/Documents-de-reservation/'.$entry;
                copy($prog_root_dir.'/Documents-de-reservation/'.$entry,'tmp/'.$pid.'/documentsdereservation/'.$entry);
	}
	$d->close();
}
 
$doc_information = array();
if (is_dir($prog_root_dir.'/Documents-information/')){
        drupal_mkdir(dirname(__FILE__).'/tmp/'.$pid.'/documentsinformation',0777);
	$d = dir($prog_root_dir.'/Documents-information/');
	while (false !== ($entry = $d->read())) {
		if (($entry==".")||($entry=="..")||(strpos(trim($entry),'.')==0)) continue;
	   	$doc_information[] = $prog_root_dir.'/Documents-information/'.$entry;
                copy($prog_root_dir.'/Documents-information/'.$entry,'tmp/'.$pid.'/documentsinformation/'.$entry);  
	}
	$d->close();
}

$doc_marketing = array();
if (is_dir($prog_root_dir.'/Documents-marketing/')){
        drupal_mkdir(dirname(__FILE__).'/tmp/'.$pid.'/documentsmarketing',0777);
	$d = dir($prog_root_dir.'/Documents-marketing/');
	while (false !== ($entry = $d->read())) {
		if (($entry==".")||($entry=="..")||(strpos(trim($entry),'.')==0)) continue;
	   	$doc_marketing[] = $prog_root_dir.'/Documents-marketing/'.$entry;
                copy($prog_root_dir.'/Documents-marketing/'.$entry,'tmp/'.$pid.'/documentsmarketing/'.$entry);  
	}
	$d->close();
}


//Supprimer d'abord si zip existant
unlink( $zipname );
//echo dirname(__FILE__).'/'.$zipname;exit;
/*require_once dirname(__FILE__).'/pclzip.lib.php' ;
$archive = new PclZip(dirname(__FILE__).'/'.$zipname);
$archive->create( 'tmp/'.$pid.'/' ); 
*/
$zip = new ZipArchive;
$zip->open($zipname, ZipArchive::CREATE);
// create recursive directory iterator
$files = new RecursiveIteratorIterator (new RecursiveDirectoryIterator(realpath( 'tmp/'.$pid.'/') ), RecursiveIteratorIterator::LEAVES_ONLY);
// let's iterate
foreach ($files as $name => $file) {
	/*$filePath = $file->getRealPath();
	$zip->addFile($filePath);*/
        $fpath = $file->getRealPath();
        $fpath = str_replace(dirname(__FILE__).'/','',$fpath);
        $fpath = str_replace('tmp/'.$pid.'/','',$fpath);
        $fpath = utf8_decode( $fpath );
        if( !in_array($fpath, array(
            'tmp/'.$pid
            ,'documentsmarketing'
            ,'documentsdereservation'
            ,'documentsinformation'
            ,'tmp'
        )) ){
            //echo  $fpath    ."\n";
            $zip->addFile($file, $fpath);
        }
}
// close the zip file
if (!$zip->close()) {
	echo 'Création du ZIP impossible. Veuillez vérifier que le dossier '.dirname(__FILE__).' est accessible en écriture par le serveur.';
}  
// exit;
if (file_exists(dirname(__FILE__).'/'.$zipname)) {
  // Serve file download.
  drupal_add_http_header('Location', url('sites/all/themes/terresens/templates/extranet/genpdf/'.$zipname)); 
  drupal_add_http_header('Status', 301);
  /*drupal_add_http_header('Pragma', 'public');
  drupal_add_http_header('Expires', '0');
  drupal_add_http_header('Cache-Control', 'must-revalidate, post-check=0, pre-check=0');
  drupal_add_http_header('Content-Type', 'application/force-download');
  drupal_add_http_header('Content-Disposition', 'attachment; filename=' . sanitize_file_name($programme->nom) . '.zip;');
  drupal_add_http_header('Content-Transfer-Encoding', 'binary');
  drupal_add_http_header('Content-Length', filesize(dirname(__FILE__).'/'.$zipname));
  readfile(dirname(__FILE__).'/'.$zipname);
  unlink(dirname(__FILE__).'/'.$zipname);*/
  drupal_exit();
}
else{
    echo 'Téléchargement impossible. Veuillez vérifier que le dossier '.dirname(__FILE__).' est accessible en écriture par le serveur.';
}

 
function sanitize_file_name( $filename ) {
	$filename_raw = $filename;
	$special_chars = array("?", "[", "]", "/", "\\", "=", "<", ">", ":", ";", ",", "'", "\"", "&", "$", "#", "*", "(", ")", "|", "~", "`", "!", "{", "}", chr(0));
	$filename = str_replace($special_chars, '', $filename);
	$filename = preg_replace('/[\s-]+/', '-', $filename);
	$filename = trim($filename, '.-_');

	return $filename;
}