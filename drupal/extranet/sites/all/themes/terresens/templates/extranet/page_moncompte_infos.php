<?php
/*
Template Name: Mon compte infos
*/
?>
<?php get_header(); ?>
    <section id="mainContainer" class="clearfix">
        <nav id="second-nav">
            <ul class="nav nav-tabs">
                <li class="current-menu-item"><a href="#">Mes infos</a></li>
                <li><a href="<?php echo get_page_link(90) ?>">Mes actions</a></li>
                <?php //<li><a href="/?p=92">Mes documents</a></li> ?>
                <?php if ($current_user->info_conseiller->qualif1=="Premium") { ?>
                <li><a href="<?php echo get_page_link(94) ?>">Opportunités</a></li>
                <?php } ?>
            </ul>
        </nav>
        <header class="search-result">
            <div class="login">
                <form action="#" method="post">
                    <p style="color:black;">
                    Nom : <?php echo $current_user->info_conseiller->nom ?><br/>
                    Prénom : <?php echo $current_user->info_conseiller->prenom ?><br/>
                    Société : <?php echo $current_user->info_conseiller->societe ?><br/>
                    Adresse : <?php echo $current_user->info_conseiller->rue ?><br/>
                    Code postal : <?php echo $current_user->info_conseiller->codepostal ?> - Ville : <?php echo $current_user->info_conseiller->ville ?><br/>
                    Mobile : <?php echo $current_user->info_conseiller->mobile ?><br/>
                    Email : <?php echo $current_user->info_conseiller->email ?><br/>
                    Login : <?php echo $current_user->user_login ?><br/>
                    </p>
                    <p class="submit">
                        <button name="modalResetPassLink" id="modalResetPassLink" class="pull-right btn btn-primary">Changer de mot de passe</button>
                    </p>
                    <p class="submit">
                        <button name="modalMyAccountLink" id="modalMyAccountLink" class="pull-left btn btn-primary">Modifier vos données personnelles</button>
                    </p>
                </form>
            </div>
        </header>
    </section>
    <div id="myAccountFormModal" class="modal hide fade" tabindex="-1" role="dialog">
        <form class="terresens-form inscription-form" id="myAccount-form" action="" method="post">
            <input type="hidden" name="consid" value="<?php echo $current_user->info_conseiller->id ?>">
            <div class="modal-header">
                <div class="inner-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img src="<?php echo get_template_directory_uri(); ?>/img/close-modal.png" alt="Fermer" width="26" height="26"></button>
                    <p>Vous pouvez modifier vos coordonnées personnelles dans le formulaire ci-dessous.</p>
                </div>
            </div>
            <div class="modal-body">
                <div class="clearfix">
                    <div class="columnForm pull-left">
                        <label class="control-label" for="inputName">Nom<span class="required">*</span></label>
                        <input class="mediumsmall" type="text" tabindex="1" id="inputName" name="inputName" value="<?php echo $current_user->info_conseiller->nom ?>" required>
                        
                        <label class="control-label" for="inputCompany">Société<span class="required">*</span></label>
                        <input class="mediumsmall" type="text"  tabindex="4" id="inputCompany" name="inputCompany" value="<?php echo $current_user->info_conseiller->societe ?>"  required>                        
                    </div>
                    <div class="columnForm pull-right">
                        <label class="control-label" for="inputFirstName">Prénom<span class="required">*</span></label>
                        <input class="mediumsmall" type="text" tabindex="2" id="inputFirstName" name="inputFirstName" value="<?php echo $current_user->info_conseiller->prenom ?>"  required>
                    </div>
                </div>
                <div class="clearfix">
                    <label class="control-label" for="inputRue">Rue<span class="required">*</span></label>
                    <input class="large" type="text" tabindex="1" id="inputRue" name="inputRue" value="<?php echo $current_user->info_conseiller->rue ?>" required>
                </div>                
                <div class="clearfix">
                    <div class="columnForm pull-left">
                        <label class="control-label" for="inputCodePostal">Code postal<span class="required">*</span></label>
                        <input class="mediumsmall" type="text" tabindex="2" id="inputCodePostal" name="inputCodePostal" value="<?php echo $current_user->info_conseiller->codepostal ?>" required>
                    </div>
                    <div class="columnForm pull-right">
                        <label class="control-label" for="inputVille">Ville<span class="required">*</span></label>
                        <input class="mediumsmall" type="text" tabindex="4" id="inputVille" name="inputVille" value="<?php echo $current_user->info_conseiller->ville ?>" required>
                    </div>
                </div>
                <div class="clearfix">
                    <div class="columnForm pull-left">
                        <label class="control-label" for="inputMobile">Mobile</label>
                        <input class="mediumsmall" type="text" tabindex="1" id="inputMobile" name="inputMobile" value="<?php echo $current_user->info_conseiller->mobile ?>">
                    </div>
                    <div class="columnForm pull-right">
                        <label class="control-label" for="inputEmail">Email de contact<span class="required">*</span></label>
                        <input class="mediumsmall" type="text" tabindex="2" id="inputEmail" name="inputEmail" value="<?php echo $current_user->info_conseiller->email ?>" required>
                    </div>
                </div>
            </div>                        
            <div class="modal-footer clearfix">
                <button id="send_requestaccount" class="pull-right btn btn-primary">Valider les modfications</button>
            </div>
            <input type="hidden" name="action" value="myaccount" />
            <?php wp_nonce_field('ajax-myaccount', 'security'); ?>
        </form>
    </div>
    <div id="resetPassFormModal" class="modal hide fade" tabindex="-1" role="dialog">
        <form class="terresens-form inscription-form" id="resetPass-form" action="" method="post">
            <div class="modal-header">
                <div class="inner-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img src="<?php echo get_template_directory_uri(); ?>/img/close-modal.png" alt="Fermer" width="26" height="26"></button>
                    <p>Modifiez votre mot de passe d'accès à l'extranet.</p>
                </div>
            </div>
            <div class="modal-body">
                <div class="clearfix">
                    <div class="columnForm pull-left" id="resetPassContent">
                        <label class="control-label" for="inputNewPass">Nouveau mot de passe<span class="required">*</span></label>
                        <input class="mediumsmall" type="password" tabindex="1" id="inputNewPass" name="inputNewPass" required>
                        
                        <label class="control-label" for="inputConfirm">Confirmation du nouveau mot de passe<span class="required">*</span></label>
                        <input class="mediumsmall" type="password"  tabindex="4" id="inputConfirm" name="inputConfirm" required>                        
                    </div>
                </div>
            </div>                        
            <div class="modal-footer clearfix">
                <button id="send_requestreset" class="pull-right btn btn-primary">Valider</button>
            </div>
            <input type="hidden" name="action" value="resetpass" />
            <?php wp_nonce_field('ajax-resetpass', 'security'); ?>
        </form>
    </div>
    <script>        
        $(document).ready(function() {
            terresens.modals.init_modal_myaccount();
            terresens.modals.init_modal_resetpass();
        });     
    </script>
<?php get_footer(); ?>