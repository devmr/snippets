<?php
/*
Template Name: les gestionnaires
*/
?>
<?php get_header(); ?>
			<section id="mainContainer" class="clearfix">
                <div id="col-left" class="pull-left">
				   <div class="editoWrapper">
				   		<h1 class="vert">Des services de gestion locative performants</h1>
				   		<h2 class="vert">Des partenaires de premier ordre :</h2>
				   		<p>
				   		Conscient du rôle prépondérant du gestionnaire, en tant que garant du bon paiement des loyers des résidences initiées par notre Groupe, Terrésens s’est associé captilistiquement à des opérateurs connus et reconnus sur leur marché, du fait de leur ancienneté, de leur savoir-faire et de toutes les valeurs communes que nous partageons ensemble…
				   		</p>
				   		<h2><span class="vert">Tourisme :</span> MMV « Les vacances club à la carte »</h2>
				   		<p>Terrésens s’est associé pour l’exploitation des résidences tourisme au groupe Mer Montagne Vacances sous la marque « mmv by Terrésens ». MMV compte parmi les principaux acteurs du marché.</p>
				   		<p>
							Un spécialiste des métiers de l’hôtellerie et du tour-opérating depuis 20 ans<br>
							Le 2ème opérateur hôtelier dans les Alpes Françaises<br>
							16 hôtels clubs dans les Alpes,<br>
							5 résidences clubs**** en France<br>
							850 salariés<br>
							CA : 60 M<br>
							Plus de 130 000 clients chaque année.<br>
							MMV est propriétaire de 50% de son parc hôtelier.
				   		</p>
				   		<h2><span class="vert">Etudiants :</span> Fac Habitat « Le réflexe logement des jeunes ! »</h2>
				   		<p>Terrésens s’est associé pour l’exploitation de résidences étudiantes de services au groupe Fac-Habitat sous la marque « Loc Habitat by Terrésens » .</p>
				   		<p>
				   			 1er acteur du logement étudiant dans le parc social en nombre de résidences gérées.<br>
							 20 années d’existence sans aucune interruption.<br>
							 36 résidences, plus de 3 550 logements.<br>
							Un taux de remplissage et d’encaissement de loyers de 98%.<br>
							Un portefeuille assis à 90% sur du Parc Social.<br>
							Qualité des emplacements des résidences assurée.<br>
							CA : 12 400 000 € H.T.<br>
							3M de fonds propres
				   		</p>
				   		<h2><span class="vert">Sénior :</span> VITAE Résidences</h2>
				   		<p>Terrésens s’est associé au Groupe Lavorel Développement – fondateur du groupe LVL Médical – sous la marque VITAE Résidences.</p>
				   		<p>
							Acteur majeur de l’assistance médicale a domicile       <br>                                                                          
							22 ans  d’expérience au service des patients<br>
							750 collaborateurs en France répartis dans 41 villes, une présence nationale<br>
							Une renommée étendu en Allemagne avec 1 800 collaborateurs<br>
							50 000 patients pris en charge chaque jour, 400 000 depuis la création du groupe<br>
							CA : 165 M<br>
							56 M de fonds propres
						</p>
				   </div>
			    </div>
			    <div id="col-right" class="pull-right">
			    	<div class="gestionnaires-colum text-center">
			    		<p id="glogo1">
			    			<a href="http://www.mmv.fr/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/media/logo-mmv.png" alt="" /></a>
			    		</p>
			    		<p id="glogo2">
			    			<a href="http://www.fac-habitat.com/fr/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/media/logo-loc-habitat.png" alt="" /></a>
			    		</p>
			    		<p id="glogo3">
			    			<a href="http://residencesvitae.com/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/media/logo-vitae-BLANC.png" alt="" /></a>
			    		</p>
			    	</div>
			    </div>
			</section><!-- end section homeContainer -->
    <script>		
		$(document).ready(function() {
			terresens.global_js_init();
		});		
	</script>
<?php get_footer(); ?>