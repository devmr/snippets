<?php
global $user;
$lots = new stdClass;
$wpuserid = $user->uid;
$sql = "SELECT `vendeur_id` FROM `conseillers` WHERE `wpuserid` = :wpuserid";
$results = db_query( $sql, array(':wpuserid' => $wpuserid ) )->fetchObject();
//print_r( $results );

//$user_id = $current_user->info_conseiller->vendeur_id;
$vendeur_id = $results->vendeur_id;
//echo $vendeur_id; 

$sql = "SELECT *,CASE WHEN date_r1 = '0000-00-00' THEN '' ELSE DATE_FORMAT(date_r1,'%d-%m-%Y') END AS date_r1_f1,DATE_FORMAT(date_r1,'%d-%m-%Y') as date_r1_f,CASE WHEN date_r2 = '0000-00-00' THEN '' ELSE DATE_FORMAT(date_r2,'%d-%m-%Y') END AS date_r2_f1,DATE_FORMAT(date_r2,'%d-%m-%Y') as date_r2_f,DATE_FORMAT(date_gagne,'%d-%m-%Y') as date_gagne_f,CASE WHEN date_gagne = '0000-00-00' THEN '' ELSE DATE_FORMAT(date_gagne,'%d-%m-%Y') END AS date_gagne_f1, DATE_FORMAT(date_perdu,'%d-%m-%Y') as date_perdu_f,CASE WHEN date_perdu = '0000-00-00' THEN '' ELSE DATE_FORMAT(date_perdu,'%d-%m-%Y') END AS date_perdu_f1 FROM opportunites WHERE vendeur_id = :vendeur_id";
$opportunites = db_query( $sql, array(':vendeur_id' => $vendeur_id ) )->fetchAll();

/*$sql = "SELECT * FROM stat_conseillers WHERE vendeur_id = :vendeur_id";
$stat_conseillers = db_query( $sql, array(':vendeur_id' => $vendeur_id ) )->fetchAll();
*/

$nombre_prospect_total_immoe = 0;
$nombre_prospect_donnee_60 = 0;
$nombre_prospect_personnel = 0;
$option_en_cours = 0;
$reservation_enregistree = 0;
$acte_signe = 0;

$res = db_query( "SELECT id FROM `prospects` WHERE (`denonce_par_id` = :vendeur_id OR `vendeur_id` = :vendeur_id) AND `lead_source` = 'IMMOE'", array(':vendeur_id' => $vendeur_id ) ) ;
$nombre_prospect_total_immoe = $res->rowCount();

$res = db_query( "SELECT id FROM `prospects` WHERE (`denonce_par_id` = :vendeur_id OR `vendeur_id` = :vendeur_id) AND `lead_source` = 'IMMOE' AND `created_at` > ADDDATE( NOW(), INTERVAL -60 DAY )", array(':vendeur_id' => $vendeur_id ) ) ;
$nombre_prospect_donnee_60 = $res->rowCount();

$res = db_query( "SELECT id FROM `prospects` WHERE `denonce_par_id` = :vendeur_id AND `lead_source` = 'IMMOE'", array(':vendeur_id' => $vendeur_id ) ) ;
$nombre_prospect_personnel = $res->rowCount();

$res = db_query( "SELECT id FROM `lots` WHERE `optionne_par_id` = :vendeur_id AND `suivivente` LIKE 'optionné'", array(':vendeur_id' => $vendeur_id ) ) ;
$option_en_cours = $res->rowCount();

$res = db_query( "SELECT id FROM `lots` WHERE `optionne_par_id` = :vendeur_id AND `suivivente` LIKE 'reservé'", array(':vendeur_id' => $vendeur_id ) ) ;
$reservation_enregistree = $res->rowCount();

$res = db_query( "SELECT id FROM `lots` WHERE `optionne_par_id` = :vendeur_id AND `suivivente` LIKE 'acté'", array(':vendeur_id' => $vendeur_id ) ) ;
$acte_signe = $res->rowCount();

?>
<script type="text/javascript">
    var iDisplayLength = 50;
</script>
<div class="float_right" style="padding-right: 20px;">
    <table  class="tableopportunites">
            <tbody>
                <tr>
                    <td>
                        <table  >
                            <tbody>
                                <tr>
                                    <th>Nbr prospect total attribué par IMMOE</th>
                                    <td><?php echo $nombre_prospect_total_immoe; ?></td>
                                </tr>
                                <tr>
                                    <th>Nbr prospect donnée 60 derniers jours par IMMOE</th>
                                    <td><?php echo $nombre_prospect_donnee_60; ?></td>
                                </tr>
                                <tr>
                                    <th>Nbr de prospect PERSONNEL dénoncé à IMMOE</th>
                                    <td><?php echo $nombre_prospect_personnel; ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    <td>
                        <table  >
                            <tbody>
                                <tr>
                                    <th>Option en cours</th>
                                    <td><?php echo $option_en_cours; ?></td>
                                </tr>
                                <tr>
                                    <th>Réservation enregistrée</th>
                                    <td><?php echo $reservation_enregistree; ?></td>
                                </tr>
                                <tr>
                                    <th>Acte signé</th>
                                    <td><?php echo $acte_signe; ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
                        </table>
    
</div>
<div id="nodepages" class="nonodepages">
<section id="mainContainer" class="clearfix monaction">
     
    <fieldset>
    <legend>Opportunités</legend>
    <div class="tableDataTable_wrapper">
        <table class="tab-terresens dataTable" id="resultOptions">
            <thead>
                <tr>
                    <th>Nom</th>
                    <th>Programme</th>
                    <th>Lot</th>
                    <th>Date R1</th>
                    <th>Date R2</th>
                    <th>Date gagné</th>
                    <th>Date perdue</th>
                    <th>Description</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($opportunites as $opportunite)
                {
                ?>
                <tr data-toggle="modal" data-opportunitehref="<?php echo url('majopporunite',array('query'=>array('id'=>$opportunite->id,'popin' => 1 ) )); ?>" id="<?php echo $opportunite->id; ?>">
                    <td><?php echo $opportunite->nom; ?></td>
                    <td><?php echo $opportunite->programme; ?></td>
                    <td><?php echo $opportunite->lot; ?></td>
                    <td><?php echo $opportunite->date_r1_f1; ?></td>
                    <td><?php echo $opportunite->date_r2_f1; ?></td>
                    <td><?php echo $opportunite->date_gagne_f1; ?></td>
                    <td><?php echo $opportunite->date_perdu_f1   ; ?></td>
                    <td><?php echo $opportunite->description; ?></td>
                </tr>
                <?php
                }
                ?>
            </tbody>
        </table>
    </div>
    </fieldset>
</section>
</div>
<div id="opportuniteFormModal" class="modal hide fade" tabindex="-1" role="dialog"></div>
 