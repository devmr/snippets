<?php
if( !user_is_logged_in() ){
    drupal_set_message(t('Vous devez être connecté pour accéder à cette page.'), 'status');
    $old_msg = drupal_get_messages(); 
    echo ( $old_msg['status'][0] );
    return;
}
if( !function_exists('quote')){
    function quote($str) {
        return sprintf("'%s'", $str);
    }
}
global $user;
$uid = $user->uid;
 
$tab_order = array(
      '' => ''
      ,'categorie'  => 'Résidence'
      ,'nom'  => 'Nom'
      ,'ville'  => 'ville'
      ,'nblots'  => 'Nombre de lots'
      ,'pctrentabilitebruteht'  => 'Renta'
      ,'offre_speciale'  => 'Offres spéciales'
      ,'dateactabilite'  => 'Statut'
      ,'datelivraison'  => 'Livraison'
  );


$order_programme = variable_get('order_programme', '');
$sort_programme = variable_get('sort_programme', '');    
$ordre_affichage_programme = variable_get('ordre_affichage_programme',array());
//print_r($ordre_affichage_programme);

foreach ($ordre_affichage_programme as $k => $v ) {
    $id[$k]  = $v['id'];
    $weight[$k] = $v['weight'];
}
$order = '';
if( count($ordre_affichage_programme) > 0 ){
    array_multisort($weight, SORT_ASC, $id, SORT_ASC, $ordre_affichage_programme);
    $order = ' ORDER BY FIELD(a.id,'.implode(',',array_map('quote', array_value_recursive('id',$ordre_affichage_programme ) ) ).')';
}
/*$sql="SELECT
a.id, a.nom, a.categorie, a.ville, a.noteeco, a.dateactabilite, a.datelivraison, a.offre_speciale, a.pctrentabilitebruteht, COUNT(b.programmeid) as nblots2, a.nblots
FROM programmes a
LEFT JOIN lots b ON b.programmeid = a.id
WHERE 
a.actif=1 AND b.mandat = 'IMMOE' AND (b.allotement='' or b.allotement IS NULL) AND b.suivivente IN('Libre','Optionne','Optionné') 
GROUP BY a.id".$order;*/

$sql="SELECT
a.id, a.nom, a.categorie, a.ville, a.noteeco, a.dateactabilite, a.datelivraison, a.offre_speciale, a.pctrentabilitebruteht, (SELECT COUNT(*) FROM lots WHERE suivivente in ('Libre','Optionne','Optionné') AND programmeid = a.id ) as nblots2, a.nblots
FROM programmes a
LEFT JOIN lots b ON b.programmeid = a.id
WHERE 
a.actif=1 AND b.mandat = 'IMMOE' AND (b.allotement='' or b.allotement IS NULL)  
GROUP BY a.id".$order;
// echo $sql; 

/*if( $order_programme!='' ){
    $sql .= " ORDER BY ".$order_programme;
    if( $sort_programme != '' ) $sql .= " ".$sort_programme;
}*/
//echo $sql;
$results = db_query( $sql, array( ) );
$programmes = $results->fetchAll();

$sql = "SELECT programme,stock FROM incweb_programme_user WHERE user = :user";
$programme = db_query( $sql, array(':user' => $user->uid ) )->fetchAssoc();

$tab_value = $tab_stvalue =array();
if( $programme ) $tab_value = explode(',',$programme['programme']);
if( $programme ) $tab_stvalue = explode(',',$programme['stock']);

$tabprog_shownblots = array();
$tabprog_shownblots = array_values( variable_get('programme33') ) ;
 
//print_r($tabprog_shownblots);
?>
<script type="text/javascript">
   // var orderindex = <?php echo (array_search($order_programme,array_keys($tab_order))>0?array_search($order_programme,array_keys($tab_order)):0); ?>;
   // var sortkey = '<?php echo (in_array(array('DESC','ASC'),$sort_programme)?strtolower($sort_programme):'asc'); ?>';
</script>
 
			<section id="mainContainer" class="clearfix">
			    <div class="tableDataTable_wrapper">
				    <p class="text-center">Cliquer sur une ligne pour accéder à la fiche descriptive du programme</p>
				    <table class="tab-terresens dataTable" id="resultRecherche">
				    	<thead>
							<tr>
								<th>Résidence</th>
								<th>Nom</th>
								<th>Ville</th>
								<th>Nombre de lots disponibles</th>
								<th>Rentabilité</th>
								<th>Offres spéciales</th>
								<th>Statut</th>
								<th>Livraison</th>
							</tr>
						</thead>
						<tbody>
							<?php
							foreach ($programmes as $programme)
			                    {
                                                            if( !in_array('all',$tab_value ) && !in_array($programme->id,$tab_value ) ) {
                                                                
                                                                
    

			                ?>
							<tr data-href="programme?pid=<?php echo $programme->id;?>">
								<td><?php echo $programme->categorie;?></td>
								<td><?php echo $programme->nom;?></td>
								<td><?php echo $programme->ville;?></td>
								<td class="text-center"><?php echo (in_array('all',$tab_stvalue ) || !in_array($programme->id,$tab_stvalue ) ? $programme->nblots2 : '');?></td>
								<td><?php echo $programme->pctrentabilitebruteht?></td>
								<td style="color:red;"><?php echo nl2br($programme->offre_speciale);?></td>
								<td><?php echo $programme->dateactabilite?></td>
								<td><?php echo $programme->datelivraison;?></td>
							</tr>
			                <?php
                                                        }
                                            }
			                ?>
						</tbody>
				    </table>
			    </div>
			</section><!-- end section homeContainer -->
    <script>		
		$(document).ready(function() {
			terresens.global_js_init();
			terresens.dataTable.init();	
		});		
	</script>
 