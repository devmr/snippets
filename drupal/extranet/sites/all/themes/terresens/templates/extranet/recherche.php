<?php
 if( !user_is_logged_in() ){
    drupal_set_message(t('Vous devez être connecté pour accéder à cette page.'), 'status');
    $old_msg = drupal_get_messages(); 
    echo ( $old_msg['status'][0] );
    return;
}
$sql="SELECT id, nom, categorie, ville, noteeco, dateactabilite, datelivraison, offre_speciale, pctrentabilitebruteht FROM programmes WHERE actif=1";
$results = db_query( $sql, array( ) );
$programmes = $results->fetchAll();
?>
 
			<section id="mainContainer" class="clearfix">
			    <div class="tableDataTable_wrapper">
				    <p class="text-center">Cliquer sur une ligne pour accéder à la fiche descriptive du programme</p>
				    <table class="tab-terresens dataTable" id="resultRecherche">
				    	<thead>
							<tr>
								<th>Résidence</th>
								<th>Nom</th>
								<th>Ville</th>
								<th>Note<br>éco</th>
								<th>Renta</th>
								<th>Offres spéciales</th>
								<th>Statut</th>
								<th>Livraison</th>
							</tr>
						</thead>
						<tbody>
							<?php
							foreach ($programmes as $programme)
			                    {
			                ?>
							<tr data-href="?pid=<?php echo $programme->id;?>">
								<td><?php echo $programme->categorie;?></td>
								<td><?php echo $programme->nom;?></td>
								<td><?php echo $programme->ville;?></td>
								<td class="text-center"><?php echo $programme->noteeco;?></td>
								<td><?php echo $programme->pctrentabilitebruteht?></td>
								<td ><?php echo nl2br($programme->offre_speciale);?></td>
								<td><?php echo $programme->dateactabilite?></td>
								<td><?php echo $programme->datelivraison;?></td>
							</tr>
			                <?php
			                    }
			                ?>
						</tbody>
				    </table>
			    </div>
			</section><!-- end section homeContainer -->
    <script>		
		$(document).ready(function() {
			terresens.global_js_init();
			terresens.dataTable.init();	
		});		
	</script>
 