<?php
/*
Template Name: detail programmes
*/
if( !user_is_logged_in() ){
    drupal_set_message(t('Vous devez être connecté pour accéder à cette page.'), 'status');
    $old_msg = drupal_get_messages(); 
    echo ( $old_msg['status'][0] );
    return;
}
$tab_value = array();
if( $programme ) $tab_value = explode(',',$programme['programme']);

$tabprog_shownblots = array();
$tabprog_shownblots = array_values( variable_get('programme33') ) ;

if ( !defined('BASE_URL') )
define('BASE_URL', (base_path()!='/'?base_path():''));
define('TEMPLATEPATH', drupal_get_path('theme',$GLOBALS['theme'] ) );
$pid = $data['pid'];
if ($pid>-1){
	$sql="SELECT p.*, g.email as gest_email, g.tel as gest_tel
	 		FROM programmes p
	 		LEFT JOIN gestionnaires g ON p.gestionnaireid = g.id
	 		WHERE p.actif=1 and p.id = :pid";
        $results = db_query( $sql, array(':pid' => $pid ) );
	//$programmes = $wpdb->get_results($sql);
	$programme = $results->fetchObject();
         
	
} 

$prog_dir = '/wp-content/documents/'.sanitize_file_name($programme->nom);

$prog_root_dir = DRUPAL_ROOT.$prog_dir;
 

$photos = array();
if (is_dir($prog_root_dir.'/Photos/')){
	$d = dir($prog_root_dir.'/Photos/');
	while (false !== ($entry = $d->read())) {
		if (($entry==".")||($entry=="..")||(strpos(trim($entry),'.')==0)) continue;
	   	$photos[] = $entry;
	}
	$d->close();
	$photo_url = BASE_URL.$prog_dir.'/Photos/';
}
//usort($photos, 'compare_filename');

$logo = array();
$prog_root_dir = DRUPAL_ROOT.$prog_dir;
if (is_dir($prog_root_dir.'/Logo/')){
	$d = dir($prog_root_dir.'/Logo/');
	while (false !== ($entry = $d->read())) {
		if (($entry==".")||($entry=="..")||(strpos(trim($entry),'.')==0)) continue;
	   	$logo[] = $entry;
	}
	$d->close();
	$logo_url = BASE_URL.$prog_dir.'/Logo/';
}
 
$fpath = $prog_dir.'/Documents-marketing/Book de vente.pdf';
$book = file_exists(DRUPAL_ROOT.$fpath)?BASE_URL.$fpath:'';
 

$fpath = $prog_dir.'/Documents-marketing/Fiche technique.pdf';
$fichetechnique = file_exists(DRUPAL_ROOT.$fpath)?BASE_URL.$fpath:'';

$fpath = $prog_dir.'/Documents-information/Plan de masse.pdf';
$planmasse = file_exists(DRUPAL_ROOT.$fpath)?BASE_URL.$fpath:'';


global $user;
$sql = "SELECT programme FROM incweb_programme_user WHERE user = :user";
$programme_user = db_query( $sql, array(':user' => $user->uid ) )->fetchAssoc();

$tab_valueprogramme_user = array();
if( $programme_user ) $tab_valueprogramme_user = explode(',',$programme_user['programme']);

if( !in_array('all',$tab_valueprogramme_user ) && in_array($programme->id,$tab_valueprogramme_user ) ) {
    drupal_set_message(t('Ce programme n\'est pas accessible.'), 'status');
    $old_msg = drupal_get_messages(); 
    echo ( $old_msg['status'][0] );
    return;
}

 
 
 
 
?>
<div class="clearfix">
<div class="btnGreen1">
    <a href="programme?pid=<?php echo $pid; ?>" >Description</a>
</div>
<div class="btnGreen1">
    <a href="programme?page=lot&pid=<?php echo $pid; ?>" >Lots/Stocks</a>
</div>
<div class="btnGreen1">
    <a href="programme?page=docs&pid=<?php echo $pid; ?>" >Documents à télécharger</a>
</div>
 
</div>
 
			<section id="mainContainer" class="clearfix">
			     
			    <div id="col-left" class="pull-left">
			    	<!--<h3 id='horodatage'>Le <?php echo date('d/m/Y à H:i');?><h3>-->
				    <br /><table class="tab-terresens">
				   	<tr>
				   		<td class="logo" style="width:175px;">
				   			<?php if ( sizeof($logo)>0 ){ ?>
				   			<img src="<?php echo $logo_url.$logo[0]; ?>" alt="" width="175"/>
				   			<?php } ?>
				   		</td>
				   		<td class="name" colspan="2">
				   			<p class="name">Nom du Programme : <?php echo $programme->nom; ?></p>
				   			<p class="type">Type de Résidence : <?php echo $programme->categorie; ?></p>
				   		</td>
				   		<td class="actions" style="width:75px;">
				   			<a target="_blank" href="sites/all/themes/terresens/templates/extranet/genpdf/generate.php?view=single&pid=<?php echo $pid; ?>" title="Imprimer">
                                                            <img src="<?php echo drupal_get_path('theme',$GLOBALS['theme'] ); ?>/img/btn_print.png" alt="" />
                                                        </a>
				   			<?php /*<a href="#" title="Envoyer par mail" onclick="javascript:emailcontent = encodeURI($('#email').html()); self.location.href='mailto:mescontacts?body='+emailcontent; return false;"><img src="<?php echo drupal_get_path('theme',$GLOBALS['theme'] ); ?>/img/btn-mailto.jpg" alt="" /></a>*/?>
				   		</td>
				   	</tr>				   	
				   	<tr>
				   		<th colspan="4">INFORMATIONS GÉNÉRALES</th>
				   	</tr>
				   	<tr>
				   		<td colspan="4"><span class="label-fiche">Description de l’opération :</span> <?php echo nl2br($programme->description); ?> </td>
				   	</tr>				   	
				   	<tr>
				   		<td colspan="2"><span class="label-fiche">Etat de la Résidence :</span> <?php echo $programme->etat; ?></td>
				   		<td colspan="2"><span class="label-fiche">Statut :</span> <?php echo $programme->categorie; ?></td>
				   	</tr>
				   	<tr>
				   		<td colspan="2"><span class="label-fiche">Fiscalité :</span><br/><?php echo str_replace(';', '<br/>', $programme->fiscalite); ?></td>
				   		<td colspan="2"><span class="label-fiche">Précisions :</span> <?php echo str_replace(';', '<br/>', $programme->precisiontype); ?></td>
				   	</tr>
				   	<tr>
				   		<td colspan="2" rowspan="2"><span class="label-fiche">Adresse :</span> <?php echo $programme->adresse; ?></td>
				   		<td colspan="2"><span class="label-fiche">Type d’acquisition :</span><br/><?php echo str_replace(';', '<br/>', $programme->typeacquisition); ?></td>
				   	</tr>
				   	<tr>
				   		<td colspan="2"><span class="label-fiche">Type d’habitat :</span> <?php echo $programme->typehabitat; ?></td>
				   	</tr>	
				   	<tr>
				   		<td colspan="2"><?php echo (in_array('all',$tabprog_shownblots ) || !in_array($programme->id,$tabprog_shownblots ) ? '<span class="label-fiche">Nombre de lots :</span> '.$programme->nblots : '&nbsp;');?> </td>
				   		<td colspan="2"><span class="label-fiche">Typologie :</span> <?php echo $programme->typologie; ?></td>
				   	</tr>			   	
				   	<tr>
				   		<th colspan="4">INFORMATIONS TECHNIQUES</th>
				   	</tr>	
				   	<tr>
				   		<td colspan="4"><span class="label-fiche">Equipements des lots :</span> <?php echo $programme->equipementlot; ?> </td>
				   	</tr>				   	
				   	<tr>
				   		<td colspan="4"><span class="label-fiche">Parties communes :</span> <?php echo $programme->partiescommunes; ?> </td>
				   	</tr>
				   	<tr>
				   		<td colspan="4"><span class="label-fiche">Normes constructives :</span> <?php echo $programme->normesconstructives; ?></td>
				   	</tr>
				   	<tr>
				   		<th colspan="2">INFORMATIONS COMMERCIALES</th>
				   		<th colspan="2">Planning</th>
				   	</tr>
				   	<tr>
				   		<td colspan="2"><span class="label-fiche">Rentabilité brute HT </span><span class="desc-tab">(sur prix appartement seul HT)</span> : <?php echo $programme->pctrentabilitebruteht; ?></td>
				   		<td colspan="2"><span class="label-fiche">Actabilité :</span> <?php echo $programme->dateactabilite; ?></td>
				   	</tr>
				   	<tr>
				   		<td colspan="2"><span class="label-fiche">Pack de vente :</span><br/><?php echo nl2br($programme->typepackvente); ?></td>
				   		<td colspan="2"><span class="label-fiche">Date de Livraison :</span> <?php echo $programme->datelivraison; ?></td>
				   	</tr>
				   	<tr>
				   		<td colspan="2"><span class="label-fiche">Prix de vente HT/m2 de l’appartement seul</span><br><span class="desc-tab">(hors parking, hors mobilier)</span> : <?php echo number_format2($programme->pvhtm2,0,',',' '); ?> &euro;</td>
				   		<td colspan="2" rowspan="2"><span class="label-fiche">Date de mise en exploitation :</span> <?php echo $programme->datemiseexploitation?></td>
				   	</tr>
				   	<tr>
				   		<td colspan="2"><span class="label-fiche">Prix HT moyen du lot</span><br><span class="desc-tab">(appartement + parking + mobilier)</span> : <?php echo number_format2($programme->pvhtmoyenlot,0,',',' '); ?> &euro;</td>
				   		
				   	</tr>
				   	<tr>
				   		<td colspan="2"><span class="label-fiche">Prix HT minimum</span><br><span class="desc-tab">(appartement + parking + cave + mobilier)</span> : <?php echo number_format2($programme->pvhtminlot,0,',',' ');?> &euro;</td>
				   		<th colspan="2">INFORMATIONS ENVIRONNEMENTALES</th>
				   	</tr>
				   	<tr>
				   		<td colspan="2"><span class="label-fiche">Prix HT maximum</span><br><span class="desc-tab">(appartement + parking + cave + mobilier)</span> : <?php echo number_format2($programme->pvhtmaxlot,0,',',' ');?> &euro;</td>
				   		<td colspan="2"><span class="label-fiche">Note éco :</span> <?php echo $programme->noteeco; ?></td>
				   	</tr>
				   	<tr>
				   		<td colspan="2"><span class="label-fiche">Agrément :</span> <?php echo $programme->agrementorganisme; ?></td>
				   		<td colspan="2"><span class="label-fiche">Les + éco :</span> <?php echo nl2br($programme->pluseco); ?></td>
				   	</tr>
				   	<tr>
				   		<th colspan="4">LES INTERVENANTS</th>
				   	</tr>	
				   	<tr>
				   		<td colspan="4"><span class="label-fiche">Promoteur 1 :</span> <?php echo $programme->promoteur1; ?></td>
				   	</tr>				   	
				   	<tr>
				   		<td colspan="4"><span class="label-fiche">Promoteur 2 :</span> <?php echo $programme->promoteur2; ?></td>
				   	</tr>
				   	<tr>
				   		<td colspan="4"><span class="label-fiche">Société de construction vente :</span> <?php echo $programme->sccv; ?></td>
				   	</tr>
				   	<tr>
				   		<td colspan="4"><span class="label-fiche">Gestionnaire :</span> <?php echo $programme->gestion; ?></td>
				   	</tr>
				   	 
				   </table>
			    </div>
			    <div id="col-right" class="pull-right">
			    	<div class="widget-colum">
				    	<div class="widget-col-right widget-contact clearfix">
				    		<img src="<?php echo drupal_get_path('theme',$GLOBALS['theme'] ); ?>/img/picto-contact.png"  alt="picto-contact" width="40" height="40">
				    		<p>
				    			<span class="contactName">
				    				<?php if (!empty($programme->gest_email)) echo '<a href="mailto:'.$programme->gest_email.'">'; ?>
				    				<?php echo $programme->gestionnaire; ?>
				    				<?php if (!empty($programme->gest_email)) echo '</a>'; ?>
				    			</span>
				    			<br>
				    			<span class="contactPhone"><?php echo !empty($programme->gest_tel)?$programme->gest_tel:'04 72 14 07 14'; ?></span>
				    		</p>
				    	</div>
				    	<div class="widget-col-right widget-actions" style="padding:20px 0;margin:20px 0;border-bottom: 6px solid #bbcc00;border-top: 6px solid #bbcc00;">
				    		<p class="text-center">
				    			Visualiser, télécharger et imprimer 
				    		</p>
                                                <div id="block-menu-block-1" class="menu-block-wrapper menu-block-1 menu-name-menu-sidebar-menu parent-mlid-0 menu-level-1">
                                                    <ul class="menu context clearfix" style="padding:0;margin:0;width:auto;">
                                                        
                                                        <li class="leaf menu-mlid-1094">
                                                            <a class="fancybox.iframe fancy" href="<?php echo url('telecharger_docmkt',array('query'=>array('pid' => $pid , 'popin' => 1, 'opt' => 1 ) )); ?>" target="_blank" title="Télécharger les documents marketings" >Télécharger les documents marketings</a>
                                                        </li>
                                                        <li class="leaf menu-mlid-1094">
                                                            <a class="fancybox.iframe fancy" href="<?php echo url('recevoirkit',array('query'=>array('pid' => $pid , 'popin' => 1  ) )); ?>" target="_blank" title="Recevoir par courrier un kit de commercialisation" >Recevoir par courrier un kit de commercialisation</a>
                                                        </li>
                                                        <li class="leaf menu-mlid-1094">
                                                            <a class="fancybox.iframe fancy" href="<?php echo url('telecharger_docmkt',array('query'=>array('pid' => $pid , 'popin' => 1, 'opt' => 3 ) )); ?>" target="_blank" title="Télécharger les documents marketings à votre enseigne" >Télécharger les documents marketings à votre enseigne</a>
                                                        </li>
                                                        <!--
                                                        <li class="leaf menu-mlid-1094">
                                                            <a class="fancybox.iframe fancy" href="<?php echo url('ask_plaquettes',array('query'=>array('pid' => $pid , 'popin' => 1 ) )); ?>" target="_blank" title="Recevoir par courrier des plaquettes" >Recevoir des plaquettes</a>
                                                        </li>
                                                        <li class="leaf menu-mlid-1094">
                                                            <a href="sites/all/themes/terresens/templates/extranet/genpdf/generate.php?pid=<?php echo $pid; ?>&view=single" target="_blank" title="Générer la fiche du programme" >Générer la fiche du programme</a>
                                                        </li>
                                                         
                                                        <li class="leaf menu-mlid-1094">
                                                            <a class="fancybox.iframe fancy" href="<?php echo url('ask_plaquettes',array('query'=>array('pid' => $pid , 'popin' => 1 ) )); ?>" target="_blank" title="Recevoir par courrier des plaquettes" >Recevoir des plaquettes</a>
                                                        </li>
                                                         	
                                                        <li class="first leaf menu-mlid-1096">
                                                            <a href="<?php echo $book; ?>"  target="_blank"  title="Télécharger la brochure" >Télécharger la brochure</a>
                                                        </li>
                                                         
                                                        <li class="leaf menu-mlid-1094">
                                                            <a href="<?php echo $fichetechnique; ?>" target="_blank" title="Télécharger la fiche technique" >Télécharger la fiche technique</a>
                                                        </li>
                                                         
                                                        <li class="last leaf menu-mlid-1095">
                                                            <a href="<?php echo $planmasse; ?>" target="_blank" title="Télécharger le plan de masse">Télécharger le plan de masse</a>
                                                        </li>
                                                          -->
                                                    </ul>
                                                </div>
				    		 
				    	</div>
				    	<?php if (sizeof($photos)>0) {?>
				    	<div class="widget-col-right widget-carousel">
				    		<div id="carouselPI" class="carousel-slide">
				    			 
								    <!-- Carousel items -->
							    <div class="carousel-inner">
							    <?php foreach ($photos as $key => $photo) { 
								    echo '<div class="' . ($key==0?'active ':'') . 'item"><a href="#"><img src="' .$photo_url.$photo. '" alt="Photo'.$key.'" width="230" height="163" /></a></div>';
								} ?>  
							    </div>
							    <!-- Carousel nav -->
							    <a class="carousel-control-left " href="#carouselPI" data-slide="prev"></a>
							    <a class="carousel-control-right " href="#carouselPI" data-slide="next"></a>
				    		</div>
				    	</div>
				    	<?php } 
				    	if ($programme->youtube>''){
				    		if (preg_match('/v=(.*)&/', $programme->youtube, $result)){
				    			$ytid = $result[1];
				    	?>
				    	<div class="widget-col-right widget-video">
				    		<iframe name="ytframe" width="277" height="175" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" allowfullscreen src="http://www.youtube.com/embed/<?php echo $ytid; ?>?modestbranding=1&showinfo=0&rel=0"></iframe>
				    	</div>
				    	<?php
				    		}
				    	}
				    	?>
				    	<div class="widget-col-right widget-plan">
				    		<!--<iframe name="mapframe" width="277" height="175" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.fr/maps?f=q&amp;source=s_q&amp;hl=fr&amp;geocode=&amp;q=<?php echo urlencode($programme->adresse.','. $programme->ville); ?>&amp;aq=0&amp;t=m&amp;oq=<?php echo urlencode($programme->adresse); ?>&amp;ll=<?php echo urlencode($programme->geocoord); ?>&amp;ie=UTF8&amp;output=embed&amp;z=14&amp;iwloc=near"></iframe> !-->
				    		<iframe name="mapframe" width="277" height="175" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.fr/maps?f=q&amp;source=embed&amp;output=embed&amp;hl=fr&amp;geocode=&amp;q=<?php echo ($programme->geocoord); ?>&amp;aq=&amp;ie=UTF8&amp;t=m&amp;z=14&amp;<?php echo ($programme->geocoord); ?>"></iframe>
				    	</div>
			    	</div>
			    </div>
			</section><!-- end section homeContainer -->
 
 