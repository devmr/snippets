<?php
 
global $user;
$lots = new stdClass;
$wpuserid = $user->wpusers;
$sql = "SELECT `vendeur_id` FROM `conseillers` WHERE `wpuserid` = :wpuserid";
$results = db_query( $sql, array(':wpuserid' => $wpuserid ) )->fetchObject();
//print_r( $results );

$prospects = array();
if( $results ){
    $user_id = $results->vendeur_id;

    $sql = "SELECT id, objet, programme, optionne_le, date_add(l.optionne_le, interval 5 day) as date_validite
            FROM lots
            WHERE suivivente = 'Optionne' AND optionne_par_id = :user_id";   
//echo $sql;
//$lots = db_query( $sql, array(':user_id' => $user_id ) )->fetchObject();

    $sql = "SELECT * 
            FROM prospects
            WHERE denonce_par_id = :user_id";
    $prospects = db_query( $sql, array(':user_id' => $user_id ) )->fetchAll();
    //$prospects = $wpdb->get_results($query);
}
$sql="SELECT nom FROM programmes WHERE actif=1";
//$programmes = $wpdb->get_results($sql);
$programmes = db_query( $sql, array() )->fetchAll();

?>
<script type="text/javascript">
    var iDisplayLength = 25;
</script>
<div id="nodepages" class="nonodepages">
<section id="mainContainer" class="clearfix monaction">
    
    <fieldset>
    <legend>Options</legend>
    <div class="tableDataTable_wrapper">
        <table class="tab-terresens dataTable" id="resultOptions">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Validité</th>
                    <th>Détail</th>
                </tr>
            </thead>
            <tbody>
                <?php
                //$pagelink = get_page_link(123);
                if(count($lots)>0){
                foreach ($lots as $lot)
                {
                ?>
                <tr data-href="<?php echo "?lid=".$lot->id; ?>">
                    <td><?php echo $lot->optionne_le?></td>
                    <td><?php echo $lot->date_validite?></td>
                    <td><?php echo $lot->programme.' '.$lot->objet; ?></td>
                </tr>
                <?php
                }
                }
                ?>
            </tbody>
        </table>
    </div>
    </fieldset>
    <div class="clearfix"></div>
    <fieldset>
    <legend>Prospects dénoncés</legend>
    <div class="tableDataTable_wrapper">
        <table class="tab-terresens dataTable" id="resultProspects">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Détail</th>
                    <th>Description</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if(count($prospects)>0){
                foreach ($prospects as $prospect)
                {
                ?>
                <tr>
                    <td><?php echo $prospect->denonce_le; ?></td>
                    <td><?php echo $prospect->nom.' '.$prospect->prenom.($prospect->programme>''?' - '.$prospect->programme:''); ?></td>
                    <td><?php echo nl2br($prospect->description); ?></td>
                </tr>
                <?php
                }
                }
                ?>
            </tbody>
        </table>
    </div>
    <div class="clearfix"><br/><br/><br/></div>
    <div class="submit">
    <input type="button" data-href="<?php echo url('node/17',array('query'=>array('popin' => 1 ) )); ?>" name="modalContactLink" id="modalContactLink" class="form-submit btn btn-primary btn-contact" value="Dénoncer un nouveau prospect" />
    </div>
    </fieldset>
</section>
</div>

 