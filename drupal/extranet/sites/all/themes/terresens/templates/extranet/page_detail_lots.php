<?php
/*
Template Name: detail lots
*/
if( !user_is_logged_in() ){
    drupal_set_message(t('Vous devez être connecté pour accéder à cette page.'), 'status');
    $old_msg = drupal_get_messages(); 
    echo ( $old_msg['status'][0] );
    return;
}
if ( !defined('BASE_URL') )
define('BASE_URL', (base_path()!='/'?base_path():''));
define('TEMPLATEPATH', drupal_get_path('theme',$GLOBALS['theme'] ) );

$lid = $data['lid'];
$pid = $data['pid'];
 
if ($lid>-1){
	$sql="SELECT l.*, date_add(optionne_le, interval 5 day) as date_validite
	 		FROM lots l
	 		WHERE l.id = :lid";
        $lot = db_query( $sql, array(':lid' => $lid ) )->fetchObject();
	/*$lots = $wpdb->get_results($sql);
	$lot = $lots[0];*/

	$sql= "SELECT p.*, g.email as gest_email, g.tel as gest_tel
	 		FROM programmes p LEFT JOIN gestionnaires g ON p.gestionnaireid = g.id
	 		WHERE p.actif=1 and p.id = :pid";
	 		//WHERE p.actif=1 and p.id = '".$lot->programmeid."'";

	//$programmes = $wpdb->get_results($sql);
	$programme = db_query( $sql, array(':pid' => $lot->programmeid ) )->fetchObject();
} 

$sanitized_progdir =  sanitize_file_name($programme->nom);
$sanitized_lotdir =  sanitize_file_name($lot->objet).'_'. ($lot->typelot);

$prog_root_dir = DRUPAL_ROOT.'/wp-content/documents/'.$sanitized_progdir;

$photos = array();
if (is_dir($prog_root_dir.'/Photos/')){
	$d = dir($prog_root_dir.'/Photos/');
	while (false !== ($entry = $d->read())) {
		if (($entry==".")||($entry=="..")||(strpos(trim($entry),'.')==0)) continue;
	   	$photos[] = $entry;
	}
	$d->close();
	$photo_url = BASE_URL.'/wp-content/documents/'.$sanitized_progdir.'/Photos/';
}
usort($photos, 'compare_filename');

$logo = array();
if (is_dir($prog_root_dir.'/Logo/')){
	$d = dir($prog_root_dir.'/Logo/');
	while (false !== ($entry = $d->read())) {
		if (($entry==".")||($entry=="..")||(strpos(trim($entry),'.')==0)) continue;
	   	$logo[] = $entry;
	}
	$d->close();
	$logo_url = BASE_URL.'/wp-content/documents/'.$sanitized_progdir.'/Logo/';
}

$doc_lots = array();
if (is_dir($prog_root_dir.'/Documents-propres-aux-lots/'.$sanitized_lotdir)){
	$d = dir($prog_root_dir.'/Documents-propres-aux-lots/'.$sanitized_lotdir);
	while (false !== ($entry = $d->read())) {
		if (($entry==".")||($entry=="..")||(strpos(trim($entry),'.')==0)) continue;
	   	$doc_lots[] = $entry;
	}
	$d->close();
	$lots_url = BASE_URL.'/wp-content/documents/'.$sanitized_progdir.'/Documents-propres-aux-lots/'.$sanitized_lotdir.'/';
}
usort($doc_lots, 'compare_filename');
?>
<div class="clearfix">
<div class="btnGreen1">
    <a href="programme?pid=<?php echo $pid; ?>" >Description</a>
</div>
<div class="btnGreen1">
    <a href="programme?page=lot&pid=<?php echo $pid; ?>" >Lots/Stocks</a>
</div>
<div class="btnGreen1">
    <a href="programme?page=docs&pid=<?php echo $pid; ?>" >Documents à télécharger</a>
</div>
<div class="btnGreen1">
    <a href="sites/all/themes/terresens/templates/extranet/genpdf/generatecontrat.php?lid=<?php echo $lid; ?>&pid=<?php echo $pid; ?>" >Générer un contrat</a>
</div>    
</div>
	<section id="mainContainer" class="clearfix">
	     
	    <div id="col-left" class="pull-left">
	    	<h3><?php echo $programme->nom;?></h3>
	    	<h3 id='horodatage'>Le <?php echo date('d/m/Y à H:i');?><h3>
		   	<table class="tab-terresens">
			   	<tr>
			   		<td class="logo" style="width:175px;">
			   			<?php if ( sizeof($logo)>0 ){ ?>
				   		<img id='logoprog' src="<?php echo $logo_url.$logo[0]; ?>" alt=""  width="175" />
				   		<?php } ?>
			   		</td>
			   		<td class="name">
			   			<h4>LOT N° <?php echo $lot->numlotplan;?></h4>
			   		</td>
			   		<td class="actions" style="width:291px;">
                                            <a href="https://demo.crmtopinvest.fr/" target="_blank" ><img src="<?php echo TEMPLATEPATH; ?>/img/topinvest_logo.png" alt="" /></a>
			   			<?php if ($lot->suivivente!='Optionne') {?>
			   			<a href="incweb?lid=<?php echo $lid; ?>&pid=<?php echo $pid; ?>" title="Poser une option" class="fancybox.iframe fancy" id="<?php echo $lot->id;?>" ><img src="<?php echo TEMPLATEPATH; ?>/img/btn-option.png" alt="" /></a>
			   			<?php } ?>
			   			<a href="sites/all/themes/terresens/templates/extranet/genpdf/generate.php?view=lot&pid=<?php echo $pid; ?>&lid=<?php echo $lid; ?>" title="Imprimer" target="_blank" ><img src="<?php echo drupal_get_path('theme',$GLOBALS['theme'] ); ?>/img/btn_print.png" alt="" /></a>
			   			<?php /*<a href="#" title="Envoyer par mail"><img src="<?php echo TEMPLATEPATH; ?>/img/btn-mailto.jpg" alt="" /></a> */ ?>
			   		</td>
			   	</tr>
			   	<tr>
			   		<th colspan="3">ADRESSE</th>
			   	</tr>
			   	<tr>
			   		<td colspan="3"><?php echo $programme->adresse ?></td>
			   	</tr>				   	
			   	<tr>
			   		<th colspan="3">INFORMATIONS GÉNÉRALES</th>
			   	</tr>
			   	<tr>
			   		<td colspan="3"><span class="label-fiche">Bâtiment :</span> <?php echo $lot->batiment ?></td>
			   	</tr>				   	
			   	<tr>
			   		<td colspan="3"><span class="label-fiche">Etage :</span> <?php echo $lot->etage ?></td>
			   	</tr>
			   	<tr>
			   		<td colspan="3"><span class="label-fiche">Type :</span> <?php echo $lot->typelot ?></td>
			   	</tr>
			   	<tr>
			   		<td colspan="3"><span class="label-fiche">Orientation :</span> <?php echo $lot->orientation ?></td>
			   	</tr>
			   	<tr>
			   		<td colspan="3"><span class="label-fiche">N° de parking :</span> <?php echo $lot->numparkingplans ?></td>
			   	</tr>			   	
			   	<tr>
			   		<th colspan="3">LES SUPERFICIES EN M<sup>2</sup></th>
			   	</tr>	
			   	<tr>
			   		<td colspan="3"><span class="label-fiche">SHAB :</span> <?php echo $lot->shab ?></td>
			   	</tr>			   		
			   	<tr>
			   		<td colspan="3"><span class="label-fiche">Surface CARREZ :</span> <?php echo $lot->surfacecarrez ?></td>
			   	</tr>
			   	<tr>
			   		<td colspan="3"><span class="label-fiche">Jardin :</span> <?php echo $lot->jardin ?></td>
			   	</tr>
			   	<tr>
			   		<td colspan="3"><span class="label-fiche">Balcon :</span> <?php echo $lot->balcon ?></td>
			   	</tr>
			   	<tr>
			   		<td colspan="3"><span class="label-fiche">Terrasse :</span> <?php echo $lot->terrasse ?></td>
			   	</tr>		   	
			   	<tr>
			   		<th colspan="3">LES PRIX</th>
			   	</tr>		
			   	<tr>
			   		<td colspan="3"><span class="label-fiche">Appartement H.T.:</span> <?php echo number_format2($lot->baseapptht,0,',',' ') ?> &euro;</td>
			   	</tr>				   	
			   	<tr>
			   		<td colspan="3"><span class="label-fiche">Parking H.T.:</span> <?php echo number_format2($lot->baseparkinght,0,',',' ') ?> &euro;</td>
			   	</tr>
			   	<tr>
			   		<td colspan="3"><span class="label-fiche">Appartement + parking H.T. :</span> <?php echo number_format2($lot->baseapptparkinght,0,',',' ') ?> &euro;</td>
			   	</tr>			   	
			   	<tr>
			   		<td colspan="3"><span class="label-fiche">Appartement + parking T.T.C. :</span> <?php echo number_format2($lot->baseapptparkingttc,0,',',' ') ?> &euro;</td>
			   	</tr>
			   	<tr>
			   		<td colspan="3"><span class="label-fiche">Mobilier H.T. :</span> <?php echo number_format2($lot->basemobilierht,0,',',' ') ?> &euro;</td>
			   	</tr>
			   	<tr>
			   		<td colspan="3"><span class="label-fiche">Mobilier T.T.C. :</span> <?php echo number_format2($lot->basemobilierttc,0,',',' ') ?> &euro;</td>
			   	</tr>
			   	<tr>
			   		<td colspan="3"><span class="label-fiche">TOTAL H.T. :</span> <?php echo number_format2($lot->basetotalht,0,',',' ') ?> &euro;</td>
			   	</tr>				   	
			   	<tr>
			   		<td colspan="3"><span class="label-fiche">Rentabilité brute H.T. sur l’appartement H.T. seul :</span> <?php echo $lot->rentabilitebruteht ?></td>
			   	</tr>
			   	<tr>
			   		<td colspan="3"><span class="label-fiche">Loyer H.T. annuel :</span> <?php echo number_format2($lot->baseloyertotalhtannuel,0,',',' ') ?> &euro;</td>
			   	</tr>   	
			   	<tr>
			   		<th colspan="3">STATUT DU LOT</th>
			   	</tr>
			   	<tr>
			   		<td colspan="3"><span class="label-fiche">Statut :</span> <?php echo $lot->statut ?></td>
			   	</tr>
			   	<tr>
			   		<td colspan="3"><span class="label-fiche">Actabilité :</span> <?php echo $programme->dateactabilite; ?></td>
			   	</tr>				   	
			   	<tr>
			   		<td colspan="3"><span class="label-fiche">Livraison :</span> <?php echo $programme->datelivraison; ?></td>
			   	</tr>
			   	<tr>
			   		<td colspan="3"><span class="label-fiche">Disponibilité :</span> <?php echo $lot->suivivente; ?></td>
			   	</tr> 
		   	</table>
	    </div>
	    <div id="col-right" class="pull-right">
	    	<div class="widget-colum">
	    		<?php if ($lot->date_validite>''){?>
	    		<div class="widget-col-right widget-contact">
		    		<p>
		    			<span class="contactName">Date de validité option</span>
		    			<br>
		    			<span class="contactName"><?php echo $lot->date_validite; ?></span>
		    		</p>
		    	</div>
		    	<?php } ?>
		    	<div class="widget-col-right widget-contact">
		    		<img src="<?php echo drupal_get_path('theme',$GLOBALS['theme'] ); ?>/img/picto-contact.png" alt="picto-contact" width="40" height="40">
		    		<p>
		    			<span class="contactName">
		    				<?php if (!empty($programme->gest_email)) echo '<a href="mailto:'.$programme->gest_email.'">'; ?>
		    				<?php echo $programme->gestionnaire; ?>
		    				<?php if (!empty($programme->gest_email)) echo '</a>'; ?>
		    			</span>
		    			<br>
		    			<span class="contactPhone"><?php echo !empty($programme->gest_tel)?$programme->gest_tel:'04 72 14 07 14'; ?></span>
		    		</p>
		    	</div>
		    	<div class="widget-col-right widget-actions">
		    		<?php if (sizeof($doc_lots)==0){ ?>
		    		<p class="text-center">
						&nbsp;Pas de documents disponibles
					</p>
					<?php }else{?>
		    		<p class="text-center">
		    			<a href="<?php echo $lots_url.$doc_lots[0]; ?>" target="_blank">Document(s) propre(s) aux lots</a>
		    		</p>
					<?php }?>
		    	</div>
				<?php if (sizeof($photos)>0) {?>
		    	<div class="widget-col-right widget-carousel">
		    		<div id="carouselPI" class="carousel-slide">
				    			 
								    <!-- Carousel items -->
							    <div class="carousel-inner">
							    <?php foreach ($photos as $key => $photo) { 
								    echo '<div class="' . ($key==0?'active ':'') . 'item"><a href="#"><img src="' .$photo_url.$photo. '" alt="Photo'.$key.'" width="230" height="163" /></a></div>';
								} ?>  
							    </div>
							    <!-- Carousel nav -->
							    <a class="carousel-control-left " href="#carouselPI" data-slide="prev"></a>
							    <a class="carousel-control-right " href="#carouselPI" data-slide="next"></a>
				    		</div>
		    	</div>
		    	<?php } 
				    	if ($programme->youtube>''){
				    		if (preg_match('/v=(.*)&/', $programme->youtube, $result)){
				    			$ytid = $result[1];
				    	?>
				    	<div class="widget-col-right widget-video">
				    		<iframe name="ytframe" width="277" height="175" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" allowfullscreen src="http://www.youtube.com/embed/<?php echo $ytid; ?>?modestbranding=1&showinfo=0&rel=0"></iframe>
				    	</div>
				    	<?php
				    		}
				    	}
				    	?>
		    	<div class="widget-col-right widget-plan">
		    		<iframe name="mapframe" width="277" height="175" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.fr/maps?f=q&amp;source=embed&amp;output=embed&amp;hl=fr&amp;geocode=&amp;q=<?php echo ($programme->geocoord); ?>&amp;aq=&amp;ie=UTF8&amp;t=m&amp;z=14&amp;<?php echo ($programme->geocoord); ?>"></iframe>
		    	</div>
	    	</div>
	    </div>
	</section><!-- end section homeContainer -->
	<div id="optionFormModal" class="modal hide fade" tabindex="-1" role="dialog">
    </div>
 
 