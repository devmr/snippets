<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//Pour récupérer les valeurs query_string
 $data = drupal_get_query_parameters(null, array());
if (arg(0) == 'node' && is_numeric(arg(1))) $nodeid = arg(1);

 $file = 'page_programmes.php';
if( isset($data['pid']) && $data['pid'] != '' ){
    $file = 'page_detail_programmes.php';
}
if( isset($data['page']) && $data['page'] == 'lot' ){
    $file = 'page_detail_programmes_lots.php';
}
if( isset($data['lid']) && $data['lid'] != '' ){
    $file = 'page_detail_lots.php';
}
if( isset($data['page']) && $data['page'] == 'docs' ){
    $file = 'page_detail_programmes_docs.php';
}
if( isset($data['page']) && $data['page'] == 'pdf'){
    $file = 'genpdf/generate.php';
}
if($nodeid == 16 || isset($data['op']) && $data['op'] == 'Chercher' ){
    $file = 'page_lots.php';
}
 
if( file_exists(DRUPAL_ROOT.'/'.drupal_get_path('theme',$GLOBALS['theme'] ).'/templates/extranet/'.$file ) ) require_once( drupal_get_path('theme',$GLOBALS['theme'] ).'/templates/extranet/'.$file );