<?php
$fieldorder = (htmlspecialchars($_COOKIE['fieldorder'])!=''?htmlspecialchars($_COOKIE['fieldorder']):'');
$order = (htmlspecialchars($_COOKIE['order'])!=''?htmlspecialchars($_COOKIE['order']):'');

if( $fieldorder != '' && !in_array($fieldorder,array('numlotplan','typelot','surfacecarrez','orientation','baseapptht','rentabilitebruteht','baseparkinght','basemobilierht','basetotalht','suivivente'))){
    $fieldorder = '';
}
if( $orderr != '' && !in_array($order,array('ASC','DESC'))){
    $order = '';
}

define('DRUPAL_ROOT', getcwd().'/../../../../../../../');
require_once DRUPAL_ROOT . 'includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL); 
$data = drupal_get_query_parameters(null, array()); 
if ( !defined('BASE_URL') )
define('BASE_URL', (base_path()!='/'?base_path():''));
define('TEMPLATEPATH', drupal_get_path('theme',$GLOBALS['theme'] ) );
$pid = $data['pid'];
$usetcpdf = false; 
$file = 'programmes.php';
if( isset($data['view']) && $data['view'] == 'single' ){
    $file = 'programme_detail.php';
    $usetcpdf = true;
}
if( isset($data['view']) && $data['view'] == 'lot' ){
    $file = 'page_detail_lots.php';
    $usetcpdf = true;
} 
include(dirname('__FILE__').'/templates/'.$file );
$content = ob_get_clean();
/*if( !user_is_logged_in() ){
    drupal_set_message(t('Vous devez &ecirc;tre connect&eacute; pour acc&eacute;der &agrave; cette page.'), 'status');
    $old_msg = drupal_get_messages(); 
    echo ( $old_msg['status'][0] );
    return;
}*/
// echo __LINE__;exit;
try
{
    if( !$usetcpdf ){
        require_once(dirname(__FILE__).'/html2pdf_v4.03/html2pdf.class.php');
        $html2pdf = new HTML2PDF('L', 'A4', 'fr');
        $html2pdf->pdf->SetDisplayMode('fullpage');
        $html2pdf->writeHTML($content);
        $html2pdf->Output('programme.pdf');
    }
    else{
         
        require_once(dirname(__FILE__).'/html2pdf_v4.03/_tcpdf_6_0_087/tcpdf.php');
        // create new PDF document
        $pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        // set margins
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);
        $pdf->SetMargins(2,2, 2);
        $pdf->SetAutoPageBreak(auto,0);
        $pdf->SetFont('helvetica', '', 10);
        $pdf->AddPage();
        $pdf->writeHTML($content, true, false, true, false, '');
        $pdf->Output('programme.pdf', 'I');
    }
}
catch(HTML2PDF_exception $e) {
    echo $e;
    exit;
}

function number_format2( $number ,  $decimals = 0 ,  $dec_point = '.' ,  $thousands_sep = ',' ){

	$retnum = str_replace(' ', '', $number);
	if (is_numeric($number)){
		if (strpos($number, '.') !== false) $decimals = 2;
		$retnum = number_format($retnum, $decimals, $dec_point, $thousands_sep);
                $retnum = str_replace(" ", "&nbsp;", $retnum);
	}
	return $retnum;
}
function sanitize_file_name( $filename ) {
	$filename_raw = $filename;
	$special_chars = array("?", "[", "]", "/", "\\", "=", "<", ">", ":", ";", ",", "'", "\"", "&", "$", "#", "*", "(", ")", "|", "~", "`", "!", "{", "}", chr(0));
	$filename = str_replace($special_chars, '', $filename);
	$filename = preg_replace('/[\s-]+/', '-', $filename);
	$filename = trim($filename, '.-_');

	return $filename;
}   
?>