<?php
/*
Template Name: documents programme
*/
if( !user_is_logged_in() ){
    drupal_set_message(t('Vous devez être connecté pour accéder à cette page.'), 'status');
    $old_msg = drupal_get_messages(); 
    echo ( $old_msg['status'][0] );
    return;
}
if ( !defined('BASE_URL') )
define('BASE_URL', (base_path()!='/'?base_path():''));
define('TEMPLATEPATH', drupal_get_path('theme',$GLOBALS['theme'] ) );

$pid = $data['pid'];
if ($pid>-1){
	 
	$sql="SELECT * FROM programmes WHERE actif=1 and id = :pid ";
        $results = db_query( $sql, array(':pid' => $pid ) );
        $programme = $results->fetchObject();
	/*$programmes = $wpdb->get_results($sql);
	$programme = $programmes[0];*/
	
} 

$prog_dir = '/wp-content/documents/'.sanitize_file_name($programme->nom);

$prog_root_dir = DRUPAL_ROOT.$prog_dir;

$doc_reservation = array();
 
if (is_dir($prog_root_dir.'/Documents-de-reservation/')){
	$d = dir($prog_root_dir.'/Documents-de-reservation/');
	while (false !== ($entry = $d->read())) {
		if (($entry==".")||($entry=="..")||(strpos(trim($entry),'.')==0)) continue;
	   	$doc_reservation[] = $entry;
	}
	$d->close();
	$resa_url = BASE_URL.$prog_dir.'/Documents-de-reservation/';
}
usort($doc_reservation, 'compare_filename');
 
$doc_information = array();
if (is_dir($prog_root_dir.'/Documents-information/')){
	$d = dir($prog_root_dir.'/Documents-information/');
	while (false !== ($entry = $d->read())) {
		if (($entry==".")||($entry=="..")||(strpos(trim($entry),'.')==0)) continue;
	   	$doc_information[] = $entry;
	}
	$d->close();
	$info_url = BASE_URL.$prog_dir.'/Documents-information/';
}
usort($doc_information, 'compare_filename');

$doc_marketing = array();
if (is_dir($prog_root_dir.'/Documents-marketing/')){
	$d = dir($prog_root_dir.'/Documents-marketing/');
	while (false !== ($entry = $d->read())) {
		if (($entry==".")||($entry=="..")||(strpos(trim($entry),'.')==0)) continue;
	   	$doc_marketing[] = $entry;
	}
	$d->close();
	$market_url = BASE_URL.$prog_dir.'/Documents-marketing/';
}
usort($doc_marketing, 'compare_filename');

?>
<div class="clearfix">
<div class="btnGreen1">
    <a href="programme?pid=<?php echo $pid; ?>" >Description</a>
</div>
<div class="btnGreen1">
    <a href="programme?page=lot&pid=<?php echo $pid; ?>" >Lots/Stocks</a>
</div>
<div class="btnGreen1">
    <a href="programme?page=docs&pid=<?php echo $pid; ?>" >Documents à télécharger</a>
</div>
<div class="btnGreen1">
    <a href="sites/all/themes/terresens/templates/extranet/genpdf/generatezip.php?pid=<?php echo $pid; ?>" >Télécharger tous les documents</a>
</div>
</div>
			<section id="mainContainer" class="clearfix">
			     
			    <div class="clearfix">
			    <div class="tableDataTable_wrapper">
			    	<table class="dataTable" id="doctable">	
			    	<thead>	    		
			    		<tr>
			    			<th>Documents de réservation</th>
			    			<th>Documents d'information</th>
			    			<th>Documents marketing</th>
			    		</tr>
			    	</thead>
			    	<tbody>
			    		<tr>
			    			<td>
								<?php if (sizeof($doc_reservation)==0){ ?>
									&nbsp;&nbsp;Aucun document dans cette section</br>
								<?php }else{?>
								<ul>
									<?php foreach ($doc_reservation as $doc) { ?>
									<li><a href="<?php echo $resa_url.$doc; ?>" target="_blank"><?php echo $doc; ?></a></li>
									<?php } ?>
								</ul>
								<?php }?>
			    			</td>
			    			<td>
								<?php if (sizeof($doc_reservation)==0){ ?>
									&nbsp;&nbsp;Aucun document dans cette section</br>
								<?php }else{?>
								<ul>
									<?php foreach ($doc_information as $doc) { ?>
									<li><a href="<?php echo $info_url.$doc; ?>" target="_blank"><?php echo $doc; ?></a></li>
									<?php } ?>
								</ul>
								<?php }?>
			    			</td>
			    			<td>
								<?php if (sizeof($doc_marketing)==0){ ?>
									&nbsp;&nbsp;Aucun document dans cette section</br>
								<?php }else{?>
								<ul>
									<?php foreach ($doc_marketing as $doc) { ?>
									<li><a href="<?php echo $market_url.$doc; ?>" target="_blank"><?php echo $doc; ?></a></li>
									<?php } ?>
								</ul>
								<?php }?>
			    			</td>
			    		</tr>
			    	</tbody>
			    	</table>
			    </div>
			    </div>
			</section><!-- end section homeContainer -->
 