<?php
define('DRUPAL_ROOT', getcwd().'/../../../../../../../');
require_once DRUPAL_ROOT . 'includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL); 
$data = drupal_get_query_parameters(null, array()); 
if ( !defined('BASE_URL') )
define('BASE_URL', (base_path()!='/'?base_path():''));
define('TEMPLATEPATH', drupal_get_path('theme',$GLOBALS['theme'] ) );
$lid = $data['lid'];
$pid = $data['pid'];
 
if ($lid>-1){
	$sql="SELECT l.*, date_add(optionne_le, interval 5 day) as date_validite
	 		FROM lots l
	 		WHERE l.id = :lid";
        $lot = db_query( $sql, array(':lid' => $lid ) )->fetchObject();
	/*$lots = $wpdb->get_results($sql);
	$lot = $lots[0];*/

	$sql= "SELECT p.*, g.email as gest_email, g.tel as gest_tel
	 		FROM programmes p LEFT JOIN gestionnaires g ON p.gestionnaireid = g.id
	 		WHERE p.actif=1 and p.id = :pid";
	 		//WHERE p.actif=1 and p.id = '".$lot->programmeid."'";

	//$programmes = $wpdb->get_results($sql);
	$programme = db_query( $sql, array(':pid' => $lot->programmeid ) )->fetchObject();
}
$zipname =  $pid.'.rtf';
 //Supprimer d'abord si rtf existant
unlink(dirname(__FILE__).'/tmp/'.$zipname); 
//Creer le fichier
$filename = DRUPAL_ROOT.'sites/default/files/docenseigne/'.sanitize_file_name($programme->nom).'/Documents-de-reservation/contrat.rtf';

 
if(file_exists($filename)){
    $rtf = file_get_contents($filename);
    //Remplacer les variables
    $rtf = str_replace('#adresse#',utf8_decode( $programme->adresse ),$rtf);
    $rtf = str_replace('#appartement_parking_ht#',number_format2($lot->baseapptparkinght,0,',',' '),$rtf);
    $rtf = str_replace('#appartement_parking_ttc#',number_format2($lot->baseapptparkingttc,0,',',' '),$rtf);
    $rtf = str_replace('#appartement_ht#',number_format2($lot->baseapptht,0,',',' '),$rtf);
    $rtf = str_replace('#base_loyer_total_ht_annuel#',number_format2($lot->baseloyertotalhtannuel,0,',',' '),$rtf);
    $rtf = str_replace('#mobilier_ht#',number_format2($lot->basemobilierht,0,',',' '),$rtf);
    $rtf = str_replace('#mobilier_ttc#',number_format2($lot->basemobilierttc,0,',',' '),$rtf);
    $rtf = str_replace('#parking_ht#',number_format2($lot->baseparkinght,0,',',' '),$rtf);
    $rtf = str_replace('#total_ht#',number_format2($lot->basetotalht,0,',',' '),$rtf);
    $rtf = str_replace('#livraison#',$programme->datelivraison,$rtf);
    $rtf = str_replace('#etage#',$lot->etage,$rtf);
    $rtf = str_replace('#nom_programme#',  utf8_decode( $programme->nom ),$rtf);
    $rtf = str_replace('#lot_n#',$lot->numlotplan,$rtf);
    $rtf = str_replace('#shab#',$lot->shab,$rtf);
    $rtf = str_replace('#surface_carrez#',$lot->surfacecarrez,$rtf);
    $rtf = str_replace('#type#',$lot->typelot,$rtf);
    $rtf = str_replace('#ville#',utf8_decode( $programme->ville ).' ---- '.$filename,$rtf);
    
    $fp = fopen(dirname(__FILE__).'/tmp/'.$zipname,'wb');
    fwrite($fp, $rtf);
    fclose($fp);
}
else{
    echo 'le document '.$filename.' n\'existe pas.';exit;
}



  
//echo dirname(__FILE__).'/'.$zipname;
 
if (file_exists(dirname(__FILE__).'/tmp/'.$zipname)) {
  // Serve file download.
  drupal_add_http_header('Pragma', 'public');
  drupal_add_http_header('Expires', '0');
  drupal_add_http_header('Cache-Control', 'must-revalidate, post-check=0, pre-check=0');
  drupal_add_http_header('Content-Type', 'application/force-download');
  drupal_add_http_header('Content-Disposition', 'attachment; filename=' . sanitize_file_name($programme->nom) . '.rtf;');
  drupal_add_http_header('Content-Transfer-Encoding', 'binary');
  drupal_add_http_header('Content-Length', filesize(dirname(__FILE__).'/tmp/'.$zipname));
  readfile(dirname(__FILE__).'/tmp/'.$zipname);
  unlink(dirname(__FILE__).'/tmp/'.$zipname);
  drupal_exit();
}
else{
    echo 'Téléchargement impossible. Veuillez vérifier que le dossier '.dirname(__FILE__).' est accessible en écriture par le serveur.';
}

 
function sanitize_file_name( $filename ) {
	$filename_raw = $filename;
	$special_chars = array("?", "[", "]", "/", "\\", "=", "<", ">", ":", ";", ",", "'", "\"", "&", "$", "#", "*", "(", ")", "|", "~", "`", "!", "{", "}", chr(0));
	$filename = str_replace($special_chars, '', $filename);
	$filename = preg_replace('/[\s-]+/', '-', $filename);
	$filename = trim($filename, '.-_');

	return $filename;
}
function number_format2( $number ,  $decimals = 0 ,  $dec_point = '.' ,  $thousands_sep = ',' ){

	$retnum = str_replace(' ', '', $number);
	if (is_numeric($number)){
		if (strpos($number, '.') !== false) $decimals = 2;
		$retnum = number_format($retnum, $decimals, $dec_point, $thousands_sep);
                $retnum = str_replace(" ", " ", $retnum);
	}
	return $retnum;
}