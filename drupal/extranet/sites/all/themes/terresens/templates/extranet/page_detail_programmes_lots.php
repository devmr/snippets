<?php
/*
Template Name: liste lots programme
*/
if( !user_is_logged_in() ){
    drupal_set_message(t('Vous devez être connecté pour accéder à cette page.'), 'status');
    $old_msg = drupal_get_messages(); 
    echo ( $old_msg['status'][0] );
    return;
}
if ( !defined('BASE_URL') )
define('BASE_URL', (base_path()!='/'?base_path():''));
define('TEMPLATEPATH', drupal_get_path('theme',$GLOBALS['theme'] ) );

$pid = $data['pid'];
if ($pid>-1){
	 
	$sql="SELECT *, date_add(optionne_le, interval 5 day) as date_validite 
			FROM lots 
			WHERE programmeid = :pid  AND mandat = 'IMMOE' AND (allotement='' or allotement IS NULL) AND suivivente in ('Libre','Optionne','Optionné')";
        $results = db_query( $sql, array(':pid' => $pid ) );
        $lots = $results->fetchAll();
	//$lots = $wpdb->get_results($sql);
        
         

	$sql="SELECT pctrentabilitebruteht, dateactabilite, nom, ville
	 		FROM programmes WHERE actif=1 and id = :pid";
	$results = db_query( $sql, array(':pid' => $pid ) );
        $programme = $results->fetchObject();
        /*$programmes = $wpdb->get_results($sql);
	$programme = $programmes[0];*/
}
global $user;
$uid = $user->uid;
$sql = "SELECT programme,stock FROM incweb_programme_user WHERE user = :user";
$programme = db_query( $sql, array(':user' => $user->uid ) )->fetchAssoc();

$tab_value = $tab_stvalue =array();
if( $programme ) $tab_value = explode(',',$programme['programme']);
if( $programme ) $tab_stvalue = explode(',',$programme['stock']);

if( !in_array('none',$tab_stvalue ) || in_array($pid,$tab_stvalue ) ) :
    echo '<p class="text-center">Accès non autorisé.</p>';
    
else :



?>
<script type="text/javascript">
    var iDisplayLength = 100;
</script>
<div class="clearfix">
<div class="btnGreen1">
    <a href="programme?pid=<?php echo $pid; ?>" >Description</a>
</div>
<div class="btnGreen1">
    <a href="programme?page=lot&pid=<?php echo $pid; ?>" >Lots/Stocks</a>
</div>
<div class="btnGreen1">
    <a href="programme?page=docs&pid=<?php echo $pid; ?>" >Documents à télécharger</a>
</div>
 <div class="btnGreen1">
    <a target="_blank" href="sites/all/themes/terresens/templates/extranet/genpdf/generate.php?pid=<?php echo $pid; ?>" id="btn_dlgs">Télécharger la grille du stock</a>
</div>   
</div>
			<section id="mainContainer" class="clearfix">
			     
			    <div class="tableDataTable_wrapper">
			    	<h4><?php echo $programme->nom .($programme->ville > '' ? ' - '.$programme->ville :'');?>
				    </h4>
				    <br/>
				    <p>Cliquer sur une ligne pour accéder à la fiche descriptive du lot</p>
				    <table class="dataTable" id="resultRecherche">
				    	<thead>
							<tr>
								<th data-fieldorder="numlotplan">N° Lot</th>
								<th data-fieldorder="typelot">Type</th>
								<th data-fieldorder="surfacecarrez"><?php echo $lots[0]->surfacecarrez>''?'Surface<br/>CARREZ':'SHAB m²'; ?></th>
								<th class="text-center" data-fieldorder="orientation"><img src="<?php echo TEMPLATEPATH; ?>/img/boussole.png" alt="orientation" width="20" height="20"></th>
								<th data-fieldorder="baseapptht">Prix HT</th>
								<th data-fieldorder="rentabilitebruteht">Renta</th>
								<th data-fieldorder="baseparkinght">Prix Pk HT</th>
								<th data-fieldorder="basemobilierht">Prix mobilier HT</th>
								<th data-fieldorder="basetotalht">Prix total HT</th>
								<th data-fieldorder="suivivente">Statut du lot</th>
								<th >Validité option</th>
							</tr>
						</thead>
						<tbody>
							<?php
							foreach ($lots as $lot)
			                {
                                                             
			                ?>
							<tr data-href="programme?lid=<?php echo $lot->id?>&pid=<?php echo $pid; ?>">
								<td><?php echo $lot->numlotplan;?></td>
								<td><?php echo $lot->typelot;?></td>
								<td><?php echo $lot->surfacecarrez>''?$lot->surfacecarrez:$lot->shab; ?></td>
								<td><?php echo $lot->orientation;?></td>
								<td nowrap  class="text-center"><?php echo $lot->baseapptht/*number_format2($lot->baseapptht,0,',',' ')*/;?></td>
								<td><?php echo $lot->rentabilitebruteht;?></td>
								<td nowrap class="text-center"><?php echo $lot->baseparkinght;/*number_format2($lot->baseparkinght,0,',',' ')*/;?></td>
								<td nowrap class="text-center"><?php echo $lot->basemobilierht;/*number_format2($lot->basemobilierht,0,',',' ')*/;?></td>
								<td nowrap class="text-center"><?php echo $lot->basetotalht;/*number_format2($lot->basetotalht,0,',',' ');*/?></td>
								<td><?php echo $lot->suivivente ?></td>
								<td><?php
                                                                $d = array();
                                                                $d = explode('|',$lot->id.'|'.$lot->date_validite.'|'.$lot->demande_option_le);
                                                                $id = $d[0];
                                                                $opt = $d[1];
                                                                $dem = $d[2];
                                                                
                                                                if ($opt>''){
								// si date d'option on affiche la date
								echo $opt;
                                                                }elseif ($dem>'0000-00-00'){
								echo 'Demande en cours...';
                                                                }else{
                                                                    echo '<a href="incweb?lid='.$id.'&pid='.$pid.'" class="fancybox.iframe fancy" id='.$id.'>Optionner</a>';
                                                                }
                                                                
                                                                ?></td>
							</tr>
							<?php
							}
							?>
						</tbody>
				    </table>
			     </div>
			</section><!-- end section homeContainer -->
	<div id="optionFormModal" class="modal hide fade" tabindex="-1" role="dialog">
        
    </div>
<?php endif; ?>                        
 
 