<?php
/*
Template Name: recherche lots
*/
 if( !user_is_logged_in() ){
    drupal_set_message(t('Vous devez être connecté pour accéder à cette page.'), 'status');
    $old_msg = drupal_get_messages(); 
    echo ( $old_msg['status'][0] );
    return;
}
// recupération et sanitization des données
$selresidence = $data['residence'];
$minprice = $data['prix_mini'];
$maxprice = $data['prix_maxi'];
$livreactable = $data['programme'];
$typeslot = isset($data['type'])?$data['type']:array();

//print_r($typeslot);

// recherche des lots correspondants
$query ="SELECT l.id, l.numlotplan, l.typelot, l.baseapptht, l.programme, p.ville, p.categorie, l.programmeid, p.dateactabilite, p.datelivraison
			FROM lots l
			INNER JOIN programmes p ON p.id = l.programmeid
			WHERE p.actif = 1 AND l.mandat = 'IMMOE' AND (l.allotement='' or l.allotement IS NULL) AND l.suivivente in ('Libre','Optionne','Optionné')";
$query .= $selresidence!='0' && $selresidence!='' ?" AND p.categorie ='".$selresidence."'":'';
if (sizeof($typeslot)>0){
        $clausetype = array();
	
	foreach ($typeslot as $typelot => $value) {
		if( $value != '0' ) $clausetype[] = "typelot like '".$value."%'";
	}
	if(count($clausetype)>0){
            $query .= ' AND (';
            $query .= implode(' OR ', $clausetype).')';
        }
}
$query .= $minprice>''?" AND CAST(l.baseapptht as UNSIGNED) >='".$minprice."'":'';
$query .= $maxprice>''?" AND CAST(l.baseapptht as UNSIGNED) <='".$maxprice."'":'';
$query .= $livreactable=='livre'?" AND p.datelivraison = 'LIVREE'":'';
$query .= $livreactable=='actable'?" AND p.dateactabilite = 'ACTABLE'":'';
//echo $query;
$lots = db_query( $query, array( ) )->fetchAll();
 

// liste des types de résidence existants
$query = 'SELECT DISTINCT(categorie) 
			FROM programmes
			WHERE actif = 1
			order by categorie asc';
$residences = db_query( $query, array( ) )->fetchAll();


?>
 
			<section id="mainContainer" class="clearfix">
			    <header class="search-result">
			    	<form action="" class="clearfix" method="get">
			    		<div class="clearfix" id="quicksearch">
                                            <div class="pull-left" id="field-programme">
			    			<span class="control-label">Programme</span>
			    			<label class="radio">
								<input type="radio" name="programme" value="all" <?php echo $livreactable=='all'?'checked':''; ?>>
								Tous
							</label>
			    			<label class="radio">
								<input type="radio" name="programme" value="livre" <?php echo $livreactable=='livre'?'checked':''; ?>>
								Livré
							</label>
							<label class="radio">
								<input type="radio" name="programme" value="actable" <?php echo $livreactable=='actable'?'checked':''; ?>>
								Actable
							</label>
			    		</div>
			    		<div class="pull-left" id="field-residence">
			    			<label class="control-label" for="inputResidence">Résidence</label>
			    			<select id="inputResidence" name="residence">
			    				<option value="0">Toutes</option>
								<?php
								foreach ($residences as $residence) {
									echo '<option value="'.addslashes($residence->categorie).'" '. ($selresidence==$residence->categorie?'selected':'') .'>'.$residence->categorie.'</option>';
								}
								?>
							</select>
			    		</div>
			    		<div class="pull-left" id="field-type">
			    			<label class="control-label" for="inputType">Type</label>
			    			<label class="radio">
								<input type='checkbox' id="inputTypeT1" name="type[T1]" tabindex="4" <?php echo isset($typeslot['T1']) && $typeslot['T1'] == 'T1'?'checked':'' ;?>>T1
							</label>
							<label class="radio">
								<input type='checkbox' id="inputTypeT2" name="type[T2]" tabindex="5" <?php echo isset($typeslot['T2']) && $typeslot['T2'] == 'T2'?'checked':'' ;?>>T2 
							</label>
							<label class="radio">
								<input type='checkbox' id="inputTypeT3" name="type[T3]" tabindex="6" <?php echo isset($typeslot['T3']) && $typeslot['T3'] == 'T3'?'checked':'' ;?>>T3 
							</label>
						</div>
						<div class="pull-left" id="field-type2">
							<label class="control-label" for="inputType">&nbsp;</label>
							<label class="radio">
								<input type='checkbox' id="inputTypeT4" name="type[T4]" tabindex="7" <?php echo isset($typeslot['T4']) && $typeslot['T4'] == 'T4'?'checked':'' ;?>>T4 
							</label>
							<label class="radio">
								<input type='checkbox' id="inputTypeT5" name="type[T5]" tabindex="8" <?php echo isset($typeslot['T5']) && $typeslot['T5'] == 'T5'?'checked':'' ;?>>T5 
			    			</label>
			    		</div>
			    		<div class="pull-left" id="field-prixmini">			    			
			    			<label class="control-label" for="inputMinPrice">Prix minimum</label>
			    			<div class="input-append clearfix">
			    				<input type="text" tabindex="9" id="inputMinPrice" name="prix_mini" placeholder="Saisie libre" value="<?php echo $minprice; ?>">
			    				<span class="add-on">€</span>
			    			</div>
			    		</div>
			    		<div class="pull-left last" id="field-prixmaxi">
			    			<label class="control-label" for="inputMaxPrice">Prix maximum</label>
			    			<div class="input-append clearfix">
			    				<input type="text" tabindex="10" id="inputMaxPrice" name="prix_maxi" placeholder="Saisie libre" value="<?php echo $maxprice; ?>">
			    				<span class="add-on">€</span>
			    			</div>
                                                

			    		</div>
                                            
                                </div>
                                    <div class="clearfix">
                                        <p class="pull-right"><input type="submit" tabindex="7" name="op" value="Chercher"  /></p>
                                    </div>
                                    
			    		<div class="clearBoth">
				    		<p class="text-center">Résultat de votre recherche : <?php echo sizeof($lots);?> lots sous réserve de disponibilité en stock</p>
				    		
			    		</div>
			    	</form>
			    </header>
			    <div class="tableDataTable_wrapper">
				    <p class="text-center">Cliquer sur une ligne pour accéder à la fiche descriptive du lot</p>
				    <table class="dataTable" id="resultRecherche">
				    	<thead>
							<tr>
								<th>Statut</th>
								<th>Type</th>
								<th>Prix H.T.</th>
								<th>N° Lot</th>
								<th>Programme</th>
								<th>Ville</th>
								<th>Résidence</th>
							</tr>
						</thead>
						<tbody>
							<?php
							foreach ($lots as $lot)
			                {
			                	$statut = $lot->datelivraison=='LIVREE'?'Livré':($lot->dateactabilite=='ACTABLE'?'Actable':'');
			                ?>
							<tr data-href="programme?lid=<?php echo $lot->id; ?>&pid=<?php echo $lot->programmeid; ?>">
								<td><?php echo $statut; ?></td>
								<td><?php echo $lot->typelot; ?></td>
								<td><?php echo number_format($lot->baseapptht,0,',',' '); ?></td>
								<td><?php echo $lot->numlotplan; ?></td>
								<td><?php echo $lot->programme; ?></td>
								<td><?php echo $lot->ville; ?></td>
								<td><?php echo $lot->categorie; ?></td>
							</tr>
							<?php
							}
							?>
						</tbody>
				    </table>
			     </div>
			</section><!-- end section homeContainer -->
    <script>		
		$(document).ready(function() {
			terresens.global_js_init();
			terresens.dataTable.init_recherchelots();	
		});		
	</script> 