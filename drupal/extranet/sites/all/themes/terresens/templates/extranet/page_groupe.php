<?php
/*
Template Name: le groupe
*/
?>
<?php get_header(); ?>
			<section id="mainContainer" class="clearfix">
                <div id="col-left" class="pull-left">
				   <div class="editoWrapper">
				   		<h1 class="vert">Présentation du groupe Terresens</h1>
				   		<h2 class="vert">Terrésens , opérateur global en immobilier durable et responsable</h2>
				   		<p>Holding du groupe, Terrésens opère dans l’immobilier géré, de manière transversale, du promoteur au gestionnaire, pour la réalisation de résidences de tourisme, résidences d’affaires, résidences étudiantes et résidences séniors.</p>
						<p>Prospection foncière, ingénierie verte, conception, étude de marché, obtention de permis de construire, financement et choix des partenaires, commercialisation, gestion et exploitation : nos équipes interviennent à chaque étape de la réalisation d’un programme.</p>
						<p>Expert de l’investissement immobilier locatif dans le domaine des résidences services, Terrésens propose, au-delà du conseil immobilier, des solutions performantes en matière de défiscalisation et d’investissement patrimonial.</p>
				   		
				   		<h2 class="vert">Le développement durable appliqué aux résidences services</h2>
				   		<p>
				   			Notre maîtrise d’œuvre globale et notre véritable expertise en matière de qualité environnementale nous permettent de relever un double défi :
				   			valoriser le patrimoine de nos clients tout en préservant les intérêts et le bien-être des générations futures.<br>
				   			Ainsi, Terrésens a développé une démarche de développement durable qui permet d’inscrire ses résidences services, dans une approche environnementale porteuse de sens.
				   		</p>
				   		<p>
				   			Un engagement fort, radicalement audacieux par rapport à l’offre classique, mais aussi et surtout un acte citoyen pour tous les investisseurs qui veulent se constituer un patrimoine haut de gamme sans se désintéresser de l’avenir de la planète.</p>
				   		<h2 class="vert">Un investissement locatif qui correspond à un nouvel art de vivre, pragmatique et responsable.</h2>
				   		<p class="text-center">
				   			<img src="<?php echo get_template_directory_uri(); ?>/media/chiffre-cle.png" alt="chiffre-cle" width="491" height="97">
				   		</p>
				   </div>
			    </div>
			    <div id="col-right" class="pull-right">
			    	<div class="graphiques text-center">
			    		<img src="<?php echo get_template_directory_uri(); ?>/media/graphique1et2.jpg" alt="graphique1et2" width="282">
			    		<a href="<?php echo get_template_directory_uri(); ?>/media/ORGANIGRAMME-CRM.pdf" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/media/BTN-ORGANIGRAMME.jpg" alt="organigramme" width="282"></a>
			    	</div>
			    </div>
			</section><!-- end section homeContainer -->
    <script>		
		$(document).ready(function() {
			terresens.global_js_init();
		});		
	</script>
<?php get_footer(); ?>