<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
  "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml"   xml:lang="<?php print $language->language; ?>" version="XHTML+RDFa 1.0" dir="<?php print $language->dir; ?>"<?php print $rdf_namespaces; ?>>


<head profile="<?php print $grddl_profile; ?>">
  <base href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/" /> 
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>
  <meta name="viewport" content="initial-scale=1, maximum-scale=1">
  <?php print $styles; ?>
  <?php print $scripts; ?>
<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
</head>
<body class="<?php print $classes; ?>" <?php print $attributes;?>>
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable"><?php print t('Skip to main content'); ?></a>
  </div>
    <!-- top -->
  <?php print $page_top; ?>
    <!-- end top -->
    <!-- page -->
  <?php print $page; ?>
    <!-- end page -->
    <!-- bottom -->
  <?php print $page_bottom; ?>
    <!-- end bottom -->
     <script type="text/javascript">
 		 window.___gcfg = {lang: 'fr'};
 
  	(function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/plusone.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  		})();
	</script>
    
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-46938951-1', 'terresens.com');
  ga('send', 'pageview');
</script></body></html>