<?php
//print_r(get_defined_vars());
?> 
<div id="block-user-login" class="block block-user">

    <p>Cet espace vous permet de consulter nos programmes en temps réel et de bénéficier d’offres exclusives</p>

    <form action="<?php print $elements['#action']; ?>" method="post" id="user-login-form" accept-charset="UTF-8"><div><div class="form-item form-type-textfield form-item-name">
  <label for="edit-name">Nom d'utilisateur <span class="form-required" title="Ce champ est requis.">*</span></label>
 <input type="text" id="edit-name" name="name" value="" size="15" maxlength="60" class="form-text required" />
</div>
<div class="form-item form-type-password form-item-pass">
  <label for="edit-pass">Mot de passe <span class="form-required" title="Ce champ est requis.">*</span></label>
 <input type="password" id="edit-pass" name="pass" size="15" maxlength="128" class="form-text required" />
</div>
<div class="item-list">
    <ul>
        <li class="first"><a class="fancybox.iframe fancy" href="user/password?popin=1" title="Demander un nouveau mot de passe par courriel.">Mot de passe oublié ?</a></li>
        <li class="last"><a class="fancybox.iframe fancy" href="user_demandeacces?popin=1" title="Créer un nouveau compte utilisateur.">Je demande un accès partenaires</a></li>
    </ul>
</div>
            
             <input type="hidden" value="<?php print $elements['form_build_id']['#value']; ?>" name="form_build_id">
             
<input type="hidden" name="form_id" value="user_login_block" />
<div class="form-actions form-wrapper" id="edit-actions"><input type="submit" id="edit-submit" name="op" value="Se connecter" class="form-submit" /></div></div></form>
</div>
