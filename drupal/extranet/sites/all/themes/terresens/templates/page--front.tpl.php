
<div id="page-wrapper">
    
    <div id="page">

  <div id="header" class="<?php print $secondary_menu ? 'with-secondary-menu': 'without-secondary-menu'; ?>">
      
      <div class="section clearfix">



    <?php if ($logo): ?>
    <div id="logo">
      <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="terresenselogo">
        <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
      </a>
        
        <?php
  if (user_is_logged_in() == TRUE) {
    global $user;
$user_fields = user_load($user->uid);
 
//print_r( $user );
//Prendre le role

$prenom_nom =  $user->name;

if( isset( $user_fields->field_prenom['und'][0]['value'] ) ){ 
    $prenom_nom = $user_fields->field_prenom['und'][0]['value'];
    if( isset( $user_fields->field_nom['und'][0]['value'] ) ){
        if( isset( $user_fields->field_prenom['und'][0]['value'] ) ) $prenom_nom .= ' ';
        $prenom_nom .= $user_fields->field_nom['und'][0]['value'];
    }
}

 

     print '<div class="infouser"><p>Bonjour ' . l($prenom_nom,'user').'<br />&raquo; '.l(t("Déconnexion"), 'user/logout').'</p></div>';
      
 
}

?>
    </div>
    <?php endif; ?>	


          <?php if ($page['topmenu']): ?>
            <?php print render($page['topmenu']); ?>
          
          <?php endif; ?>

<?php if ($page['header']): ?>
    <?php print render($page['header']); ?>
<?php endif; ?>	
     
     
      
    

  </div></div> <!-- /.section, /#header -->

  

  <div id="main-wrapper" class="clearfix">
      <div id="main" class="clearfix">


    


    

    <div id="content" class="column frontpage">
   
      <a id="main-content"></a>
      
      
     
      <div class="front-content clearfix row"  >
  <?php  //print render($page['content']); ?>
        <div id="sidebar-first" class="column sidebar"><div class="section">
                
 <?php if ($page['sidebar_first']): ?>
       
        <?php print render($page['sidebar_first']); ?>
      
    <?php endif; ?>
      </div></div> <!-- /.section, /#sidebar-first -->
      
      <div id="sidebar-second" class="column sidebar">
          <div class="section">
      <?php if ($page['sidebar_second']): ?>
        <?php print render($page['sidebar_second']); ?>
      <?php endif; ?>
              
              </div></div> <!-- /.section, /#sidebar-second -->
              
      </div>
    </div> <!-- /.section, /#content -->
    
    

    

  </div></div> <!-- /#main, /#main-wrapper -->
 
  
  
  
  

  <div id="footer-wrapper">
      <div class="section">

          <div id="footer-columns" class="clearfix">
              <div id="footer-columns-inner">
                  <div class="cola">
                       <?php if ($page['triptych_first']): ?>
                        <?php print render($page['triptych_first']); ?>
                      <?php endif; ?>
                  </div>
                  <div class="colb">
                       <?php if ($page['triptych_middle']): ?>
                        <?php print render($page['triptych_middle']); ?>
                      <?php endif; ?>
                  </div>
                  <div class="colc">
                       <?php if ($page['triptych_last']): ?>
                        <?php print render($page['triptych_last']); ?>
                      <?php endif; ?>
                  </div>
                  
                  
              </div>
          </div> <!-- /#footer-columns -->

          <div id="footer" class="clearfix">
              <div class="region region-footer">
                  <div id="block-block-1" class="block block-block">



                      <div id="presse-legal-social">
                          <div class="legal">
                              <?php if ($page['footer_firstcolumn']): ?>
                                <?php print render($page['footer_firstcolumn']); ?>
                              <?php endif; ?>
                          </div>
                          <div class="social">
                              <div class="icons">
                                  <a href="http://fr.viadeo.com/fr/profile/groupe.terresens" target="_blank">
                                      <img alt="" src="<?php echo drupal_get_path('theme',$GLOBALS['theme'] ); ?>/images/icon-viadeo.png" style="width: 21px; height: 21px; margin: 12px 5px 12px 48px; float: left;" />
                                  </a>
                                  <a href="https://plus.google.com/103622127010127912305/posts" target="_blank">
                                      <img alt="" src="<?php echo drupal_get_path('theme',$GLOBALS['theme'] ); ?>/images/icon-gplus.png" style="width: 21px; height: 21px; margin: 12px 5px; float: right;" />
                                  </a>
                                  <a href="https://twitter.com/terresensinvest" target="_blank">
                                      <img alt="" src="<?php echo drupal_get_path('theme',$GLOBALS['theme'] ); ?>/images/icon-twi.png" style="width: 21px; height: 21px; margin: 12px 5px; float: right;" />
                                  </a>
                                  <a href="https://www.facebook.com/Groupe.Terresens" target="_blank">
                                      <img alt="" src="<?php echo drupal_get_path('theme',$GLOBALS['theme'] ); ?>/images/icon-fbk.png" style="width: 21px; height: 21px; margin: 12px 5px; float: right;" />
                                  </a>
                              </div>
                              <div class="gratuit">
                                  <img alt="" src="<?php echo drupal_get_path('theme',$GLOBALS['theme'] ); ?>/images/gratuit-small-bottom_0.png" style="width: 205px; height: 40px; margin-top: 12px; margin-bottom: 12px;" />
                              </div>
                          </div>
                      </div>
                      <div class="copyright" >
                          <?php if ($page['footer_thirdcolumn']): ?>
                            <?php print render($page['footer_thirdcolumn']); ?>
                          <?php endif; ?>
                      </div>
                  </div>
              </div>
          </div> <!-- /#footer -->

      </div>
  </div>

</div>

</div> <!-- /#page, /#page-wrapper -->
