<div style="width:720px; margin-left:115px;padding: 30px 0px;">
<div  id="proimm">
<div id="node-<?php print $node->nid; ?>" class="proimmleft <?php print $classes; ?> clearfix"<?php print $attributes; ?>> <?php print render($title_prefix); ?>
  <?php if ($title): ?>
    <h2 class="pibreadtitle">Programmes immobiliers écologiques - <?php print $title; ?></h2>
    <h5 class="pibread"><a href="http://vps13494.ovh.net/clients/terresens/site/programme-immobilier">Programmes immobiliers</a> > Résidence :<?php print render(field_view_field('node', $node, 'field_category'));?> > <?php print $title; ?></h5>
  <?php endif; ?>
  
  </div>
    
</div>
<div style="width:630px; display:block;">
  <div id="conleft">    <?php
      //print render($content);
	  /*echo "<pre>";
   print_r($content);
echo "</pre>";die(); die;*/
 ?>


    

    <div id="immoref">
<div style="float:left; padding-right:15px; width:270px;"> <?php print render($content['field_main_image']);?>   </div>
<?php print render($content['body']); ?> 
  <?php if ($field_issue_brochure): ?>

<?php 
    render(field_view_field('node', $node, 'field_issue_brochure', array(
        'label'=>'hidden', 
        'type' => 'text', 
        'settings'=>array('trim_length' => 255),
    )));
?>

<div class="btnGreen1" ><a href="<?php print render($content['field_issue_brochure']);?>" target="_blank">Consulter la brochure en ligne </a></div>
  <?php endif; ?>
</br>
<?php //print render(field_view_field('node', $node, 'field_pdf'));?>
<?php foreach($node->field_pdf['und'] as $id =>$trm){
	$url = $trm['uri'];
    $url = file_create_url($url);?>
<div class="btnGreen1" ><a href="<?php print($url);?>" >Consulter la fiche technique</a></div>
<?php }
	?>
  </div>  
<br />

<div class="clearfix"></div>
<div class="tabber">

     <div class="tabbertab">
	  <h2>Présentation</h2>
<?php print render(field_view_field('node', $node, 'field_presentation'));?>
     </div>


     <div class="tabbertab">
	  <h2>Fiche technique</h2>
     	
	<?php print render(field_view_field('node', $node, 'field_fiche_technique'));?>
     </div>


     <div class="tabbertab">
	  <h2>Prestations</h2>
		  <?php print render(field_view_field('node', $node, 'field_prestations'));?>   
     </div>
     
     
      <div class="tabbertab">    
<h2>Simuler votre achat </h2>    
 <?php print render(field_view_field('node', $node, 'field_simulervotreachat'));?>   

      </div>    

</div>

 
 
   
  </div>
  
  <div class="clearfix" style="width:600px;"></div>
</div>

  </div>   


