 <?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012  <>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

require_once(PATH_tslib . 'class.tslib_pibase.php');
require_once(t3lib_extMgm::extPath("tsara").'lib/class.tx_tsara_util.php');

/**
 * Plugin 'tsara' for the 'tsar' extension.
 *
 * @author     <>
 * @package    TYPO3
 * @subpackage    tx_tsar
 */
class tx_tsara_pi2 extends tslib_pibase {
    public $prefixId      = 'tx_tsara_pi2';        // Same as class name
    public $scriptRelPath = 'pi2/class.tx_tsara_pi2.php';    // Path to this script relative to the extension dir.
    public $extKey        = 'tsara';    // The extension key.
    public $pi_checkCHash = TRUE;
    
    /**
     * The main method of the Plugin.
     *
     * @param string $content The Plugin content
     * @param array $conf The Plugin configuration
     * @return string The content that is displayed on the website
     */
    public function main($content, array $conf) {
        $this->conf = $conf;
        $this->pi_setPiVarDefaults();
        
         // Init FlexForm configuration for plugin:
        $this->pi_initPIflexForm();
        $this->preInit();
        $this->mergeflexFormValuesIntoConf();         
        $this->pi_loadLL();
        
         
        
        $this->util = t3lib_div::makeInstance('tx_tsara_util');
        $this->confArr = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['tsara']);
        
        $tabuidf = $row = array();
        $varttnews = t3lib_div::_GP('tx_ttnews');
        
        //if( !is_array( $varttnews ) ) return $this->cObj->stdWrap( $this->pi_getLL( 'nonews' ) , $this->conf['nonews_stdWrap.'] );
        
        $uid = $varttnews['tt_news'];
        $swords = $varttnews['swords'];
        $author = $varttnews['author'];
        
        
            
         //return $this->cObj->stdWrap( $this->pi_getLL( 'nonews' ) , $this->conf['nonews_stdWrap.'] );
        
        
        //Si on affiche même thematique
        switch( $this->flexFormValue('view', 's_template') )  {
            case  'memetheme' :
                $parent_category = $this->flexFormValue('parent_category', 's_template');
                if( !t3lib_utility_Math::convertToPositiveInteger( $uid ) ) return $this->cObj->stdWrap( $this->pi_getLL( 'nonews' ) , $this->conf['nonews_stdWrap.'] );
                $row =  $this->util->getcategoriesofarticle($uid,$parent_category );
                 
                foreach( $row as $uidf ){
                    if( t3lib_utility_Math::convertToPositiveInteger( $this->conf['uidcatexcludeinsearch'] ) && !t3lib_div::inList($this->conf['uidcatexcludeinsearch'],$uidf['uid_foreign']  ) ){
                        $tabuidf[] = $uidf['uid_foreign'];
                    }
                }
                
                if( count( $tabuidf ) == 0 )  return $this->cObj->stdWrap( $this->pi_getLL( 'nocategory' ) , $this->conf['nonews_stdWrap.'] );
                $row = $this->util->getarticleincategory($uid,$tabuidf,$this,$this->flexFormValue('pages', 's_template'));
            break;
            
            case  'pluslus' :
            case  'pluscommente' :
                //cas 1 - On est sur une fiche
                if( t3lib_utility_Math::convertToPositiveInteger( $uid ) ){
                    $row = $this->util->getarticlepluscommente( $uid, $this, $this->flexFormValue('pages', 's_template') );
                }        
                //cas 2 - On est sur un mot clé
                elseif( $swords != '' ){
                    $row = $this->util->getarticlepluscommente( 0, $this,$this->flexFormValue('pages', 's_template'), $swords );
                }        
                //cas 3 - On est sur un auteur
                elseif( t3lib_utility_Math::convertToPositiveInteger( $author ) ){
                    $row = $this->util->getarticlepluscommente( 0, $this,$this->flexFormValue('pages', 's_template'), '', $author );
                }        
                //Cas 4 - Une ou plusieurs categories filtres
                elseif( $this->flexFormValue('categorySelection', 's_template')!='' ){
                    $row = $this->util->getarticlepluscommente( 0, $this,$this->flexFormValue('pages', 's_template'), '', 0, $this->flexFormValue('categorySelection', 's_template')  );
                }        
                //cas 5 - Rien de tout ça
                else{
                    $row = $this->util->getarticlepluscommente( 0, $this,$this->flexFormValue('pages', 's_template')  );
                }
                
            break;
            case 'event_aujourdhui':
            case 'event_cettesemaine':
            case 'event_cemois':
            case 'event_cetteannee':
            case 'event_intervalledate':
                    $row = $this->util->get_event( $this   );
            break;
            case 'event_complete':
                    $row = $this->util->get_event_complete( $this   );
            break;
            case 'event_memelieu':
                    $row = $this->util->get_event_memelieu( $this   );
            break;
            case 'event_memethematique':
                    $row = $this->util->get_event_memethematique( $this   );
            break;
            case 'event_memeregion':
                    $row = $this->util->get_event_memeregion( $this   );
            break;
        
            
            case 'video_commented_today':
                    $row = $this->util->get_video_commented_today( $this   );
            break;
            case 'video_commented_recently':
                    $row = $this->util->get_video_commented_recently( $this   );
            break;
             
        }
        
        
        
        
        
        
            
            
          
            
        if( !is_array($row) || count( $row ) ==  0 ) return $this->cObj->stdWrap( $this->pi_getLL( 'nonews2' ) , $this->conf['nonews_stdWrap.'] );
            
         
         
        // Get Template
        $templateFile = $this->flexFormValue('templateFile', 's_template') ? $this->flexFormValue('templateFile', 's_template') : 'EXT:tsara/pi2/template.html';
        $this->template = $this->cObj->fileResource($templateFile);
        
        
        
        $subpartArray = $linkpartArray = $markerArray = array();        
        $subpart = $this->cObj->getSubpart($this->template, '###TEMPLATE###');
        
        
        $w = $pObj->config['FFimgW'] ;
        
            //Recup le bloc
            $subpartsub = $this->cObj->getSubpart($subpart, '###BOUCLE###');
            if( is_array($row) && count( $row ) > 0 ){
              //  \TYPO3\CMS\Core\Utility\GeneralUtility::writeFile(PATH_site.'typo3temp/logpi2.txt', print_r($row,1) );
            foreach( $row as $ligne ){
                $markersub['###TITLE###'] = $this->cObj->stdWrap( $ligne['title'], $this->conf['title_stdWrap.'] ); 
                $markersub['###DATE###'] = $this->cObj->stdWrap( $ligne['datetime'], $this->conf['date_stdWrap.'] ); 
                $markersub['###DATEEVENT###'] = $this->util->get_date_event($ligne, $this); 
                $markersub['###AUTHOR###'] = $this->cObj->stdWrap( $ligne['author'], $this->conf['author_stdWrap.'] ); 
                $markersub['###IMG###'] = ($ligne['image']!='' && file_exists(PATH_site.'uploads/pics/'.$ligne['image'])? '<img alt="" src="thb_'.$this->conf['widthimage'].'_'.$this->conf['heightimage'].'_1_/uploads/pics/'.$ligne['image'].'" />':'');
                $markersub['###URL###'] = ( $GLOBALS['TSFE']->cObj->typoLink( '', array(
             'parameter' => ($this->flexFormValue('PIDitemDisplay', 's_template')?$this->flexFormValue('PIDitemDisplay', 's_template'):$GLOBALS['TSFE']->id)
             ,'additionalParams' => '&tx_ttnews[tt_news]='.$ligne['uid']
             , 'useCacheHash'  => 1
             ,'returnLast' => 'url'
         )) );
                $markersub['###NEWSIMAGE_YT###'] = ($ligne['tx_tsara_youtubeid']!='' ? '<img alt="kuETZ3vySGQ/hqdefault.jpg" src="thbx_'.$this->conf['widthimage'].'__1_i.ytimg.com/vi/'.$ligne['tx_tsara_youtubeid'].'/hqdefault.jpg" />' :'');
                $markersub['###NEWS_ARTISTE###'] = $this->cObj->stdWrap( $this->showartiste( $ligne['uid'], $this ),  $this->conf['tsara.']['newartiste_stdWrap.']);

                
                $contentboucle .= $this->cObj->substituteMarkerArray($subpartsub, $markersub );
            }
            $subpartArray['###BOUCLE###'] = $contentboucle;
             $contentboucle = '';
            
            }
            
            
            //Si on affiche même thematique
        
            $subpartsub = $this->cObj->getSubpart($subpart, '###BOUCLECAT###');
           
        switch( $this->flexFormValue('view', 's_template') )  {
            case  'memetheme' :
                 
               foreach( $tabuidf as $uidf ){
                $pageuidf = ($this->util->getfieldincat( $uidf )>0?$this->util->getfieldincat( $uidf ):($this->flexFormValue('backPid', 's_template')>0?$this->flexFormValue('backPid', 's_template'):$this->conf['pid_actualites']));
                
                
                
                    $markersub['###LINKALL###'] = ( $GLOBALS['TSFE']->cObj->typoLink( '', array(
                 'parameter' => $pageuidf
                 ,'additionalParams' => ($this->util->getfieldincat( $uidf )>0?'':'&tx_ttnews[cat]='.$uidf)
                 , 'useCacheHash'  => ($this->util->getfieldincat( $uidf )>0?0:1)
                 ,'returnLast' => 'url'
             )) );
                
                    $markersub['###TXTALL###'] = sprintf($this->conf['txtall'],$this->util->getfieldincat( $uidf, 'title' ));
                 
                
                $contentboucle .= $this->cObj->substituteMarkerArray($subpartsub, $markersub );
                
            }
            break;
            case 'pluslus' :
            case 'pluscommente' :
                $markersub['###LINKALL###'] = ( $GLOBALS['TSFE']->cObj->typoLink( '', array(
                 'parameter' => ($this->flexFormValue('backPid', 's_template')?$this->flexFormValue('backPid', 's_template'):$this->conf['pid_actualites'])
                 ,'additionalParams' => '&tx_ttnews['.($this->flexFormValue('view', 's_template')=='pluslus'?'pluslus':'pluscommente').']=1'
                 , 'useCacheHash'  => 0
                 ,'returnLast' => 'url'
                )) );
                $markersub['###TXTALL###'] = ($this->flexFormValue('txtall', 's_template')?$this->flexFormValue('txtall', 's_template'):$this->pi_getLL('txtall'.($this->flexFormValue('view', 's_template')=='pluslus'?'pluslus':'pluscommentes') ));
                $contentboucle = $this->cObj->substituteMarkerArray($subpartsub, $markersub );
            break;
            
    }
            
            $subpartArray['###BOUCLECAT###'] = ($this->flexFormValue('show_linkall', 's_template')==1?$contentboucle:'');
         
            $contentboucle = '';
            $content = $this->cObj->substituteMarkerArrayCached($subpart, $markerArray, $subpartArray, $linkpartArray);
        
        $content = $this->replace( $content  );
        return $this->conf['wrapContentInBaseClass'] ? $this->pi_wrapInBaseClass($content) : $content;

    }
    function showartiste( $uid, $pobj ){
        
        $backPid =  ($this->flexFormValue('backPid', 's_template')>0?$this->flexFormValue('backPid', 's_template')  :$GLOBALS['TSFE']->id) ;
        //82.230.218.34
        if( t3lib_div::getIndpEnv('REMOTE_ADDR') == '82.230.218.34' ){
               //print_r($backPid.', ');
        }
        $html = '';
        $parent_category_artiste = intval($this->confArr['parent_category_artiste']);
        if( $parent_category_artiste <= 0 ) return '';
         
        $nb = $GLOBALS['TYPO3_DB']->exec_SELECTcountRows('tt_news_cat_mm.uid_foreign','tt_news_cat_mm LEFT JOIN tt_news ON tt_news.uid = tt_news_cat_mm.uid_local LEFT JOIN tt_news_cat ON tt_news_cat.uid = tt_news_cat_mm.uid_foreign ','1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news').' AND tt_news_cat.parent_category = '.$parent_category_artiste.' AND tt_news_cat_mm.uid_local = '.$uid );
        if( $nb > 0 ){
             
            $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows('tt_news_cat.uid, tt_news_cat.title ','tt_news_cat_mm LEFT JOIN tt_news ON tt_news.uid = tt_news_cat_mm.uid_local LEFT JOIN tt_news_cat ON tt_news_cat.uid = tt_news_cat_mm.uid_foreign ','1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news').' AND tt_news_cat.parent_category = '.$parent_category_artiste.' AND tt_news_cat_mm.uid_local = '.$uid );
            if( count( $row ) > 0 ){
                $subpart  = $pobj->cObj->getSubpart($this->template, '###TEMPLATE_LISTEARTISTE###' );
                $i = 0;
                
                foreach( $row as $ligne ){
                    $data['CONTENT'][$i]['title'] = $ligne['title'];
                    $data['CONTENT'][$i]['uid'] = $ligne['uid'];
                    $data['CONTENT'][$i]['separator'] = (($i+1)!=count( $row ) ? ', ' : '');
                    
                    
                    $data['CONTENT'][$i]['link'] = $GLOBALS['TSFE']->cObj->typoLink( '', array(
                        'parameter' => $backPid
                       ,'additionalParams' => '&tx_ttnews[artiste][0]='.$ligne['uid'] 
                        ,'returnLast' => 'url'
                        ,'useCacheHash' => 1
                         
                       ));
                    $i++;
                }
                $html = $pobj->cObj->substituteMarkerAndSubpartArrayRecursive($subpart, $data, '###|###' );
            }
            
        }
        
        return $html;

    }
    function preInit() {

		 

		$flexformTyposcript = $this->flexFormValue('myTS', 's_template');
		if ($flexformTyposcript) {
			require_once(PATH_t3lib.'class.t3lib_tsparser.php');
			$tsparser = t3lib_div::makeInstance('t3lib_tsparser');
			// Copy conf into existing setup
			$tsparser->setup = $this->conf;
			// Parse the new Typoscript
			$tsparser->parse($flexformTyposcript);
			// Copy the resulting setup back into conf
			$this->conf = $tsparser->setup;
		}

		 
	}
    function getDisplayText($label,$value,$stdWrapArray=array()) {
		return $this->cObj->stdWrap( sprintf($this->pi_getLL( 'value_'.$label ),$value), $stdWrapArray);		
	}
    function replace( $content ){
            // 1. replace ll markers
            $content = preg_replace_callback ( // Automaticly fill locallangmarkers with fitting value of locallang.xml
                    '$###LL_(.*)###$Uis', // regulare expression
                    array($this, 'DynamicLocalLangMarker'), // open function
                    $content // current content
            );
            $content = preg_replace_callback ( // Automaticly fill locallangmarkers with fitting value of locallang.xml
                    '$###CONF_(.*)###$Uis', // regulare expression
                    array($this, 'DynamicConfMarker'), // open function
                    $content // current content
            );
            $content = preg_replace_callback ( // Automaticly fill locallangmarkers with fitting value of locallang.xml
                    '$###PID_(.*)###$Uis', // regulare expression
                    array($this, 'DynamicPidMarker'), // open function
                    $content // current content
            );

            return $content;
        }
        function DynamicLocalLangMarker($array) {
            if ($this->pi_getLL(strtolower($array[1]))) { // if there is an entry in locallang.xml
                $string = $this->pi_getLL(strtolower($array[1])); // search for a fitting entry in locallang.xml or typoscript
            }
            if (!empty($string)) return $string;
        }

        function DynamicConfMarker($array) {
            if ($this->conf[(strtolower($array[1]))]) { // if there is an entry in locallang.xml
                $string = $this->conf[(strtolower($array[1]))]; // search for a fitting entry in locallang.xml or typoscript
            }
            if (!empty($string)) return $string;
        }
        function DynamicPidMarker($array) {
            if ($this->conf['pid_'.(strtolower($array[1]))]) { // if there is an entry in locallang.xml
                $string = $GLOBALS['TSFE']->cObj->typoLink('', array(
                                                                'parameter' => $this->conf['pid_'.(strtolower($array[1]))],
                                                                'useCacheHash' => 1,
                                                                'returnLast' => 'url',
                                                                )
                                                            );
            }
            if (!empty($string)) return $string;
        }
        /**
	 * Loads a variable from the flexform
	 *
	 * @param	string		name of variable
	 * @param	string		name of sheet
	 * @return	string		value of var
	 */
    function flexFormValue($var, $sheet) {
            return $this->pi_getFFvalue($this->cObj->data['pi_flexform'], $var,$sheet);
    }
    function setSession($varname,$varcontent) {
   	 $GLOBALS['TSFE']->fe_user->setKey('ses',$varname,$varcontent);
   	 $GLOBALS['TSFE']->storeSessionData(); // validate the session
    }

    /**
     * Get a variable in typo3 session (without params return all the session table)
     */

    function getSession ($varname="") {
   	 if($varname!="") {
   		 return $GLOBALS['TSFE']->fe_user->getKey('ses',$varname);
   	 } else {
   		 return $GLOBALS['TSFE']->fe_user->sesData;
   	 }
    }
    /**
	 * Reads flexform configuration and merge it with $this->conf
	 *
	 * @return	void
	 */
	 function mergeflexFormValuesIntoConf() {
		$flex = array();
		
                if ($this->flexFormValue('view', 's_template')) {
			$flex['view'] = $this->flexFormValue('view','s_template');
		}

		

		$this->conf = array_merge($this->conf, $flex);
	}
}



if (defined('TYPO3_MODE') && isset($GLOBALS['TYPO3_CONF_VARS'][TYPO3_MODE]['XCLASS']['ext/tsara/pi2/class.tx_tsara_pi2.php'])) {
    include_once($GLOBALS['TYPO3_CONF_VARS'][TYPO3_MODE]['XCLASS']['ext/tsara/pi2/class.tx_tsara_pi2.php']);
}

?> 