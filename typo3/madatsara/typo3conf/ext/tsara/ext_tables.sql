#
# Table structure for table 'pages'
#
CREATE TABLE `pages` (
	`tx_tsara_metatitle` text,
);

#
# Table structure for table 'tt_news'
#
CREATE TABLE tt_news (
	tx_newsreadedcount_readedcounter int(11) DEFAULT '0' NOT NULL
	tx_tsara_author int(11) DEFAULT '0' NOT NULL
	tx_tsarattnews_slug text,
	tx_tsarattnews_nbcomments int(11) DEFAULT '0' NOT NULL,
	tx_tsara_datecreation int(11) DEFAULT '0' NOT NULL,
	tx_tsara_pr int(1) DEFAULT '0' NOT NULL,
	tx_tsara_exturl2 varchar(255) DEFAULT '' NOT NULL,
	tx_tsara_youtubeid varchar(100) DEFAULT '' NOT NULL,
	tx_tsara_youtube_thumbnail varchar(255) DEFAULT '' NOT NULL,
	tx_tsara_youtube_published varchar(20) DEFAULT '' NOT NULL,
	tx_tsara_youtube_duration int(5) DEFAULT '0' NOT NULL,
	tx_tsara_youtube_viewcount int(5) DEFAULT '0' NOT NULL,
	tx_tsara_youtube_favoritecount int(20) DEFAULT '0' NOT NULL,
	tx_tsara_youtube_numlikes int(20) DEFAULT '0' NOT NULL,
	tx_tsara_youtube_numdislikes int(20) DEFAULT '0' NOT NULL,
	tx_tsara_youtube_numcomment int(20) DEFAULT '0' NOT NULL,
	tx_tsara_tstamp int(10) DEFAULT '0' NOT NULL,
        tx_tsara_yttitle text,

        tx_tsara_event_date int(11) DEFAULT '0' NOT NULL,
        tx_tsara_event_date_end int(11) DEFAULT '0' NOT NULL,
        tx_tsara_event_lieu TEXT,
        tx_tsara_event_lieugps varchar(255) DEFAULT '' NOT NULL,
        tx_tsara_event_prix varchar(50) DEFAULT '' NOT NULL,
        tx_tsara_event_reservation TEXT,
        tx_tsara_event_prevente TEXT,
        tx_tsara_event_location TEXT,
        tx_tsara_event_contacts TEXT,
        tx_tsara_event_endroit int(11) DEFAULT '0' NOT NULL,
		
        tx_tsara_cine_acteurs  int(11) DEFAULT '0' NOT NULL,
        tx_tsara_cine_auteurs  int(11) DEFAULT '0' NOT NULL,
        tx_tsara_cine_realisateurs  int(11) DEFAULT '0' NOT NULL,
        tx_tsara_cine_musique  int(11) DEFAULT '0' NOT NULL,
        
        tx_tsara_cine_production  int(11) DEFAULT '0' NOT NULL,
        tx_tsara_cine_scenario  int(11) DEFAULT '0' NOT NULL,
        tx_tsara_cine_directeur_photo  int(11) DEFAULT '0' NOT NULL,
        tx_tsara_cine_montage_son  int(11) DEFAULT '0' NOT NULL,
        tx_tsara_cine_perchman  int(11) DEFAULT '0' NOT NULL,
        tx_tsara_cine_effets_visuels  int(11) DEFAULT '0' NOT NULL,
        tx_tsara_cine_videosfilm  int(11) DEFAULT '0' NOT NULL,
         
        
        tx_tsara_cine_format int(11) DEFAULT '0' NOT NULL,
        tx_tsara_cine_duree int(11) DEFAULT '0' NOT NULL,
        tx_tsara_cine_anneesortie int(11) DEFAULT '0' NOT NULL,
        tx_tsara_cine_siteweb TEXT 
);

#
# Table structure for table 'tx_tsara_cine_acteurs_mm'
#
#
CREATE TABLE tx_tsara_cine_musique_mm (
  uid_local int(11) DEFAULT '0' NOT NULL,
  uid_foreign int(11) DEFAULT '0' NOT NULL,
  tablenames varchar(30) DEFAULT '' NOT NULL,
  sorting int(11) DEFAULT '0' NOT NULL,
  KEY uid_local (uid_local),
  KEY uid_foreign (uid_foreign)
);

#
# Table structure for table 'tx_tsara_cine_acteurs_mm'
#
#
CREATE TABLE tx_tsara_cine_acteurs_mm (
  uid_local int(11) DEFAULT '0' NOT NULL,
  uid_foreign int(11) DEFAULT '0' NOT NULL,
  tablenames varchar(30) DEFAULT '' NOT NULL,
  sorting int(11) DEFAULT '0' NOT NULL,
  KEY uid_local (uid_local),
  KEY uid_foreign (uid_foreign)
);

#
# Table structure for table 'tx_tsara_cine_realisateurs_mm'
#
#
CREATE TABLE tx_tsara_cine_realisateurs_mm (
  uid_local int(11) DEFAULT '0' NOT NULL,
  uid_foreign int(11) DEFAULT '0' NOT NULL,
  tablenames varchar(30) DEFAULT '' NOT NULL,
  sorting int(11) DEFAULT '0' NOT NULL,
  KEY uid_local (uid_local),
  KEY uid_foreign (uid_foreign)
);

#
# Table structure for table 'tx_tsara_cine_auteurs_mm'
#
#
CREATE TABLE tx_tsara_cine_auteurs_mm (
  uid_local int(11) DEFAULT '0' NOT NULL,
  uid_foreign int(11) DEFAULT '0' NOT NULL,
  tablenames varchar(30) DEFAULT '' NOT NULL,
  sorting int(11) DEFAULT '0' NOT NULL,
  KEY uid_local (uid_local),
  KEY uid_foreign (uid_foreign)
);

#
# Table structure for table 'tx_tsara_cine_production_mm'
#
#
CREATE TABLE tx_tsara_cine_production_mm (
  uid_local int(11) DEFAULT '0' NOT NULL,
  uid_foreign int(11) DEFAULT '0' NOT NULL,
  tablenames varchar(30) DEFAULT '' NOT NULL,
  sorting int(11) DEFAULT '0' NOT NULL,
  KEY uid_local (uid_local),
  KEY uid_foreign (uid_foreign)
);

#
# Table structure for table 'tx_tsara_cine_scenario_mm'
#
#
CREATE TABLE tx_tsara_cine_scenario_mm (
  uid_local int(11) DEFAULT '0' NOT NULL,
  uid_foreign int(11) DEFAULT '0' NOT NULL,
  tablenames varchar(30) DEFAULT '' NOT NULL,
  sorting int(11) DEFAULT '0' NOT NULL,
  KEY uid_local (uid_local),
  KEY uid_foreign (uid_foreign)
);

#
# Table structure for table 'tx_tsara_cine_directeur_photo_mm'
#
#
CREATE TABLE tx_tsara_cine_directeur_photo_mm (
  uid_local int(11) DEFAULT '0' NOT NULL,
  uid_foreign int(11) DEFAULT '0' NOT NULL,
  tablenames varchar(30) DEFAULT '' NOT NULL,
  sorting int(11) DEFAULT '0' NOT NULL,
  KEY uid_local (uid_local),
  KEY uid_foreign (uid_foreign)
);

#
# Table structure for table 'tx_tsara_cine_montage_son_mm'
#
#
CREATE TABLE tx_tsara_cine_montage_son_mm (
  uid_local int(11) DEFAULT '0' NOT NULL,
  uid_foreign int(11) DEFAULT '0' NOT NULL,
  tablenames varchar(30) DEFAULT '' NOT NULL,
  sorting int(11) DEFAULT '0' NOT NULL,
  KEY uid_local (uid_local),
  KEY uid_foreign (uid_foreign)
);

#
# Table structure for table 'tx_tsara_cine_perchman_mm'
#
#
CREATE TABLE tx_tsara_cine_perchman_mm (
  uid_local int(11) DEFAULT '0' NOT NULL,
  uid_foreign int(11) DEFAULT '0' NOT NULL,
  tablenames varchar(30) DEFAULT '' NOT NULL,
  sorting int(11) DEFAULT '0' NOT NULL,
  KEY uid_local (uid_local),
  KEY uid_foreign (uid_foreign)
);

#
# Table structure for table 'tx_tsara_cine_effets_visuels_mm'
#
#
CREATE TABLE tx_tsara_cine_effets_visuels_mm (
  uid_local int(11) DEFAULT '0' NOT NULL,
  uid_foreign int(11) DEFAULT '0' NOT NULL,
  tablenames varchar(30) DEFAULT '' NOT NULL,
  sorting int(11) DEFAULT '0' NOT NULL,
  KEY uid_local (uid_local),
  KEY uid_foreign (uid_foreign)
);

#
# Table structure for table 'tx_tsara_cine_videosfilm_mm'
#
#
CREATE TABLE tx_tsara_cine_videosfilm_mm (
  uid_local int(11) DEFAULT '0' NOT NULL,
  uid_foreign int(11) DEFAULT '0' NOT NULL,
  tablenames varchar(30) DEFAULT '' NOT NULL,
  sorting int(11) DEFAULT '0' NOT NULL,
  KEY uid_local (uid_local),
  KEY uid_foreign (uid_foreign)
);

#
# Table structure for table 'tt_news_cat'
#
CREATE TABLE tt_news_cat (
	tx_tsara_pagelist int(11) DEFAULT '0' NOT NULL,
	tx_ablinklistcat int(11) DEFAULT '0' NOT NULL,
	tx_tsara_keywords text
);

#
# Table structure for table 'tx_tsara_author'
#
CREATE TABLE fe_users (
	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,
	tstamp int(11) DEFAULT '0' NOT NULL,
	crdate int(11) DEFAULT '0' NOT NULL,
	cruser_id int(11) DEFAULT '0' NOT NULL,
	deleted tinyint(4) DEFAULT '0' NOT NULL,
	hidden tinyint(4) DEFAULT '0' NOT NULL,
	
        
	description LONGTEXT, 
	souhaitecontact int(11) DEFAULT '0' NOT NULL,
	 
	comptes int(11) DEFAULT '0' NOT NULL
	
	PRIMARY KEY (uid),
	KEY parent (pid)
);

#
# Table structure for table 'tx_tsara_compteauthor'
#
CREATE TABLE tx_tsara_compteauthor (
	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,
	tstamp int(11) DEFAULT '0' NOT NULL,
	crdate int(11) DEFAULT '0' NOT NULL,
	cruser_id int(11) DEFAULT '0' NOT NULL,
	deleted tinyint(4) DEFAULT '0' NOT NULL,
	hidden tinyint(4) DEFAULT '0' NOT NULL,
	
        title tinytext,
        typecompte tinytext,
        url tinytext,	  
	showinfront int(11) DEFAULT '0' NOT NULL,
        comptes int(11) DEFAULT '0' NOT NULL,
	
	PRIMARY KEY (uid),
	KEY parent (pid)
);

# Table structure for table 'tx_tsara_videosfilm'
#
CREATE TABLE tx_tsara_videosfilm (
	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,
	tstamp int(11) DEFAULT '0' NOT NULL,
	crdate int(11) DEFAULT '0' NOT NULL,
	cruser_id int(11) DEFAULT '0' NOT NULL,
	deleted tinyint(4) DEFAULT '0' NOT NULL,
	hidden tinyint(4) DEFAULT '0' NOT NULL,
	
        title tinytext,
        typecompte tinytext,
        url tinytext,	
description text,  
	 
        tx_tsara_cine_videosfilm int(11) DEFAULT '0' NOT NULL,
	
	PRIMARY KEY (uid),
	KEY parent (pid)
);

# Table structure for table 'tx_tsara_cine_acteurs'
#
CREATE TABLE tx_tsara_cine_acteurs (
	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,
	tstamp int(11) DEFAULT '0' NOT NULL,
	crdate int(11) DEFAULT '0' NOT NULL,
	cruser_id int(11) DEFAULT '0' NOT NULL,
	deleted tinyint(4) DEFAULT '0' NOT NULL,
	hidden tinyint(4) DEFAULT '0' NOT NULL,
	
        title tinytext,
        ttaddressuid int(11) DEFAULT '0' NOT NULL, 
        
        tx_tsara_cine_acteurs int(11) DEFAULT '0' NOT NULL,
	
	PRIMARY KEY (uid),
	KEY parent (pid)
);

# Table structure for table 'tx_comments_comments'
#
CREATE TABLE tx_comments_comments (
	 tx_tsara_youtube_published varchar(20) DEFAULT '' NOT NULL
);

#
# Table structure for table 'pages'
#
CREATE TABLE `tt_address` (
	tx_tsara_event_lieugps varchar(255) DEFAULT '' NOT NULL,
);





