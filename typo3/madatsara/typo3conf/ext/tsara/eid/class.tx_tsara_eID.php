<?php

// Inclusion des librairies nécessaires

require_once(t3lib_extMgm::extPath('lang', 'lang.php'));
require_once(t3lib_extMgm::extPath("tsara").'lib/class.tx_tsara_util.php');
require_once(t3lib_extMgm::extPath("tsara").'lib/simple_html_dom.php');

use \TYPO3\CMS\Core as Core;
use \TYPO3\CMS\Extbase\Utility as Utility;     
use \TYPO3\CMS\Frontend as Frontend;
                     

class tx_tsara_eID {

    var $db;
    protected function initTSFE() {
		if (version_compare(TYPO3_version, '4.3.0', '<')) {
			$tsfeClassName = t3lib_div::makeInstanceClassName('tslib_fe');
			$GLOBALS['TSFE'] = new $tsfeClassName($GLOBALS['TYPO3_CONF_VARS'], t3lib_div::_GP('id'), '');
		}
		else {
			$GLOBALS['TSFE'] = t3lib_div::makeInstance('tslib_fe', $GLOBALS['TYPO3_CONF_VARS'], t3lib_div::_GP('id'), '');
		}
                
                
                
                $GLOBALS['TSFE'] = Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController', $TYPO3_CONF_VARS, 0, 0);
                
                $GLOBALS['TSFE']->set_no_cache();
                $GLOBALS['TSFE']->initTemplate();
                $GLOBALS['TSFE']->getConfigArray();
                Core\Core\Bootstrap::getInstance()->loadCachedTca();
                $GLOBALS['TSFE']->cObj = Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer');
                $GLOBALS['TSFE']->settingLanguage();
                $GLOBALS['TSFE']->settingLocale();
		/*$GLOBALS['TSFE']->connectToDB();
		$GLOBALS['TSFE']->initFEuser();
		$GLOBALS['TSFE']->determineId();
		$GLOBALS['TSFE']->getCompressedTCarray();
		$GLOBALS['TSFE']->initTemplate();
		$GLOBALS['TSFE']->getConfigArray();
                
                $GLOBALS['TSFE']->cObj = t3lib_div::makeInstance('tslib_cObj');
                */
		// Get linkVars, absRefPrefix, etc
		TSpagegen::pagegenInit();
	}
    /* MÃ©thode principale */
    function main() {
        /*require_once(PATH_tslib.'class.tslib_content.php');
        require_once(PATH_t3lib.'class.t3lib_page.php');
        */
                     
        //$this->initTSFE();
        $this->confArr = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['tsara']);
        
        // Connexion Ã  la base de donnÃ©es
        tslib_eidtools::connectDB();
        $this->db = $GLOBALS['TYPO3_DB'];
        $task = 'accueil';
        $pid = $uid = 0;
        $this->util = t3lib_div::makeInstance('tx_tsara_util');        
        if( t3lib_div::_GP('task') ) $task = t3lib_div::_GP('task');
        
        if( $task == 'batch' && t3lib_div::_GP('fname') ){
            $this->batch(t3lib_div::_GP('fname'));
        }
        
        if( $task == 'addselection_video' ){
            $this->addselection_video(); 
        }
        if( $task == 'getselection_video' ){
                echo $this->getSession('uidliste');exit;
        }
        if( $task == 'savevideonew' ){
            $this->savevideonew();
        }
        if( $task == 'savevideogenre' ){
            $this->savevideogenre();
        }
        if( $task == 'savevideorealisation' ){
            $this->savevideorealisation();
        }
        if( $task == 'savevideo' ){
             $this->savevideo();
        }
        if( $task == 'getartiste' ){             
            $this->get_ttnewscat($this->confArr['parent_category_artiste']);
        }
        if( $task == 'getgenre' ){
            $this->get_ttnewscat($this->confArr['parent_category_rythme']);
        }
        if( $task == 'getrealisation' ){
            $this->get_ttnewscat($this->confArr['parent_category_realisateur']);
        }
        if( $task == 'getgenrecine' ){
            $this->get_ttnewscat($this->confArr['parent_category_genrecinema']);
        }
        if( $task == 'getrealisateurs' ){
            $this->get_address($this->confArr['uid_ttaddressgroup_realisateur']); 
        }
        if( $task == 'getacteurs' ){
            $this->get_address($this->confArr['uid_ttaddressgroup_acteurs']); 
        }
        if( $task == 'getauteurs' ){
            $this->get_address($this->confArr['uid_ttaddressgroup_auteurs']); 
        }
        if( $task == 'getmusique' ){
            $this->get_address($this->confArr['uid_ttaddressgroup_musique']); 
        }
        //deleteselection_video
        if( $task == 'deleteselection_video' ){
            $this->deleteselection_video(); 
        }
        if( $task == 'search_website' ){
            
            $this->search_website(); 
        }
        if( $task == 'valid' ){
            $this->valid_ttnews( t3lib_div::removeXSS(t3lib_div::_GP('opt')) ); 
        }
        if( $task == 'majsite' ){
            $this->majsite_ttnews( t3lib_div::removeXSS(t3lib_div::_GP('opt')) ); 
        }
        if( $task == 'sitemap' ){
            $this->sitemap(); 
        }
		if( $task == 'track' ){
            $this->track_email(); 
        }
        
        if( $task == 'event'  ){
            $this->eventDate();
        }
                     
		
        return false;
    }
    
    /**
     * Afficher les event entre 2 dates
     * @return boolean
     */
    function eventDate( )
    {
        
                     
        
        $day = date('w')-1;
        $week_start = date('Y-m-d', strtotime('-'.$day.' days'));
        $week_end = date('Y-m-d', strtotime('+'.(6-$day).' days'));
        
                     
        
        
        $rows = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                    'CASE TRIM(t.title) 
		WHEN \'\' THEN REPLACE(REPLACE(t.image,\'.jpg\',\'\'),\'_\',\' \') 
		ELSE t.title 
	END AS `title`,
	FROM_UNIXTIME(t.`tx_tsara_event_date`,\'%d-%m-%Y\') as `startdate`,
	FROM_UNIXTIME(t.`tx_tsara_event_date_end`,\'%d-%m-%Y\') as `enddate`, 
	CONCAT(\'http://www.madatsara.com/uploads/pics/\',t.image) as flyer,
	CONCAT(\'http://www.madatsara.com/evenement_\',r.value_alias,\'.html\') as `eventurl`',
                '`tt_news` t
JOIN `tx_realurl_uniqalias` r ON r.value_id = t.uid',
                "r.tablename = 'tt_news' AND field_alias = 'title' AND
t.`pid` = '55' AND !t.deleted AND !t.hidden 
AND ((FROM_UNIXTIME(t.`tx_tsara_event_date`,'%Y-%m-%d')>='".$week_start."' AND FROM_UNIXTIME(t.`tx_tsara_event_date_end`,'%Y-%m-%d')<='".$week_end."')
 OR ('".$week_start."' BETWEEN FROM_UNIXTIME(t.`tx_tsara_event_date`,'%Y-%m-%d') AND FROM_UNIXTIME(t.`tx_tsara_event_date_end`,'%Y-%m-%d')) 
 OR ('".$week_end."' BETWEEN FROM_UNIXTIME(t.`tx_tsara_event_date`,'%Y-%m-%d') AND FROM_UNIXTIME(t.`tx_tsara_event_date_end`,'%Y-%m-%d')) )",
                '',
                '`startdate` asc'
                );
        $templateFile = 'EXT:tsara/eid/tafolanitra.html';
        
        if ( count($rows) <= 0 ) {
            $templateFile = 'EXT:tsara/eid/tafolanitra_empty.html';
        }
        
        
        $template = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName( $templateFile );
        $fluid = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Fluid\\View\\StandaloneView'); // instanciate standalone view
        $fluid->setTemplatePathAndFilename($template);
                     
        //on assigne une variable
        $fluid->assign('data', $rows );
                     
        echo $fluid->render();exit();
        
        
    }
    
    
	function gettrack_email(){
		if( t3lib_div::_GP('uidtrack') !='' ) $uidtrack = t3lib_div::_GP('uidtrack'); 
		$tlog = array();
		$tlog[] = 'Date : '.date('d-m-Y H:i:s');
		$tlog[] = 'IP : '.t3lib_div::getIndpEnv('REMOTE_ADDR');
		$tlog[] = 'USER Agent : '.t3lib_div::getIndpEnv('HTTP_USER_AGENT');
		$value = implode(', ',$tlog )."\n";
		if( file_exists( PATH_site.'typo3temp/'.$uidtrack.'.txt' ) ){
			$fp = fopen( PATH_site.'typo3temp/'.$uidtrack.'.txt', 'a+');
			fwrite( $fp, $value );
			fclose( $fp );
		}
		else t3lib_div::writeFile( PATH_site.'typo3temp/'.$uidtrack.'.txt', $value );
		return true;
	}
	function track_email(){
		if( t3lib_div::_GP('uidtrack') !='' ) $this->gettrack_email();
		Header('Content-Type:image/jpg');
		echo file_get_contents(PATH_site.'fileadmin/user_upload/sfx.jpg');
		exit;
	}
    function sitemap(){ 
        t3lib_div::requireOnce(t3lib_extMgm::extPath('tsara', 'lib/class.tx_tsarasitemap_pages.php'));
        $generator = t3lib_div::makeInstance('tx_tsarasitemap_pages');
        /* @var $generator tx_ddgooglesitemap_pages */
        $generator->main();
    }
    function batch($fname){
            $fileauto = $this->confArr['fileauto'];
            $file = PATH_site.'fileadmin/batch/'.$fname.'.php';
            if( file_exists($file) && t3lib_div::inList($fileauto,$fname) ) include_once $file;
            else echo $file.' non existant';
            exit;
    }
    
    function majsite_ttnews( $opt = 0 ){
        if( $this->isadmin() ){
            
             if( t3lib_utility_Math::convertToPositiveInteger( $opt ) && t3lib_utility_Math::convertToPositiveInteger( t3lib_div::removeXSS(t3lib_div::_GP('uid')) ) ){
                 $uid = t3lib_div::removeXSS(t3lib_div::_GP('uid'));
                 switch ($opt){
                     case '1' :
                        //MAJ URL
                        if( t3lib_div::removeXSS(t3lib_div::_GP('newurl')) && filter_var(t3lib_div::removeXSS(t3lib_div::_GP('newurl')),FILTER_VALIDATE_URL) ){
                            $sql = $GLOBALS['TYPO3_DB']->UPDATEquery('tt_news','uid='.$GLOBALS['TYPO3_DB']->fullQuoteStr($uid,'tt_news'),array('ext_url'=>t3lib_div::removeXSS(t3lib_div::_GP('newurl'))));
                            $msg = 'URL mise &agrave; jour';
                        }
                     break;
                     case '2' :
                        //DESIGN Change
                        $sql = $GLOBALS['TYPO3_DB']->UPDATEquery('tt_news','uid='.$GLOBALS['TYPO3_DB']->fullQuoteStr($uid,'tt_news'),array('image'=>''));
                         $msg = 'DESIGN Chang&eacute;';
                     break;
                     case '3' :
                        //Maj info site
                         $tmaj = array();
                        if( t3lib_div::removeXSS(t3lib_div::_GP('jsonfile')) && file_exists( rawurldecode( t3lib_div::removeXSS(t3lib_div::_GP('jsonfile')) ) ) ){
                            $json = json_decode(file_get_contents( rawurldecode( t3lib_div::removeXSS(t3lib_div::_GP('jsonfile')) ) ), true );
                            
                            if( $json['author'] != '' ) $tmaj['author'] = $json['author'];
                            if( $json['email_author'] != '' ) $tmaj['author_email'] = $json['email_author'];
                            if( $json['short'] != '' ) $tmaj['short'] = $json['short'];
                     
                            $sql = $GLOBALS['TYPO3_DB']->UPDATEquery(
                                        'tt_news',
                                        'uid='.$GLOBALS['TYPO3_DB']->fullQuoteStr($uid,'tt_news'),
                                        $tmaj
                                    );
                            $msg = 'Infos du site mise &agrave; jour ';
                            $msg .=  $sql ;
                            
                            if( is_array($json['cat']) ){ 
                                
                                $GLOBALS['TYPO3_DB']->exec_DELETEquery(
                                        'tt_news_cat_mm',
                                        'uid_local = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($uid,'tt_news')
                                        );
                                foreach( $json['cat'] as $uid_foreign ){
                                    $GLOBALS['TYPO3_DB']->exec_INSERTquery(
                                            'tt_news_cat_mm',
                                            array(
                                                'uid_local' => $uid
                                                ,'uid_foreign' => $uid_foreign
                                            )
                                        );
                                }
                                
                               
                            }
                            
                            //Suppr json
                            unlink( rawurldecode( t3lib_div::removeXSS(t3lib_div::_GP('jsonfile')) ) );
                            
                            
                            
                        } 
                     
                     break;
                 }
                 echo  $msg;
                     
                 $GLOBALS['TYPO3_DB']->sql_query( $sql );
             }
            
            
        }
        else echo 'Non connect&eacute; &agrave; TYPO3 !';
        exit;
    }
    function valid_ttnews( $opt = 0 ){
        if( $this->isadmin() ){
            
             if( t3lib_utility_Math::convertToPositiveInteger( $opt ) && t3lib_utility_Math::convertToPositiveInteger( t3lib_div::removeXSS(t3lib_div::_GP('uid')) ) ){
                 $uid = t3lib_div::removeXSS(t3lib_div::_GP('uid'));
                 switch ($opt){
                     case '1' :
                        //Valider
                        $sql = $GLOBALS['TYPO3_DB']->UPDATEquery('tt_news','uid='.$GLOBALS['TYPO3_DB']->fullQuoteStr($uid,'tt_news'),array('hidden'=>0,'deleted'=>0));
                        $msg = 'valid&eacute;';
                     break;
                     case '2' :
                        //De-Valider
                        $sql = $GLOBALS['TYPO3_DB']->UPDATEquery('tt_news','uid='.$GLOBALS['TYPO3_DB']->fullQuoteStr($uid,'tt_news'),array('hidden'=>1,'deleted'=>0));
                         $msg = 'De-valid&eacute;';
                     break;
                     case '3' :
                        //Supprumer
                        $sql = $GLOBALS['TYPO3_DB']->UPDATEquery('tt_news','uid='.$GLOBALS['TYPO3_DB']->fullQuoteStr($uid,'tt_news'),array('deleted'=>1));
                         $msg = 'supprim&eacute;';
                     break;
                 }
                 echo 'L\'item a &eacute;t&eacute; '.$msg;
                 echo '<!-- '.$sql.' -->';
                 $GLOBALS['TYPO3_DB']->sql_query( $sql );
             }
            
            
        }
        else echo 'Non connect&eacute; &agrave; TYPO3 !';
        exit;
    }
    function search_website(){
        $nb = 0;
        if( t3lib_div::_GP('url') ) $url = urldecode( t3lib_div::_GP('url') );
        $pidsites = $this->confArr['pidsites'];
        $url = t3lib_div::removeXSS( $url );
        $log = array();
        if( $url != '' ){
            $tab_url = parse_url($url);
            $host = $tab_url['host'];
            
            $cs = t3lib_div::makeInstance('t3lib_cs');
            $host = $cs->conv_case('utf-8',$host,'toLower');
            $host = trim($host);
            $scheme = $cs->conv_case('utf-8',$tab_url['scheme'],'toLower');
            $www = $cs->substr('utf-8',$host,0,4);
            $url_without_www = 'http://'.($www=='www.'?$cs->substr('utf-8',$host,4):$host).'/';
            $urls_without_www = 'https://'.($www=='www.'?$cs->substr('utf-8',$host,4):$host).'/';
            $url_with_www = 'http://'.($www=='www.'?$host:'www.'.$host).'/';
            $urls_with_www = 'https://'.($www=='www.'?$host:'www.'.$host).'/';
            
            $report = array();
            $str = t3lib_div::getUrl($url,1,FALSE,$report);
            if( $report['http_code'] != '200') {
                $log = array(
                    'error' => 1
                    ,'nb' => 0
                    ,'infositevide' => '0'
                    ,'infosite' => array()
                    ,'errormsg' => 'http_code different de 200'
                );
                //echo '1';exit;
                echo json_encode($log);exit;
            }
            
           
            $nb = $GLOBALS['TYPO3_DB']->exec_SELECTcountRows(
                    'uid',
                    'tt_news',
                    ' ext_url IN('.$GLOBALS['TYPO3_DB']->fullQuoteStr($url_without_www,'tt_news').','.$GLOBALS['TYPO3_DB']->fullQuoteStr($url_with_www,'tt_news').','.$GLOBALS['TYPO3_DB']->fullQuoteStr($urls_with_www,'tt_news').','.$GLOBALS['TYPO3_DB']->fullQuoteStr($urls_without_www,'tt_news').')  AND deleted=0 AND pid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($pidsites,'tt_news')
                    );
             
            /*echo $GLOBALS['TYPO3_DB']->SELECTquery(
                    '*',
                    'tt_news',
                    ' ext_url IN('.$GLOBALS['TYPO3_DB']->fullQuoteStr($url_without_www,'tt_news').','.$GLOBALS['TYPO3_DB']->fullQuoteStr($url_with_www,'tt_news').','.$GLOBALS['TYPO3_DB']->fullQuoteStr($urls_with_www,'tt_news').','.$GLOBALS['TYPO3_DB']->fullQuoteStr($urls_without_www,'tt_news').')  AND deleted=0 AND pid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($pidsites,'tt_news')
                    );
              */
        }
        //echo $nb;
        //Charger DOM PARSER
        $html = new simple_html_dom;
        $html->load($str, 1);
        
        $titresite = $cs->entities_to_utf8( html_entity_decode( trim( $html->find('title',0)->plaintext ) ) );
        $descsite = $cs->entities_to_utf8( html_entity_decode( trim( $html->find("meta[name='description']", 0)->content ) ) );
        $tabsite = array();
        
        if( $titresite!='' || $descsite!='' ){
            $tabsite = array(
                'titre' => $titresite
                ,'description' => $descsite
            );
        }
        
        
        $log = array(
                    'error' => '0'
                    ,'nb' => $nb
                    ,'infositevide' => ($titresite=='' && $descsite==''?'1':'0')
                    ,'infosite' => $tabsite
                    ,'errormsg' => ''
        );
                //echo '1';exit;
        echo json_encode($log);exit;
        exit;
    }
    function deleteselection_video(){
            $tabuidliste = $tabuidlistenew = array();
            $olduidliste = $this->getSession('uidliste');
            //echo __LINE__.' - '.$olduidliste."\n";
            $uidliste = '';
            if( t3lib_div::_GP('uidliste') ) $uidliste = urldecode( t3lib_div::_GP('uidliste') );
            //echo __LINE__.' - '.$uidliste."\n";
            if( $uidliste != '' ){
                 
                $tabuidliste = t3lib_div::trimExplode(',',$olduidliste );
                
                
                $tabuidliste = array_unique( $tabuidliste );
                foreach( $tabuidliste as $uid ){
                    if( $uidliste != $uid ) $tabuidlistenew[] = $uid;
                }
                
                $uidliste = implode(',',$tabuidlistenew);
                //echo __LINE__.' - '.$uidliste."\n"; 
            }
             
            $this->setSession('uidliste',$uidliste);
            
            echo 'La sélection a été mise à jour. Elle contient maintenant '.($uidliste!=''?count(t3lib_div::trimExplode(',',$uidliste )):'aucune').' vidéo(s). Actualisez cette page pour afficher la mise à jour.';
            exit;
    }
    function addselection_video(){
        
            $tabuidliste = array();
            $olduidliste = $this->getSession('uidliste');
            $uidliste = '';
            if( t3lib_div::_GP('uidliste') ) $uidliste = urldecode( t3lib_div::_GP('uidliste') );
            
            
            if( $uidliste != '' ){
                if( $olduidliste != '' ) $uidliste = $olduidliste.','.$uidliste;
                $tabuidliste = t3lib_div::trimExplode(',',$uidliste );
                
                $tabuidliste = array_unique( $tabuidliste );
                
            }
            
            $this->setSession('uidliste',implode(',',$tabuidliste));
            
            echo 'Le(s) vidéo(s) ont été ajoutées à votre sélection. Votre sélection contient maintenant '.count($tabuidliste).' vidéo(s). Actualisez cette page pour afficher cette sélection.';
            exit;
    }
    function savevideonew(){
        
              
             $tabartiste = array();
             if( t3lib_div::_GP('uid')!='' ) $uid = t3lib_div::removeXSS( t3lib_div::_GP('uid') );
             if( t3lib_div::_GP('str')!='' ) $str = t3lib_div::removeXSS( t3lib_div::_GP('str') );
             if( t3lib_div::_GP('type')!='' ) $type = t3lib_div::removeXSS( t3lib_div::_GP('type') );
             
             $parent_category = $this->confArr[($type=='input_artiste2'?'parent_category_artiste':'parent_category_rythme')];
             
             if( $type == 'input_realisation2' ) $parent_category = $this->confArr['parent_category_realisateur'];
              
             //echo $parent_category;exit;
            //SI admin > Enregistrer tout de suite
            if( $this->isadmin() ){
                
                $nb = $GLOBALS['TYPO3_DB']->exec_SELECTcountRows(
                        'uid',
                        'tt_news_cat',
                        '1=1 AND deleted = 0 AND hidden = 0  parent_category = '.$parent_category.' AND title LIKE '.$GLOBALS['TYPO3_DB']->fullQuoteStr($str,'tt_news_cat')
                        );
                if( $nb > 0 ){
                    
                   $rowc = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow(
                        'uid',
                        'tt_news_cat',
                        '1=1 AND deleted = 0 AND hidden = 0  parent_category = '.$parent_category.' AND title LIKE '.$GLOBALS['TYPO3_DB']->fullQuoteStr($str,'tt_news_cat')
                        );
                   $tabartiste[] = $rowc['uid'];
                   
                   $tabartiste = $this->update_cat($uid,$parent_category,$tabartiste);
                   
                                     
                }
                else{
                    $cs = t3lib_div::makeInstance('t3lib_cs');
                    //Save in ttnews_cat
                    $sqli = $GLOBALS['TYPO3_DB']->INSERTquery(
                            'tt_news_cat',
                            array(
                                'pid' => 138
                                ,'title' => $cs->convCaseFirst('utf-8', $str, 'toUpper')
                                ,'parent_category' => $parent_category
                                ,'tstamp' => time()
                                ,'crdate' => time()
                            ));
                    $GLOBALS['TYPO3_DB']->sql_query( $sqli );
                    $tabartiste[] = $GLOBALS['TYPO3_DB']->sql_insert_id();
                    
                    $tabartiste = $this->update_cat($uid,$parent_category,$tabartiste);
                    
                    
                }
                if( $type == 'input_realisation2' ) echo count($tabartiste).' réalisateur(s) appartiennent maintenant à la vidéo.';
                else echo count($tabartiste).' '.($type=='input_artiste2'?'artistes':'genres musicaux').' appartiennent maintenant à la vidéo.';
 
                exit;
                
                

            }
            else{
                
                //http://vvv.madatsara.com/index.php?eID=tsara&task=savevideonew&uid=27410&str=johrr&type=input_artiste2
                echo 'Votre proposition a été envoyée. L\'(es) '.($type=='input_artiste2'?'artistes':'genres musicaux').' que vous avez proposés seront affichés dès validation.';
                exit;
            }
            
    }
    function update_cat($uid,$parent_category,$tabartiste){
                //Get artiste of video
                   $row = $this->util->getcategoriesofarticle( $uid, $parent_category, 1 );
                    if( count( $row ) > 0 ){
                        foreach( $row as $ligne ){
                            $tabartiste[] = $ligne['uid_foreign'];
                        }
                    }
                    
                    $tabartiste = $this->util->array_delete( $tabartiste ) ;
                    $tabartiste = array_unique($tabartiste);
                   
                    //Delete all artiste
                    $sqld = 'DELETE tt_news_cat_mm FROM  tt_news_cat_mm
                                    LEFT JOIN tt_news_cat ON tt_news_cat.uid = tt_news_cat_mm.uid_foreign
                                 WHERE tt_news_cat.parent_category = '.$parent_category.' AND tt_news_cat_mm.uid_local = '.$uid
                             ;
                    $GLOBALS['TYPO3_DB']->sql_query( $sqld );
                    //echo $sqld."\n";
                    //insert
                    foreach( $tabartiste as $uid_foreign ){
                            $sqli = $GLOBALS['TYPO3_DB']->INSERTquery(
                            'tt_news_cat_mm',
                            array(
                                'uid_foreign' => $uid_foreign
                                ,'uid_local' => $uid
                            )
                            );
                            $GLOBALS['TYPO3_DB']->sql_query( $sqli );
                            //echo $sqli."\n";
                    }
                    return $tabartiste;
            }
    function savevideo(){
        
              
             $tabartiste = array();
             if( t3lib_div::_GP('uid')!='' ) $uid = t3lib_div::removeXSS( t3lib_div::_GP('uid') );
             if( t3lib_div::_GP('artiste')!='' ) $artiste = t3lib_div::removeXSS( t3lib_div::_GP('artiste') );
             if( t3lib_div::_GP('playliste')!='' ) $playliste = t3lib_div::removeXSS( t3lib_div::_GP('playliste') );
              
//Get artiste of video
                     
                $row = $this->util->getcategoriesofarticle( $uid, $this->confArr['parent_category_artiste'], 1 );
                
                if( count( $row ) > 0 ){
                    foreach( $row as $ligne ){
                        $tabartiste[] = $ligne['uid_foreign'];
                    }
                }
				
		 
            //SI admin > Enregistrer tout de suite
            if( $this->isadmin() ){
                   
                if( is_array(t3lib_div::trimExplode(',',$artiste)) && count(t3lib_div::trimExplode(',',$artiste))>0 ) $tabartiste = t3lib_div::trimExplode(',',$artiste);
                if( $playliste > 0 ) $tabartiste = array_merge($tabartiste,t3lib_div::trimExplode(',',$artiste));
                
                $tabartiste = $this->util->array_delete( $tabartiste ) ;
                $tabartiste = array_unique($tabartiste);
                //Delete all artiste
                $sqld = 'DELETE tt_news_cat_mm FROM  tt_news_cat_mm
                                LEFT JOIN tt_news_cat ON tt_news_cat.uid = tt_news_cat_mm.uid_foreign
                             WHERE tt_news_cat.parent_category = '.$this->confArr['parent_category_artiste'].' AND tt_news_cat_mm.uid_local = '.$uid
                         ;
                $GLOBALS['TYPO3_DB']->sql_query( $sqld );
                //echo $sqld."\n";
                //insert
                foreach( $tabartiste as $uid_foreign ){
                        $sqli = $GLOBALS['TYPO3_DB']->INSERTquery(
                        'tt_news_cat_mm',
                        array(
                            'uid_foreign' => $uid_foreign
                            ,'uid_local' => $uid
                        )
                        );
                        $GLOBALS['TYPO3_DB']->sql_query( $sqli );
                        //echo $sqli."\n";
                }
                $GLOBALS['TYPO3_DB']->sql_query( "UPDATE tt_news SET tx_tsara_artistecount = (SELECT COUNT(*) FROM tt_news_cat_mm LEFT JOIN tt_news_cat ON tt_news_cat.uid = tt_news_cat_mm.uid_foreign where tt_news_cat.parent_category = ".$this->confArr['parent_category_artiste']." AND tt_news_cat_mm.uid_local = tt_news.uid AND tt_news.pid = 138)");
				
                echo count($tabartiste).' artistes appartiennent maintenant à la vidéo.';
                exit;
                
                

            }
            else{
                $tab_artistetitle = $tab_artisteok = $tabartisteoriginal = $tabartistestitle_o = array();
                //http://vvv.madatsara.com/index.php?eID=tsara&task=savevideo&uid=27410&artiste=15572,15623&playliste=0
                $html = '';
                $tabartisteoriginal = $tabartiste;
                foreach($tabartisteoriginal as $uid_foreign ){
                    $tab_artistetitle[] = '<strong>'.$this->util->getfieldincat( $uid_foreign, 'title' ).'</strong>';
                }
                
                $html .= 'Vidéo : '.$this->util->get_fieldintable( $uid , 'tt_news', 'title' );
                $html .= '<br />URL sur Madatsara :  '.$GLOBALS['TSFE']->baseUrl.$GLOBALS['TSFE']->cObj->typoLink('', array(
                                                                'parameter' => $this->confArr['uidsinglevideo'],
                                                                'additionalParams' => '&tx_ttnews[tt_news]='.$uid,
                                                                'useCacheHash' => 1,
                                                                'returnLast' => 'url',
                        )                                                   );
                $html .= '<br />URL sur Youtube : http://youtu.be/'.$this->util->get_fieldintable( $uid , 'tt_news', 'tx_tsara_youtubeid' );
                
                $html .= '<br /><br />Artistes enregistrés dans la vidéo : '.implode(', ',$tab_artistetitle);
                //$html .= '<br />Différences : '.var_export($this->identical_values( $tabartisteoriginal,t3lib_div::trimExplode(',',$artiste) ),1);
                $tabartistestitle_o = $tab_artistetitle;
                $html .= '<br /><br /><hr /><br /><br />';
                if( is_array(t3lib_div::trimExplode(',',$artiste)) && count(t3lib_div::trimExplode(',',$artiste))>0 && !$this->identical_values( $tabartisteoriginal,t3lib_div::trimExplode(',',$artiste) ) ){
                    unset($tab_artistetitle);
                    $tabartiste = t3lib_div::trimExplode(',',$artiste);
                    foreach($tabartiste as $uid_foreign ){
                        $tab_artistetitle[] = '<strong>'.$this->util->getfieldincat( $uid_foreign, 'title' ).'</strong>';
                        $tab_artisteok[] = '<li>Modifier l\'artiste en seulement : <a href="'.$GLOBALS['TSFE']->baseUrl.'index.php?eID=tsara&task=savevideo&uid='.$uid.'&artiste='.$uid_foreign.'">'.$this->util->getfieldincat( $uid_foreign, 'title' ).'</a><br /></li><li>Ajouter <a href="'.$GLOBALS['TSFE']->baseUrl.'index.php?eID=tsara&task=savevideo&uid='.$uid.'&artiste='.implode(',',array_merge(array($uid_foreign),$tabartisteoriginal)).'">'.$this->util->getfieldincat( $uid_foreign, 'title' ).'</a> à la liste des artistes existants.<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Les artistes dans la vidéo seront donc : '.implode(', ',array_merge(array('<strong>'.$this->util->getfieldincat( $uid_foreign, 'title' ).'</strong>'),$tabartistestitle_o)).'<br />-----<br /></li>';
                    }
                    $html .= 'Proposition de l\'internaute : '.implode(', ',$tab_artistetitle);
                    
                    $html .= '<ul>';
                    
                    $html .= '<li><a href="'.$GLOBALS['TSFE']->baseUrl.'index.php?eID=tsara&task=savevideo&uid='.$uid.'&artiste='.$artiste.'">Accepter la proposition</a> qui est : '.implode(', ',$tab_artistetitle).'</li> ' ;
                    $html .= implode('',$tab_artisteok ).'</ul>';
                }
                
                $html .= '<br />Adresse IP : '.t3lib_div::getIndpEnv('REMOTE_ADDR');
                $html .= '<br />USER_AGENT : '.t3lib_div::getIndpEnv('HTTP_USER_AGENT');
                
                //Envoi mail                 
                if(!$this->identical_values( $tabartisteoriginal,t3lib_div::trimExplode(',',$artiste) )){
				$this->mail(
                            'madatsara@gmail.com',
                            '[Madatsara] Proposition d\'artistes dans la vidéo : '.$this->util->get_fieldintable( $uid , 'tt_news', 'title' )
                            ,$html
                            ,array(
                                'name' => $GLOBALS['TYPO3_CONF_VARS']['MAIL']['defaultMailFromName']
                                ,'email' => $GLOBALS['TYPO3_CONF_VARS']['MAIL']['defaultMailFromAddress']
                            )
                        );
				}
                //if( $playliste > 0 ) $tabartiste = array_merge($tabartiste,t3lib_div::trimExplode(',',$artiste));
                echo 'Votre proposition a été envoyée. L\'(es) artiste(s) que vous avez proposés seront affichés dès validation.';
                exit;
            }
    }
	function identical_values( $arrayA , $arrayB ) {
		sort( $arrayA ); 
		sort( $arrayB ); 
		return $arrayA == $arrayB; 
	}
    function mail($to,$subject,$body,$from=array()){
        $mail = t3lib_div::makeInstance('t3lib_mail_Message');
        $mail->setTo(array($to))
            ->setFrom(array($from['email'] => $from['name'] ))
            ->setSubject( $subject  )
            ->setCharset('utf-8');
        $mail->setBody( $body , 'text/html');
        $mail->send();
        return true;
    }
    function get_address($uid){
         $s = '';
        if( t3lib_div::_GP('s')!='' ) $s = t3lib_div::removeXSS( t3lib_div::_GP('s') );
        if( t3lib_div::_GP('term')!='' ) $s = t3lib_div::removeXSS( t3lib_div::_GP('term') );
        if( $s != '' ){

            $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
            'tt_address.uid AS id,tt_address.name',
            'tt_address
                LEFT JOIN tt_address_group_mm ON tt_address_group_mm.uid_local = tt_address.uid 
                LEFT JOIN tt_address_group ON tt_address_group.uid = tt_address_group_mm.uid_foreign
            ',
            '1=1 AND 
                tt_address_group.deleted=0 AND 
                tt_address_group.hidden = 0  AND 
                tt_address.hidden = 0  AND 
                tt_address.deleted = 0  AND 
                tt_address_group_mm.uid_foreign = '.$uid
            ,''
            ,'tt_address.name ASC'
            );

            echo json_encode( $row );
        }
        exit;
    }
    function get_ttnewscat( $parent ){
        $s = '';
                if( t3lib_div::_GP('s')!='' ) $s = t3lib_div::removeXSS( t3lib_div::_GP('s') );
                if( t3lib_div::_GP('term')!='' ) $s = t3lib_div::removeXSS( t3lib_div::_GP('term') );
                if( $s != '' ){
                         
                        $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
            //'uid AS id,title AS label, title as value',
            'uid AS id,title AS name',
            'tt_news_cat',
            '1=1 AND tt_news_cat.deleted=0 AND tt_news_cat.hidden = 0 AND parent_category = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($parent,'tt_news_cat').' AND title LIKE '.$GLOBALS['TYPO3_DB']->fullQuoteStr('%'.$s.'%','tt_news_cat')
            ,''
            ,'title ASC'
            );

                        echo json_encode( $row );
                }
                exit;
    }
     
    function isadmin(){
        $isadmin = false;
        if( trim( $_COOKIE['be_typo_user'] ) != '' ){
                $be_typo_user = t3lib_div::removeXSS($_COOKIE['be_typo_user']);                
                $rowsession = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow(
                        'a.admin',
                        'be_users AS a LEFT JOIN be_sessions AS b ON b.ses_userid = a.uid ',
                        'a.disable = 0 AND b.ses_id = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($be_typo_user ,'be_sessions') 
                        );                
                $isadmin = ($rowsession['admin']==1?true:false) ;
            }
        return $isadmin;
    }
    function savevideogenre(){
               
             $playliste = 0;
             $tabartiste = array();
             if( t3lib_div::_GP('uid')!='' ) $uid = t3lib_div::removeXSS( t3lib_div::_GP('uid') );
             if( t3lib_div::_GP('genre')!='' ) $genre = t3lib_div::removeXSS( t3lib_div::_GP('genre') );
             if( t3lib_div::_GP('playliste')!='' ) $playliste = t3lib_div::removeXSS( t3lib_div::_GP('playliste') );
             
              //Get genre of video
                $row = $this->util->getcategoriesofarticle( $uid, $this->confArr['parent_category_rythme'], 1 );
                if( count( $row ) > 0 ){
                    foreach( $row as $ligne ){
                        $tabartiste[] = $ligne['uid_foreign'];
                    }
                }
            //SI admin > Enregistrer tout de suite
            if( $this->isadmin() ){
                
				
		if( is_array(t3lib_div::trimExplode(',',$genre)) && count(t3lib_div::trimExplode(',',$genre))>0 ) $tabartiste = t3lib_div::trimExplode(',',$genre);
                if( $playliste > 0 ) $tabartiste = array_merge($tabartiste,t3lib_div::trimExplode(',',$genre));
                
                $tabartiste = $this->util->array_delete( $tabartiste ) ;
                $tabartiste = array_unique($tabartiste);

				//echo json_encode($tabartiste);
                
                //Delete all genre
                $sqld = 'DELETE tt_news_cat_mm FROM  tt_news_cat_mm
                                LEFT JOIN tt_news_cat ON tt_news_cat.uid = tt_news_cat_mm.uid_foreign
                             WHERE tt_news_cat.parent_category = '.$this->confArr['parent_category_rythme'].' AND tt_news_cat_mm.uid_local = '.$uid
                         ;
                $GLOBALS['TYPO3_DB']->sql_query( $sqld );
                //echo $sqld."\n";
                //insert
                foreach( $tabartiste as $uid_foreign ){
                        $sqli = $GLOBALS['TYPO3_DB']->INSERTquery(
                        'tt_news_cat_mm',
                        array(
                            'uid_foreign' => $uid_foreign
                            ,'uid_local' => $uid
                        )
                        );
                        $GLOBALS['TYPO3_DB']->sql_query( $sqli );
                        //echo $sqli."\n";
                }
                
                echo count($tabartiste).' genre(s) musical(ux) appartiennent maintenant à la vidéo.';
                exit;
                
                

            }
            else{
                $tab_artistetitle = $tab_artisteok = $tabartisteoriginal = $tabartistestitle_o = array();
                //http://vvv.madatsara.com/index.php?eID=tsara&task=savevideo&uid=27410&artiste=15572,15623&playliste=0
                $html = '';
                $tabartisteoriginal = $tabartiste;
                foreach($tabartisteoriginal as $uid_foreign ){
                    $tab_artistetitle[] = '<strong>'.$this->util->getfieldincat( $uid_foreign, 'title' ).'</strong>';
                }
                
                $html .= 'Vidéo : '.$this->util->get_fieldintable( $uid , 'tt_news', 'title' );
                $html .= '<br />URL sur Madatsara :  '.$GLOBALS['TSFE']->baseUrl.$GLOBALS['TSFE']->cObj->typoLink('', array(
                                                                'parameter' => $this->confArr['uidsinglevideo'],
                                                                'additionalParams' => '&tx_ttnews[tt_news]='.$uid,
                                                                'useCacheHash' => 1,
                                                                'returnLast' => 'url',
                        )                                                   );
                $html .= '<br />URL sur Youtube : http://youtu.be/'.$this->util->get_fieldintable( $uid , 'tt_news', 'tx_tsara_youtubeid' );
                
                $html .= '<br /><br />Genre musical enregistrés dans la vidéo : '.implode(', ',$tab_artistetitle);
                $tabartistestitle_o = $tab_artistetitle;
                $html .= '<br /><br /><hr /><br /><br />';
                if( is_array(t3lib_div::trimExplode(',',$genre)) && count(t3lib_div::trimExplode(',',$genre))>0 ){
                    unset($tab_artistetitle);
                    $tabartiste = t3lib_div::trimExplode(',',$genre);
                    foreach($tabartiste as $uid_foreign ){
                        $tab_artistetitle[] = '<strong>'.$this->util->getfieldincat( $uid_foreign, 'title' ).'</strong>';
                        $tab_artisteok[] = '<li>Modifier le genre en seulement : <a href="'.$GLOBALS['TSFE']->baseUrl.'index.php?eID=tsara&task=savevideogenre&uid='.$uid.'&genre='.$uid_foreign.'">'.$this->util->getfieldincat( $uid_foreign, 'title' ).'</a><br /></li><li>Ajouter <a href="'.$GLOBALS['TSFE']->baseUrl.'index.php?eID=tsara&task=savevideogenre&uid='.$uid.'&genre='.implode(',',array_merge(array($uid_foreign),$tabartisteoriginal)).'">'.$this->util->getfieldincat( $uid_foreign, 'title' ).'</a> à la liste des genres musicaux existants.<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Les genres musicaux de la vidéo seront donc : '.implode(', ',array_merge(array('<strong>'.$this->util->getfieldincat( $uid_foreign, 'title' ).'</strong>'),$tabartistestitle_o)).'<br />-----<br /></li>';
                    }
                    $html .= 'Proposition de l\'internaute : '.implode(', ',$tab_artistetitle);
                    
                    $html .= '<ul>';
                    
                    $html .= '<li><a href="'.$GLOBALS['TSFE']->baseUrl.'index.php?eID=tsara&task=savevideogenre&uid='.$uid.'&genre='.$genre.'">Accepter la proposition</a> qui est : '.implode(', ',$tab_artistetitle).'</li> ' ;
                    $html .= implode('',$tab_artisteok ).'</ul>';
                }
                
                $html .= '<br />Adresse IP : '.t3lib_div::getIndpEnv('REMOTE_ADDR');
                $html .= '<br />USER_AGENT : '.t3lib_div::getIndpEnv('HTTP_USER_AGENT');
                
                //Envoi mail                 
                $this->mail(
                            'madatsara@gmail.com',
                            '[Madatsara] Proposition de genre musical dans la vidéo : '.$this->util->get_fieldintable( $uid , 'tt_news', 'title' )
                            ,$html
                            ,array(
                                'name' => $GLOBALS['TYPO3_CONF_VARS']['MAIL']['defaultMailFromName']
                                ,'email' => $GLOBALS['TYPO3_CONF_VARS']['MAIL']['defaultMailFromAddress']
                            )
                        );
                echo 'Votre proposition a été envoyée. Le(s) genre(s) musical(ux) que vous avez proposé(s) sera(ont) affiché(s) dès validation.';
                exit;
            }
            
            
         
    }
    function savevideorealisation(){
               
             $playliste = 0;
             $tabartiste = array();
             if( t3lib_div::_GP('uid')!='' ) $uid = t3lib_div::removeXSS( t3lib_div::_GP('uid') );
             if( t3lib_div::_GP('realisateur')!='' ) $realisateur = t3lib_div::removeXSS( t3lib_div::_GP('realisateur') );
             if( t3lib_div::_GP('playliste')!='' ) $playliste = t3lib_div::removeXSS( t3lib_div::_GP('playliste') );
             
              //Get genre of video
                $row = $this->util->getcategoriesofarticle( $uid, $this->confArr['parent_category_realisateur'], 1 );
                if( count( $row ) > 0 ){
                    foreach( $row as $ligne ){
                        $tabartiste[] = $ligne['uid_foreign'];
                    }
                }
            //SI admin > Enregistrer tout de suite
            if( $this->isadmin() ){
                
				
		if( is_array(t3lib_div::trimExplode(',',$realisateur)) && count(t3lib_div::trimExplode(',',$realisateur))>0 ) $tabartiste = t3lib_div::trimExplode(',',$realisateur);
                if( $playliste > 0 ) $tabartiste = array_merge($tabartiste,t3lib_div::trimExplode(',',$realisateur));
                
                $tabartiste = $this->util->array_delete( $tabartiste ) ;
                $tabartiste = array_unique($tabartiste);

				//echo json_encode($tabartiste);
                
                //Delete all genre
                $sqld = 'DELETE tt_news_cat_mm FROM  tt_news_cat_mm
                                LEFT JOIN tt_news_cat ON tt_news_cat.uid = tt_news_cat_mm.uid_foreign
                             WHERE tt_news_cat.parent_category = '.$this->confArr['parent_category_realisateur'].' AND tt_news_cat_mm.uid_local = '.$uid
                         ;
                $GLOBALS['TYPO3_DB']->sql_query( $sqld );
                //echo $sqld."\n";
                //insert
                foreach( $tabartiste as $uid_foreign ){
                        $sqli = $GLOBALS['TYPO3_DB']->INSERTquery(
                        'tt_news_cat_mm',
                        array(
                            'uid_foreign' => $uid_foreign
                            ,'uid_local' => $uid
                        )
                        );
                        $GLOBALS['TYPO3_DB']->sql_query( $sqli );
                        //echo $sqli."\n";
                }
                
                echo count($tabartiste).' réalisateur(s) appartiennent maintenant à la vidéo.';
                exit;
                
                

            }
            else{
                     
                echo 'Votre proposition a été envoyée. Le(s) genre(s) musical(ux) que vous avez proposé(s) sera(ont) affiché(s) dès validation.';
                exit;
            }
            
            
         
    }
    public function setSession($varname,$varcontent) {
            $GLOBALS['TSFE']->fe_user->setKey('ses',$varname,$varcontent);
            $GLOBALS['TSFE']->storeSessionData(); // validate the session
        }

        

        public function getSession ($varname="") {
            if($varname!="") {
                    return $GLOBALS['TSFE']->fe_user->getKey('ses',$varname);
            } else {
                    return $GLOBALS['TSFE']->fe_user->sesData;
            }
        }
      
     
    
 



 

    
 






}

if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/tsara/eid/class.tx_tsara_eID.php']) {
    include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/tsara/eid/class.tx_tsara_eID.php']);
}

// instanciation
$SOBE = t3lib_div::makeInstance('tx_tsara_eID');
$SOBE->main();

?>