<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
if( t3lib_extMgm::isLoaded('powermail') ) require_once(t3lib_extMgm::extPath('powermail') . 'lib/class.tx_powermail_functions_div.php');
if( t3lib_extMgm::isLoaded('ttb_base') ) require_once(t3lib_extMgm::extPath('ttb_base') . 'simple_html_dom.php');
if( t3lib_extMgm::isLoaded('tsara') ) require_once(t3lib_extMgm::extPath("tsara").'lib/class.tx_tsara_util.php');


class tx_tsara_felogin{
    
    public function __construct(){
        $this->util = t3lib_div::makeInstance('tx_tsara_util'); 
    }
    
    


    function forgotPasswordMail(&$params, &$reference ){
        
        $message = $params['message'] ;        
        $user = $params['user'] ; 
        
        $mlog = array();
        $mlog[] = json_encode( $user );
        $mlog[] = __LINE__;
        
        

        $receiver = $user['email'];
        
        $from = $reference->pi_getFFvalue($reference->cObj->data['pi_flexform'],  'from_email', 's_mails');
        $fromName = $reference->pi_getFFvalue($reference->cObj->data['pi_flexform'],  'fromname_email', 's_mails'); 
        $subject = $reference->pi_getFFvalue($reference->cObj->data['pi_flexform'],  'subject_email', 's_mails'); 

        $plainText = '';
        
        $html = $reference->pi_getFFvalue($reference->cObj->data['pi_flexform'],  'corps_email', 's_mails');
        $mlog[] = $html;
        $html = $reference->pi_RTEcssText( $html );
        
        
        //Get passwordclair
        $rowp = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow('passwordclair','fe_users','1=1 '.$GLOBALS['TSFE']->cObj->enableFields('fe_users').' AND `fe_users`.`uid` = '.$GLOBALS['TYPO3_DB']->fullQuoteStr( $user['uid'],'fe_users' ) ) ;
        
        
        //Replace variables in RTE
        $html = str_replace('###email###',$user['email'],$html);
        $html = str_replace('###password###',$rowp['passwordclair'],$html);
        $html = str_replace('###username###',$user['username'],$html);
        
        if( t3lib_extMgm::isLoaded('ttb_base') ) {
            $mlog[] = 'Simple dom est chargé';
            $mlog[] = $html;
            //prefix link with http
            $htmldom = str_get_html($html);
            foreach($htmldom->find('a') as $a) 
            {
                    $href = $a->href;
                    
                    if( mb_strtolower( trim( substr($href,0,4) ) ) != 'http'){
                        $a->href = $GLOBALS['TSFE']->baseUrl.$href;
                    }
            }
            $html = $htmldom->innertext;
            $mlog[] = $html;
        }
        $mlog[] = $fromName;
        
        if( trim( $fromName ) == '' ) $fromName = $GLOBALS['TYPO3_CONF_VARS']['MAIL']['defaultMailFromName'];
        if( trim( $from ) == '' ) $from = $GLOBALS['TYPO3_CONF_VARS']['MAIL']['defaultMailFromAddress'];
        
        
        if( t3lib_extMgm::isLoaded('powermail') ){
            $div = t3lib_div::makeInstance('tx_powermail_functions_div');
            $plainText = $div->makePlain( $html );
            $mlog[] = 'Powermail est chargé';
        }
        else {
            $plainText = strip_tags( $html );
            $mlog[] = 'Powermail est non chargé';
        }
        
        
        
        if (t3lib_div::int_from_ver(TYPO3_version) >= t3lib_div::int_from_ver('4.5')) {
            
            $mlog[] = 'swiftmailer';
            
            // new TYPO3 swiftmailer code
            $mail = t3lib_div::makeInstance('t3lib_mail_Message');
            $mail->setTo(array($receiver))
                ->setFrom(array($from => $fromName))
                ->setSubject($subject)
                ->setCharset($GLOBALS['TSFE']->metaCharset);
            $mail->addPart($plainText, 'text/plain');
            $mail->setBody($html, 'text/html');
            $mail->send();
            
            
            
        }
        else{
            
            $mlog[] = 't3lib_htmlmail';
            
            require_once(PATH_t3lib . 'class.t3lib_htmlmail.php');
            $mail = t3lib_div::makeInstance('t3lib_htmlmail'); // New object: TYPO3 mail class
            $mail->start(); // start htmlmail
            $mail->recipient = $receiver; // main receiver email address
            $mail->recipient_copy = $this->maildata['cc']; // cc field (other email addresses)
            $mail->subject = $subject;
            $mail->from_email = $from; // sender email address
            $mail->from_name = $fromName; // sender email name      
            $mail->charset = $GLOBALS['TSFE']->metaCharset; // set current charset
            $mail->defaultCharset = $GLOBALS['TSFE']->metaCharset; // set current charset
            $mail->addPlain($plainText);
            $mail->setHTML($mail->encodeMsg($html));
            $mail->send($receiver);
            
            
        }
        $mlog[] = 'To: '.$receiver.'/ From : '.$from.'/ Fromname : '.$fromName.'/ Subject : '.$subject.' / Message plaintext : '.$plainText.' / Message html '.($html).'/';
        
        
       //  t3lib_div::writeFile(PATH_site.'typo3temp/logforgotpasswordmail.txt',implode("\n",$mlog ));

        //t3lib_div::devlog(implode("\n",$mlog ), $reference->extKey );
                
        //Modifie le user email apres
        unset( $params['user']['email'] );
        /*
         * Array
        (
            [uid] => 0
            [username] => username
            [password] => passw
            [email] => 
        )
         */
    }
    function login_confirmed( &$reference,&$_params ){
        $tabdata = array();
          
        $uid = $GLOBALS['TSFE']->fe_user->user['uid'];
        
        //Verifier info carte
        $tabdata = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                        'numcarte,codeactivation AS code,codepin, montant as soldecarte, dateactivation as dateheureactivation',
                        'tx_tsara_carte',
                        '1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tx_tsara_carte').' AND feuseruid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($uid,'tx_tsara_carte') 
                    );
          
         
        
       
         if( count( $tabdata ) > 0 ){
             //parcourir les cartes pour verifier le solde
             foreach( $tabdata as $carte ){
                 //Appeler le webservice
                 $wsresult  = $this->util->callwebservice( 'ETAPE3', $carte );
                 //Si tout est OK
                 if(  is_array( $wsresult ) && t3lib_div::inList('000,007',$wsresult['code'] ) ){
                     
                     $solde = $wsresult['solde'];
                     $date = $wsresult['datevalidite'];
                     
                     //Maj la BDD
                     $GLOBALS['TYPO3_DB']->exec_UPDATEquery(
                        'tx_tsara_carte',
                        'numcarte = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($carte['numcarte'],'tx_tsara_carte').' AND feuseruid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($uid,'tx_tsara_carte')
                             ,array(
                                 'montant' => $solde
                                 ,'dateactivation' => $date
                             )
                     );
                     
                 }
                 
                  
             }
         }
        
         //t3lib_div::writeFile(PATH_site.'logpowermail.txt',implode("\n",$mlog ));

        
         return true;
    }
}
?>
