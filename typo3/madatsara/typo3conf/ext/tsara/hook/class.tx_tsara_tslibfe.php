<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of class
 *
 * @author mrabehasy
 */
require_once(t3lib_extMgm::extPath('tsara').'lib/simple_html_dom.php');
require_once(t3lib_extMgm::extPath('tsara').'lib/class.tx_tsara_util.php');

class tx_tsara_tslibfe  {
    function __construct() {
        $this->confArr = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['tsara']);
        $this->util = t3lib_div::makeInstance('tx_tsara_util');
    }
    function main(&$params, &$reference) {
                         
		
			// Keep a copy of the generated page
			$this->content = $params['pObj']->content;
                        
                        //Remplacer tous les liens
                        $this->content = $this->remplacer($this->content);
                        
                        $this->content = $this->replace_shortcode( $this->content );
                        
			$params['pObj']->content =  $this->content;
                        
                        //Si la page en cours est 404, 501 ou 503
                        
                        /*if( t3lib_div::getIndpEnv('REMOTE_ADDR') == '82.230.218.34' ){
                            echo $GLOBALS['TSFE']->id.' - '.$this->confArr['uid404'];
                        }*/
                        
                            if( $this->confArr['uid404'] == $GLOBALS['TSFE']->id ){
                                $file404 = $GLOBALS['TYPO3_CONF_VARS']['FE']['pageNotFound_handling'];
                                $readFile = 'fileadmin/templates/404.html';
                                if (t3lib_div::isFirstPartOfStr($file404,'READFILE:')) {
                                    $readFile = trim(substr($file404,9));
                                }
                                if( t3lib_div::getIndpEnv('REMOTE_ADDR') == '82.230.218.34' ){
                                   // echo PATH_site.$readFile;
                                }
                                t3lib_div::writeFile(PATH_site.$readFile, $this->content);
                            }
                            if( $this->confArr['uid501'] == $GLOBALS['TSFE']->id ){
                                $readFile = 'fileadmin/templates/501.html';
                                t3lib_div::writeFile(PATH_site.$readFile, $this->content);
                            }
                            if( $this->confArr['uid503'] == $GLOBALS['TSFE']->id ){
                                $readFile = 'fileadmin/templates/503.html';
                                t3lib_div::writeFile(PATH_site.$readFile, $this->content);
                            }
                            if( $this->confArr['uidforum'] == $GLOBALS['TSFE']->id ){
                                $readFile = 'fileadmin/templates/forum.html';
                                t3lib_div::writeFile(PATH_site.$readFile, $this->content);
                            }
                             
                        
		
	}
        
        function replace_shortcode($str){
            $tagnames = array_keys(
                    array(
                        'adsense' => 'adsense'
                        ,'videos' => 'videos'
                        ,'event' => 'event'
                        ,'photos' => 'photos'
                        ,'albumfb' => 'albumfb'
                        ,'related' => 'related'
                        )
                    );
            $tagregexp = join( '|', array_map('preg_quote', $tagnames) );
            $pattern = 
		  '\\['                              // Opening bracket
		. '(\\[?)'                           // 1: Optional second opening bracket for escaping shortcodes: [[tag]]
		. "($tagregexp)"                     // 2: Shortcode name
		. '\\b'                              // Word boundary
		. '('                                // 3: Unroll the loop: Inside the opening shortcode tag
		.     '[^\\]\\/]*'                   // Not a closing bracket or forward slash
		.     '(?:'
		.         '\\/(?!\\])'               // A forward slash not followed by a closing bracket
		.         '[^\\]\\/]*'               // Not a closing bracket or forward slash
		.     ')*?'
		. ')'
		. '(?:'
		.     '(\\/)'                        // 4: Self closing tag ...
		.     '\\]'                          // ... and closing bracket
		. '|'
		.     '\\]'                          // Closing bracket
		.     '(?:'
		.         '('                        // 5: Unroll the loop: Optionally, anything between the opening and closing shortcode tags
		.             '[^\\[]*+'             // Not an opening bracket
		.             '(?:'
		.                 '\\[(?!\\/\\2\\])' // An opening bracket not followed by the closing shortcode tag
		.                 '[^\\[]*+'         // Not an opening bracket
		.             ')*+'
		.         ')'
		.         '\\[\\/\\2\\]'             // Closing shortcode tag
		.     ')?'
		. ')'
		. '(\\]?)'; 
             $tab = preg_match_all( "/$pattern/s", $str, $m );
             // $attr = $this->shortcode_parse_atts( $m[3] );
             
              //82.230.218.34
              if( t3lib_div::getIndpEnv('REMOTE_ADDR') == '82.230.218.34' ){
                    //print_r($m);
              }
              
              
             if( count($m[0])<=0 ) return $str;
             
             $i = 0;
             foreach( $m[3] as $shortcode ){
                $shortcode = str_replace('&nbsp;',' ',$shortcode); 
                $attr = $this->shortcode_parse_atts( $shortcode );
                $func_name =  $m[2][$i] ;
                     
                 switch( $func_name ){
                     case 'adsense' :
                         $html = $this->show_adsense($attr);
                         $str = str_replace($m[0][$i],$html,$str);
                     break;
                     default :    
                         $func_name = 'show_'.$func_name;
                         $html = $this->${func_name}($attr);
                         $str = str_replace($m[0][$i],$html,$str);
                     break;
                     
                 }
                 
                

                
                $i++;
             }
             
              
             return $str;
        }
        function show_albumfb($attr =  array()){
            $id = str_replace('&quot;','',$attr['id']);  
            $id = str_replace('.json','',$id);
            $id = str_replace('album_','',$id);
            
            if( empty( $attr['templatefile'] ) ) $attr['templatefile'] = 'fileadmin/templates/shortcode_albumfb.html';
            if( empty( $attr['w'] ) ) $attr['w'] = '150';
            if( empty( $attr['h'] ) ) $attr['h'] = '150';
            
            $templateFile = str_replace('&quot;','',$attr['templatefile']);
            $h = str_replace('&quot;','',$attr['h']);
            $w = str_replace('&quot;','',$attr['w']);
            
            if( !t3lib_utility_Math::convertToPositiveInteger($id) ) return '';
            
            if( !t3lib_utility_Math::convertToPositiveInteger($h) ) $h = '150';
            if( !t3lib_utility_Math::convertToPositiveInteger($w) ) $w = '150';
            
            $json = PATH_site.'fileadmin/json/fbalbum/album_'.$id.'.json';
            
            if(!file_exists( $json ) || filesize( $json ) == 0 ) return '';
            
            $str = t3lib_div::getUrl($json);
            $row = json_decode($str,1);
            
            //return print_r($tab,1);
            $data = array();
            $i = 0;
            foreach ($row['photos'] as $ligne ){
                if( trim(substr($ligne,0,7)) == 'http://' ){
                    $img = 'thbx_'.$w.'_'.$h.'_1_'.str_replace('http://','',$ligne);
                }
                else{
                    $img = 'thb_'.$w.'_'.$h.'_1_/'.$ligne ;
                }
                $data['BOUCLE'][$i]['IMG'] = '<a rel="gallery"  href="'.$ligne.'"><img src="'.$img.'" alt="" /></a>' ;
                $i++;
            }
            $data['ALBUMNAME'] = ($row['albumname']!=''?'&laquo; '.$row['albumname'].' &raquo;':'');
            $data['AUTHOR'] = $row['author'];
            $data['W'] = $w;
            $data['H'] = $h;
                     
            $template = $GLOBALS['TSFE']->cObj->fileResource($templateFile);
            $subpart  = $GLOBALS['TSFE']->cObj->getSubpart($template, '###TEMPLATE###');            
            $content = $GLOBALS['TSFE']->cObj->substituteMarkerAndSubpartArrayRecursive($subpart, $data, '###|###' );
            return $content ;
            
            
                     
        }
        function getuidlocalother($uidf,$uidl,$tablenames){
            $trow = array();
            $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                    'uid_local',
                    'tx_tsara_ttnews',
                    'uid_foreign = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($uidf,'tx_tsara_ttnews').' AND uid_local != '.$GLOBALS['TYPO3_DB']->fullQuoteStr($uidl,'tx_tsara_ttnews').' AND tablenames = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($tablenames,'tx_tsara_ttnews')
                    );
            if(count($row)>0){
                foreach($row as $ligne){
                    $trow[] = $ligne['uid_local'];
                }
            }
            return $trow;
        }
        function show_related($attr =  array()){
             //82.230.218.34
                    if( t3lib_div::getIndpEnv('REMOTE_ADDR') == '82.230.218.34' ){
                       // print_r($_GET);
                    }
            $varttnews = t3lib_div::_GP('tx_ttnews');
            $uidlocal = array();
            if( isset($varttnews) && $varttnews['tt_news'] > 0 ){
                $uid = $varttnews['tt_news'];
                $nb = $GLOBALS['TYPO3_DB']->exec_SELECTcountRows('*','tx_tsara_ttnews','uid_local = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($uid,'tx_tsara_ttnews').' OR uid_foreign = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($uid,'tx_tsara_ttnews')); 
                if( $nb > 0 ){
                    $tabuid = $tab = array();
                    $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                            'uid_local,uid_foreign,tablenames',
                            'tx_tsara_ttnews',
                            'uid_local = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($uid,'tx_tsara_ttnews').' OR uid_foreign = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($uid,'tx_tsara_ttnews')
                           );
                   foreach($row as $ligne ){
                       if( $ligne['uid_local'] == $uid ){
                           $tabuid[] = array( 'uid'=>$ligne['uid_foreign'],'tablenames' => $ligne['tablenames'] );
                           //get uid local
                           $uidlocal = $this->getuidlocalother($ligne['uid_foreign'],$uid,$ligne['tablenames']);
                           foreach($uidlocal as $uid_){
                               $tabuid[] = array( 'uid'=>$uid_,'tablenames' => $ligne['tablenames'] );
                           }
                       }
                       if( $ligne['uid_foreign'] == $uid ){
                           $tabuid[] = array( 'uid'=>$ligne['uid_local'],'tablenames' => $ligne['tablenames'] );
                       }
                   }
                   
                  
              
                   //get PID
                   foreach( $tabuid as $uidliste ){
                       $tab[] = array(
                           'uid' => $uidliste['uid']
                           ,'tablenames' => ($this->util->get_fieldintable( $uidliste['uid'], $uidliste['tablenames'], 'pid')>0?$uidliste['tablenames']:'tt_news')
                           ,'pid' => ($this->util->get_fieldintable( $uidliste['uid'], $uidliste['tablenames'], 'pid')>0?$this->util->get_fieldintable( $uidliste['uid'], $uidliste['tablenames'], 'pid'):$this->util->get_fieldintable( $uidliste['uid'], 'tt_news', 'pid'))
                       );
                   }
                   $tabuid = $tab;
                   unset($tab);
                   
                   
                   //Grouper par PID
                   foreach( $tabuid as $uidliste ){
                       switch( $uidliste['pid'] ) {
                           case '103' : //Presse locale
                               $tab[103]['template'] = ($GLOBALS['TSFE']->id==106?'fileadmin/templates/t3x_related_articles_same.html':'fileadmin/templates/t3x_related_articles.html');
                               $tab[103]['pidsingle'] = '106';
                               $tab[103]['items'][] = array(
                                        'uid' => $uidliste['uid']
                                        ,'title' => $this->util->get_fieldintable( $uidliste['uid'], $uidliste['tablenames'], 'title')
                                        ,'author' => $this->util->get_fieldintable( $uidliste['uid'], $uidliste['tablenames'], 'author')
                                        ,'tablenames' => $uidliste['tablenames']
                                    );
                           break;
                           case '54' : //articles
                               $tab[54]['template'] = 'fileadmin/templates/t3x_related_articles.html';
                               $tab[54]['pidsingle'] = '77';
                               $tab[54]['items'][] = array(
                                        'uid' => $uidliste['uid']
                                        ,'title' => $this->util->get_fieldintable( $uidliste['uid'], $uidliste['tablenames'], 'title')
                                        ,'image' => $this->util->get_fieldintable( $uidliste['uid'], $uidliste['tablenames'], 'image')
                                        ,'tablenames' => $uidliste['tablenames']
                                    );
                           break;
                           case '88' : //Sites
                               $tab[88]['template'] = 'fileadmin/templates/t3x_related_sites.html';
                               $tab[88]['items'][] = array(
                                        'uid' => $uidliste['uid']
                                        ,'title' => $this->util->get_fieldintable( $uidliste['uid'], $uidliste['tablenames'], 'title')
                                        ,'image' => $this->util->get_fieldintable( $uidliste['uid'], $uidliste['tablenames'], 'image')
                                        ,'ext_url' => $this->util->get_fieldintable( $uidliste['uid'], $uidliste['tablenames'], 'ext_url')
                                        ,'tablenames' => $uidliste['tablenames']
                                       ,'w' => '120'
                                        ,'h' => '92'
                                    );
                           break;
                           case '138' : //Videos
                               $tab[138]['template'] = 'fileadmin/templates/t3x_related_videos.html';
                               $tab[138]['pidsingle'] = '141';
                               $tab[138]['items'][] = array(
                                        'uid' => $uidliste['uid']
                                        ,'title' => $this->util->get_fieldintable( $uidliste['uid'], $uidliste['tablenames'], 'title')
                                        ,'tablenames' => $uidliste['tablenames']
                                        ,'pidsingle' => '141'
                                    );
                           break;
                           case '159' : //Photos
                               $tab[159]['template'] = 'fileadmin/templates/t3x_related_photos.html';
                               $tab[159]['pidsingle'] = '160';
                               $tab[159]['items'][] = array(
                                        'uid' => $uidliste['uid']
                                        ,'title' => $this->util->get_fieldintable( $uidliste['uid'], $uidliste['tablenames'], 'title')
                                        ,'tablenames' => $uidliste['tablenames']
                                    );
                           break;
                           case '104' : //Cinemas
                               $tab[104]['template'] = 'fileadmin/templates/t3x_related_cinemas.html';
                               $tab[104]['pidsingle'] = '106';
                               $tab[107]['items'][] = array(
                                        'uid' => $uidliste['uid']
                                        ,'title' => $this->util->get_fieldintable( $uidliste['uid'], $uidliste['tablenames'], 'title')
                                        ,'image' => $this->util->get_fieldintable( $uidliste['uid'], $uidliste['tablenames'], 'image')
                                        ,'tablenames' => $uidliste['tablenames']
                                        ,'template' => 'fileadmin/templates/t3x_related_cinemas.html'
                                        ,'pidsingle' => '106'
                                        ,'w' => '120'
                                        ,'h' => '160'
                                    );
                           break;
                           case '152' : //Livres
                               $tab[152]['template'] = 'fileadmin/templates/t3x_related_livres.html';
                               $tab[152]['pidsingle'] = '153';
                               $tab[152]['items'][] = array(
                                        'uid' => $uidliste['uid']
                                        ,'title' => $this->util->get_fieldintable( $uidliste['uid'], $uidliste['tablenames'], 'title')
                                        ,'image' => $this->util->get_fieldintable( $uidliste['uid'], $uidliste['tablenames'], 'image')
                                        ,'tablenames' => $uidliste['tablenames']
                                       ,'w' => '120'
                                        ,'h' => '160'
                                    );
                           break;
                           case '155' : //Traditions
                               $tab[155]['template'] = 'fileadmin/templates/t3x_related_traditions.html';
                               $tab[155]['pidsingle'] = '156';
                               $tab[155]['items'][] = array(
                                        'uid' => $uidliste['uid']
                                        ,'title' => $this->util->get_fieldintable( $uidliste['uid'], $uidliste['tablenames'], 'title')
                                        ,'image' => $this->util->get_fieldintable( $uidliste['uid'], $uidliste['tablenames'], 'image')
                                        ,'tablenames' => $uidliste['tablenames']
                                    );
                           break;
                           case '55' : //Event
                               $tab[55]['template'] = 'fileadmin/templates/t3x_related_event.html';
                               $tab[55]['pidsingle'] = '146';
                               $tab[55]['items'][] = array(
                                        'uid' => $uidliste['uid']
                                        ,'title' => $this->util->get_fieldintable( $uidliste['uid'], $uidliste['tablenames'], 'title')
                                        ,'image' => $this->util->get_fieldintable( $uidliste['uid'], $uidliste['tablenames'], 'image')
                                        ,'tablenames' => $uidliste['tablenames']
                                        ,'w' => '120'
                                        ,'h' => '160'
                                    );
                           break;
                       }
                   }
                   $tabuid = $tab;
                   unset($tab);
                   
                   //return print_r($tabuid,1);
                    
                    foreach ($tabuid as $k => $v ){
                        $templateFile = $v['template'];
                        $pidsingle = $v['pidsingle'];
                        
                        $data = array();
                        $i = 0;
                        
                        foreach( $v['items'] as $ligne ){
                            $link = $GLOBALS['TSFE']->cObj->typoLink( '', array(
                                'parameter' => $pidsingle
                                ,'additionalParams' => '&tx_ttnews[tt_news]='.$ligne['uid']
                                , 'useCacheHash'  => 1
                                ,'returnLast' => 'url'
                            ));
                            $data['BOUCLE'][$i]['TITLE'] = $ligne['title'];
                            $data['BOUCLE'][$i]['link'] = $link;
                            $data['BOUCLE'][$i]['ext_url'] = $ligne['ext_url'];
                            $data['BOUCLE'][$i]['AUTHOR'] = $ligne['author'];
                            $data['BOUCLE'][$i]['IMAGE'] = ($ligne['image']!=''?'<img src="thb_'.$ligne['w'].'_'.$ligne['h'].'_1_/uploads/pics/'.$ligne['image'].'" alt="" />':'');
                            $data['BOUCLE'][$i]['VIDEOS'] = $this->showvideosof($ligne['uid']);
                            $i++;
                        }
                        
                        if( t3lib_div::getIndpEnv('REMOTE_ADDR') == '82.230.218.34' ){
                          // print_r($data);
                        }
                        
                        $template = $GLOBALS['TSFE']->cObj->fileResource($templateFile);
                        $subpart  = $GLOBALS['TSFE']->cObj->getSubpart($template, '###TEMPLATE###');
                        $content = $GLOBALS['TSFE']->cObj->substituteMarkerAndSubpartArrayRecursive($subpart, $data, '###|###' );
                        unset($data);
                        $html[] = $content;
                        
                           
                        
                    }
                    
                    return implode('<hr />',$html);
                           
                }
            }
            return '';
            
        }
        function showvideosof($value_id){
            $listorderby = 'datetime';
            $ascdesc = 'DESC';
            $listlimit = '4';
            $pidsinglevideo = 141;
            $pidvideo = 22;
            $templatefile  = 'fileadmin/templates/shortcode_videos_related.html';
            
            $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                        'tt_news_cat.title as artiste,tt_news.uid,tt_news.title,tt_news.tx_tsara_youtubeid,tt_news.datetime,tt_news.author',
                        'tt_news 
                            LEFT JOIN tt_news_cat_mm ON tt_news_cat_mm.uid_local = tt_news.uid
                            LEFT JOIN tt_news_cat ON tt_news_cat.uid = tt_news_cat_mm.uid_foreign
                            
                        ',
                        '1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news').' AND tt_news_cat_mm.uid_foreign IN('.$value_id.')'
                        ,'tt_news.uid'
                        ,($listorderby!=''?$listorderby.($ascdesc!=''?' '.$ascdesc:'ASC'):'')
                        ,(t3lib_utility_Math::convertToPositiveInteger( $listlimit )?$listlimit:'')
                );
            $data = array();
            $i = 0;
            foreach ($row as $ligne ){
                $link = $GLOBALS['TSFE']->cObj->typoLink( '', array(
                    'parameter' => $pidsinglevideo
                    ,'additionalParams' => '&tx_ttnews[tt_news]='.$ligne['uid']
                    , 'useCacheHash'  => 1
                    ,'returnLast' => 'url'
                ));
                $data['BOUCLE'][$i]['TITLE'] = $ligne['title'];
                $data['BOUCLE'][$i]['IMG'] = ($ligne['tx_tsara_youtubeid']!=''?'<a href="'.$link.'"><img src="thbx_250__1_i.ytimg.com/vi/'.$ligne['tx_tsara_youtubeid'].'/hqdefault.jpg" alt="" /></a>':'');
                $i++;
            }
            $data['ARTISTEGENRE'] = $this->util->get_fieldintable( $value_id, 'tt_news_cat', 'title'  );
            $data['LINKALL'] = $GLOBALS['TSFE']->cObj->typoLink( '', array(
                    'parameter' => $pidvideo
                    ,'additionalParams' => '&tx_ttnews[artiste][0]='.$value_id
                    , 'useCacheHash'  => 1
                    ,'returnLast' => 'url'
                ));
            $template = $GLOBALS['TSFE']->cObj->fileResource($templatefile);
            $subpart  = $GLOBALS['TSFE']->cObj->getSubpart($template, '###TEMPLATE###');            
            $content = $GLOBALS['TSFE']->cObj->substituteMarkerAndSubpartArrayRecursive($subpart, $data, '###|###' );
            return $content ;
        }
        function show_videos($attr =  array()){
		
            if( empty( $attr['listorderby'] ) ) $attr['listorderby'] = 'datetime';
            if( empty( $attr['ascdesc'] ) ) $attr['ascdesc'] = 'DESC';
            if( empty( $attr['listlimit'] ) ) $attr['listlimit'] = '5';
            if( empty( $attr['templatefile'] ) ) $attr['templatefile'] = 'fileadmin/templates/shortcode_videos.html';
                     
            if( empty($attr['url']) && isset($attr['artiste'] ) ) $attr['url'] = $attr['artiste'];
            
            
            $url = str_replace('&quot;','',$attr['url']);            
            $listOrderBy = str_replace('&quot;','',$attr['listorderby']);
            $ascDesc = str_replace('&quot;','',$attr['ascdesc']);
            $listLimit = str_replace('&quot;','',$attr['listlimit']);
            $templateFile = str_replace('&quot;','',$attr['templatefile']);
            
            $url = str_replace($GLOBALS['TSFE']->baseUrl,'',$url);
            $url = str_replace('.html','',$url);
            $url = str_replace('clips-chansons_','',$url);
            
                     
            
            $pidvideo = 22;
            $pidsinglevideo = 141;
            
            $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow(
                        'tablename,value_id',
                        'tx_realurl_uniqalias',
                        '1=1 AND tablename=\'tt_news_cat\' AND value_alias = '.$GLOBALS['TYPO3_DB']->fullQuoteStr(trim($url),'tx_realurl_uniqalias')
                    );
            
            $value_id = $row['value_id'];
            
            $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                        'tt_news_cat.title as artiste,tt_news.uid,tt_news.title,tt_news.tx_tsara_youtubeid,tt_news.datetime,tt_news.author',
                        'tt_news 
                            LEFT JOIN tt_news_cat_mm ON tt_news_cat_mm.uid_local = tt_news.uid
                            LEFT JOIN tt_news_cat ON tt_news_cat.uid = tt_news_cat_mm.uid_foreign
                            
                        ',
                        '1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news').' AND tt_news_cat_mm.uid_foreign IN('.$value_id.')'
                        ,'tt_news.uid'
                        ,($listOrderBy!=''?$listOrderBy.($ascDesc!=''?' '.$ascDesc:'ASC'):'')
                        ,(t3lib_utility_Math::convertToPositiveInteger( $listLimit )?$listLimit:'')
                );
            $data = array();
            $i = 0;
            if( is_array($row) && count( $row ) > 0 ){
                foreach ($row as $ligne ){
                    $link = $GLOBALS['TSFE']->cObj->typoLink( '', array(
                        'parameter' => $pidsinglevideo
                        ,'additionalParams' => '&tx_ttnews[tt_news]='.$ligne['uid']
                        , 'useCacheHash'  => 1
                        ,'returnLast' => 'url'
                    ));
                    $data['BOUCLE'][$i]['TITLE'] = $ligne['title'];
                    $data['BOUCLE'][$i]['IMG'] = ($ligne['tx_tsara_youtubeid']!=''?'<a href="'.$link.'"><img src="thbx_250__1_i.ytimg.com/vi/'.$ligne['tx_tsara_youtubeid'].'/hqdefault.jpg" alt="" /></a>':'');
                    $i++;
                }
            }
            $data['ARTISTEGENRE'] = $this->util->get_fieldintable( $value_id, 'tt_news_cat', 'title'  );
            $data['LINKALL'] = $GLOBALS['TSFE']->cObj->typoLink( '', array(
                    'parameter' => $pidvideo
                    ,'additionalParams' => '&tx_ttnews[artiste][0]='.$value_id
                    , 'useCacheHash'  => 1
                    ,'returnLast' => 'url'
                ));
            $template = $GLOBALS['TSFE']->cObj->fileResource($templateFile);
            $subpart  = $GLOBALS['TSFE']->cObj->getSubpart($template, '###TEMPLATE###');            
            $content = $GLOBALS['TSFE']->cObj->substituteMarkerAndSubpartArrayRecursive($subpart, $data, '###|###' );
            return $content ;
            //return $row[0]['title'].' - '.$row[0]['image'];
        }
        function show_photos($attr =  array()){
            //print_r($attr);
            if( empty( $attr['listorderby'] ) ) $attr['listorderby'] = 'RAND()';
            if( empty( $attr['listlimit'] ) ) $attr['listlimit'] = '5';
            if( empty( $attr['templatefile'] ) ) $attr['templatefile'] = 'fileadmin/templates/shortcode_photos.html';
            
            if( empty($attr['url']) && isset($attr['album'] ) ) $attr['url'] = $attr['album'];
            
            $url = str_replace('&quot;','',$attr['url']);
            $listOrderBy = str_replace('&quot;','',$attr['listorderby']);
            $listLimit = str_replace('&quot;','',$attr['listlimit']);
            $templateFile = str_replace('&quot;','',$attr['templatefile']);
            
            $url = str_replace($GLOBALS['TSFE']->baseUrl,'',$url);
            $url = str_replace('.html','',$url);
            $url = str_replace('album-photo_','',$url);
            
                     
            
            $pidvideo = 21;
            $pidsinglevideo = 160;
            
            $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow(
                        'tablename,value_id',
                        'tx_realurl_uniqalias',
                        '1=1 AND value_alias = '.$GLOBALS['TYPO3_DB']->fullQuoteStr(trim($url),'tx_realurl_uniqalias')
                    );
            
            $value_id = $row['value_id'];
            
            $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                        'tt_news.title as titlealbum,tx_tsara_videosfilm.*',
                        'tx_tsara_videosfilm 
                            LEFT JOIN tt_news ON tt_news.uid = tx_tsara_videosfilm.tx_tsara_cine_videosfilm 
                            
                        ',
                        '1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news').$GLOBALS['TSFE']->cObj->enableFields('tx_tsara_videosfilm').' AND tt_news.uid IN('.$value_id.')'
                        ,''
                        ,($listOrderBy!=''?$listOrderBy :'')
                        ,(t3lib_utility_Math::convertToPositiveInteger( $listLimit )?$listLimit:'')
                );
              /*echo $GLOBALS['TYPO3_DB']->SELECTquery(
                        'tt_news.title as titlealbum,tx_tsara_videosfilm.*',
                        'tx_tsara_videosfilm 
                            LEFT JOIN tt_news ON tt_news.uid = tx_tsara_videosfilm.tx_tsara_cine_videosfilm 
                            
                        ',
                        '1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news').$GLOBALS['TSFE']->cObj->enableFields('tx_tsara_videosfilm').' AND tt_news.uid IN('.$value_id.')'
                        ,''
                        ,($listOrderBy!=''?$listOrderBy :'')
                        ,(t3lib_utility_Math::convertToPositiveInteger( $listLimit )?$listLimit:'')
                );   */
            $data = array();
            $i = 0;
            foreach ($row as $ligne ){
                $link = $GLOBALS['TSFE']->cObj->typoLink( '', array(
                    'parameter' => $pidsinglevideo
                    ,'additionalParams' => '&tx_ttnews[tt_news]='.$value_id
                    , 'useCacheHash'  => 1
                    ,'returnLast' => 'url'
                ));
                     
                $data['BOUCLE'][$i]['IMG'] = ($ligne['description']!=''?'<a href="'.$link.'"><img src="thbx_150_100_1_'.str_replace('http://','',$ligne['description']).'" alt="" /></a>':'');
                $i++;
            }
            $data['TITLEALBUM'] = $this->util->get_fieldintable( $value_id, 'tt_news', 'title'  );
            $data['LINKALL'] = $GLOBALS['TSFE']->cObj->typoLink( '', array(
                    'parameter' => $pidsinglevideo
                    ,'additionalParams' => '&tx_ttnews[tt_news]='.$value_id
                    , 'useCacheHash'  => 1
                    ,'returnLast' => 'url'
                ));
            $template = $GLOBALS['TSFE']->cObj->fileResource($templateFile);
            $subpart  = $GLOBALS['TSFE']->cObj->getSubpart($template, '###TEMPLATE###');            
            $content = $GLOBALS['TSFE']->cObj->substituteMarkerAndSubpartArrayRecursive($subpart, $data, '###|###' );
            return $content ;
            //return $row[0]['title'].' - '.$row[0]['image'];
        }
        function show_test($attr =  array()){
            return __CLASS__;
        }
        function show_adsense($attr =  array()){
            $html = '<script type="text/javascript"><!--';
            $html .= "\r\n".'google_ad_client = "ca-pub-'.$this->confArr['idadsense'].'";';
            $html .= "\r\n".'google_ad_slot = "'.str_replace('&quot;','',$attr['slot']).'";';
            $html .= "\r\n".'google_ad_width = '.str_replace('&quot;','',$attr['width']).';';
            $html .= "\r\n".'google_ad_height = '.str_replace('&quot;','',$attr['height']).';';
            $html .= "\r\n".'//--></script>';
            $html .= "\r\n".'<script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js"></script>';
            return $html;
        }
         
        function shortcode_parse_atts($text) {
            $atts = array();
            $pattern = '/(\w+)\s*=\s*"([^"]*)"(?:\s|$)|(\w+)\s*=\s*\'([^\']*)\'(?:\s|$)|(\w+)\s*=\s*([^\s\'"]+)(?:\s|$)|"([^"]*)"(?:\s|$)|(\S+)(?:\s|$)/';
            $text = preg_replace("/[\x{00a0}\x{200b}]+/u", " ", $text);
            if ( preg_match_all($pattern, $text, $match, PREG_SET_ORDER) ) {
                    foreach ($match as $m) {
                            if (!empty($m[1]))
                                    $atts[strtolower($m[1])] = stripcslashes($m[2]);
                            elseif (!empty($m[3]))
                                    $atts[strtolower($m[3])] = stripcslashes($m[4]);
                            elseif (!empty($m[5]))
                                    $atts[strtolower($m[5])] = stripcslashes($m[6]);
                            elseif (isset($m[7]) and strlen($m[7]))
                                    $atts[] = stripcslashes($m[7]);
                            elseif (isset($m[8]))
                                    $atts[] = stripcslashes($m[8]);
                    }
            } else {
                    $atts = ltrim($text);
            }
            return $atts;
        }
        
        function remplacer( $body ){


			$mp3Html = '';

			

			//Charger DOM PARSER
			$html = new simple_html_dom;
			$html->load($body, 1);

                        $tlog = array();           

			foreach( $html->find('a' ) as $r ){

				$href =  $r->href;
                                //echo $href."\n";

                                $host = parse_url($href, PHP_URL_HOST);

                                $infoUrl = t3lib_div::split_fileref($href);

                                
				$baseurl = 	$GLOBALS['TSFE']->baseUrl;
				$host_baseurl = parse_url($baseurl, PHP_URL_HOST);

				if( $host!='' && $host != $host_baseurl ){
					
                                    $r->setAttribute('target','_blank'); 

									$tlog[] = 'Host : '.$host.' / Base URL : '.$host_baseurl.' / Lien : '.$href; 
				}

                                if( in_array( $infoUrl['fileext'], array('pdf','doc','rtf','docx','xlsx') ) ) {
                                    $r->setAttribute('target','_blank');
                                }

                                

			}
                        
                        foreach( $html->find('.noarticleinlist' ) as $r ){
                           // $r->parent()->parent()->parent()->setAttribute('style','display:none;');
                        }
			
			/*foreach( $html->find('img' ) as $r ){
				$src =  $r->src;				
				if( substr( mb_strtolower($src),0,7) != 'http://' ){
					$r->src = 'http://assets.madatsara.com/'.$src;
				}	
			} 
			
			foreach( $html->find('link' ) as $r ){
				$href =  $r->href;				
				if( substr( mb_strtolower($href),0,7) != 'http://' ){
					$r->href = 'http://assets.madatsara.com/'.$href;
				}	
			}
			
			foreach( $html->find('script' ) as $r ){
				$src =  $r->src;				
				if( substr( mb_strtolower($src),0,7) != 'http://' ){
					$r->src = 'http://assets.madatsara.com/'.$src;
				}	
			}*/
			
			//t3lib_div::writeFile( PATH_site.'typo3temp/loghooktslibfe.txt',implode("\n",$tlog ) );

			return $html->innertext;

			
        }
}
?>
