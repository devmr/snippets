<?php
 
/**
 * Description of class
 *
 * @author mrabehasy
 */
require_once(t3lib_extMgm::extPath('tsara').'lib/simple_html_dom.php');
require_once(t3lib_extMgm::extPath('tsara').'lib/class.tx_tsara_util.php');

class  tx_tsara_ttnews  {
    
        public function __construct() {
            $this->util = t3lib_div::makeinstance('tx_tsara_util');
            $this->confArr = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['tsara']);
            $this->cs = t3lib_div::makeInstance('t3lib_cs');
        }
        
        function additionalFormSearchFields(&$pObj, &$searchMarkers){
            
            $this->confArr = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['tsara']);
            
            $searchMarkers['###SELECTION###'] = $this->show_selection($pObj);   
            $searchMarkers['###LL_ADVANCEDSEARCH###'] = $pObj->pi_getLL('advancedsearch');
            $searchMarkers['###LL_CLOSEADVANCEDSEARCH###'] = $pObj->pi_getLL('closeadvancedsearch');
            
            $searchMarkers['###LL_filterartiste###'] = $pObj->pi_getLL('filterartiste');
            $searchMarkers['###LL_filtergenre###'] = $pObj->pi_getLL('filtergenre');
            $searchMarkers['###LL_filterregion###'] = $pObj->pi_getLL('filterregion');
            $searchMarkers['###LL_filtertype###'] = $pObj->pi_getLL('filtertype');
            $searchMarkers['###LL_filterlieu###'] = $pObj->pi_getLL('filterlieu');
            
            $searchMarkers['###LL_afficher###'] = $pObj->pi_getLL('afficher');
            $searchMarkers['###LL_pluscommentes###'] = $pObj->pi_getLL('pluscommentes');
            $searchMarkers['###LL_plusvus###'] = $pObj->pi_getLL('plusvus');
            $searchMarkers['###LL_plusaimes###'] = $pObj->pi_getLL('plusaimes');
            $searchMarkers['###LL_duration###'] = $pObj->pi_getLL('duration');
            $searchMarkers['###LL_filterdatele###'] = $pObj->pi_getLL('filterdatele');
            $searchMarkers['###LL_filterdateentrele###'] = $pObj->pi_getLL('filterdateentrele');
            $searchMarkers['###LL_filterdateetle###'] = $pObj->pi_getLL('filterdateetle');
            $searchMarkers['###LL_duo###'] = $pObj->pi_getLL('duo');
            $searchMarkers['###LL_multiple###'] = $pObj->pi_getLL('multiple');
            
            $searchMarkers['###valuestatutbloc###'] = ($pObj->piVars['statusbloc']?$pObj->piVars['statusbloc']:'open');
            $searchMarkers['###VALUE_DATETSTAMPLE###'] = ($pObj->piVars['datetstample']?$pObj->piVars['datetstample']:'');
            $searchMarkers['###VALUE_DATETSTAMPENTRELE###'] = ($pObj->piVars['datetstampentrele']?$pObj->piVars['datetstample']:'');
            $searchMarkers['###VALUE_DATETSTAMPETLE###'] = ($pObj->piVars['datetstampetle']?$pObj->piVars['datetstampetle']:'');
            $searchMarkers['###VALUE_DATE_ENTRELE###'] = ($pObj->piVars['date_entrele']?$pObj->piVars['date_entrele']:'');
            $searchMarkers['###VALUE_DATE_ETLE###'] = ($pObj->piVars['date_etle']?$pObj->piVars['date_etle']:'');
            $searchMarkers['###VALUE_DATE###'] = ($pObj->piVars['date']?$pObj->piVars['date']:'');
            
            $searchMarkers['###SELECTOR_ARTISTE###'] = $this->show_selector($this->confArr['parent_category_artiste'], $pObj, 'artiste' );
             $searchMarkers['###SELECTOR_GENRE###'] = $this->show_selector($this->confArr['parent_category_rythme'], $pObj, 'genre' );
             $searchMarkers['###SELECTOR_TYPE###'] = $this->show_selector($this->confArr['parent_category_type'], $pObj, 'type' );
             $searchMarkers['###SELECTOR_LIEU###'] = $this->show_selector($this->confArr['parent_category_lieu'], $pObj, 'lieu' );
             $searchMarkers['###SELECTOR_REGION###'] = $this->show_selector($this->confArr['parent_category_region'], $pObj, 'region' );
            
            $searchMarkers['###CURRENTcomment###'] = ($pObj->piVars['order'] == 'numcomments'?' current':'');
            $searchMarkers['###CURRENTvus###'] = ($pObj->piVars['order'] == 'countview'?' current':'');
            $searchMarkers['###CURRENTaimes###'] = ($pObj->piVars['order'] == 'numlikes'?' current':'');
            $searchMarkers['###CURRENTduration###'] = ($pObj->piVars['order'] == 'duration'?' current':'');
            $searchMarkers['###CURRENTduo###'] = ($pObj->piVars['filter'] == 'duo'?' current':'');
            $searchMarkers['###CURRENTMULTIPLE###'] = ($pObj->piVars['filter'] == 'multiple'?' current':'');
            
            $searchMarkers['###JSON_REALISATEURS###'] = $this->show_json('realisateurs', $pObj);
            $searchMarkers['###JSON_AUTEURS###'] = $this->show_json('auteurs', $pObj);
            $searchMarkers['###JSON_ACTEURS###'] = $this->show_json('acteurs', $pObj);
            $searchMarkers['###JSON_GENRE###'] = $this->show_json('genre', $pObj);
            $searchMarkers['###VALUE_REALISATEURS###'] = ($pObj->piVars['realisateurs'] != ''?$pObj->piVars['realisateurs']:'');
            $searchMarkers['###VALUE_AUTEURS###'] = ($pObj->piVars['auteurs'] != ''?$pObj->piVars['auteurs']:'');
            $searchMarkers['###VALUE_ACTEURS###'] = ($pObj->piVars['acteurs'] != ''?$pObj->piVars['acteurs']:'');
            $searchMarkers['###VALUE_GENRECINE###'] = ($pObj->piVars['genrecine'] != ''?$pObj->piVars['genrecine']:'');
            
            
            
            $additionalParams = '';
            if(is_array( $pObj->piVars['artiste'] ) ){
                $artiste = $pObj->piVars['artiste'];
                $i = 0;
                foreach( $artiste as $art){
                    $tab_params[] = 'tx_ttnews[artiste]['.$i.']='.$art;
                    $i++;
                }
                $additionalParams .= implode('&',$tab_params );
                unset( $tab_params );
            }
            if(is_array( $pObj->piVars['genre'] ) ){
                $artiste = $pObj->piVars['genre'];
                $i = 0;
                foreach( $artiste as $art){
                    $tab_params[] = 'tx_ttnews[genre]['.$i.']='.$art;
                    $i++;
                }
                $additionalParams .= implode('&',$tab_params );
                unset( $tab_params );
            }
            
            $searchMarkers['###LINK_COMMENT###'] = $GLOBALS['TSFE']->cObj->typoLink( '', array(
             'parameter' => $GLOBALS['TSFE']->id
             ,'addQueryString' => 1
            ,'additionalParams' => '&tx_ttnews[order]=numcomments&'.$additionalParams 
             ,'returnLast' => 'url'
                ,'useCacheHash'  => 1
            )) ;
            $searchMarkers['###LINK_VUS###'] = $GLOBALS['TSFE']->cObj->typoLink( '', array(
             'parameter' => $GLOBALS['TSFE']->id
             ,'addQueryString' => 1
            ,'additionalParams' => '&tx_ttnews[order]=countview&'.$additionalParams 
             ,'returnLast' => 'url'
                ,'useCacheHash'  => 1
            )) ;
            $searchMarkers['###LINK_LIKE###'] = $GLOBALS['TSFE']->cObj->typoLink( '', array(
             'parameter' => $GLOBALS['TSFE']->id
             ,'addQueryString' => 1
            ,'additionalParams' => '&tx_ttnews[order]=numlikes&'.$additionalParams 
             ,'returnLast' => 'url'
             ,'useCacheHash'  => 1   
            )) ;
            $searchMarkers['###LINK_LONG###'] = $GLOBALS['TSFE']->cObj->typoLink( '', array(
             'parameter' => $GLOBALS['TSFE']->id
             ,'addQueryString' => 1
            ,'additionalParams' => '&tx_ttnews[order]=duration&'.$additionalParams 
             ,'returnLast' => 'url'
             ,'useCacheHash'  => 1   
            )) ;
            $searchMarkers['###LINK_DUO###'] = $GLOBALS['TSFE']->cObj->typoLink( '', array(
             'parameter' => $GLOBALS['TSFE']->id
             ,'addQueryString' => 1
            ,'additionalParams' => '&tx_ttnews[filter]=duo&'.$additionalParams 
             ,'returnLast' => 'url'
             ,'useCacheHash'  => 1   
            )) ;
            $searchMarkers['###LINK_MULTIPLE###'] = $GLOBALS['TSFE']->cObj->typoLink( '', array(
             'parameter' => $GLOBALS['TSFE']->id
             ,'addQueryString' => 1
            ,'additionalParams' => '&tx_ttnews[filter]=multiple&'.$additionalParams 
             ,'returnLast' => 'url'
             ,'useCacheHash'  => 1   
            )) ;
            
            return $searchMarkers;
            
        } 
        function show_json( $valeur, $pObj ){
            $data = array();
            
            switch( $valeur ){
                case 'realisateurs' :
                    if( $pObj->piVars['realisateurs'] != '' ){
                         
                           $data = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                                   'tt_address.uid as id,tt_address.name',
                                   'tt_address '
                                   ,'1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_address').' AND tt_address.uid  IN ('.t3lib_div::removeXSS( $pObj->piVars['realisateurs'] ).')'
                                   );
                    }
                break;
                case 'auteurs' :
                    if( $pObj->piVars['auteurs'] != '' ){
                         
                           $data = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                                   'tt_address.uid as id,tt_address.name',
                                   'tt_address '
                                   ,'1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_address').' AND tt_address.uid  IN ('.t3lib_div::removeXSS( $pObj->piVars['auteurs'] ).')'
                                   );
                    }
                break;
                case 'acteurs' :
                    if( $pObj->piVars['acteurs'] != '' ){
                         
                           $data = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                                   'tt_address.uid as id,tt_address.name',
                                   'tt_address '
                                   ,'1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_address').' AND tt_address.uid  IN ('.t3lib_div::removeXSS( $pObj->piVars['acteurs'] ).')'
                                   );
                    }
                break;
                case 'genre' :
                    if( $pObj->piVars['genrecine'] != '' ){
                         
                           $data = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                                   'tt_news_cat.uid as id,tt_news_cat.title AS name',
                                   'tt_news_cat '
                                   ,'1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news_cat').' AND tt_news_cat.uid  IN ('.t3lib_div::removeXSS( $pObj->piVars['genrecine'] ).')'
                                   );
                    }
                break;
            }
             
            return json_encode( $data  );
        }
        function show_selection( $pobj ){
            $html = '';
            $tab_selection = array();
            $swords = t3lib_div::removeXSS($pobj->piVars['swords']!=''?$pobj->piVars['swords']:'');
            $artiste = (is_array($pobj->piVars['artiste'])?$pobj->piVars['artiste']:array());
            $genre = (is_array($pobj->piVars['genre'])?$pobj->piVars['genre']:array());
            
            if( $swords == '' && count( $artiste ) <= 0 && count($genre) <= 0 ) return '';
            
            if( $swords != '' ){
                $tab_selection[] = $pobj->cObj->stdWrap( sprintf( $pobj->conf['tsara.']['swords'],$swords), $pobj->conf['tsara.']['swords_stdWrap.']);
            }
            
            if( count( $artiste ) > 0 ){
                $tab_selection[] = $pobj->cObj->stdWrap( sprintf( $pobj->conf['tsara.']['artiste'],$this->showtitle( $artiste, $this->confArr['parent_category_artiste'], $pobj ) ), $pobj->conf['tsara.']['artiste_stdWrap.']);
            }
            
            if( count($genre) > 0 ){
                $tab_selection[] = $pobj->cObj->stdWrap( sprintf( $pobj->conf['tsara.']['genre'],$this->showtitle( $genre, $this->confArr['parent_category_rythme'], $pobj ) ), $pobj->conf['tsara.']['genre_stdWrap.']);
            }
            $data['ALL'] = implode(" ", $tab_selection );
            $subpart  = $pobj->cObj->getSubpart($pobj->templateCode, '###TEMPLATE_SELECTIONALL###' );
            $html = $pobj->cObj->substituteMarkerAndSubpartArrayRecursive($subpart, $data, '###|###' );
            return $html;
            
        }
        
        function show_playall( $pobj, $uidliste ){
            $html = '';
            $tab_selection = array();
            $swords = t3lib_div::removeXSS($pobj->piVars['swords']!=''?$pobj->piVars['swords']:'');
            $filter = t3lib_div::removeXSS($pobj->piVars['filter']!=''?$pobj->piVars['filter']:'');
            $artiste = (is_array($pobj->piVars['artiste'])?$pobj->piVars['artiste']:array());
            $genre = (is_array($pobj->piVars['genre'])?$pobj->piVars['genre']:array());
            
            if($filter == '' && $swords == '' && count( $artiste ) <= 0 && count($genre) <= 0 ) return '';
            
              
            $data['LISTID'] = $uidliste;
$data['PLAYALL'] = ($this->getSession('uidliste')!=''?$this->show_playerall($pobj):'');
            $subpart  = $pobj->cObj->getSubpart($pobj->templateCode, '###TEMPLATE_PLAYALL###' );
            $html = $pobj->cObj->substituteMarkerAndSubpartArrayRecursive($subpart, $data, '###|###' );
            return $html;
            
        }
        function show_acteursfilm($row, $pobj){
            $html = '';
            $id = $GLOBALS['TSFE']->id;
            if($pobj->config['code'] == 'SINGLE' ){
                 $id = $pobj->config['backPid'];
            }
            $uid = $row['uid'];
            
            if( $row['tx_tsara_cine_acteurs'] <= 0 ) return $pobj->cObj->stdWrap( $pobj->pi_getLL('emptyitem'), $pobj->conf['tsara.']['emptyitem_stdWrap.']);
            
            $countvideo = $GLOBALS['TYPO3_DB']->exec_SELECTcountRows(
                    'uid',
                    'tx_tsara_cine_acteurs',
                    '1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tx_tsara_cine_acteurs').' AND tx_tsara_cine_acteurs = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($uid,'tx_tsara_cine_acteurs')
                        );
            
            if( $countvideo <= 0 ) return $pobj->cObj->stdWrap( $pobj->pi_getLL('emptyitem'), $pobj->conf['tsara.']['emptyitem_stdWrap.']);          
            $row = array();
            $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                    'tt_address.uid,tt_address.name,tt_address.image,tx_tsara_cine_acteurs.title',
                    'tx_tsara_cine_acteurs LEFT JOIN tt_address ON tt_address.uid = tx_tsara_cine_acteurs.ttaddressuid',
                    '1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tx_tsara_cine_acteurs').$GLOBALS['TSFE']->cObj->enableFields('tt_address').' AND tx_tsara_cine_acteurs = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($uid,'tx_tsara_cine_acteurs')
                        );
            
             
            if( count( $row )>0){
                $subpart  = $pobj->cObj->getSubpart($pobj->templateCode, '###TEMPLATE_ACTEURSFILM###' );
                $i = 0;
                foreach($row as $ligne ){
                    $data['CONTENT'][$i]['image'] =   (is_file(PATH_site.'uploads/pics/'.$ligne['image'])?'<img src="thb_'.$pobj->conf['tsara.']['photoacteurfim.']['width'].'_'.$pobj->conf['tsara.']['photoacteurfim.']['height'].'_1_/uploads/pics/'.$ligne['image'].'" alt="" />':'');
                    $data['CONTENT'][$i]['name'] = $ligne['name'];
                    $data['CONTENT'][$i]['title'] = $ligne['title'];
                    $data['CONTENT'][$i]['link'] = $GLOBALS['TSFE']->cObj->typoLink( '', array(
             'parameter' => $id    
             ,'additionalParams' => '&tx_ttnews[acteurs]='.$ligne['uid']
             ,'returnLast' => 'url'
             ,'useCacheHash'  => 1   
            )) ;
                    $i++;
                }
                $html = $pobj->cObj->substituteMarkerAndSubpartArrayRecursive($subpart, $data, '###|###' );
            }
            return $html;
        }
        function show_videofilm($row, $pobj){
            $html = '';
            
            $uid = $row['uid'];
            
            if( $row['tx_tsara_cine_videosfilm'] <= 0 ) return $pobj->cObj->stdWrap( $pobj->pi_getLL('emptyitem'), $pobj->conf['tsara.']['emptyitem_stdWrap.']);
            
            $countvideo = $GLOBALS['TYPO3_DB']->exec_SELECTcountRows(
                    'uid',
                    'tx_tsara_videosfilm',
                    '1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tx_tsara_videosfilm').' AND tx_tsara_cine_videosfilm = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($row['uid'],'tx_tsara_videosfilm')
                        );
            
            if( $countvideo <= 0 ) return $pobj->cObj->stdWrap( $pobj->pi_getLL('emptyitem'), $pobj->conf['tsara.']['emptyitem_stdWrap.']);
            
            $row = array();
            $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                    'tx_tsara_videosfilm.url,tx_tsara_videosfilm.title, 
                        CASE
                            WHEN tx_tsara_videosfilm.typecompte in ("ba") THEN "Bande Annonce"
                            WHEN tx_tsara_videosfilm.typecompte in ("extrait") THEN "Extrait"
                        END AS type
                    ',
                    'tx_tsara_videosfilm',
                    '1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tx_tsara_videosfilm').' AND tx_tsara_cine_videosfilm = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($uid,'tx_tsara_videosfilm')
                        );
             
            if( count( $row )>0){
                $subpart  = $pobj->cObj->getSubpart($pobj->templateCode, '###TEMPLATE_VIDEOSFILM###' );
                $i = 0;
                foreach($row as $ligne ){
                     
                    $data['CONTENT'][$i]['url'] = $ligne['url'];
$data['CONTENT'][$i]['url_thumbpicasa'] = str_replace('http://','',$ligne['url']);
$data['CONTENT'][$i]['url_bigpicasa'] = str_replace('s288','s900',$ligne['url']);
                    $data['CONTENT'][$i]['title'] = $ligne['title'];
                    $data['CONTENT'][$i]['type'] = $ligne['type'];
                    $data['CONTENT'][$i]['SEP'] = (($i+1)!=$countvideo && $countvideo>1?$pobj->conf['tsara.']['separator']:'');
                    $i++;
                }
                $html = $pobj->cObj->substituteMarkerAndSubpartArrayRecursive($subpart, $data, '###|###' );
            }
else return $pobj->cObj->stdWrap( $pobj->pi_getLL('emptyitem'), $pobj->conf['tsara.']['emptyitem_stdWrap.']);
            return $html;
        }
        function show_photofilm($row, $pobj){
 
            $html = '';
            if( trim($row['news_files']) == '' ){  return $pobj->cObj->stdWrap( $pobj->pi_getLL('emptyitem'), $pobj->conf['tsara.']['emptyitem_stdWrap.']);}
            $news_files = $row['news_files'];
            $row = array();
            $row = t3lib_div::trimExplode(',',$news_files);

            if( count( $row )>0){
                $subpart  = $pobj->cObj->getSubpart($pobj->templateCode, '###TEMPLATE_PHOTOFILM###' );
                $i = 0;
                foreach($row as $file ){
                     
                    $data['CONTENT'][$i]['image_mini'] = (file_exists(PATH_site.'uploads/media/'.$file)?'<img src="thb_'.$pobj->conf['tsara.']['photofim.']['width'].'_'.$pobj->conf['tsara.']['photofim.']['height'].'_1_/uploads/media/'.$file.'" alt="" />':'');
                    $data['CONTENT'][$i]['image_original'] = (file_exists(PATH_site.'uploads/media/'.$file)?'uploads/media/'.$file :'');
                    $data['CONTENT'][$i]['SEP'] = (($i+1)!=count( $row ) && count( $row )>1?$pobj->conf['tsara.']['separator']:'');
                    $i++;
                }
                $html = $pobj->cObj->substituteMarkerAndSubpartArrayRecursive($subpart, $data, '###|###' );
            }
else return $pobj->cObj->stdWrap( $pobj->pi_getLL('emptyitem'), $pobj->conf['tsara.']['emptyitem_stdWrap.']);
            return $html;
        }
function show_auteur( $row, $pobj, $paramlink = 'auteurs' ){
    $id = $GLOBALS['TSFE']->id;
    if($pobj->config['code'] == 'SINGLE' ){
         $id = $pobj->config['backPid'];
    }
    if( t3lib_utility_Math::convertToPositiveInteger( $pobj->conf['tsara.']['pidacteur'] ) ) $id = $pobj->conf['tsara.']['pidacteur'];
    
    $html = '';
    if( count( $row )  > 0 ){
        $subpart  = $pobj->cObj->getSubpart($pobj->templateCode, '###TEMPLATE_AUTEUR###' );
        $i = 0;
        foreach( $row as $ligne ){

            $data['CONTENT'][$i]['link'] = $GLOBALS['TSFE']->cObj->typoLink( '', array(
             'parameter' => $id    
             ,'additionalParams' => '&tx_ttnews['.$paramlink.']='.$ligne['uid']
             ,'returnLast' => 'url'
             ,'useCacheHash'  => 1   
            )) ;
            $data['CONTENT'][$i]['name'] = $ligne['name'];
            $data['CONTENT'][$i]['SEP'] = (($i+1)!=count( $row ) && count( $row )>1?$pobj->conf['tsara.']['separator']:'');
            $i++;
        }
        $html = $pobj->cObj->substituteMarkerAndSubpartArrayRecursive($subpart, $data, '###|###' ); 
    }
else return $pobj->cObj->stdWrap( $pobj->pi_getLL('emptyitem'), $pobj->conf['tsara.']['emptyitem_stdWrap.']);
    return $html;
}
     
 
function show_playerall($pobj){
 $html = '';
            $tab_selection = array();
            $pid_playall = $pobj->conf['tsara.']['pid_playall'];
            
            if( $pid_playall <= 0 ) return '';
            
              
            $data['LINK_PLAYALL'] = $GLOBALS['TSFE']->cObj->typoLink( '', array(
             'parameter' => $pid_playall           
             ,'returnLast' => 'url'
             ,'useCacheHash'  => 1   
            )) ;
 
            $subpart  = $pobj->cObj->getSubpart($pobj->templateCode, '###TEMPLATE_PLAYERALL###' );
            $html = $pobj->cObj->substituteMarkerAndSubpartArrayRecursive($subpart, $data, '###|###' );
            return $html;
}
        
        function showtitle( $row, $parent, $pobj ){
             
            $uidg = implode(',',$row);
            $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                    'uid,title,image',
                    'tt_news_cat',
                    '1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news_cat').' AND uid IN('.$uidg.') AND parent_category = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($parent,'tt_news_cat')
                    ,''
                    ,'title ASC'
                    );
            
             
             
            $crow = $GLOBALS['TYPO3_DB']->exec_SELECTcountRows(
                    'uid',
                    'tt_news_cat',
                    '1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news_cat').' AND uid IN('.$uidg.') AND parent_category = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($parent,'tt_news_cat')
                    
                    );
            if( $crow == 0 ) return '';
            if( $crow  > 0 ){
                 
                $subpart  = $pobj->cObj->getSubpart($pobj->templateCode, '###TEMPLATE_SELECTIONTITLE###' );
                
                
                $i = 0;
                foreach( $row as $ligne ){
                    
                    $data['CONTENT'][$i]['title'] = $ligne['title'];
                    $data['CONTENT'][$i]['SEP'] = (($i+1)!=$crow && $crow>1?$pobj->conf['tsara.']['separator']:'');
                    $i++;
                }
                
                $html = $pobj->cObj->substituteMarkerAndSubpartArrayRecursive($subpart, $data, '###|###' ); 


            }
            return $html;
        }
        function show_selector( $parent, $pobj, $name ){
            $html = '';
            $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                    'uid,title,image',
                    'tt_news_cat',
                    '1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news_cat').' AND parent_category = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($parent,'tt_news_cat')
                    ,''
                    ,'title ASC'
                    );
            $crow = $GLOBALS['TYPO3_DB']->exec_SELECTcountRows(
                    'uid',
                    'tt_news_cat',
                    '1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news_cat').' AND parent_category = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($parent,'tt_news_cat')
                     
                    );
            if( $crow == 0 ) return '';
            if( $crow  > 0 ){
                 
                $subpart  = $pobj->cObj->getSubpart($pobj->templateCode, '###TEMPLATE_SELECTOR###' );
                $data['name'] = $name;
                
                
                $i = 0;
                foreach( $row as $ligne ){
                    $image = ($ligne['image']!='' && file_exists(PATH_site.'uploads/pics/'.$ligne['image'])?' data-image="thb_'.$pobj->conf['tsara.']['selectorw'].'_'.$pobj->conf['tsara.']['selectorh'].'_1_/uploads/pics/'.$ligne['image'].'"':'');
                    
                    $data['CONTENT'][$i]['title'] = str_replace('_Rythme','',$ligne['title']);
                    $data['CONTENT'][$i]['uid'] = $ligne['uid'];
                    $data['CONTENT'][$i]['name'] = $name;
                    $data['CONTENT'][$i]['image'] = $image;
                    $data['CONTENT'][$i]['checked'] = (is_array($pobj->piVars[$name]) && in_array($ligne['uid'],$pobj->piVars[$name])?' checked="checked"':'');
                    $data['CONTENT'][$i]['selected'] = (is_array($pobj->piVars[$name]) && in_array($ligne['uid'],$pobj->piVars[$name])?' selected="selected"':'');
                    $i++;
                }
                
                $html = $pobj->cObj->substituteMarkerAndSubpartArrayRecursive($subpart, $data, '###|###' ); 


            }
            
            return $html;
        }
    
	function processSelectConfHook(&$pObj, &$selectConf){
                
            $this->confArr = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['tsara']);
            // print_r( $pObj->piVars  );
                if($pObj->config['code'] == 'SEARCH'){
                    
                    if(  t3lib_div::inList( $this->confArr['uidevenement'], $GLOBALS['TSFE']->id ) ){
                        $selectConf['where'] .= " AND (FROM_UNIXTIME(`tt_news`.`tx_tsara_event_date`,'%Y-%m-%d') >= ".$GLOBALS['TYPO3_DB']->fullQuoteStr( date('Y-m-d'),'tt_news')." OR ('".date('Y-m-d')."' BETWEEN FROM_UNIXTIME(`tt_news`.`tx_tsara_event_date`,'%Y-%m-%d') AND FROM_UNIXTIME(`tt_news`.`tx_tsara_event_date_end`,'%Y-%m-%d')))";               
                        $selectConf['orderBy']  = " `tt_news`.`tx_tsara_event_date` ASC";               
                    }
                    
                    if(  t3lib_div::inList( $this->confArr['pid_videos'], $GLOBALS['TSFE']->id ) ){
                        $selectConf['leftjoin'] .= ' LEFT JOIN tt_news_cat ON tt_news_cat.uid = tt_news_cat_mm.uid_foreign ';
                    
                        if( $pObj->piVars['swords'] != '' ){
                            $selectConf['where'] .= ' OR (tt_news_cat.title LIKE '.$GLOBALS['TYPO3_DB']->fullQuoteStr( '%'.$pObj->piVars['swords'].'%','tt_news_cat').')';
                        }
                    }
                    
                    
                    if(   t3lib_utility_Math::convertToPositiveInteger( $pObj->piVars['author'] )  ){
                        $selectConf['where'] .= ' AND tt_news.tx_tsara_author = '.$GLOBALS['TYPO3_DB']->fullQuoteStr( $pObj->piVars['author'],'tt_news');
                    }
                    if(  $pObj->piVars['filter'] == 'duo'   ){
                        $selectConf['where'] .= ' AND tt_news.tx_tsara_artistecount = 2';
                    }
                    if(  $pObj->piVars['filter'] == 'multiple'   ){
                        $selectConf['where'] .= ' AND tt_news.tx_tsara_artistecount > 2';
                    }
                    
                    
                    
                    if(   t3lib_utility_Math::convertToPositiveInteger( $pObj->piVars['pluslus'] )  ){
                        $selectConf['orderBy']  = 'tx_newsreadedcount_readedcounter DESC ' ;
                    }
                    
                    if(    $pObj->piVars['order'] == 'numcomments'   ){
                        $selectConf['orderBy']  = 'tx_tsara_youtube_numcomment DESC ' ;
                    }
                    if(   $pObj->piVars['order'] == 'countview'   ){
                        $selectConf['orderBy']  = 'tx_tsara_youtube_viewcount DESC ' ;
                    }
                    if(   $pObj->piVars['order'] == 'numlikes'   ){
                        $selectConf['orderBy']  = 'tx_tsara_youtube_numlikes DESC ' ;
                    }
                    if(   $pObj->piVars['order'] == 'duration'   ){
                        $selectConf['orderBy']  = 'tx_tsara_youtube_duration DESC ' ;
                    }
                    
                    if( (is_array($pObj->piVars['artiste']) || is_array($pObj->piVars['genre']) || is_array($pObj->piVars['region']) || is_array($pObj->piVars['type']) || is_array($pObj->piVars['lieu'])) && (count($pObj->piVars['artiste'])>0 || count($pObj->piVars['genre'])>0 || count($pObj->piVars['type'])>0 || count($pObj->piVars['lieu'])>0 || count($pObj->piVars['region'])>0)  ){
                        $tab_groupe[] =  $pObj->piVars['artiste'];
                        $tab_groupe[] =  $pObj->piVars['genre'];
                        $tab_groupe[] =  $pObj->piVars['type'];
                        $tab_groupe[] =  $pObj->piVars['region'];
                        $tab_groupe[] =  $pObj->piVars['lieu'];
                        
                        $tgr = $this->flatten_array($tab_groupe) ;
                        unset($tab_groupe);
                        foreach( $tgr as $groupe  ){
                            if( trim( $groupe ) != '' ) $tab_groupe[] = $groupe;
                        }
                        
                        $id_groupe = implode(',', $tab_groupe  );
                        $selectConf['where'] .= ' AND tt_news_cat_mm.uid_foreign IN('.$id_groupe.')';
                        
                        
                    }
                    
                    if(   
                            $pObj->piVars['auteurs'] != '' || 
                            $pObj->piVars['realisateurs'] != '' || 
                            $pObj->piVars['acteurs'] != ''   ||
                            $pObj->piVars['production'] != ''  || 
                            $pObj->piVars['scenario'] != ''  || 
                            $pObj->piVars['directeur_photo'] != ''  || 
                            $pObj->piVars['montage_son'] != ''  || 
                            $pObj->piVars['perchman'] != ''  || 
                            $pObj->piVars['effets_visuels'] != '' 
                       ){
                        
                        if( $pObj->piVars['acteurs'] != '' ){
                            $selectConf['leftjoin'] .= ' LEFT JOIN tx_tsara_cine_acteurs_mm ON tx_tsara_cine_acteurs_mm.uid_local = tt_news.uid ';
                            $selectConf['where'] .= ' AND tx_tsara_cine_acteurs_mm.uid_foreign IN('.t3lib_div::removeXSS( $pObj->piVars['acteurs'] ).')';
                        }
                        if( $pObj->piVars['realisateurs'] != '' ){
                            $selectConf['leftjoin'] .= ' LEFT JOIN tx_tsara_cine_realisateurs_mm ON tx_tsara_cine_realisateurs_mm.uid_local = tt_news.uid ';
                            $selectConf['where'] .= ' AND tx_tsara_cine_realisateurs_mm.uid_foreign IN('.t3lib_div::removeXSS( $pObj->piVars['realisateurs'] ).')';
                            
                        }
                        if( $pObj->piVars['auteurs'] != '' ){
                            $selectConf['leftjoin'] .= ' LEFT JOIN tx_tsara_cine_auteurs_mm ON tx_tsara_cine_auteurs_mm.uid_local = tt_news.uid ';
                            $selectConf['where'] .= ' AND tx_tsara_cine_auteurs_mm.uid_foreign IN('.t3lib_div::removeXSS( $pObj->piVars['auteurs'] ).')';
                        }
                        if( $pObj->piVars['production'] != '' ){
                            $selectConf['leftjoin'] .= ' LEFT JOIN tx_tsara_cine_production_mm ON tx_tsara_cine_production_mm.uid_local = tt_news.uid ';
                            $selectConf['where'] .= ' AND tx_tsara_cine_production_mm.uid_foreign IN('.t3lib_div::removeXSS( $pObj->piVars['production'] ).')';
                        }
                        if( $pObj->piVars['scenario'] != '' ){
                            $selectConf['leftjoin'] .= ' LEFT JOIN tx_tsara_cine_scenario_mm ON tx_tsara_cine_scenario_mm.uid_local = tt_news.uid ';
                            $selectConf['where'] .= ' AND tx_tsara_cine_scenario_mm.uid_foreign IN('.t3lib_div::removeXSS( $pObj->piVars['scenario'] ).')';
                        }
                        if( $pObj->piVars['directeur_photo'] != '' ){
                            $selectConf['leftjoin'] .= ' LEFT JOIN tx_tsara_cine_directeur_photo_mm ON tx_tsara_cine_directeur_photo_mm.uid_local = tt_news.uid ';
                            $selectConf['where'] .= ' AND tx_tsara_cine_directeur_photo_mm.uid_foreign IN('.t3lib_div::removeXSS( $pObj->piVars['directeur_photo'] ).')';
                        }
                        if( $pObj->piVars['montage_son'] != '' ){
                            $selectConf['leftjoin'] .= ' LEFT JOIN tx_tsara_cine_montage_son_mm ON tx_tsara_cine_montage_son_mm.uid_local = tt_news.uid ';
                            $selectConf['where'] .= ' AND tx_tsara_cine_montage_son_mm.uid_foreign IN('.t3lib_div::removeXSS( $pObj->piVars['montage_son'] ).')';
                        }
                        if( $pObj->piVars['perchman'] != '' ){
                            $selectConf['leftjoin'] .= ' LEFT JOIN tx_tsara_cine_perchman_mm ON tx_tsara_cine_perchman_mm.uid_local = tt_news.uid ';
                            $selectConf['where'] .= ' AND tx_tsara_cine_perchman_mm.uid_foreign IN('.t3lib_div::removeXSS( $pObj->piVars['perchman'] ).')';
                        }
                        if( $pObj->piVars['effets_visuels'] != '' ){
                            $selectConf['leftjoin'] .= ' LEFT JOIN tx_tsara_cine_effets_visuels_mm ON tx_tsara_cine_effets_visuels_mm.uid_local = tt_news.uid ';
                            $selectConf['where'] .= ' AND tx_tsara_cine_effets_visuels_mm.uid_foreign IN('.t3lib_div::removeXSS( $pObj->piVars['effets_visuels'] ).')';
                        }
                    
                        
                    }
                    if(   $pObj->piVars['genrecine'] != ''   ){
                        $selectConf['where'] .= ' AND tt_news_cat_mm.uid_foreign IN('.t3lib_div::removeXSS( $pObj->piVars['genrecine'] ).')';
                    }
                    
                    if(  t3lib_utility_Math::convertToPositiveInteger($pObj->piVars['datetstample']) || t3lib_utility_Math::convertToPositiveInteger($pObj->piVars['datetstampentrele']) || t3lib_utility_Math::convertToPositiveInteger($pObj->piVars['datetstampetle'])   ){
                        
                        $datetstample = (t3lib_utility_Math::convertToPositiveInteger($pObj->piVars['datetstample'])?t3lib_utility_Math::convertToPositiveInteger($pObj->piVars['datetstample']):'');
                        $datetstampentrele = (t3lib_utility_Math::convertToPositiveInteger($pObj->piVars['datetstampentrele'])?t3lib_utility_Math::convertToPositiveInteger($pObj->piVars['datetstampentrele']):'');
                        $datetstampetle = (t3lib_utility_Math::convertToPositiveInteger($pObj->piVars['datetstampetle'])?t3lib_utility_Math::convertToPositiveInteger($pObj->piVars['datetstampetle']):'');
                        
                        if( $datetstample > 0 ){
                            $selectConf['where'] .= " AND (FROM_UNIXTIME(`tx_tsara_event_date`,'%Y-%m-%d') = '".date('Y-m-d',$datetstample)."' OR ('".date('Y-m-d',$datetstample)."' BETWEEN FROM_UNIXTIME(`tx_tsara_event_date`,'%Y-%m-%d') AND FROM_UNIXTIME(`tx_tsara_event_date_end`,'%Y-%m-%d'))) ";
                        }
                        else{
                            $selectConf['where'] .= " AND ((FROM_UNIXTIME(`tx_tsara_event_date`,'%Y-%m-%d')>='".date('Y-m-d',$datetstampentrele)."' AND FROM_UNIXTIME(`tx_tsara_event_date_end`,'%Y-%m-%d')<='".date('Y-m-d',$datetstampetle)."') OR ('".date('Y-m-d',$datetstampentrele)."' BETWEEN FROM_UNIXTIME(`tx_tsara_event_date`,'%Y-%m-%d') AND FROM_UNIXTIME(`tx_tsara_event_date_end`,'%Y-%m-%d')) OR ('".date('Y-m-d',$datetstampetle)."' BETWEEN FROM_UNIXTIME(`tx_tsara_event_date`,'%Y-%m-%d') AND FROM_UNIXTIME(`tx_tsara_event_date_end`,'%Y-%m-%d')) )    ";
                        }
                    }
                    
                   //  print_r( $selectConf );
                    $this->util->setSession('selectConf',$selectConf );
                    $this->util->setSession('urlencours',( $GLOBALS['TSFE']->baseUrl.$GLOBALS['TSFE']->cObj->typoLink( '', array(
             'parameter' => $GLOBALS['TSFE']->id
             ,'addQueryString' => 1
             ,'returnLast' => 'url'
         )) ) );
                
                }
                
                if ( \TYPO3\CMS\Core\Utility\GeneralUtility::getIndpEnv('REMOTE_ADDR') == '82.230.218.34' ) {
                   // print_r( $selectConf['where'] );
                }
                
               
		 //print_r( $selectConf );
		 //print_r( $pObj->piVars );
		return $selectConf;
	
	}
        function flatten_array($array, $preserve_keys = 0, &$out = array()) {
            # Flatten a multidimensional array to one dimension, optionally preserving keys.
            #
            # $array - the array to flatten
            # $preserve_keys - 0 (default) to not preserve keys, 1 to preserve string keys only, 2 to preserve all keys
            # $out - internal use argument for recursion
            foreach($array as $key => $child)
                if(is_array($child))
                    $out = $this->flatten_array($child, $preserve_keys, $out);
                elseif($preserve_keys + is_string($key) > 1)
                    $out[$key] = $child;
                else
                    $out[] = $child;
            return $out;
        }
    function extraItemMarkerProcessor($markerArray, $row, $lConf, &$pObj) { 
        //echo  print_r( $this->util->getSession('selectConf'  )  );
        $this->confArr = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['tsara']);
        $selectconf = array();
        if($pObj->config['code'] == 'SINGLE' ){
            if( is_array( $this->util->getSession('selectConf'  ) ) ){
                $selectconf = $this->util->getSession('selectConf'  );
            }
        }
         //print_r($pObj->config);
         //print_r($lConf['tsara.']['pid_publierevent.']['value']);
         // print_r($lConf['tsara.']);
        $h = $pObj->config['FFimgH'] ;
	$w = $pObj->config['FFimgW'] ;
		
       $markerArray['###NEWSIMAGE###'] = ($row['image']!='' && file_exists(PATH_site.'uploads/pics/'.$row['image'])? '<img alt="" src="thb_'.$w.'_'.$h.'_1_/uploads/pics/'.$row['image'].'" />':'');
       $markerArray['###NEWSIMAGE_ORIGINALE###'] = ($row['image']!='' && file_exists(PATH_site.'uploads/pics/'.$row['image'])? 'uploads/pics/'.$row['image']:'');
       $markerArray['###CREDITS###'] = ($row['image']!='' && file_exists(PATH_site.'uploads/pics/'.$row['image']) && $row['author']!='' ? $pObj->cObj->stdWrap( $row['author'], $pObj->conf['tsara.']['creditphoto_stdWrap.']) :'');
       $markerArray['###AUTEURS###'] = ($row['tx_tsara_cine_auteurs']>0  ? $pObj->cObj->stdWrap( $this->show_auteur( $this->util->get_ttaddress_name( $row['uid']  ), $pObj ) , $pObj->conf['tsara.']['cineauteur_stdWrap.']) :'');
       $markerArray['###ACTEURS###'] = ($row['tx_tsara_cine_acteurs']>0  ? $pObj->cObj->stdWrap( $this->show_auteur( $this->util->get_ttaddress_name( $row['uid'], 'tx_tsara_cine_acteurs_mm'  ), $pObj, 'acteurs' ) , $pObj->conf['tsara.']['cineacteur_stdWrap.']) :'');
       $markerArray['###REALISATEURS###'] = ($row['tx_tsara_cine_realisateurs']>0  ? $pObj->cObj->stdWrap( $this->show_auteur( $this->util->get_ttaddress_name( $row['uid'], 'tx_tsara_cine_realisateurs_mm'  ), $pObj, 'realisateurs' ) , $pObj->conf['tsara.']['cinerealisateur_stdWrap.']) :'');
       $markerArray['###PRODUCTEURS###'] = ($row['tx_tsara_cine_production']>0  ? $pObj->cObj->stdWrap( $this->show_auteur( $this->util->get_ttaddress_name( $row['uid'], 'tx_tsara_cine_production_mm'  ), $pObj, 'production' ) , $pObj->conf['tsara.']['cineproduction_stdWrap.']) :'');
       $markerArray['###SCENARIO###'] = ($row['tx_tsara_cine_scenario']>0  ? $pObj->cObj->stdWrap( $this->show_auteur( $this->util->get_ttaddress_name( $row['uid'], 'tx_tsara_cine_scenario_mm'  ), $pObj, 'scenario' ) , $pObj->conf['tsara.']['cinescenario_stdWrap.']) :'');
       $markerArray['###DIRECTIONPHOTO###'] = ($row['tx_tsara_cine_directeur_photo']>0  ? $pObj->cObj->stdWrap( $this->show_auteur( $this->util->get_ttaddress_name( $row['uid'], 'tx_tsara_cine_directeur_photo_mm'  ), $pObj, 'directeur_photo' ) , $pObj->conf['tsara.']['cinedirecteurphoto_stdWrap.']) :'');
       $markerArray['###EFFETSONORES###'] = ($row['tx_tsara_cine_montage_son']>0  ? $pObj->cObj->stdWrap( $this->show_auteur( $this->util->get_ttaddress_name( $row['uid'], 'tx_tsara_cine_montage_son_mm'  ), $pObj, 'montage_son' ) , $pObj->conf['tsara.']['cinemontageson_stdWrap.']) :'');
       $markerArray['###PERCHMAN###'] = ($row['tx_tsara_cine_perchman']>0  ? $pObj->cObj->stdWrap( $this->show_auteur( $this->util->get_ttaddress_name( $row['uid'], 'tx_tsara_cine_perchman_mm'  ), $pObj, 'perchman' ) , $pObj->conf['tsara.']['cineperchman_stdWrap.']) :'');
       $markerArray['###EFFETSVISUELS###'] = ($row['tx_tsara_cine_effets_visuels']>0  ? $pObj->cObj->stdWrap( $this->show_auteur( $this->util->get_ttaddress_name( $row['uid'], 'tx_tsara_cine_effets_visuels_mm'  ), $pObj, 'effets_visuels' ) , $pObj->conf['tsara.']['cineeffetsvisuels_stdWrap.']) :'');
       
       $markerArray['###PHOTOSFILM###'] = ($row['news_files']!=''  ? $pObj->cObj->stdWrap( $this->show_photofilm(  $row  , $pObj  ) , $pObj->conf['tsara.']['cine_photofilms_stdWrap.']) :$pObj->cObj->stdWrap( $pObj->pi_getLL('emptyitem'), $pObj->conf['tsara.']['emptyitem_stdWrap.']));
       $markerArray['###VIDEOSFILM###'] = ($row['tx_tsara_cine_videosfilm']>0? $pObj->cObj->stdWrap( $this->show_videofilm(  $row  , $pObj  ) , $pObj->conf['tsara.']['cine_videosfilms_stdWrap.']) :$pObj->cObj->stdWrap( $pObj->pi_getLL('emptyitem'), $pObj->conf['tsara.']['emptyitem_stdWrap.']));
       $markerArray['###ACTEURSFILM###'] = ($row['tx_tsara_cine_acteurs']>0? $pObj->cObj->stdWrap( $this->show_acteursfilm(  $row  , $pObj  ) , $pObj->conf['tsara.']['cine_acteursfilms_stdWrap.']) :'');
       
       $markerArray['###ANNEESORTIE###'] = ($row['tx_tsara_cine_anneesortie']>0  ? $pObj->cObj->stdWrap( $row['tx_tsara_cine_anneesortie'] , $pObj->conf['tsara.']['cineanneesortie_stdWrap.']) :'');
       $markerArray['###DUREE###'] = ($row['tx_tsara_cine_duree']>0  ? $pObj->cObj->stdWrap( $row['tx_tsara_cine_duree'] , $pObj->conf['tsara.']['cineduree_stdWrap.']) :'');
       $markerArray['###GENRE###'] = $pObj->cObj->stdWrap( $this->showgenre( $row['uid'], $pObj ),  $pObj->conf['tsara.']['newgenre_stdWrap.']);
        
       $markerArray['###STYLEH_NOIMAGE###'] = ($row['image']!='' && file_exists(PATH_site.'uploads/pics/'.$row['image'])? '':' style="height:auto;"');
       $markerArray['###STYLEW_NOIMAGE###'] = ($row['image']!='' && file_exists(PATH_site.'uploads/pics/'.$row['image'])? '':' style="height:auto;width:94%;"');
       $markerArray['###FLOATNONE###'] = ($row['image']!='' && file_exists(PATH_site.'uploads/pics/'.$row['image'])? '':' floatnone');
       $markerArray['###HEIGHTAUTO###'] = ($row['image']!='' && file_exists(PATH_site.'uploads/pics/'.$row['image'])? '':' heightauto');
       
       $markerArray['###NEWS_KEYWORDSTAG###'] = ($row['keywords']!='' ? $this->getkeywords( $row, $pObj ):'');
       $markerArray['###RELATED_TSARA###'] = $this->getrelated( $row, $pObj );
        
        
        $markerArray['###LINK_BACKBUTTON###'] = sprintf( $pObj->conf['tsara.']['sprintf_backbutton'] , ($this->util->getSession('urlencours')!=''?$this->util->getSession('urlencours'):$GLOBALS['TSFE']->cObj->typoLink('', array('parameter' => $pObj->config['backPid'],'returnLast' => 'url' ))));
        $markerArray['###LINK_URLNEXT###'] = ($row['tx_tsara_exturl2']!='' ? $row['tx_tsara_exturl2'] :'');
        $markerArray['###LINK_EXTURL###'] = ($row['ext_url']!='' ? $row['ext_url'] :'');
        
        $markerArray['###URLVIDEO###'] = ($row['tx_tsara_youtubeid']!='' ? 'http://www.youtube.com/watch?v='.$row['tx_tsara_youtubeid'] :'');
        $markerArray['###NEWSIMAGE_YT###'] = ($row['tx_tsara_youtubeid']!='' ? '<img alt="kuETZ3vySGQ/hqdefault.jpg" src="thbx_'.$w.'__1_i.ytimg.com/vi/'.$row['tx_tsara_youtubeid'].'/hqdefault.jpg" />' :'');
$markerArray['###NEWSIMAGE_youtube_thumbnail###'] = ($row['tx_tsara_youtube_thumbnail']!='' ? '<img alt="" src="thbx_'.$w.'__1_'.str_replace('http://','',$row['tx_tsara_youtube_thumbnail']).'" />' :'');
        
        $markerArray['###NEWS_YT_viewcount###'] = ($row['tx_tsara_youtube_viewcount']>0 ? $row['tx_tsara_youtube_viewcount'] :'0');
        $markerArray['###NEWS_YT_numcomment###'] = ($row['tx_tsara_youtube_numcomment']>0 ? $row['tx_tsara_youtube_numcomment'] :'0');
        $markerArray['###NEWS_YT_numlikes###'] = ($row['tx_tsara_youtube_numlikes']>0 ? $row['tx_tsara_youtube_numlikes'] :'0');
        $markerArray['###NEWS_YT_duration###'] = ($row['tx_tsara_youtube_duration']>0 ? $this->secondsToDuration( $row['tx_tsara_youtube_duration'] ) :'0');
        
        $markerArray['###NEWS_ARTISTE###'] = $pObj->cObj->stdWrap( $this->showartiste( $row['uid'], $pObj ),  $pObj->conf['tsara.']['newartiste_stdWrap.']);
                
        $markerArray['###URLENCOURS_ENCODE###'] = rawurlencode( $GLOBALS['TSFE']->baseUrl.$GLOBALS['TSFE']->cObj->typoLink( '', array(
             'parameter' => $GLOBALS['TSFE']->id
             ,'addQueryString' => 1
            /*,'additionalParams' => '&utm_source=pinterest&utm_medium=toolbox&utm_campaign='.  $this->encodeTitle($row['title'])*/
             ,'returnLast' => 'url'
         )) );
        
        $markerArray['###URLENCOURS_TWITTER###'] = ( $GLOBALS['TSFE']->baseUrl.$GLOBALS['TSFE']->cObj->typoLink( '', array(
             'parameter' => $GLOBALS['TSFE']->id
             ,'addQueryString' => 1
             /*,'additionalParams' => '&utm_source=twitter&utm_medium=toolbox&utm_campaign='.  $this->encodeTitle($row['title'])*/
             ,'returnLast' => 'url'
         )) );
        $markerArray['###URLENCOURS_FB###'] = ( $GLOBALS['TSFE']->baseUrl.$GLOBALS['TSFE']->cObj->typoLink( '', array(
             'parameter' => $GLOBALS['TSFE']->id
             ,'addQueryString' => 1
             /*,'additionalParams' => '&utm_source=facebook&utm_medium=toolbox&utm_campaign='.  $this->encodeTitle($row['title'])*/
             ,'returnLast' => 'url'
         )) );
        $markerArray['###URLENCOURS_GG###'] = ( $GLOBALS['TSFE']->baseUrl.$GLOBALS['TSFE']->cObj->typoLink( '', array(
             'parameter' => $GLOBALS['TSFE']->id
             ,'addQueryString' => 1
             /*,'addQueryString' => 1
             ,'additionalParams' => '&utm_source=googleplus&utm_medium=toolbox&utm_campaign='.  $this->encodeTitle($row['title'])*/
             ,'returnLast' => 'url'
         )) );
        
        $markerArray['###NEWS_TITLE_ENCODE###'] = rawurldecode($row['title']);
        $markerArray['###NEXT_ARTICLE_HREF###'] = $this->get_info($markerArray['###NEXT_ARTICLE###'],'href');
        $markerArray['###NEXT_ARTICLE_TITLE###'] = $this->get_info($markerArray['###NEXT_ARTICLE###'],'title');
		
	$markerArray['###URLDISQUS###'] =  ( $GLOBALS['TSFE']->baseUrl.$GLOBALS['TSFE']->cObj->typoLink( '', array(
             'parameter' => $pObj->config['singlePid']
             ,'additionalParams' => '&tx_ttnews[tt_news]='.$row['uid']
             , 'useCacheHash'  => 1           
             ,'returnLast' => 'url'
         )) ).'#disqus_thread';
        
        
        
        
        
        $markerArray['###PREV_ARTICLE_TSARA###'] = $this->getnextprev_article( $row,'<', $pObj, 'tt_news.datetime DESC','PREV', $selectconf );
        $markerArray['###NEXT_ARTICLE_TSARA###'] = $this->getnextprev_article( $row,'>', $pObj, 'tt_news.datetime ASC','NEXT', $selectconf );
        
        $markerArray['###NEWS_STYLE_ADDVIDEO###'] =  (is_array(t3lib_div::trimExplode(',',$this->getSession('uidliste'))) && in_array($row['uid'],t3lib_div::trimExplode(',',$this->getSession('uidliste'))) ? ' style="display:none;"': '');
        $markerArray['###NEWS_STATE###'] =  (is_array(t3lib_div::trimExplode(',',$this->getSession('uidliste'))) && in_array($row['uid'],t3lib_div::trimExplode(',',$this->getSession('uidliste'))) ? ' ok': '');
        $markerArray['###TITLESTATE###'] =  (is_array(t3lib_div::trimExplode(',',$this->getSession('uidliste'))) && in_array($row['uid'],t3lib_div::trimExplode(',',$this->getSession('uidliste'))) ? $pObj->pi_getLL('TITLESTATE'): '');
        $markerArray['###NEWS_STYLE_REMOVEVIDEO###'] =  (is_array(t3lib_div::trimExplode(',',$this->getSession('uidliste'))) && in_array($row['uid'],t3lib_div::trimExplode(',',$this->getSession('uidliste'))) ? '' : ' style="display:none;"' );
        
        $markerArray['###RELATED_ITEMS###'] =  $this->get_relateditems($pObj);
        
        $markerArray['###NEWS_EVENT_DATE###'] =  $this->util->get_date_event($row, $pObj);
        $markerArray['###NEWS_EVENT_LIEU###'] =  ($row['tx_tsara_event_lieu']!=''?$pObj->cObj->stdWrap( $row['tx_tsara_event_lieu'] ,$pObj->conf['tsara.']['eventlieu_stdWrap.'] ):'');
        $markerArray['###NEWS_EVENT_PRIX###'] =  ($row['tx_tsara_event_prix']!=''?$pObj->cObj->stdWrap( $row['tx_tsara_event_prix'] ,$pObj->conf['tsara.']['eventprix_stdWrap.'] ):'');
        $markerArray['###NEWS_EVENT_RESERVATION###'] =  ($row['tx_tsara_event_reservation']!=''?$pObj->cObj->stdWrap( $row['tx_tsara_event_reservation'] ,$pObj->conf['tsara.']['eventreservation_stdWrap.'] ):'');
        $markerArray['###NEWS_EVENT_PREVENTE###'] =  ($row['tx_tsara_event_prevente']!=''?$pObj->cObj->stdWrap( $row['tx_tsara_event_prevente'] ,$pObj->conf['tsara.']['eventprevente_stdWrap.'] ):'');
        $markerArray['###NEWS_EVENT_LOCATION###'] =  ($row['tx_tsara_event_location']!=''?$pObj->cObj->stdWrap( $row['tx_tsara_event_location'] ,$pObj->conf['tsara.']['eventlocation_stdWrap.'] ):'');
        $markerArray['###NEWS_EVENT_CONTACTS###'] =  ($row['tx_tsara_event_contacts']!=''?$pObj->cObj->stdWrap( $this->pi_RTEcssText( $row['tx_tsara_event_contacts'] ),$pObj->conf['tsara.']['eventcontacts_stdWrap.'] ):'');
        $markerArray['###NEWS_EVENT_LATITUDE###'] =  $this->get_gps($row,$pObj,'latitude');
        $markerArray['###NEWS_EVENT_LONGITUDE###'] =  $this->get_gps($row,$pObj,'longitude');
        // XML
        if ($pObj->theCode == 'XML') {
                if ($row['type'] == 2) {
                                // external URL
                        $exturl = trim(strpos($row['ext_url'], 'http://') !== FALSE ? $row['ext_url'] : 'http://' . $row['ext_url']);
                        $exturl = (strpos($exturl, ' ') ? substr($exturl, 0, strpos($exturl, ' ')) : $exturl);
                        $rssUrl = $exturl;
                } elseif ($row['type'] == 1) {
                                // internal URL
                        $rssUrl = $pObj->pi_getPageLink($row['page'], '');
                        if (strpos($rssUrl, '://') === FALSE) {
                                $rssUrl = $pObj->config['siteUrl'] . $rssUrl;
                        }
                } else {
                        $singlePid = $pObj->config['singlePid'];
                        $piVarsArray = array('backPid' => ($pObj->conf['dontUseBackPid'] ? null : $pObj->config['backPid']),
				'year' => ($pObj->conf['dontUseBackPid'] ? null : ($pObj->piVars['year'] ? $pObj->piVars['year'] : null)),
				'month' => ($pObj->conf['dontUseBackPid'] ? null : ($pObj->piVars['month'] ? $pObj->piVars['month'] : null)));
                                // News detail link
                        $link = $pObj->getSingleViewLink($singlePid, $row, $piVarsArray, true);
                        $rssUrl = trim(strpos($link, '://') === FALSE ? $pObj->config['siteUrl'] : '') . $link;
                        $cat = t3lib_div::_GP('tx_ttnews');
                        $cat = $cat['cat'];
                        $catlabel = '';
                        if(  t3lib_utility_Math::convertToPositiveInteger($cat) ) {
                            $catlabel = $this->util->getfieldincat($cat,'title');
                            $catlabel = $this->makeslug( $catlabel );
                        }
                        
                        $titlenews = $this->makeslug( $row['title'], 50 );
                        $rssUrl .= (strpos($rssUrl,'?')===FALSE?'?':'&').'utm_source=rss&utm_medium='.$catlabel.'&utm_campaign='.$titlenews;

                }
                
                // replace square brackets [] in links with their URLcodes and replace the &-sign with its ASCII code
                $rssUrl = preg_replace(array('/\[/', '/\]/', '/&/'), array('%5B', '%5D', '&#38;'), $rssUrl);
                $markerArray['###NEWS_LINK2###'] = $rssUrl;

                
        }
        
        $markerArray['###LINK_EDITSITE_1###'] =   $GLOBALS['TSFE']->cObj->typoLink( '', array(
             'parameter' => $pObj->conf['tsara.']['pid_form_edit_1']
             ,'additionalParams' => '&uidsite='.$row['uid'].'&opt=1'
             , 'useCacheHash'  => 1           
             ,'returnLast' => 'url'
         )) ;
        $markerArray['###LINK_EDITSITE_2###'] =   $GLOBALS['TSFE']->cObj->typoLink( '', array(
             'parameter' => $pObj->conf['tsara.']['pid_form_edit_1']
             ,'additionalParams' => '&uidsite='.$row['uid'].'&opt=2'
             , 'useCacheHash'  => 1           
             ,'returnLast' => 'url'
         )) ;
        $markerArray['###LINK_EDITSITE_3###'] =   $GLOBALS['TSFE']->cObj->typoLink( '', array(
             'parameter' => $pObj->conf['tsara.']['pid_form_edit_1']
             ,'additionalParams' => '&uidsite='.$row['uid'].'&opt=3'
             , 'useCacheHash'  => 1           
             ,'returnLast' => 'url'
         )) ;
        $markerArray['###LINK_EDITSITE_4###'] =   $GLOBALS['TSFE']->cObj->typoLink( '', array(
             'parameter' => $pObj->conf['tsara.']['pid_form_edit_1']
             ,'additionalParams' => '&uidsite='.$row['uid'].'&opt=4'
             , 'useCacheHash'  => 1           
             ,'returnLast' => 'url'
         )) ;
        return $markerArray;
    }
    function makeslug($title, $len=50){
        $space = '_';
        $charset = 'utf-8';
        $title = $this->cs->conv_case($charset,$title,'toLower');
        $title = preg_replace('/[ \-+_]+/', $space, $title);
        $title = $this->cs->specCharsToASCII($charset, $title);
        $title = preg_replace('/[^a-zA-Z0-9' . ($space ? preg_quote($space) : '') . ']/', '', $title);
        $title = preg_replace('/\\' . $space . '{2,}/', $space, $title); // Convert multiple 'spaces' to a single one
        $title = trim($title, $space);
        $title = $this->cs->crop($charset, $title, $len );
        return $title;
    }
	function get_gps($row,$pObj,$field = 'latitude'){
                $tab = array();
		if( $row['tx_tsara_event_lieugps'] == '' && t3lib_utility_Math::convertToPositiveInteger($row['tx_tsara_event_endroit']) ){
                    //Chercher dans la table lieu
                    $lieugps = $this->util->get_fieldintable( $row['tx_tsara_event_endroit'], 'tt_address', 'tx_tsara_event_lieugps'  );
                    $tab = t3lib_div::trimExplode(',', $lieugps );
                    if( count( $tab ) != 2 ) return '';
                    
                    if( $field != 'latitude' ) return $tab[1];
                    return $tab[0];
                }
		
		$tab = t3lib_div::trimExplode(',',$row['tx_tsara_event_lieugps']);
		
		if( count( $tab ) != 2 ) return '';
		if( $field != 'latitude' ) return $tab[1];
		return $tab[0];
	}
     
    function secondsToDuration($s) {
	$time = round($s);
	$parts = array();
	while ($time >= 1) {
		array_unshift($parts, $time % 60);
		$time /= 60;
	}
	if ($s < 60) {
		// if it is seconds only, prepend "0:"
		array_unshift($parts, '0');
	}
	$last = count($parts) - 1;
	if ($parts[$last] < 10) {
		$parts[$last] .= '0';
	}
	$duration = join(':', $parts);
	return $duration;
    }
    function get_relateditems($pObj){
        $html = '';
        $tabuid = $tabartiste = $tablink = $artrow = array();
        $uid_news = $pObj->piVars['tt_news'];
         
        if( !t3lib_utility_Math::convertToPositiveInteger($uid_news) ) return $html;
        
        $row = $this->util->getcategoriesofarticle( $uid_news, $this->confArr['parent_category_artiste'] );
        
        foreach( $row as $uid_foreign ){
            $tabuid[] = $uid_foreign['uid_foreign'];
        }
        
        // print_r($pObj->config);
         
        $limit = $pObj->config['limit'];
         
        
        $where .= ' 1=1 AND tt_news.deleted = 0 AND tt_news.hidden = 0 AND tt_news_cat_mm.uid_local != '.$GLOBALS['TYPO3_DB']->fullQuoteStr($uid_news,'tt_news').' AND tt_news_cat_mm.uid_foreign IN ('.implode(',',$tabuid).')';
        
        $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                'DISTINCT tt_news.*',
                'tt_news LEFT JOIN tt_news_cat_mm  ON tt_news_cat_mm.uid_local = tt_news.uid ' ,
                $where,
                '',
                '',
                $limit
                );
        
         $artrow = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows('tt_news_cat.title','tt_news_cat','1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news_cat').' AND tt_news_cat.uid IN ('. implode(',',$tabuid) .')');
         if( count( $artrow ) > 0 ){
             foreach( $artrow as $artiste ){
                $tabartiste[] = $artiste['title'];
            }
         }
         
          
          
        
        $h = $pObj->config['FFimgH'] ;
	$w = $pObj->config['FFimgW'] ; 
        if( count( $row ) > 0 ){
            $subpart  = $pObj->cObj->getSubpart($pObj->templateCode, '###TEMPLATE_RELATEDITEMS###');
            $i = 0;
            foreach( $row as $ligne ){
                    
                    $data['CONTENT'][$i]['BEGIN_DIV'] = ($i==0 ? '<div>' : '');
                    $data['CONTENT'][$i]['END_DIV'] = ($i+1==count( $row ) || ($i+1)%3==0 ? '</div>'.($i+1<count( $row )?'<div>':'') : '');
                    
                    $data['CONTENT'][$i]['title'] = $ligne['title'];
                    $data['CONTENT'][$i]['uid'] = $ligne['uid'];
                    $data['CONTENT'][$i]['NEWSIMAGE_YT'] = ($ligne['tx_tsara_youtubeid']!='' ? '<img alt="" src="thbx_'.$w.'__1_i.ytimg.com/vi/'.$ligne['tx_tsara_youtubeid'].'/hqdefault.jpg" />' :'');
                    $data['CONTENT'][$i]['separator'] = (($i+1)!=count( $row ) ? ', ' : '');
                    $data['CONTENT'][$i]['ARTISTES'] = $pObj->cObj->stdWrap( $this->showartiste( $ligne['uid'], $pObj ),  $pObj->conf['tsara.']['newartiste_stdWrap.']);
                    
                    
                    $data['CONTENT'][$i]['link'] = $GLOBALS['TSFE']->cObj->typoLink( '', array(
                        'parameter' => $GLOBALS['TSFE']->id
                        ,'addQueryString' => 1
                       ,'additionalParams' => '&tx_ttnews[tt_news]='.$ligne['uid'] 
                        ,'returnLast' => 'url'
                         
                       ));
                    $i++;
                }
                $data['ARTISTE'] =  implode(', ',$tabartiste);
                $i = 0;
                foreach( $tabuid as $uid_a ){
                    $tablink[] = 'tx_ttnews[artiste]['.$i.']='.$uid_a;
                    $i++;
                }
                 
                 
                $data['LINK_ARTISTE'] =  $GLOBALS['TSFE']->cObj->typoLink( '', array(
                        'parameter' => $pObj->config['backPid']
                       ,'additionalParams' => '&'.implode('&',$tablink)  
                        ,'returnLast' => 'url'
                         
                       ));
                
                $html = $pObj->cObj->substituteMarkerAndSubpartArrayRecursive($subpart, $data, '###|###' );
        }
        return $html;
         
    }
    function showgenre( $uid, $pobj ){
        $backPid =  ($pobj->config['backPid']>0?$pobj->config['backPid']:$GLOBALS['TSFE']->id) ;
        $html = '';
        $parent_category_artiste = intval($this->confArr['parent_category_genrecinema']);
        if( $parent_category_artiste <= 0 ) return '';
         
        
        $nb = $GLOBALS['TYPO3_DB']->exec_SELECTcountRows('tt_news_cat_mm.uid_foreign','tt_news_cat_mm LEFT JOIN tt_news ON tt_news.uid = tt_news_cat_mm.uid_local LEFT JOIN tt_news_cat ON tt_news_cat.uid = tt_news_cat_mm.uid_foreign ','1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news').' AND tt_news_cat.parent_category = '.$parent_category_artiste.' AND tt_news_cat_mm.uid_local = '.$uid );
        if( $nb > 0 ){
             
            $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows('tt_news_cat.uid, tt_news_cat.title ','tt_news_cat_mm LEFT JOIN tt_news ON tt_news.uid = tt_news_cat_mm.uid_local LEFT JOIN tt_news_cat ON tt_news_cat.uid = tt_news_cat_mm.uid_foreign ','1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news').' AND tt_news_cat.parent_category = '.$parent_category_artiste.' AND tt_news_cat_mm.uid_local = '.$uid );
            if( count( $row ) > 0 ){
                $subpart  = $pobj->cObj->getSubpart($pobj->templateCode, '###TEMPLATE_LISTEGENRE###' );
                $i = 0;
                
                foreach( $row as $ligne ){
                    $data['CONTENT'][$i]['title'] = $ligne['title'];
                    $data['CONTENT'][$i]['uid'] = $ligne['uid'];
                    $data['CONTENT'][$i]['separator'] = (($i+1)!=count( $row ) ? ', ' : '');
                    
                    
                    $data['CONTENT'][$i]['link'] = $GLOBALS['TSFE']->cObj->typoLink( '', array(
                        'parameter' => $backPid
                        ,'addQueryString' => 1
                       ,'additionalParams' => '&tx_ttnews[pointer]=0&tx_ttnews[artiste][0]='.$ligne['uid'] 
                        ,'returnLast' => 'url'
                         
                       ));
                    $i++;
                }
                $html = $pobj->cObj->substituteMarkerAndSubpartArrayRecursive($subpart, $data, '###|###' );
            }
            
        }
        
        return $html;

    }
    function showartiste( $uid, $pobj ){
        $backPid =  ($pobj->config['backPid']>0?$pobj->config['backPid']:$GLOBALS['TSFE']->id) ;
        //82.230.218.34
        if( t3lib_div::getIndpEnv('REMOTE_ADDR') == '82.230.218.34' ){
               //print_r($backPid.', ');
        }
        $html = '';
        $parent_category_artiste = intval($this->confArr['parent_category_artiste']);
        if( $parent_category_artiste <= 0 ) return '';
         
        
        $nb = $GLOBALS['TYPO3_DB']->exec_SELECTcountRows('tt_news_cat_mm.uid_foreign','tt_news_cat_mm LEFT JOIN tt_news ON tt_news.uid = tt_news_cat_mm.uid_local LEFT JOIN tt_news_cat ON tt_news_cat.uid = tt_news_cat_mm.uid_foreign ','1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news').' AND tt_news_cat.parent_category = '.$parent_category_artiste.' AND tt_news_cat_mm.uid_local = '.$uid );
        if( $nb > 0 ){
             
            $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows('tt_news_cat.uid, tt_news_cat.title ','tt_news_cat_mm LEFT JOIN tt_news ON tt_news.uid = tt_news_cat_mm.uid_local LEFT JOIN tt_news_cat ON tt_news_cat.uid = tt_news_cat_mm.uid_foreign ','1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news').' AND tt_news_cat.parent_category = '.$parent_category_artiste.' AND tt_news_cat_mm.uid_local = '.$uid );
            if( count( $row ) > 0 ){
                $subpart  = $pobj->cObj->getSubpart($pobj->templateCode, '###TEMPLATE_LISTEARTISTE###' );
                $i = 0;
                
                foreach( $row as $ligne ){
                    $data['CONTENT'][$i]['title'] = $ligne['title'];
                    $data['CONTENT'][$i]['uid'] = $ligne['uid'];
                    $data['CONTENT'][$i]['separator'] = (($i+1)!=count( $row ) ? ', ' : '');
                    
                    
                    $data['CONTENT'][$i]['link'] = $GLOBALS['TSFE']->cObj->typoLink( '', array(
                        'parameter' => $backPid
                       ,'additionalParams' => '&tx_ttnews[artiste][0]='.$ligne['uid'] 
                        ,'returnLast' => 'url'
                        ,'useCacheHash' => 1
                         
                       ));
                    $i++;
                }
                $html = $pobj->cObj->substituteMarkerAndSubpartArrayRecursive($subpart, $data, '###|###' );
            }
            
        }
        
        return $html;

    }
    function getrelated( $row, $pObj ){
        //Chercher dans  tt_news_related_mm 
        $nb = $GLOBALS['TYPO3_DB']->exec_SELECTcountRows('tt_news_related_mm.uid_foreign','tt_news_related_mm LEFT JOIN tt_news ON tt_news.uid = tt_news_related_mm.uid_local ','1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news').' AND tt_news_related_mm.uid_local = '.$row['uid']);
        if( $nb == 0 ) {
             
            $nb = $GLOBALS['TYPO3_DB']->exec_SELECTcountRows('tt_news_related_mm.uid_local','tt_news_related_mm LEFT JOIN tt_news ON tt_news.uid = tt_news_related_mm.uid_local ','1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news').' AND tt_news_related_mm.uid_foreign = '.$row['uid']);
            if( $nb > 0 ){
                 
                $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows('tt_news_related_mm.uid_local AS uid_foreign','tt_news_related_mm LEFT JOIN tt_news ON tt_news.uid = tt_news_related_mm.uid_local ','1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news').' AND tt_news_related_mm.uid_foreign = '.$row['uid']);
            }
            else{
                 
                return '';
            }
             
        }
        else{
            $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows('tt_news_related_mm.uid_foreign','tt_news_related_mm LEFT JOIN tt_news ON tt_news.uid = tt_news_related_mm.uid_local ','1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news').' AND tt_news_related_mm.uid_local = '.$row['uid']);
        }
        
        
        
        $templatecode = $pObj->getNewsSubpart( $pObj->templateCode, '###TEMPLATE_RELATED_ARTICLE###' );
        $templatebouclecode = $pObj->getNewsSubpart( $templatecode, '###BOUCLE###' );
        foreach( $row as $ligne ){
            //Get image,datetime,title
            $data = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows('image,datetime,title','tt_news','1=1 '.$GLOBALS["TSFE"]->cObj->enableFields('tt_news').' AND uid = '.$ligne['uid_foreign']);
            $markerSub['###TITLE###'] = $pObj->cObj->stdWrap( $data[0]['title'], $pObj->conf['tsara.']['titlerelated_stdWrap.'] );
            $markerSub['###IMG###'] = ($data[0]['image']!='' && file_exists(PATH_site.'uploads/pics/'.$data[0]['image'])? '<img alt="'.  htmlspecialchars($data[0]['title']).'" src="thb_120_90_1_/uploads/pics/'.$data[0]['image'].'" />':'');
            $markerSub['###LINK###'] = ( $GLOBALS['TSFE']->cObj->typoLink( '', array(
             'parameter' => $GLOBALS['TSFE']->id
             ,'additionalParams' => '&tx_ttnews[tt_news]='.$ligne['uid_foreign']
             , 'useCacheHash'  => 1
             ,'returnLast' => 'url'
         )) );
            $markerSub['###DATE###'] = $pObj->cObj->stdWrap( $data[0]['datetime'], $pObj->conf['tsara.']['daterelated_stdWrap.'] );
            $contentboucle .= $pObj->cObj->substituteMarkerArray( $templatebouclecode, $markerSub );
        }
        
        
        $templatecode = $pObj->cObj->substituteSubpart( $templatecode, '###BOUCLE###', $contentboucle );
        
        $marker['###HIDE###'] = $pObj->conf['tsara.']['related.']['hide'];
        $marker['###TITLEHEADER###'] = $pObj->conf['tsara.']['related.']['title'];
        $marker['###SHOW###'] = $pObj->conf['tsara.']['related.']['show'];
        
        $templatecode = $pObj->cObj->substituteMarkerArray( $templatecode  , $marker );
        
        return $templatecode;
    }
    
    function getnextprev_article( $row, $operator, $pObj, $order, $template = 'PREV', $selectconf = array() ){
         
         if( $row['uid'] == '' && $row['title'] == '' ) return '';
         
         $leftjoin = $where = '';
         
         if( count( $selectconf ) > 0 ){
             $where = ($selectconf['where']!=''?str_replace('1=1','',$selectconf['where']):'');
             $leftjoin = ($selectconf['leftjoin']!=''?' LEFT JOIN '.$selectconf['leftjoin']:'') ;
         }
         //Get image and datetime
        //echo $GLOBALS['TYPO3_DB']->SELECTquery('tt_news.title,tt_news.uid,tt_news.datetime,tt_news.image','tt_news'.$leftjoin,'1=1 AND tt_news.pid='.$GLOBALS['TYPO3_DB']->fullQuoteStr($row['pid'],'tt_news').' AND tt_news.datetime '.$operator.(int) $row['datetime'].$where,'',$order,'1' );
        $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows('tt_news.title,tt_news.uid,tt_news.datetime,tt_news.image','tt_news'.$leftjoin,'1=1 AND tt_news.pid='.$GLOBALS['TYPO3_DB']->fullQuoteStr($row['pid'],'tt_news').' AND tt_news.datetime '.$operator.(int) $row['datetime'].$where,'',$order,'1' );
         
        if( count( $row ) <= 0 ) return '';

         
          
         $templatecode = $pObj->getNewsSubpart( $pObj->templateCode, '###TEMPLATE_'.  mb_strtoupper($template).'_ARTICLE###' );
         
         $markerSub['###TITLE###'] = $pObj->cObj->stdWrap( $row[0]['title'], $pObj->conf['tsara.']['title'.  mb_strtolower($template).'_stdWrap.'] );
         $markerSub['###TITLEPLUS###'] = (strlen(trim($row[0]['title']))<28?$pObj->conf['tsara.']['titleplus']:'');
         $markerSub['###LINK###'] = ( $GLOBALS['TSFE']->cObj->typoLink( '', array(
             'parameter' => $GLOBALS['TSFE']->id
             ,'additionalParams' => '&tx_ttnews[tt_news]='.$row[0]['uid']
             , 'useCacheHash'  => 1
             ,'returnLast' => 'url'
         )) );
         $markerSub['###DATE###'] = $pObj->cObj->stdWrap( $row[0]['datetime'], $pObj->conf['tsara.']['date'.  mb_strtolower($template).'_stdWrap.'] );
         $markerSub['###IMG###'] = ($row[0]['image']!='' && file_exists(PATH_site.'uploads/pics/'.$row[0]['image'])? '<img alt="'.  htmlspecialchars($row[0]['title']).'" src="thb_120_90_1_/uploads/pics/'.$row[0]['image'].'" />':'');;
         
         $html = $pObj->cObj->substituteMarkerArray( $templatecode, $markerSub );
         return $html;
         
    }
        function extraGlobalMarkerProcessor(&$pObj, $markerArray) {
         //   print_r($pObj->conf['tsara.'] );
         //if( t3lib_div::getIndpEnv('REMOTE_ADDR') == '82.230.218.34') print_r( $pObj->getSelectConf('')   );
            
         $tabuid = array();
         $res = $pObj->exec_getQuery('tt_news', $pObj->getSelectConf('') ); //get query for list contents
         while($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)){
               $tabuid[] = $row['uid_local'];
         }
         
         
         
        $html = '';
        $backPid =  $pObj->config['backPid'] ;
        $tlconf = array(
                'parameter' => $backPid,
                'additionalParams' => '',
                'useCacheHash' => 1,
                'returnLast' => 'url',
            );
        
        if( is_array( $pObj->conf['tsara.']['linkall.']['typolink.'] ) ){
            $tlconf = $pObj->conf['tsara.']['linkall.']['typolink.'];
        }
        
         
        
         $markerArray['###LINK_PUBLIEREVENT###'] = sprintf( $pObj->conf['tsara.']['sprint_buttonpublier'] , $GLOBALS['TSFE']->cObj->typoLink('', array('parameter' => $pObj->conf['tsara.']['pid_publierevent'],'returnLast' => 'url' )));

         $markerArray['###PID###'] = $GLOBALS['TSFE']->id;
         $markerArray['###LIMIT###'] = $pObj->config['limit'];
         $markerArray['###LINK_ALLNEWS###'] = $GLOBALS['TSFE']->cObj->typoLink( '', $tlconf);
         $markerArray['###TXTLINK_ALLNEWS###'] = sprintf( $pObj->conf['tsara.']['sprint_txtlink_allnews'] , $pObj->cObj->data['header'] );
         $markerArray['###SELECTION###'] = $this->show_selection($pObj);
         $markerArray['###PLAYALL###'] = $this->show_playall($pObj, implode(',',$tabuid));
         
         
         
         
         
        
        return $markerArray;
    }
    function get_info( $marker, $field ){
        //Charger DOM PARSER
        $html = new simple_html_dom;
        $html->load($marker, 1);
        return $html->find('a',0 )->$field;
    }
    function pi_RTEcssText($str)	{
            $parseFunc = $GLOBALS['TSFE']->tmpl->setup['lib.']['parseFunc_RTE.'];
            if (is_array($parseFunc))	$str = $GLOBALS['TSFE']->cObj->parseFunc($str, $parseFunc);
            return $str;
    }
    function getkeywords( $row, $pObj ){
        
       $separateur = ($pObj->config['code']=='SINGLE'?' ':', ');
        
        if( $row['keywords'] == '' ) return '';
        
        $tab = $thtml = array();
        $tab = t3lib_div::trimExplode(',',$row['keywords']);
        
        foreach( $tab as $tag ){
            $tag = trim( $tag );
            $thtml[] = $GLOBALS['TSFE']->cObj->typoLink($tag, array(
                'parameter' => $pObj->config['backPid'],
                'additionalParams' => '&tx_ttnews[swords]='.$tag,
                'useCacheHash' => 1
            ));;
        }
        
        return implode($separateur,$thtml);
    }
    function getLien($r = array()){
        
        //Si le champ uidpartenaire est positif - On redirige vers la page partenaire
        if( t3lib_utility_Math::convertToPositiveInteger( $r ) ){
            $lien = $GLOBALS['TSFE']->cObj->typoLink('', array('parameter' => (t3lib_utility_Math::convertToPositiveInteger($r)?$r:$GLOBALS['TSFE']->id), 'useCacheHash' => 1, 'returnLast' => 'url' ));
            $lien = '<a   href="'.$lien.'"  >'.$lien.'</a>';

        }
        //Si le champ Lien est rempli dans TYPO
        elseif( trim( $r ) != '' ){
            $target = '';
            $lien = trim( $r );
            //Si ce lien est un chiffre
            if( t3lib_utility_Math::convertToPositiveInteger($lien)>0 ){
                $lien = $GLOBALS['TSFE']->cObj->typoLink('', array('parameter' => $lien,  'returnLast' => 'url' ));    


            }
            else{

                //SI c'est un email
                $lien = trim( $lien );
                $tablien = t3lib_div::trimExplode(' ', $lien);

                $lien = $tablien[0];

                if( t3lib_div::validEmail($lien)) {
                    $lien = $lientexte = $this->util->addMailto($lien);
                    if( trim( $tablien[3] ) != '' ) $title = trim( $tablien[3] );
                }
                //Si c'est un lien
                elseif( filter_var( 'http://'.$lien,FILTER_VALIDATE_URL ) !== FALSE ){
                    
                    
                    $lientexte = $this->util->addHttp($lien,0);
                    $lien = $this->util->addHttp($lien );
                    
                    $target = ' target="_blank"';
                    if( trim( $tablien[3] ) != '' ) $title = trim( $tablien[3] );
                }
                elseif(t3lib_utility_Math::convertToPositiveInteger($lien)) {
                    $lien = $lientexte = $GLOBALS['TSFE']->cObj->typoLink('', array('parameter' => $lien,  'returnLast' => 'url' ));

                    if( trim( $tablien[3] ) != '' ) $title = trim( $tablien[3] );
                }


            }
            $lien = '<a title="'.$title.'" '.$target.' href="'.$lien.'"  > '.$lientexte.'</a>';

        }
        



        return $lien;
    }
	function encodeTitle($title){
             
            $space = '_';
            $charset = 'utf-8'; 
            $title = $this->cs->conv_case('utf-8',$title,'toLower');
            $title = preg_replace('/[ \-+_]+/', $space, $title);
            $title = $this->cs->specCharsToASCII($charset, $title);
            $title = preg_replace('/[^a-zA-Z0-9' . ($space ? preg_quote($space) : '') . ']/', '', $title);
            $title = preg_replace('/\\' . $space . '{2,}/', $space, $title); // Convert multiple 'spaces' to a single one
            $title = trim($title, $space);
            
            return $title;
        }
public function setSession($varname,$varcontent) {
            $GLOBALS['TSFE']->fe_user->setKey('ses',$varname,$varcontent);
            $GLOBALS['TSFE']->storeSessionData(); // validate the session
        }

        

        public function getSession ($varname="") {
            if($varname!="") {
                    return $GLOBALS['TSFE']->fe_user->getKey('ses',$varname);
            } else {
                    return $GLOBALS['TSFE']->fe_user->sesData;
            }
        }
}