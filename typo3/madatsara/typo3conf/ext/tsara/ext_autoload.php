<?php

	// Register necessary classes with autoloader
return array(
	'tx_tsara_test' => t3lib_extMgm::extPath('tsara', 'scheduler/class.tx_tsara_test.php')
	,'tx_tsara_disqus_listpopular' => t3lib_extMgm::extPath('tsara', 'scheduler/class.tx_tsara_disqus_listpopular.php')
	,'tx_tsara_fetchsite_midi' => t3lib_extMgm::extPath('tsara', 'scheduler/class.tx_tsara_fetchsite_midi.php')
	,'tx_tsara_fetchsite_midi_fields' => t3lib_extMgm::extPath('tsara', 'scheduler/class.tx_tsara_fetchsite_midi_fields.php')
	,'tx_tsara_fetchsite_express' => t3lib_extMgm::extPath('tsara', 'scheduler/class.tx_tsara_fetchsite_express.php')
	,'tx_tsara_fetchsite_express_fields' => t3lib_extMgm::extPath('tsara', 'scheduler/class.tx_tsara_fetchsite_express_fields.php')
	,'tx_tsara_fetchsite_gazetiko' => t3lib_extMgm::extPath('tsara', 'scheduler/class.tx_tsara_fetchsite_gazetiko.php')
	,'tx_tsara_fetchsite_gazetiko_fields' => t3lib_extMgm::extPath('tsara', 'scheduler/class.tx_tsara_fetchsite_gazetiko_fields.php')
	,'tx_tsara_fetchsite_inovaovao' => t3lib_extMgm::extPath('tsara', 'scheduler/class.tx_tsara_fetchsite_inovaovao.php')
	,'tx_tsara_fetchsite_inovaovao_fields' => t3lib_extMgm::extPath('tsara', 'scheduler/class.tx_tsara_fetchsite_inovaovao_fields.php')
	,'tx_tsara_fetchsite_lanation' => t3lib_extMgm::extPath('tsara', 'scheduler/class.tx_tsara_fetchsite_lanation.php')
	,'tx_tsara_fetchsite_lanation_fields' => t3lib_extMgm::extPath('tsara', 'scheduler/class.tx_tsara_fetchsite_lanation_fields.php')
	,'tx_tsara_fetchsite_madapro' => t3lib_extMgm::extPath('tsara', 'scheduler/class.tx_tsara_fetchsite_madapro.php')
	,'tx_tsara_fetchsite_madapro_fields' => t3lib_extMgm::extPath('tsara', 'scheduler/class.tx_tsara_fetchsite_madapro_fields.php')
	,'tx_tsara_fetchsite_rss' => t3lib_extMgm::extPath('tsara', 'scheduler/class.tx_tsara_fetchsite_rss.php')
	,'tx_tsara_fetchsite_rss_fields' => t3lib_extMgm::extPath('tsara', 'scheduler/class.tx_tsara_fetchsite_rss_fields.php')
	,'tx_tsara_fetchpicasa_album' => t3lib_extMgm::extPath('tsara', 'scheduler/class.tx_tsara_fetchpicasa_album.php')
        ,'tx_tsara_fetchpicasa_photos' => t3lib_extMgm::extPath('tsara', 'scheduler/class.tx_tsara_fetchpicasa_photos.php')
	,'tx_tsara_fetchpicasa_fields' => t3lib_extMgm::extPath('tsara', 'scheduler/class.tx_tsara_fetchpicasa_fields.php')
	,'tx_tsara_fetch_ytpl' => t3lib_extMgm::extPath('tsara', 'scheduler/class.tx_tsara_fetch_ytpl.php')
	,'tx_tsara_fetch_ytpl_fields' => t3lib_extMgm::extPath('tsara', 'scheduler/class.tx_tsara_fetch_ytpl_fields.php')
	,'tx_tsara_fetch_yt' => t3lib_extMgm::extPath('tsara', 'scheduler/class.tx_tsara_fetch_yt.php')
	,'tx_tsara_fetch_yt_fields' => t3lib_extMgm::extPath('tsara', 'scheduler/class.tx_tsara_fetch_yt_fields.php')
	,'tx_tsara_fetch_ytmaj' => t3lib_extMgm::extPath('tsara', 'scheduler/class.tx_tsara_fetch_ytmaj.php')
	,'tx_tsara_fetch_ytmaj_fields' => t3lib_extMgm::extPath('tsara', 'scheduler/class.tx_tsara_fetch_ytmaj_fields.php')
	,'tx_tsara_fetch_ytvideoinpl' => t3lib_extMgm::extPath('tsara', 'scheduler/class.tx_tsara_fetch_ytvideoinpl.php')
	,'tx_tsara_blugawebsite' => t3lib_extMgm::extPath('tsara', 'scheduler/class.tx_tsara_blugawebsite.php')
	,'tx_tsara_fetch_ytvideoinpl_fields' => t3lib_extMgm::extPath('tsara', 'scheduler/class.tx_tsara_fetch_ytvideoinpl_fields.php')
        ,'tx_tsara_fetchsite_bcrm' => t3lib_extMgm::extPath('tsara', 'scheduler/class.tx_tsara_fetchsite_bcrm.php')
	,'tx_tsara_fetchsite_bcrm_fields' => t3lib_extMgm::extPath('tsara', 'scheduler/class.tx_tsara_fetchsite_bcrm_fields.php')
	,'tx_tsara_ytupdate' => t3lib_extMgm::extPath('tsara', 'scheduler/class.tx_tsara_ytupdate.php')

    
        ,'tx_tsara_lib_powermailslotvalid' => t3lib_extMgm::extPath('tsara', 'lib/Powermailslotvalid.php')
        ,'tx_tsara_lib_powermailbeforerenderview' => t3lib_extMgm::extPath('tsara', 'lib/powermailbeforerenderview.php')
);
?>