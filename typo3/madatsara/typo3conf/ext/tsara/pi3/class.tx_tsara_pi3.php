<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2012 Miary <rabehasy@gmail.com>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * [CLASS/FUNCTION INDEX of SCRIPT]
 *
 * Hint: use extdeveval to insert/update function index above.
 */
require_once(t3lib_extMgm::extPath("tsara").'lib/class.tx_tsara_util.php');
require_once(t3lib_extMgm::extPath("tsara").'lib/class.bbcode.php');

/**
 * Plugin 'La une' for the 'tsara' extension.
 *
 * @author	Miary <rabehasy@gmail.com>
 * @package	TYPO3
 * @subpackage	tx_tsara
 */
class tx_tsara_pi3  extends tslib_pibase {
	var $prefixId      = 'tx_tsara_pi3';		// Same as class name
	var $scriptRelPath = 'pi3/class.tx_tsara_pi3.php';	// Path to this script relative to the extension dir.
	var $extKey        = 'tsara';	// The extension key.
	var $pi_checkCHash = true;
        
          
		var $rowall = array();
	/**
	 * The main method of the PlugIn
	 *
	 * @param	string		$content: The PlugIn content
	 * @param	array		$conf: The PlugIn configuration
	 * @return	The content that is displayed on the website
	 */
	function main($content, $conf) { 
            $this->confArr = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['tsara']); 
            $this->conf = $conf;
            $this->pi_setPiVarDefaults();
            
            $this->util = t3lib_div::makeInstance('tx_tsara_util');
            // Init FlexForm configuration for plugin:
            $this->pi_initPIflexForm();
            $this->preInit();
            $this->pi_loadLL();
            
            // Get Template
            $templateFile = $this->flexFormValue('templateFile', 's_template') ? $this->flexFormValue('templateFile', 's_template') : 'EXT:tsara/pi3/template.html';
            $this->template = $this->cObj->fileResource($templateFile);
            
            $content = '';
            
            $row = $data = array();
            
            $row = $this->util->getforum($this);
            
            if( count( $row ) == 0 )  return $this->cObj->stdWrap( $this->pi_getLL( 'nonews' ) , $this->conf['nonews_stdWrap.'] );
            
            switch( $this->flexFormValue('view', 's_template') ){
               case 'listsposts' :
                    $subpart  = $this->cObj->getSubpart($this->template, '###TEMPLATE_LISTSPOSTS###');
                   
                    $i = 0;
                     
                    foreach( $row as $ligne ){
                        
                        $text = $ligne['message'];
                        $parser = new bbcode($text);
                        $text = $parser->bbc2html();
                        $text = strip_tags($text);
                        
                        $text = $this->cObj->stdWrap($text,$this->conf['message_stdWrap.']);
                        
                        $data['BOUCLE'][$i]['message'] = $text;
                        $data['BOUCLE'][$i]['posted'] = $this->getDisplayText( 'posted', $this->cObj->stdWrap( $ligne['posted'],$this->conf['posted_stdWrap.']) );
                        $data['BOUCLE'][$i]['poster'] = $this->getDisplayText( 'poster', $ligne['poster'] , $this->conf['poster_stdWrap.'] );
                        $data['BOUCLE'][$i]['subject'] = $ligne['subject'];
                        $data['BOUCLE'][$i]['forum_name'] = $ligne['forum_name'];
                        
                        $data['BOUCLE'][$i]['id'] = $ligne['id'];
                        $data['BOUCLE'][$i]['link'] = $this->confArr['urlforum'].'post'.$ligne['id'].'.html#p'.$ligne['id'];
                        
                        $i++;
                    }
                    
                    $data['linkall'] = $this->confArr['urlforum'];
                    $data['TXTALL'] = $this->flexFormValue('txtall', 's_template');
               break;
               case 'listsforum' :
                    $subpart  = $this->cObj->getSubpart($this->template, '###TEMPLATE_LISTSFORUMS###');
                   $i = 0;
                    foreach( $row as $ligne ){
                        
                         
                        $text = $ligne['forum_desc'];
                        $parser = new bbcode($text);
                        $text = $parser->bbc2html();
                        $text = strip_tags($text);
                        $text = $this->cObj->stdWrap($text,$this->conf['forum_desc_stdWrap.']);
                        
                        $data['BOUCLE'][$i]['forum_name'] = $ligne['forum_name'];
                        $data['BOUCLE'][$i]['forum_desc'] = $text;
                        $data['BOUCLE'][$i]['num_posts'] = $this->getDisplayText( 'num_posts', $this->cObj->stdWrap( $ligne['num_posts'],$this->conf['num_posts_stdWrap.']) );
                        $data['BOUCLE'][$i]['num_topics'] = $this->getDisplayText( 'num_topics', $this->cObj->stdWrap( $ligne['num_topics'],$this->conf['num_topics_stdWrap.']) );
                        
                        $data['BOUCLE'][$i]['id'] = $ligne['id'];
                        $data['BOUCLE'][$i]['link'] = $this->confArr['urlforum'].'forum'.$ligne['id'].'-'.$this->encodeTitle($ligne['forum_name']).'.html';
                        
                        $i++;
                    }
                    
                    $data['linkall'] = $this->confArr['urlforum'];
                    $data['TXTALL'] = $this->flexFormValue('txtall', 's_template');
               break;
               case 'liststopics' :
                    $subpart  = $this->cObj->getSubpart($this->template, '###TEMPLATE_LISTSTOPICS###');
                    $i = 0;
                     
                    foreach( $row as $ligne ){
                        
                        $text = $this->cObj->stdWrap($ligne['subject'],$this->conf['subject_stdWrap.']);
                        
                        
                        $data['BOUCLE'][$i]['posted'] = $this->getDisplayText( 'posted', $this->cObj->stdWrap( $ligne['posted'],$this->conf['posted_stdWrap.']) );
                        $data['BOUCLE'][$i]['poster'] = $this->getDisplayText( 'poster', $ligne['poster'] , $this->conf['poster_stdWrap.'] );
                        $data['BOUCLE'][$i]['subject'] = $text;
                        $data['BOUCLE'][$i]['num_replies'] = $ligne['num_replies'];
                        
                        $data['BOUCLE'][$i]['id'] = $ligne['id'];
                        $data['BOUCLE'][$i]['link'] = $this->confArr['urlforum'].'topic'.$ligne['id'].'-'.$this->encodeTitle($ligne['subject']).'.html';
                        
                        $i++;
                    }
                    
                    $data['linkall'] = (t3lib_div::intval_positive( $this->flexFormValue('uidforum', 's_template') )?$this->confArr['urlforum'].'forum'.$ligne['forum_id'].'-'.$this->encodeTitle($ligne['forum_name']).'.html':$this->confArr['urlforum']);
                    $data['TXTALL'] = $this->flexFormValue('txtall', 's_template');
               break;
           }
           
    
            $content = $this->cObj->substituteMarkerAndSubpartArrayRecursive($subpart, $data, '###|###' ); 
            
            $content = $this->replace( $content  );
            return $this->conf['wrapContentInBaseClass'] ? $this->pi_wrapInBaseClass($content) : $content;

            
            
        }
        function getDisplayText($label,$value,$stdWrapArray=array()) {
                if( $value == '' ) return '';
		return $this->cObj->stdWrap( sprintf($this->pi_getLL( 'value_'.$label ),$value), $stdWrapArray);		
	}
        function preInit() {

		 

		$flexformTyposcript = $this->flexFormValue('myTS', 's_template');
		if ($flexformTyposcript) {
			require_once(PATH_t3lib.'class.t3lib_tsparser.php');
			$tsparser = t3lib_div::makeInstance('t3lib_tsparser');
			// Copy conf into existing setup
			$tsparser->setup = $this->conf;
			// Parse the new Typoscript
			$tsparser->parse($flexformTyposcript);
			// Copy the resulting setup back into conf
			$this->conf = $tsparser->setup;
		}

		 
	}
        function encodeTitle($title){
            $charset = 'utf-8';
            $space = '-';
            $cs = t3lib_div::makeInstance('t3lib_cs');
            $title = $cs->conv_case($charset, $title, 'toLower'  );
            $title = strip_tags($title);
            $title = preg_replace('/[ \-+_]+/', $space, $title);
            $title = $cs->specCharsToASCII($charset, $title);
            $title = preg_replace('/[^a-zA-Z0-9' . ($space ? preg_quote($space) : '') . ']/', '', $title);
            $title = preg_replace('/\\' . $space . '{2,}/', $space, $title); // Convert multiple 'spaces' to a single one
            $title = trim($title, $space);   
            return $title;
        }
        function replace( $content ){
            // 1. replace ll markers
            $content = preg_replace_callback ( // Automaticly fill locallangmarkers with fitting value of locallang.xml
                    '$###LL_(.*)###$Uis', // regulare expression
                    array($this, 'DynamicLocalLangMarker'), // open function
                    $content // current content
            );
            $content = preg_replace_callback ( // Automaticly fill locallangmarkers with fitting value of locallang.xml
                    '$###CONF_(.*)###$Uis', // regulare expression
                    array($this, 'DynamicConfMarker'), // open function
                    $content // current content
            );
            $content = preg_replace_callback ( // Automaticly fill locallangmarkers with fitting value of locallang.xml
                    '$###PID_(.*)###$Uis', // regulare expression
                    array($this, 'DynamicPidMarker'), // open function
                    $content // current content
            );
            $content = preg_replace_callback ( // Automaticly fill locallangmarkers with fitting value of locallang.xml
                    '$###DATA_(.*)###$Uis', // regulare expression
                    array($this, 'DynamicDataMarker'), // open function
                    $content // current content
            );

            return $content;
        }
        function DynamicLocalLangMarker($array) {
            if ($this->pi_getLL(strtolower($array[1]))) { // if there is an entry in locallang.xml
                $string = $this->pi_getLL(strtolower($array[1])); // search for a fitting entry in locallang.xml or typoscript
            }
            if (!empty($string)) return $string;
        }
        function DynamicDataMarker($array) {
            if ($this->cObj->data[(strtolower($array[1]))]) { // if there is an entry in locallang.xml
                $string = $this->cObj->data[(strtolower($array[1]))]; // search for a fitting entry in locallang.xml or typoscript
            }
            if (!empty($string)) return $string;
        }
        function DynamicConfMarker($array) {
            if ($this->conf[(strtolower($array[1]))]) { // if there is an entry in locallang.xml
                $string = $this->conf[(strtolower($array[1]))]; // search for a fitting entry in locallang.xml or typoscript
            }
            if (!empty($string)) return $string;
        }
        function DynamicPidMarker($array) {
            if ($this->conf['pid_'.(strtolower($array[1]))]) { // if there is an entry in locallang.xml
                $string = $GLOBALS['TSFE']->cObj->typoLink('', array(
                                                                'parameter' => $this->conf['pid_'.(strtolower($array[1]))],
                                                                'useCacheHash' => 1,
                                                                'returnLast' => 'url',
                                                                )
                                                            );
            }
            if (!empty($string)) return $string;
        }
        /**
	 * Loads a variable from the flexform
	 *
	 * @param	string		name of variable
	 * @param	string		name of sheet
	 * @return	string		value of var
	 */
    function flexFormValue($var, $sheet) {
            return $this->pi_getFFvalue($this->cObj->data['pi_flexform'], $var,$sheet);
    }
       
}



if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/tsara/pi3/class.tx_tsara_pi3.php'])	{
	include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/tsara/pi3/class.tx_tsara_pi3.php']);
}

?>