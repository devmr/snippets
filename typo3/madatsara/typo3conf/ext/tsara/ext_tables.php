<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}
if (TYPO3_MODE == 'BE' ) {
	require_once(t3lib_extMgm::extPath($_EXTKEY) . 'lib/user_tca.php');
}

// ajout chapô et logo
$tempColumns = array(
	'tx_tsara_metatitle' => array(
		'exclude' => 0,
		'label' => 'LLL:EXT:tsara/locallang_db.xml:tx_tsara_page.metatitle',
		'config' => array(
			'type' => 'input' 
		)
	) 
);
t3lib_extMgm::addTCAcolumns('pages', $tempColumns, 1);
t3lib_extMgm::addToAllTCAtypes('pages', 'tx_tsara_metatitle', '', 'after:subtitle');


t3lib_div::loadTCA('tt_content');
$TCA['tt_content']['types']['list']['subtypes_excludelist'][$_EXTKEY.'_pi1']='layout,select_key,pages';
$TCA['tt_content']['types']['list']['subtypes_excludelist'][$_EXTKEY.'_pi2']='layout,select_key,pages';
$TCA['tt_content']['types']['list']['subtypes_excludelist'][$_EXTKEY.'_pi3']='layout,select_key,pages';
$TCA['tt_content']['types']['list']['subtypes_excludelist'][$_EXTKEY.'_pi4']='layout,select_key,pages';



 

//Ts
t3lib_extMgm::addStaticFile($_EXTKEY,'static/', 'Madatsara');

//Flexform
$TCA['tt_content']['types']['list']['subtypes_addlist'][$_EXTKEY . '_pi1'] = 'pi_flexform';
$TCA['tt_content']['types']['list']['subtypes_addlist'][$_EXTKEY . '_pi2'] = 'pi_flexform';
$TCA['tt_content']['types']['list']['subtypes_addlist'][$_EXTKEY . '_pi3'] = 'pi_flexform';
$TCA['tt_content']['types']['list']['subtypes_addlist'][$_EXTKEY . '_pi4'] = 'pi_flexform';

t3lib_extMgm::addPiFlexFormValue($_EXTKEY . '_pi1', 'FILE:EXT:' . $_EXTKEY . '/pi1/flexform_ds.xml');
t3lib_extMgm::addPiFlexFormValue($_EXTKEY . '_pi2', 'FILE:EXT:' . $_EXTKEY . '/pi2/flexform_ds.xml');
t3lib_extMgm::addPiFlexFormValue($_EXTKEY . '_pi3', 'FILE:EXT:' . $_EXTKEY . '/pi3/flexform_ds.xml');
t3lib_extMgm::addPiFlexFormValue($_EXTKEY . '_pi4', 'FILE:EXT:' . $_EXTKEY . '/pi4/flexform_ds.xml');



//Hook
//require_once(t3lib_extMgm::extPath('tsara').'hook/class.tx_tsara_felogin.php');





$TCA['pages']['columns']['module']['config']['items'][] = array('Carte SS Plugin','tsara',t3lib_extMgm::extRelPath($_EXTKEY) . 'ext_icon.gif');
t3lib_SpriteManager::addTcaTypeIcon('pages', 'contains-tsara',t3lib_extMgm::extRelPath($_EXTKEY) . 'ext_icon.gif');



$tempColumns = Array (
	"tx_newsreadedcount_readedcounter" => Array (		
		"exclude" => 0,		
		"label" => "LLL:EXT:tsara/hook/locallang.php:tt_news.tx_newsreadedcount_readedcounter",		
		"config" => Array (
			"type" => "input",
			"size" => "6",
			"max" => "6",
			"eval" => "int",
			"checkbox" => "0",
			"default" => 0
		)
	),
    
        "tx_tsara_author" => Array (		
		"exclude" => 0,		
		"label" => "LLL:EXT:tsara/locallang_db.xml:tx_tsara_author",		
		"config" => Array (
                    'type' => 'group',
                    'internal_type' => 'db',
                    'allowed' => 'fe_users',
                    'size' => 1,
                    'minitems' => 0,
                    'maxitems' => 100,
                    'wizards' => array(
                        'suggest' => array(
                        'type' => 'suggest',
                        'fe_users' => array(
                            'maxItemsInResultList' => 5,
                            'pidList' => '79',
                        )
                        )
                    )
		)
	),
        'tx_tsarattnews_slug' => Array (		
		"exclude" => 0,		
		"label" => "LLL:EXT:tsara/locallang_db.xml:tx_tsarattnews_slug",		
		"config" => Array (
                    'type' => 'input'
		)
	), 
        'tx_tsarattnews_nbcomments' => Array (		
		"exclude" => 0,		
		"label" => "LLL:EXT:tsara/locallang_db.xml:tx_tsarattnews_nbcomments",		
		"config" => Array (
                    'type' => 'none'
                    ,'size' => '5'
                    ,'readonly' => true
		)
	), 
        'tx_tsara_exturl2' => Array (
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:tsara/locallang_db.xml:tx_tsara_exturl2',
			'config' => Array (
				'type' => 'input',
				'size' => '40',
				'max' => '256',
				'wizards' => Array(
					'_PADDING' => 2,
					'link' => Array(
						'type' => 'popup',
						'title' => 'Link',
						'icon' => 'link_popup.gif',
						'script' => 'browse_links.php?mode=wizard',
						'JSopenParams' => 'height=300,width=500,status=0,menubar=0,scrollbars=1'
					)
				)
			)
		),
          'tx_tsara_youtube_numcomment' => Array (		
		"exclude" => 0,		
		"label" => "LLL:EXT:tsara/locallang_db.xml:tx_tsara_youtube_numcomment",		
		"config" => Array (
                    'type' => 'none'
                    ,'size' => '5'
                    ,'readonly' => true
		)
	),   
        'tx_tsara_youtube_numdislikes' => Array (		
		"exclude" => 0,		
		"label" => "LLL:EXT:tsara/locallang_db.xml:tx_tsara_youtube_numdislikes",		
		"config" => Array (
                    'type' => 'none'
                    ,'size' => '5'
                    ,'readonly' => true
		)
	),  
        'tx_tsara_youtube_numlikes' => Array (		
		"exclude" => 0,		
		"label" => "LLL:EXT:tsara/locallang_db.xml:tx_tsara_youtube_numlikes",		
		"config" => Array (
                    'type' => 'none'
                    ,'size' => '5'
                    ,'readonly' => true
		)
	),
        'tx_tsara_youtube_favoritecount' => Array (		
		"exclude" => 0,		
		"label" => "LLL:EXT:tsara/locallang_db.xml:tx_tsara_youtube_favoritecount",		
		"config" => Array (
                    'type' => 'none'
                    ,'size' => '5'
                    ,'readonly' => true
		)
	),
        'tx_tsara_youtube_viewcount' => Array (		
		"exclude" => 0,		
		"label" => "LLL:EXT:tsara/locallang_db.xml:tx_tsara_youtube_viewcount",		
		"config" => Array (
                    'type' => 'none'
                    ,'size' => '5'
                    ,'readonly' => true
		)
	),
        'tx_tsara_youtube_duration' => Array (		
		"exclude" => 0,		
		"label" => "LLL:EXT:tsara/locallang_db.xml:tx_tsara_youtube_duration",		
		"config" => Array (
                    'type' => 'none'
                    ,'size' => '5'
                    ,'readonly' => true
		)
	),
        'tx_tsara_youtube_thumbnail' => Array (		
		"exclude" => 0,		
		"label" => "LLL:EXT:tsara/locallang_db.xml:tx_tsara_youtube_thumbnail",		
		"config" => Array (
                    'type' => 'none'
                    ,'size' => '50'
                    ,'readonly' => true
		)
	),
        'tx_tsara_youtubeid' => Array (		
		"exclude" => 0,		
		"label" => "LLL:EXT:tsara/locallang_db.xml:tx_tsara_youtubeid",		
		"config" => Array (
                    'type' => 'none'
                    ,'size' => '10'
                    ,'readonly' => true
		)
	),
        'tx_tsara_yttitle' => Array (		
		"exclude" => 0,		
		"label" => "LLL:EXT:tsara/locallang_db.xml:tx_tsara_yttitle",		
		"config" => Array (
                    'type' => 'none'
                    ,'size' => '50'
                    ,'readonly' => true
		)
	),
        'tx_tsara_tstamp' => Array (		
		"exclude" => 0,		
		"label" => "LLL:EXT:tsara/locallang_db.xml:tx_tsara_tstamp",		
		"config" => Array (
                    'type' => 'input'
                    ,'size' => '10'
                    ,'eval' => 'date'
                    ,'readonly' => true
		)
	),
    
        
        'tx_tsara_event_date' => Array (		
		"exclude" => 0,		
		"label" => "LLL:EXT:tsara/locallang_db.xml:tx_tsara_event_date",		
		"config" => Array (
                    'type' => 'input'
                    ,'size' => '10'
                    ,'eval' => 'datetime' 
		)
	),
         
        'tx_tsara_event_date_end' => Array (		
		"exclude" => 0,		
		"label" => "LLL:EXT:tsara/locallang_db.xml:tx_tsara_event_date_end",		
		"config" => Array (
                    'type' => 'input'
                    ,'size' => '10'
                    ,'eval' => 'date' 
		)
	),
        'tx_tsara_event_lieu' => Array (		
		"exclude" => 0,		
		"label" => "LLL:EXT:tsara/locallang_db.xml:tx_tsara_event_lieu",		
		"config" => Array (
                    'type' => 'text'
		)
	),
	/*'tx_tsara_event_lieugps' => Array (		
		"exclude" => 0,		
		"label" => "LLL:EXT:tsara/locallang_db.xml:tx_tsara_event_lieugps",		
		"config" => Array (
                    'type' => 'input'
                    ,'size' => '100'  
		)
	),*/
	'tx_tsara_event_lieugps' => Array (		
		"exclude" => 0,		
		"label" => "LLL:EXT:tsara/locallang_db.xml:tx_tsara_event_lieugps",		
		"config" => Array (
                    'type' => 'input'
                    ,'size' => '30'  
					,"wizards" => Array (
						"_PADDING" => 2,
						"link" => Array (
							"type" => "popup",
							"title" => "Google Map",
							"icon" => "EXT:tsara/lib/map16.png",
							"script" => "EXT:tsara/lib/geo_selector.php",
							"JSopenParams" => "height=350,width=500,status=0,menubar=0,scrollbars=1"
						)
					),
		)
	),
	 
        'tx_tsara_event_prix' => Array (		
		"exclude" => 0,		
		"label" => "LLL:EXT:tsara/locallang_db.xml:tx_tsara_event_prix",		
		"config" => Array (
                    'type' => 'input'
                    ,'size' => '50'  
		)
	),
        'tx_tsara_event_reservation' => Array (		
		"exclude" => 0,		
		"label" => "LLL:EXT:tsara/locallang_db.xml:tx_tsara_event_reservation",		
		"config" => Array (
                    'type' => 'text'
		)
	),
        'tx_tsara_event_prevente' => Array (		
		"exclude" => 0,		
		"label" => "LLL:EXT:tsara/locallang_db.xml:tx_tsara_event_prevente",		
		"config" => Array (
                    'type' => 'text'
		)
	),
        'tx_tsara_event_location' => Array (		
		"exclude" => 0,		
		"label" => "LLL:EXT:tsara/locallang_db.xml:tx_tsara_event_location",		
		"config" => Array (
                    'type' => 'text'
		)
	),
        'tx_tsara_event_contacts' => Array (		
		"exclude" => 0,		
		"label" => "LLL:EXT:tsara/locallang_db.xml:tx_tsara_event_contacts",		
		"config" => Array (
                    'type' => 'text' 
                    ,'cols' => '30',
				'rows' => '5',
				'wizards' => array(
					'_PADDING' => 2,
					'RTE' => array(
						'notNewRecords' => 1,
						'RTEonly'       => 1,
						'type'          => 'script',
						'title'         => 'Full screen Rich Text Editing|Formatteret redigering i hele vinduet',
						'icon'          => 'wizard_rte2.gif',
						'script'        => 'wizard_rte.php',
					),
				),
		)
	),
        'tx_tsara_cine_format' => Array (		
		"exclude" => 0,		
		"label" => "LLL:EXT:tsara/locallang_db.xml:tx_tsara_cine_format",		
		"config" => Array (
                    'type' => 'input'
                    ,'size' => '20'  
		)
	),
        'tx_tsara_cine_duree' => Array (		
		"exclude" => 0,		
		"label" => "LLL:EXT:tsara/locallang_db.xml:tx_tsara_cine_duree",		
		"config" => Array (
                    'type' => 'input'
                    ,'size' => '10'  
		)
	),
        'tx_tsara_cine_anneesortie' => Array (		
		"exclude" => 0,		
		"label" => "LLL:EXT:tsara/locallang_db.xml:tx_tsara_cine_anneesortie",		
		"config" => Array (
                    'type' => 'input'
                    ,'size' => '5'  
		)
	),
        'tx_tsara_cine_siteweb' => Array (
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:tsara/locallang_db.xml:tx_tsara_cine_siteweb',
			'config' => Array (
				'type' => 'input',
				'size' => '40',
				'max' => '256',
				'wizards' => Array(
					'_PADDING' => 2,
					'link' => Array(
						'type' => 'popup',
						'title' => 'Link',
						'icon' => 'link_popup.gif',
						'script' => 'browse_links.php?mode=wizard',
						'JSopenParams' => 'height=300,width=500,status=0,menubar=0,scrollbars=1'
					)
				)
			)
		),
    "tx_tsara_cine_auteurs" => Array (		
		"exclude" => 0,		
		"label" => "LLL:EXT:tsara/locallang_db.xml:tx_tsara_cine_auteurs",		
		'config' => array(
				'type' => 'group',
                                'internal_type' => 'db',
                                'allowed' => 'tt_address',
				'size' => 6,
				'autoSizeMax' => 10,
				'minitems' => 0,
				'maxitems' => 20
				,'MM'            => 'tx_tsara_cine_auteurs_mm',
                                'wizards' => array(
                                    'suggest' => array(
                                    'type' => 'suggest',
                                    'fe_users' => array(
                                        'maxItemsInResultList' => 5,
                                        'pidList' => '79',
                                    )
                                    )
                                )
			)
	),
     
    "tx_tsara_cine_acteurs" => Array (		
		"exclude" => 0,		
		"label" => "LLL:EXT:tsara/locallang_db.xml:tx_tsara_cine_acteurs",		
		'config' => array(
				
                                
                                'type' => 'inline',
                                'foreign_table' => 'tx_tsara_cine_acteurs',
                                'foreign_field' => 'tx_tsara_cine_acteurs',
                                'maxitems' => 20,
                                'appearance' => Array(
                                  'collapseAll' => 1,
                                  'expandSingle' => 1,
                                ),
			)
	),
    "tx_tsara_cine_realisateurs" => Array (		
		"exclude" => 0,		
		"label" => "LLL:EXT:tsara/locallang_db.xml:tx_tsara_cine_realisateurs",		
		'config' => array(
                                'type' => 'group',
                                'internal_type' => 'db',
                                'allowed' => 'tt_address',
				 
				'size' => 6,
				'autoSizeMax' => 10,
				'minitems' => 0,
				'maxitems' => 20
				,'MM'            => 'tx_tsara_cine_realisateurs_mm',
                                'wizards' => array(
                                    'suggest' => array(
                                    'type' => 'suggest',
                                    'fe_users' => array(
                                        'maxItemsInResultList' => 5,
                                        'pidList' => '79',
                                    )
                                    )
                                )
			)
	),
    
    "tx_tsara_cine_musique" => Array (		
		"exclude" => 0,		
		"label" => "LLL:EXT:tsara/locallang_db.xml:tx_tsara_cine_musique",		
		'config' => array(
				'type' => 'group',
                                'internal_type' => 'db',
                                'allowed' => 'tt_address',
				'size' => 6,
				'autoSizeMax' => 10,
				'minitems' => 0,
				'maxitems' => 20
				,'MM'            => 'tx_tsara_cine_musique_mm',
                                'wizards' => array(
                                    'suggest' => array(
                                    'type' => 'suggest',
                                    'fe_users' => array(
                                        'maxItemsInResultList' => 5,
                                        'pidList' => '79',
                                    )
                                    )
                                )
			)
	),
    "tx_tsara_cine_production" => Array (		
		"exclude" => 0,		
		"label" => "LLL:EXT:tsara/locallang_db.xml:tx_tsara_cine_production",		
		'config' => array(
				'type' => 'group',
                                'internal_type' => 'db',
                                'allowed' => 'tt_address',
				'size' => 6,
				'autoSizeMax' => 10,
				'minitems' => 0,
				'maxitems' => 20
				,'MM'            => 'tx_tsara_cine_production_mm',
                                'wizards' => array(
                                    'suggest' => array(
                                    'type' => 'suggest',
                                    'fe_users' => array(
                                        'maxItemsInResultList' => 5,
                                        'pidList' => '79',
                                    )
                                    )
                                )
			)
	),
    "tx_tsara_cine_scenario" => Array (		
		"exclude" => 0,		
		"label" => "LLL:EXT:tsara/locallang_db.xml:tx_tsara_cine_scenario",		
		'config' => array(
				'type' => 'group',
                                'internal_type' => 'db',
                                'allowed' => 'tt_address',
				'size' => 6,
				'autoSizeMax' => 10,
				'minitems' => 0,
				'maxitems' => 20
				,'MM'            => 'tx_tsara_cine_scenario_mm',
                                'wizards' => array(
                                    'suggest' => array(
                                    'type' => 'suggest',
                                    'fe_users' => array(
                                        'maxItemsInResultList' => 5,
                                        'pidList' => '79',
                                    )
                                    )
                                )
			)
	),
    "tx_tsara_cine_directeur_photo" => Array (		
		"exclude" => 0,		
		"label" => "LLL:EXT:tsara/locallang_db.xml:tx_tsara_cine_directeur_photo",		
		'config' => array(
				'type' => 'group',
                                'internal_type' => 'db',
                                'allowed' => 'tt_address',
				'size' => 6,
				'autoSizeMax' => 10,
				'minitems' => 0,
				'maxitems' => 20
				,'MM'            => 'tx_tsara_cine_directeur_photo_mm',
                                'wizards' => array(
                                    'suggest' => array(
                                    'type' => 'suggest',
                                    'fe_users' => array(
                                        'maxItemsInResultList' => 5,
                                        'pidList' => '79',
                                    )
                                    )
                                )
			)
	),
    "tx_tsara_cine_montage_son" => Array (		
		"exclude" => 0,		
		"label" => "LLL:EXT:tsara/locallang_db.xml:tx_tsara_cine_montage_son",		
		'config' => array(
				'type' => 'group',
                                'internal_type' => 'db',
                                'allowed' => 'tt_address',
				'size' => 6,
				'autoSizeMax' => 10,
				'minitems' => 0,
				'maxitems' => 20
				,'MM'            => 'tx_tsara_cine_montage_son_mm',
                                'wizards' => array(
                                    'suggest' => array(
                                    'type' => 'suggest',
                                    'fe_users' => array(
                                        'maxItemsInResultList' => 5,
                                        'pidList' => '79',
                                    )
                                    )
                                )
			)
	),
    "tx_tsara_cine_perchman" => Array (		
		"exclude" => 0,		
		"label" => "LLL:EXT:tsara/locallang_db.xml:tx_tsara_cine_perchman",		
		'config' => array(
				'type' => 'group',
                                'internal_type' => 'db',
                                'allowed' => 'tt_address',
				'size' => 6,
				'autoSizeMax' => 10,
				'minitems' => 0,
				'maxitems' => 20
				,'MM'            => 'tx_tsara_cine_perchman_mm',
                                'wizards' => array(
                                    'suggest' => array(
                                    'type' => 'suggest',
                                    'fe_users' => array(
                                        'maxItemsInResultList' => 5,
                                        'pidList' => '79',
                                    )
                                    )
                                )
			)
	),
    "tx_tsara_cine_effets_visuels" => Array (		
		"exclude" => 0,		
		"label" => "LLL:EXT:tsara/locallang_db.xml:tx_tsara_cine_effets_visuels",		
		'config' => array(
				'type' => 'group',
                                'internal_type' => 'db',
                                'allowed' => 'tt_address',
				'size' => 6,
				'autoSizeMax' => 10,
				'minitems' => 0,
				'maxitems' => 20
				,'MM'            => 'tx_tsara_cine_effets_visuels_mm',
                                'wizards' => array(
                                    'suggest' => array(
                                    'type' => 'suggest',
                                    'fe_users' => array(
                                        'maxItemsInResultList' => 5,
                                        'pidList' => '79',
                                    )
                                    )
                                )
			)
	),
    "tx_tsara_cine_videosfilm" => Array (		
		"exclude" => 0,		
		"label" => "LLL:EXT:tsara/locallang_db.xml:tx_tsara_cine_videosfilm",		
		'config' => array(
                                'type' => 'inline',
                                'foreign_table' => 'tx_tsara_videosfilm',
                                'foreign_field' => 'tx_tsara_cine_videosfilm',
                                'maxitems' => 20,
                                'appearance' => Array(
                                  'collapseAll' => 1,
                                  'expandSingle' => 1,
                                ),
				//'MM'            => 'tx_tsara_cine_videosfilm_mm',
			)
	),
    "tx_tsara_event_endroit" => Array (		
		"exclude" => 0,		
		"label" => "Lieu dans la BDD",		
		'config' => array(
                                'type' => 'group',
                                'internal_type' => 'db',
                                'allowed' => 'tt_address',
				'size' => 1,
				'autoSizeMax' => 10,
				'minitems' => 0,
				'maxitems' => 1
                                ,'wizards' => array(
                                    'suggest' => array(
                                    'type' => 'suggest',
                                    'fe_users' => array(
                                        'maxItemsInResultList' => 5,
                                        'pidList' => '55',
                                    )
                                    )
                                )
			)
	),
        
);


t3lib_div::loadTCA("tt_news");
t3lib_extMgm::addTCAcolumns("tt_news",$tempColumns,1);
t3lib_extMgm::addToAllTCAtypes("tt_news","tx_newsreadedcount_readedcounter","","after:short");
t3lib_extMgm::addToAllTCAtypes("tt_news","tx_tsara_author","","after:author");
t3lib_extMgm::addToAllTCAtypes("tt_news","tx_tsara_exturl2","","after:title");
t3lib_extMgm::addToAllTCAtypes("tt_news","tx_tsara_cine_videosfilm","","after:news_files" );
t3lib_extMgm::addToAllTCAtypes("tt_news","tx_tsarattnews_slug,tx_tsarattnews_nbcomments,tx_tsara_youtubeid,tx_tsara_youtube_thumbnail,tx_tsara_youtube_duration,tx_tsara_youtube_viewcount,tx_tsara_youtube_favoritecount,tx_tsara_youtube_numlikes,tx_tsara_youtube_numdislikes,tx_tsara_youtube_numcomment,tx_tsara_yttitle,tx_tsara_tstamp, tx_tsara_cine_format, tx_tsara_cine_duree,  tx_tsara_cine_anneesortie,  tx_tsara_cine_videosfilm, tx_tsara_cine_acteurs, tx_tsara_cine_auteurs, tx_tsara_cine_realisateurs, tx_tsara_cine_musique, tx_tsara_cine_production,tx_tsara_cine_scenario,tx_tsara_cine_directeur_photo,tx_tsara_cine_montage_son,tx_tsara_cine_perchman,tx_tsara_cine_effets_visuels" );
t3lib_extMgm::addToAllTCAtypes("tt_news","tx_tsara_event_date,tx_tsara_event_date_end","","after:datetime" );
t3lib_extMgm::addToAllTCAtypes("tt_news","tx_tsara_event_endroit,tx_tsara_event_lieu,tx_tsara_event_lieugps","","after:image" );
t3lib_extMgm::addToAllTCAtypes("tt_news","tx_tsara_event_prix,tx_tsara_event_reservation,tx_tsara_event_prevente,tx_tsara_event_location,tx_tsara_event_contacts;;;richtext[]:rte_transform[mode=ts]","","after:bodytext"  );

$tempColumns = Array (
	 
    
        "tx_tsara_pagelist" => Array (		
		"exclude" => 0,		
		"label" => "LLL:EXT:tsara/locallang_db.xml:tx_tsara_pagelist",		
		"config" => Array (
                    'type' => 'group',
                    'internal_type' => 'db',
                    'allowed' => 'pages',
                    'size' => 1,
                    'minitems' => 0,
                    'maxitems' => 100,
                    'wizards' => array(
                        'suggest' => array(
                        'type' => 'suggest',
                        'fe_users' => array(
                            'maxItemsInResultList' => 5,
                            'pidList' => '79',
                        )
                        )
                    )
		)
	),
    
        "tx_tsara_keywords" => Array (		
		"exclude" => 0,		
		"label" => "LLL:EXT:tsara/locallang_db.xml:tx_tsara_keywords",		
		"config" => Array (
                    'type' => 'text' 
		)
	),
        
);


t3lib_div::loadTCA("tt_news_cat");
t3lib_extMgm::addTCAcolumns("tt_news_cat",$tempColumns,1);
t3lib_extMgm::addToAllTCAtypes("tt_news_cat","tx_tsara_pagelist","","after:title");
t3lib_extMgm::addToAllTCAtypes("tt_news_cat","tx_tsara_keywords");

//Add fields in other tables
$tempColumns = Array (
     'description' => array (		
			'exclude' => 0,		
			'label' => 'LLL:EXT:tsara/locallang_db.xml:tx_tsara_author.description',		
			'config' => array (
				'type' => 'text',
				'cols' => '30',
				'rows' => '5',
				'wizards' => array(
					'_PADDING' => 2,
					'RTE' => array(
						'notNewRecords' => 1,
						'RTEonly'       => 1,
						'type'          => 'script',
						'title'         => 'Full screen Rich Text Editing|Formatteret redigering i hele vinduet',
						'icon'          => 'wizard_rte2.gif',
						'script'        => 'wizard_rte.php',
					),
				),
			)
		),
    'comptes' => array(
			'label' => 'LLL:EXT:tsara/locallang_db.xml:tx_tsara_author.comptes',		
			'config' => Array(
                            'type' => 'inline',
                            'foreign_table' => 'tx_tsara_compteauthor',
                            'foreign_field' => 'comptes',
                            'maxitems' => 10,
                            'appearance' => Array(
                              'collapseAll' => 1,
                              'expandSingle' => 1,
                            ),
                          ),
		),
	'souhaitecontact'  => array (		
			'exclude' => 1,
			'label'   => 'LLL:EXT:tsara/locallang_db.xml:tx_tsara_author.souhaitecontact',
			'config'  => array (
				'type'    => 'check',
				'default' => '0'
			)
		),
     
     
     
);
t3lib_extMgm::addTCAcolumns("fe_users",$tempColumns,1);
t3lib_extMgm::addToAllTCAtypes("fe_users","description;;;richtext[]:rte_transform[mode=ts]","","after:name");
t3lib_extMgm::addToAllTCAtypes("fe_users","comptes","","after:image");
t3lib_extMgm::addToAllTCAtypes("fe_users","souhaitecontact","","after:email");


$TCA['tx_tsara_compteauthor'] = array (
	'ctrl' => array (
		'title'     => 'LLL:EXT:tsara/locallang_db.xml:tx_tsara_compteauthor',		
		'label'     => 'typecompte',	
		'tstamp'    => 'tstamp',
		'crdate'    => 'crdate',
		'cruser_id' => 'cruser_id',
		'default_sortby' => 'ORDER BY crdate',	
		'delete' => 'deleted',	
                'dividers2tabs' => true,
		'enablecolumns' => array (		
			'disabled' => 'hidden',
		),
		'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY).'tca.php',
		'iconfile'          => t3lib_extMgm::extRelPath($_EXTKEY).'icon_tx_tsara_author.gif',
	),
);

$TCA['tx_tsara_videosfilm'] = array (
	'ctrl' => array (
		'title'     => 'LLL:EXT:tsara/locallang_db.xml:tx_tsara_videosfilm',		
		'label'     => 'typecompte',	
		'tstamp'    => 'tstamp',
		'crdate'    => 'crdate',
		'cruser_id' => 'cruser_id',
		'default_sortby' => 'ORDER BY crdate',	
		'delete' => 'deleted',	
                'dividers2tabs' => true,
		'enablecolumns' => array (		
			'disabled' => 'hidden',
		),
		'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY).'tca.php',
		'iconfile'          => t3lib_extMgm::extRelPath($_EXTKEY).'icon_tx_tsara_author.gif',
	),
);

$TCA['tx_tsara_cine_acteurs'] = array (
	'ctrl' => array (
		'title'     => 'LLL:EXT:tsara/locallang_db.xml:tx_tsara_cine_acteurs',		
		'label'     => 'title',	
		'tstamp'    => 'tstamp',
		'crdate'    => 'crdate',
		'cruser_id' => 'cruser_id',
		'default_sortby' => 'ORDER BY crdate',	
		'delete' => 'deleted',	
                'dividers2tabs' => true,
		'enablecolumns' => array (		
			'disabled' => 'hidden',
		),
		'dynamicConfigFile' => t3lib_extMgm::extPath($_EXTKEY).'tca.php',
		'iconfile'          => t3lib_extMgm::extRelPath($_EXTKEY).'icon_tx_tsara_author.gif',
	),
);


$TCA['tt_news']['ctrl']['searchFields'] = 'title';
$TCA['tt_news_cat']['ctrl']['searchFields'] = 'title';
$TCA['tt_address']['ctrl']['searchFields'] = 'title';
$TCA['tt_news_cat']['columns']['image']['config']['max_size'] = 2000;


t3lib_extMgm::addPlugin(array(
    'LLL:EXT:tsara/locallang_db.xml:tt_content.list_type_pi1',
    $_EXTKEY . '_pi1',
    t3lib_extMgm::extRelPath($_EXTKEY) . 'ext_icon.gif'
),'list_type');

t3lib_extMgm::addPlugin(array(
    'LLL:EXT:tsara/locallang_db.xml:tt_content.list_type_pi2',
    $_EXTKEY . '_pi2',
    t3lib_extMgm::extRelPath($_EXTKEY) . 'ext_icon.gif'
),'list_type');
t3lib_extMgm::addPlugin(array(
    'LLL:EXT:tsara/locallang_db.xml:tt_content.list_type_pi3',
    $_EXTKEY . '_pi3',
    t3lib_extMgm::extRelPath($_EXTKEY) . 'ext_icon.gif'
),'list_type');
t3lib_extMgm::addPlugin(array(
    'LLL:EXT:tsara/locallang_db.xml:tt_content.list_type_pi4',
    $_EXTKEY . '_pi4',
    t3lib_extMgm::extRelPath($_EXTKEY) . 'ext_icon.gif'
),'list_type');
 

//pi5
$TCA['tt_content']['types']['list']['subtypes_excludelist'][$_EXTKEY.'_pi5']='layout,select_key,pages';
$TCA['tt_content']['types']['list']['subtypes_addlist'][$_EXTKEY . '_pi5'] = 'pi_flexform';
t3lib_extMgm::addPiFlexFormValue($_EXTKEY . '_pi5', 'FILE:EXT:' . $_EXTKEY . '/pi5/flexform_ds.xml');
t3lib_extMgm::addPlugin(array(
    'LLL:EXT:tsara/locallang_db.xml:tt_content.list_type_pi5',
    $_EXTKEY . '_pi5',
    t3lib_extMgm::extRelPath($_EXTKEY) . 'ext_icon.gif'
),'list_type');

//pi6
$TCA['tt_content']['types']['list']['subtypes_excludelist'][$_EXTKEY.'_pi6']='layout,select_key,pages';
$TCA['tt_content']['types']['list']['subtypes_addlist'][$_EXTKEY . '_pi6'] = 'pi_flexform';
t3lib_extMgm::addPiFlexFormValue($_EXTKEY . '_pi6', 'FILE:EXT:' . $_EXTKEY . '/pi6/flexform_ds.xml');
t3lib_extMgm::addPlugin(array(
    'LLL:EXT:tsara/locallang_db.xml:tt_content.list_type_pi6',
    $_EXTKEY . '_pi6',
    t3lib_extMgm::extRelPath($_EXTKEY) . 'ext_icon.gif'
),'list_type');

//Add fields in other tables
$tempColumns = Array (
     'tx_tsara_youtube_published' => array (		
			'exclude' => 0,		
			'label' => 'Date Youtube',		
			'config' => array (
				'type' => 'input',
				'size' => '50'
			)
		),
     
	 
     
     
);
t3lib_extMgm::addTCAcolumns("tx_comments_comments",$tempColumns,1);
t3lib_extMgm::addToAllTCAtypes("tx_comments_comments","tx_tsara_youtube_published","","");

//Add fields in other tables
$tempColumns = Array (
     'tx_tsara_event_lieugps' => array (		
			'exclude' => 0,		
			'label' => 'Coordonnées GPS',		
			'config' => array (
				'type' => 'input',
				'size' => '30'  
                                ,"wizards" => Array (
                                        "_PADDING" => 2,
                                        "link" => Array (
                                                "type" => "popup",
                                                "title" => "Google Map",
                                                "icon" => "EXT:tsara/lib/map16.png",
                                                "script" => "EXT:tsara/lib/geo_selector.php",
                                                "JSopenParams" => "height=350,width=500,status=0,menubar=0,scrollbars=1"
                                        )
                                ),
			)
		),
     
	 
     
     
);
t3lib_extMgm::addTCAcolumns("tt_address",$tempColumns,1);
t3lib_extMgm::addToAllTCAtypes("tt_address","tx_tsara_event_lieugps","","");

?>