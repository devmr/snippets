<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2010 Miary RABEHASY <mrabehasy@netbooster.com>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * [CLASS/FUNCTION INDEX of SCRIPT]
 *
 * Hint: use extdeveval to insert/update function index above.
 */


/**
 * Plugin 'ttb' for the 'ttb_base' extension.
 *
 * @author    Miary RABEHASY <mrabehasy@netbooster.com>
 * @package    TYPO3
 * @subpackage    tx_ttbbase
 */
require_once(t3lib_extMgm::extPath("tsara").'lib/class.tx_tsara_util.php');
class tx_tsara_pi4 extends  tslib_pibase {
    var $prefixId      = 'tx_tsara_pi4';        // Same as class name
    var $scriptRelPath = 'pi4/class.tx_tsara_pi4.php';    // Path to this script relative to the extension dir.
    var $extKey        = 'tsara';    // The extension key.
    var $pi_checkCHash = true;

    /**
     * The main method of the PlugIn
     *
     * @param    string        $content: The PlugIn content
     * @param    array        $conf: The PlugIn configuration
     * @return    The content that is displayed on the website
     */
    function main($content, $conf) {
        $this->confArr = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['tsara']); 
        $this->conf = $conf;
        $this->pi_setPiVarDefaults();
        $this->util = t3lib_div::makeInstance('tx_tsara_util');
        // Init FlexForm configuration for plugin:
        $this->pi_initPIflexForm();
        $this->preInit();
        $this->pi_loadLL();
        
        $templateFile = $this->flexFormValue('templateFile', 's_template') ? $this->flexFormValue('templateFile', 's_template') : 'EXT:tsara/pi3/template.html';
//        $this->template = $this->cObj->fileResource($templateFile);
        $this->template = t3lib_div::getFileAbsFileName( $templateFile );
         
//        if( $this->flexFormValue('jsonFile', 's_template') == '' ) return $this->pi_getLL('error_json_inexistant');
        
//        if( $this->flexFormValue('jsonFile', 's_template') !='' && $this->cObj->fileResource($this->flexFormValue('jsonFile', 's_template'))=='' ){             
//            return $this->pi_getLL('error_json_inexistant');           
//        }
        
//        $this->conf['jsonFile'] = $this->flexFormValue('jsonFile', 's_template');
        $this->conf['page_listephotos'] = $this->flexFormValue('page_listephotos', 's_template');
        $this->conf['limit'] = $this->flexFormValue('limit', 's_template');
        $this->conf['view'] = $this->flexFormValue('view', 's_template');
        $this->conf['width'] = $this->flexFormValue('width', 's_template');
        $this->conf['height'] = $this->flexFormValue('height', 's_template');
        //Exploder le view
        $view =  $this->conf['view'] ;
        
        
        $this->page_listephotos = ($this->conf['page_listephotos']>0?$this->conf['page_listephotos']:$GLOBALS['TSFE']->id);
        $this->pidList = ($this->flexFormValue('pidList', 's_template')>0?$this->flexFormValue('pidList', 's_template'):$GLOBALS['TSFE']->id);
//        $this->json = json_decode( $this->cObj->fileResource($this->conf['jsonFile']), true ); 
        
        
        

        $this->limit = (intval( $this->conf['limit'] ) > 0 ? intval( $this->conf['limit'] ) : ( intval( $this->conf['pagebrowser.']['limit'] ) > 0 ? intval( $this->conf['pagebrowser.']['limit'] ) : '*') );

        $content = $this->afficher_new($view);
	
                
	$content = $this->replace( $content  );
        return $this->conf['wrapContentInBaseClass'] ? $this->pi_wrapInBaseClass($content) : $content;
    }
    function afficher_new($view ){
        $row = $tabdim = array();
        
        $row = $this->util->get_random_photos($this);
                
            
        $fluid = t3lib_div::makeInstance('Tx_Fluid_View_StandaloneView'); // instanciate standalone view
        // Emplacement du template
        

        //on spécifie l'emplacement du template
        $fluid->setTemplatePathAndFilename($this->template);

                

        //on assigne une variable
        $fluid->assign('news', $row );
        $fluid->assign('width', $this->conf['width'] );
        $fluid->assign('height', $this->conf['height'] );
        $fluid->assign('page_listephotos', $this->page_listephotos);

        return $fluid->render();
    }
    function afficher($view ){
            
            $content = '';
            
                 if( $this->piVars['albumid'] != ''  ) $view = 'LISTPHOTOS';
            
                $tabdata = $this->getRowdata( $view );
                
                //print_r( $tabdata );

		
                
                $subpart  = $this->cObj->getSubpart($this->template, '###TEMPLATE'.($view!='' ? '_'.$view:'').'###');

		$row = array();
                $tab = array();
                $data = array();

                $condition = null;
                

                
                
                if( count( $tabdata ) > 0 ) {
                    
                    $nbphoto = count( $tabdata );
                    
                    $this->totalresult = $nbphoto;
                    
                    $first = 0;
                    $limit = $this->conf['limit'];
                        
                    if(  t3lib_utility_Math::convertToPositiveInteger( $this->piVars['pointer'] )  ) {
                        $first =  $limit*t3lib_div::removeXSS( $this->piVars['pointer'] ) ;
                        $limit =  $limit*(t3lib_div::removeXSS( $this->piVars['pointer'] )+1) ;                         
                    }
                    
                    if($limit>0 && $nbphoto < $limit ) $limit = $nbphoto;
                    if($limit>0 && $nbphoto > $limit ) $limit = $limit;
                    
                    
                    if( $limit >0){
                        
                        
                        
                         
                        
                        for( $i = $first; $i < $limit; $i++ ){
                            $tabdatanew[] = array(
                                'title' => $tabdata[$i]['title'],
                                'thumbnail' => $tabdata[$i]['thumbnail'],
                                'nbphotos' => $tabdata[$i]['nbphotos'],
                                'description' => $tabdata[$i]['description'],
                                'content' => $tabdata[$i]['content'],
                                'id' => $tabdata[$i]['id']
                            );
                        }
                        
                        $tabdata = $tabdatanew;
                    }
                    
                    
                    
                    
                    
                    $i = 0;
                    foreach( $tabdata as $r  ){
                        
                        $tabfile = t3lib_div::split_fileref($r['thumbnail']);
                        
                        $fichier = $tabfile['file'];
                        
                        $data['CONTENT'][$i]['TITLE'] = ($fichier==$r['title']?'':$r['title']);
                        $data['CONTENT'][$i]['nbphotos'] = ($r['nbphotos']>0?$r['nbphotos']:'');
                        $data['CONTENT'][$i]['DESCRIPTION'] = $r['description'];
                        $data['CONTENT'][$i]['HEIGHT'] = $this->conf['height'];
                        $data['CONTENT'][$i]['WIDTH'] = $this->conf['width'];
                        $data['CONTENT'][$i]['THUMBNAILS'] = str_replace("http://","", $r['thumbnail']);
                        $data['CONTENT'][$i]['BIGIMG'] = ($r['content']);
                        
                        $data['CONTENT'][$i]['LINK_LISTEPHOTOS'] = $GLOBALS['TSFE']->cObj->typoLink('', array(
                                                                'parameter' => $this->page_listephotos,
                                                                'additionalParams' => '&'.$this->prefixId.'[albumid]='.$r['id'],
                                                                'useCacheHash' => 1,
                                                                'returnLast' => 'url',
                        )                                                   );
                        
                       
                        
                        $i++;
                    }
                    
                    
                }
                 
                $data['TITLE_ALBUM'] = self::gettitle();
                $data['LINK_RETOUR'] = $data['LINK_LISTEPHOTOS'] = $GLOBALS['TSFE']->cObj->typoLink('', array(
                                                                'parameter' => $this->page_listephotos,
                                                                'additionalParams' => '',
                                                                'useCacheHash' => 1,
                                                                'returnLast' => 'url',
                                                ));

                $data['PAGINATION'] = (is_array($tabdata) && $this->totalresult > $this->conf['limit'] ? $this->afficherPagination( 'pointer' ): '');
                $content = $this->cObj->substituteMarkerAndSubpartArrayRecursive($subpart, $data, '###|###' ); 


                
                
                return $content;
	}
  


        function gettitle(){
            $albumid = '';
            if( $this->piVars['albumid'] != ''  ) $albumid = $this->piVars['albumid'];
            else return '';
            if( trim( $albumid ) != '' ){
                        foreach( $this->json['detailcompte'] as $liste ){
                            if( $liste['id'] == $albumid ) {
                                return $liste['title'];
                                
                            }
                        }
             }
                    
        }
        function getRowdata( $view ){
            
            
            $row = array();
            //print_r( $this->json );
            
            
            
            switch( $view ){
                default:
                case 'LISTALBUM' :
                    $i=1;
                    foreach( $this->json['detailcompte'] as $liste ){
                        $row[] = array(
                            'id' => $liste['id'],
                            'date' => $liste['date'],
                            'maj' => $liste['maj'],
                            'title' => $liste['title'],
                            'description' => $liste['description'],
                            'thumbnail' => $liste['thumbnail'],
                            'nbphotos' => $liste['photos'][0]['nbphotos']
                        );
                        
                        $i++;
                    }
                    
                    

                break;
                case 'LISTPHOTOS' :
                    
                    if( $this->conf['albumid'] == '' && $this->piVars['albumid'] == '' ) return array();
                    if( $this->conf['albumid'] != ''  ) $albumid = $this->conf['albumid'];
                    if( $this->piVars['albumid'] != ''  ) $albumid = $this->piVars['albumid'];
                    
                    if( trim( $albumid ) != '' ){
                        foreach( $this->json['detailcompte'] as $liste ){
                            if( $liste['id'] == $albumid ) {
                                $i=1;
                                foreach( $liste['photos'][0]['liste'] as $photos ){
                                    $row[] = array(
                                        'date' => $photos['published'],
                                        'maj' => $photos['updated'],
                                        'title' => $photos['title'],
                                        'description' => '',
                                        'content' => $photos['content'],
                                        'thumbnail' => $photos['thumbnail']
                                    );
                                    $i++;
                                }
                            }
                        }
                    }
                    
                    

                break;
                
            }
            
            //print_r ($row);
            return $row;
            
            
        }
        function afficherPagination( $pointerName = 'pointer' )  {
	$pObj = $this; // make a reference to the parent object
	//echo $this->totalresult.',';
        if( $this->totalresult > $this->conf['limit'] ) {


		    $this->internal['res_count'] = $this->totalresult;
		    $this->internal['results_at_a_time'] = ($this->conf['limit'] > 0 ? $this->conf['limit'] : ($this->conf['pageBrowser.']['limit'] > 0 ? $this->conf['pageBrowser.']['limit'] : ''));
		    $this->internal['maxPages'] = ($this->conf['pageBrowser.']['maxPages'] > 0 ? $this->conf['pageBrowser.']['maxPages'] : '5');

		} else {
		    return  '';
		}    

	// Initializing variables:
	$showResultCount = $pObj->conf['pageBrowser.']['showResultCount'];
	$tableParams = $pObj->conf['pageBrowser.']['tableParams'];
	$pointer = $pObj->piVars['pointer'];
	$count = $pObj->internal['res_count'];
	$results_at_a_time = t3lib_div::intInRange($pObj->internal['results_at_a_time'], 1, 1000);
	$maxPages = t3lib_div::intInRange($pObj->internal['maxPages'], 1, 100);
	$max = t3lib_div::intInRange(ceil($count / $results_at_a_time), 1, $maxPages);
	$pointer = intval($pointer);
	$links = $linkslist = array();
	$pageCount = ceil($count/$results_at_a_time);

	// Make browse-table/links:
	if ($pObj->pi_alwaysPrev >= 0) {
            
                $links[] = '<a   href="'.$pObj->pi_linkTP_keepPIvars_url (array( 'pointer' => 0  ), 1 ).'">«</a>';

		if ($pointer > 0) {
			$links[] = '<a    href="' . $pObj->pi_linkTP_keepPIvars_url( array('pointer' => ($pointer-1?$pointer-1:'')), $pObj->allowCaching) . '">‹</a> ';
		} elseif ($pObj->pi_alwaysPrev) {
			$links[] = '
					' . $pObj->pi_getLL('pi_list_browseresults_prev', '< Previous') . '';
		}
	}

	if($pointer<$maxPages) $debut = 0;
	else $debut = ($pointer+$maxPages);

        $links[] = ' ';

	for($a=0;$a<$pageCount;$a++)    {
		$min = max(0, $pointer+1-ceil($maxPages/2));
		$max = $min+$maxPages;
		if($max>$pageCount)    {
		    $min = $min - ($max-$pageCount);
		}

		if($a >= $min && $a < $max)    {
		    $linkslist[] =  (
				 $a==$pointer ?
					'<span class="current">'.($a+1).'</span> '
					 :
					'<a  href="'.$pObj->pi_linkTP_keepPIvars_url( array(  'pointer'  => ( $a ? $a : '' ) ), $pObj->allowCaching ).'" >'.trim( ( $a + 1) ).'</a>'

				);
		}
	}
        $links[] = implode(' ', $linkslist );
        $links[] = ' ';

	/*

	for($a = 0;$a < $max;$a++) {
		$links[] = ($a==$pointer ? '<span>'.($a+1).'</span>' : $pObj->pi_linkTP_keepPIvars(trim($pObj->pi_getLL('pi_list_browseresults_page', 'Page') . ' ' . ($a + 1)), array('pointer' => ($a?$a:'')), $pObj->allowCaching));
	}

	*/


	if ($pointer < ceil($count / $results_at_a_time)-1) {
		$links[] = '<a  href="'.$pObj->pi_linkTP_keepPIvars_url( array('pointer' => $pointer + 1), $pObj->allowCaching).'">›</a>';
	
                $links[] = '<a   href="'.$pObj->pi_linkTP_keepPIvars_url (array(   'pointer' => (ceil( $count/$results_at_a_time ) - 1)), 1 ).'">»</a>' ;
        }

	$pR1 = $pointer * $results_at_a_time + 1;
	$pR2 = $pointer * $results_at_a_time + $results_at_a_time;


	$sTables = '

		<!--
			List browsing box:
		-->
                
		' .
	($showResultCount ? '
			<div id="paginationliste"><p class="align-center">' .
		($pObj->internal['res_count'] ?
			sprintf(
				str_replace('###SPAN_BEGIN###', '<span' . $pObj->pi_classParam('browsebox-strong') . '>', $pObj->pi_getLL('pi_list_browseresults_displays', 'Displaying results ###SPAN_BEGIN###%s to %s</span> out of ###SPAN_BEGIN###%s</span>')),
				$pObj->internal['res_count'] > 0 ? $pR1 : 0,
				min(array($pObj->internal['res_count'], $pR2)),
				$pObj->internal['res_count']
				) :
			$pObj->pi_getLL('pi_list_browseresults_noResults', 'Sorry, no items were found.')) . '</p><div class="section group " ><div class="col span_1_of_6">Aller à la page</div>':''
		) . '



					<div class="col span_5_of_6"><div class="pagination-wrapper clearfix"><div class="pagination clearfix">' . implode('', $links) . '</div></div></div>



<!--
			End List browsing box:
		-->
		';


	return $sTables;
}
         function preInit() {

		 

		$flexformTyposcript = $this->flexFormValue('myTS', 's_template');
		if ($flexformTyposcript) {
			require_once(PATH_t3lib.'class.t3lib_tsparser.php');
			$tsparser = t3lib_div::makeInstance('t3lib_tsparser');
			// Copy conf into existing setup
			$tsparser->setup = $this->conf;
			// Parse the new Typoscript
			$tsparser->parse($flexformTyposcript);
			// Copy the resulting setup back into conf
			$this->conf = $tsparser->setup;
		}

		 
	}
        function replace( $content ){
            // 1. replace ll markers
            $content = preg_replace_callback ( // Automaticly fill locallangmarkers with fitting value of locallang.xml
                    '$###LL_(.*)###$Uis', // regulare expression
                    array($this, 'DynamicLocalLangMarker'), // open function
                    $content // current content
            );
            $content = preg_replace_callback ( // Automaticly fill locallangmarkers with fitting value of locallang.xml
                    '$###CONF_(.*)###$Uis', // regulare expression
                    array($this, 'DynamicConfMarker'), // open function
                    $content // current content
            );
            $content = preg_replace_callback ( // Automaticly fill locallangmarkers with fitting value of locallang.xml
                    '$###PID_(.*)###$Uis', // regulare expression
                    array($this, 'DynamicPidMarker'), // open function
                    $content // current content
            );
            $content = preg_replace_callback ( // Automaticly fill locallangmarkers with fitting value of locallang.xml
                    '$###DATA_(.*)###$Uis', // regulare expression
                    array($this, 'DynamicDataMarker'), // open function
                    $content // current content
            );

            return $content;
        }
        function DynamicLocalLangMarker($array) {
            if ($this->pi_getLL(strtolower($array[1]))) { // if there is an entry in locallang.xml
                $string = $this->pi_getLL(strtolower($array[1])); // search for a fitting entry in locallang.xml or typoscript
            }
            if (!empty($string)) return $string;
        }
        function DynamicDataMarker($array) {
            if ($this->cObj->data[(strtolower($array[1]))]) { // if there is an entry in locallang.xml
                $string = $this->cObj->data[(strtolower($array[1]))]; // search for a fitting entry in locallang.xml or typoscript
            }
            if (!empty($string)) return $string;
        }
        function DynamicConfMarker($array) {
            if ($this->conf[(strtolower($array[1]))]) { // if there is an entry in locallang.xml
                $string = $this->conf[(strtolower($array[1]))]; // search for a fitting entry in locallang.xml or typoscript
            }
            if (!empty($string)) return $string;
        }
        function DynamicPidMarker($array) {
            if ($this->conf['pid_'.(strtolower($array[1]))]) { // if there is an entry in locallang.xml
                $string = $GLOBALS['TSFE']->cObj->typoLink('', array(
                                                                'parameter' => $this->conf['pid_'.(strtolower($array[1]))],
                                                                'useCacheHash' => 1,
                                                                'returnLast' => 'url',
                                                                )
                                                            );
            }
            if (!empty($string)) return $string;
        }
        /**
	 * Loads a variable from the flexform
	 *
	 * @param	string		name of variable
	 * @param	string		name of sheet
	 * @return	string		value of var
	 */
    function flexFormValue($var, $sheet) {
            return $this->pi_getFFvalue($this->cObj->data['pi_flexform'], $var,$sheet);
    }
       

       
}



if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/tsara/pi4/class.tx_ttbbase_pi4.php'])    {
    include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/tsara/pi4/class.tx_ttbbase_pi4.php']);
}

?>