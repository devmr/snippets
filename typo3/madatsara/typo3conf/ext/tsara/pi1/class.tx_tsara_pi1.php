 <?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2012  <>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

// require_once(PATH_tslib . 'class.tslib_pibase.php');
require_once(t3lib_extMgm::extPath("tsara").'lib/class.tx_tsara_util.php');
/**
 * Plugin 'tsara' for the 'tsar' extension.
 *
 * @author     <>
 * @package    TYPO3
 * @subpackage    tx_tsar
 */
class tx_tsara_pi1 extends tslib_pibase {
    public $prefixId      = 'tx_tsara_pi1';        // Same as class name
    public $scriptRelPath = 'pi1/class.tx_tsara_pi1.php';    // Path to this script relative to the extension dir.
    public $extKey        = 'tsara';    // The extension key.
    public $pi_checkCHash = TRUE;
    
    /**
     * The main method of the Plugin.
     *
     * @param string $content The Plugin content
     * @param array $conf The Plugin configuration
     * @return string The content that is displayed on the website
     */
    public function main($content, array $conf) {
        $this->conf = $conf;
        $this->pi_setPiVarDefaults();
        $this->pi_loadLL();
        $this->util = t3lib_div::makeInstance('tx_tsara_util');
        // Init FlexForm configuration for plugin:
        $this->pi_initPIflexForm();        
        $this->mergeflexFormValuesIntoConf();
        // Get Template
        $templateFile = $this->flexFormValue('templateFile', 's_template') ? $this->flexFormValue('templateFile', 's_template') : 'EXT:tsara/pi1/template.html';
        $this->template = $this->cObj->fileResource($templateFile);
        $this->varttnews = t3lib_div::_GP('tx_ttnews');
        
        switch( $this->flexFormValue('view', 's_template')){
            case 'listeauteur':
                 $content = $this->show_listeauteur();
            break;
            case 'ficheauteur' :
            default :
                $content = $this->show_ficheauteur();
            break;
        }
        
        
        $content = $this->replace( $content  );
        return $this->conf['wrapContentInBaseClass'] ? $this->pi_wrapInBaseClass($content) : $content;

    }
    function show_listeauteur(){
        if( !is_array( $this->varttnews ) ) return '';
        $author = $this->varttnews['author'];
        if( ! $author  ) return '';
        
        $row = $this->util->getlisteauthor($author,$this);
        
        $subpartArray = $linkpartArray = $markerArray = array();        
        $subpart = $this->cObj->getSubpart($this->template, '###TEMPLATE_LISTEAUTHOR###');
        
        $heightprofileauthor = (t3lib_utility_Math::convertToPositiveInteger($this->flexFormValue('heightprofileauthor', 's_template'))?$this->flexFormValue('heightprofileauthor', 's_template'):$this->conf['widthimage']);
        $widthprofileauthor = (t3lib_utility_Math::convertToPositiveInteger($this->flexFormValue('widthprofileauthor', 's_template'))?$this->flexFormValue('widthprofileauthor', 's_template'):$this->conf['heightimage']);
        
        foreach( $row as $ligne ){
            $markersub['###IMG###'] = ($ligne['image']!='' && file_exists(PATH_site.'uploads/pics/'.$ligne['image'])? '<img alt="" src="thb_'.$widthprofileauthor.'_'.$heightprofileauthor.'_1_/uploads/pics/'.$ligne['image'].'" />':'');
            $markersub['###URL###'] = ( $GLOBALS['TSFE']->cObj->typoLink( '', array(
                'parameter' => $GLOBALS['TSFE']->id
                ,'additionalParams' => '&tx_ttnews[author]='.$ligne['uid']
                , 'useCacheHash'  => 0
                ,'returnLast' => 'url'
            )) );
             $markersub['###NAME###'] = $this->cObj->stdWrap( $ligne['name'], $this->conf['nameauthor_stdWrap.'] );
             $markersub['###NB###'] = $this->cObj->stdWrap( sprintf( $this->pi_getLL('sprintftotalarticles'),$ligne['totalarticles'] ), $this->conf['nbarticle_stdWrap.'] );
            $contentboucle .= $this->cObj->substituteMarkerArray($this->cObj->getSubpart($subpart, '###BOUCLE###'), $markersub );
        }
        
        $subpartArray['###BOUCLE###'] = $contentboucle;
        $content = $this->cObj->substituteMarkerArrayCached($subpart, $markerArray, $subpartArray, $linkpartArray);
        return $content;
        
        
    }
    function show_ficheauteur(){
        
        if( !is_array( $this->varttnews ) ) return '';
        
        $uid = $this->varttnews['tt_news'];
        $author = $this->varttnews['author'];
        
        if( !t3lib_utility_Math::convertToPositiveInteger( $uid ) ){
            if( !t3lib_utility_Math::convertToPositiveInteger( $author ) ) return '';
        }
        
        if(  t3lib_utility_Math::convertToPositiveInteger( $uid ) ) {
            
            
            $row = $this->util->getauthorbyarticle( $uid );
            
            $uid_author = $row['tx_tsara_author'];
            $souhaitecontact = $row['souhaitecontact'];
            
        }
        
        if(  t3lib_utility_Math::convertToPositiveInteger( $author ) ) {
            
            
            $row = $this->util->getauthor( $author );
            
            $uid_author = $author;
            $souhaitecontact = $row['souhaitecontact'];
            
        }
        
        
        $subpartArray = $linkpartArray = $markerArray = array();        
        $subpart = $this->cObj->getSubpart($this->template, '###TEMPLATE_AUTHOR###');
        
        $markerArray['###NAME###'] = $row['name'];
        $markerArray['###IMG###'] = ($row['image']!='' && file_exists(PATH_site.'uploads/pics/'.$row['image'])? '<img alt="" src="thb_92_123_1_/uploads/pics/'.$row['image'].'" />':'');;
        $markerArray['###DESCRIPTION###'] = $this->pi_RTEcssText( $row['description'] );
        
        $row =  $this->util->getcomptesbyauthor($uid_author);
        
         if( $souhaitecontact == 0 ) $subpartArray['###BLOC_RESEAU###'] = '';
        else{ 
            //Recup le bloc
            $subpartsub = $this->cObj->getSubpart($subpart, '###BLOC_RESEAU###');
			
            $markersub['###PAGE_CONTACTERAUTEUR###'] = $GLOBALS['TSFE']->cObj->typoLink('', array(
                                                                'parameter' => $this->flexFormValue('page_contacterauteur', 's_template'),
                                                                'useCacheHash' => 1,
                                                                'additionalParams' => '&'.$this->prefixId.'[articleuid]='.$uid.'&'.$this->prefixId.'[author]='.$uid_author,
                                                                'returnLast' => 'url',
                                                                )
                                                            );
            $contentboucleother = $this->cObj->substituteMarkerArray($this->cObj->getSubpart($subpartsub, '###BOUCLEOTHER###'), $markersub );
			
            foreach( $row as $ligne ){
                $markersub['###URL###'] = $ligne['url'];
                $markersub['###ICO###'] = $ligne['typecompte'];
                $markersub['###TITLE###'] = $ligne['title'];
                $contentboucle .= $this->cObj->substituteMarkerArray($this->cObj->getSubpart($subpartsub, '###BOUCLE###'), $markersub );
            }
            $subpartsubArray['###BOUCLE###'] = $contentboucle;
            unset( $contentboucle );
			
            $subpartsubArray['###BOUCLEOTHER###'] = $contentboucleother;
			
            $subpartArray['###BLOC_RESEAU###'] = $this->cObj->substituteMarkerArrayCached($subpartsub, array(), $subpartsubArray, array() );
            
         }
         
         //Autres articles de l'auteur
         if( $this->flexFormValue('show_otherarticles', 's_template') != 1 )  $subpartArray['###BLOC_AUTRESARTICLES###'] = '';
         else{   
             $limitotherarticles = (t3lib_utility_Math::convertToPositiveInteger($this->flexFormValue('limitotherarticles', 's_template'))?$this->flexFormValue('limitotherarticles', 's_template'):3);
             $order = (trim($this->flexFormValue('orderarticlesauthor', 's_template'))!=''?' '.$this->flexFormValue('orderarticlesauthor', 's_template').' '.$this->flexFormValue('ascdesc', 's_template'):' datetime DESC');
             $row = $this->util->getarticlebyauthor( $uid_author,$limitotherarticles,$order);
             
             if( count( $row )  ) {
                 $subpartsub = $this->cObj->getSubpart($subpart, '###BLOC_AUTRESARTICLES###');
                foreach( $row as $ligne ){
                   $markersub['###IMG###'] = ($ligne['image']!='' && file_exists(PATH_site.'uploads/pics/'.$ligne['image'])? '<img alt="" src="thb_'.$this->conf['widthimage'].'_'.$this->conf['heightimage'].'_1_/uploads/pics/'.$ligne['image'].'" />':'');
                   $markersub['###URL###'] = ( $GLOBALS['TSFE']->cObj->typoLink( '', array(
                'parameter' => $GLOBALS['TSFE']->id
                ,'additionalParams' => '&tx_ttnews[tt_news]='.$ligne['uid']
                , 'useCacheHash'  => 1
                ,'returnLast' => 'url'
            )) );
                   $markersub['###TITLE###'] = $this->cObj->stdWrap( $ligne['title'], $this->conf['title_stdWrap.'] );
                   $markersub['###DATE###'] = $this->cObj->stdWrap( $ligne['datetime'], $this->conf['date_stdWrap.'] );
                   $contentboucle .= $this->cObj->substituteMarkerArray($this->cObj->getSubpart($subpartsub, '###BOUCLE_AUTRESARTICLES###'), $markersub );
               }
               $subpartsubArray['###BOUCLE_AUTRESARTICLES###'] = $contentboucle;
               unset( $contentboucle );

               $markersubArray['###LINKALL###'] = ( $GLOBALS['TSFE']->cObj->typoLink( '', array(
                    'parameter' => ($this->flexFormValue('page_allarticles', 's_template')?$this->flexFormValue('page_allarticles', 's_template'):$GLOBALS['TSFE']->id)
                    ,'additionalParams' => '&tx_ttnews[author]='.$uid_author
                    , 'useCacheHash'  => 0
                    ,'returnLast' => 'url'
                )) );;

               $subpartArray['###BLOC_AUTRESARTICLES###'] = $this->cObj->substituteMarkerArrayCached($subpartsub, $markersubArray, $subpartsubArray, array() );

             }
             else  $subpartArray['###BLOC_AUTRESARTICLES###'] = '';
             
         }
        
        if( !count( $row ) ) return '';
        $content = $this->cObj->substituteMarkerArrayCached($subpart, $markerArray, $subpartArray, $linkpartArray);
        return $content;
    }
    function replace( $content ){
            // 1. replace ll markers
            $content = preg_replace_callback ( // Automaticly fill locallangmarkers with fitting value of locallang.xml
                    '$###LL_(.*)###$Uis', // regulare expression
                    array($this, 'DynamicLocalLangMarker'), // open function
                    $content // current content
            );
            $content = preg_replace_callback ( // Automaticly fill locallangmarkers with fitting value of locallang.xml
                    '$###CONF_(.*)###$Uis', // regulare expression
                    array($this, 'DynamicConfMarker'), // open function
                    $content // current content
            );
            $content = preg_replace_callback ( // Automaticly fill locallangmarkers with fitting value of locallang.xml
                    '$###PID_(.*)###$Uis', // regulare expression
                    array($this, 'DynamicPidMarker'), // open function
                    $content // current content
            );
            $content = preg_replace_callback ( // Automaticly fill locallangmarkers with fitting value of locallang.xml
                    '$###DATA_(.*)###$Uis', // regulare expression
                    array($this, 'DynamicDataMarker'), // open function
                    $content // current content
            );

            return $content;
        }
        function DynamicLocalLangMarker($array) {
            if ($this->pi_getLL(strtolower($array[1]))) { // if there is an entry in locallang.xml
                $string = $this->pi_getLL(strtolower($array[1])); // search for a fitting entry in locallang.xml or typoscript
            }
            if (!empty($string)) return $string;
        }
        function DynamicDataMarker($array) {
            if ($this->cObj->data[(strtolower($array[1]))]) { // if there is an entry in locallang.xml
                $string = $this->cObj->data[(strtolower($array[1]))]; // search for a fitting entry in locallang.xml or typoscript
            }
            if (!empty($string)) return $string;
        }
        function DynamicConfMarker($array) {
            if ($this->conf[(strtolower($array[1]))]) { // if there is an entry in locallang.xml
                $string = $this->conf[(strtolower($array[1]))]; // search for a fitting entry in locallang.xml or typoscript
            }
            if (!empty($string)) return $string;
        }
        function DynamicPidMarker($array) {
            if ($this->conf['pid_'.(strtolower($array[1]))]) { // if there is an entry in locallang.xml
                $string = $GLOBALS['TSFE']->cObj->typoLink('', array(
                                                                'parameter' => $this->conf['pid_'.(strtolower($array[1]))],
                                                                'useCacheHash' => 1,
                                                                'returnLast' => 'url',
                                                                )
                                                            );
            }
            if (!empty($string)) return $string;
        }
        /**
	 * Loads a variable from the flexform
	 *
	 * @param	string		name of variable
	 * @param	string		name of sheet
	 * @return	string		value of var
	 */
    function flexFormValue($var, $sheet) {
            return $this->pi_getFFvalue($this->cObj->data['pi_flexform'], $var,$sheet);
    }
    function setSession($varname,$varcontent) {
   	 $GLOBALS['TSFE']->fe_user->setKey('ses',$varname,$varcontent);
   	 $GLOBALS['TSFE']->storeSessionData(); // validate the session
    }

    /**
     * Get a variable in typo3 session (without params return all the session table)
     */

    function getSession ($varname="") {
   	 if($varname!="") {
   		 return $GLOBALS['TSFE']->fe_user->getKey('ses',$varname);
   	 } else {
   		 return $GLOBALS['TSFE']->fe_user->sesData;
   	 }
    }
    /**
	 * Reads flexform configuration and merge it with $this->conf
	 *
	 * @return	void
	 */
	 function mergeflexFormValuesIntoConf() {
		$flex = array();
		
                if ($this->flexFormValue('view', 's_template')) {
			$flex['view'] = $this->flexFormValue('view','s_template');
		}

		

		$this->conf = array_merge($this->conf, $flex);
	}
}



if (defined('TYPO3_MODE') && isset($GLOBALS['TYPO3_CONF_VARS'][TYPO3_MODE]['XCLASS']['ext/tsara/pi1/class.tx_tsara_pi1.php'])) {
    include_once($GLOBALS['TYPO3_CONF_VARS'][TYPO3_MODE]['XCLASS']['ext/tsara/pi1/class.tx_tsara_pi1.php']);
}

?> 