<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2010 Miary RABEHASY <mrabehasy@netbooster.com>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * [CLASS/FUNCTION INDEX of SCRIPT]
 *
 * Hint: use extdeveval to insert/update function index above.
 */


/**
 * Plugin 'ttb' for the 'ttb_base' extension.
 *
 * @author    Miary RABEHASY <mrabehasy@netbooster.com>
 * @package    TYPO3
 * @subpackage    tx_ttbbase
 */
require_once(t3lib_extMgm::extPath("tsara").'lib/class.tx_tsara_util.php');
class tx_tsara_pi6 extends  tslib_pibase {
    var $prefixId      = 'tx_tsara_pi6';        // Same as class name
    var $scriptRelPath = 'pi6/class.tx_tsara_pi6.php';    // Path to this script relative to the extension dir.
    var $extKey        = 'tsara';    // The extension key.
    var $pi_checkCHash = true;

    /**
     * The main method of the PlugIn
     *
     * @param    string        $content: The PlugIn content
     * @param    array        $conf: The PlugIn configuration
     * @return    The content that is displayed on the website
     */
    function main($content, $conf) {
        $this->confArr = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['tsara']); 
        $this->conf = $conf;
        $this->pi_setPiVarDefaults();
        $this->util = t3lib_div::makeInstance('tx_tsara_util');
        // Init FlexForm configuration for plugin:
        $this->pi_initPIflexForm();
        $this->preInit();
        $this->pi_loadLL();
        
        $templateFile = $this->flexFormValue('templateFile', 's_template') ? $this->flexFormValue('templateFile', 's_template') : 'EXT:tsara/pi3/template.html';
//        $this->template = $this->cObj->fileResource($templateFile);
        $this->template = t3lib_div::getFileAbsFileName( $templateFile );
         
        $this->conf['page_listephotos'] = $this->flexFormValue('page_listephotos', 's_template');
        $this->conf['limit'] = $this->flexFormValue('limit', 's_template');
        $this->conf['view'] = $this->flexFormValue('view', 's_template');
        $this->conf['width'] = $this->flexFormValue('width', 's_template');
        $this->conf['height'] = $this->flexFormValue('height', 's_template');
        //Exploder le view
        $view =  $this->conf['view'] ;
        
        
        $this->folderjson = ($this->flexFormValue('folderjson', 's_template')>0?$this->flexFormValue('folderjson', 's_template'):'fileadmin/json/bcrm/');
        $this->templateFile = ($this->flexFormValue('templateFile', 's_template')>0?$this->flexFormValue('templateFile', 's_template'):'EXT:tsara/pi6/template.html');
        $this->limit = ($this->flexFormValue('limit', 's_template')>0?$this->flexFormValue('limit', 's_template'):'0');
     
        

        
        $content = $this->afficher();
	
                
            
        return $this->conf['wrapContentInBaseClass'] ? $this->pi_wrapInBaseClass($content) : $content;
    }
    function afficher(){
        $tab = $row = array();
         $d = opendir(PATH_site.$this->folderjson);
        while( $f = readdir( $d )){
            if( $f != '.' && $f!='..' ){
                $tab[filemtime(PATH_site.$this->folderjson.$f)] = $f;
            }
        }
        
        if( count($tab)<=0){
            return '';
        }
        
        $file = max($tab);
        if( !file_exists( PATH_site.$this->folderjson.$file ) ){
            return '';
        }
        
        //Prendre le contenu du fichier
        $str = file_get_contents( PATH_site.$this->folderjson.$file );
        $str = trim($str);
        
        unset($tab);
        //Decode
        $tab = json_decode($str,1);
        
        //date
        $date = $tab['date']['datestr'];
        
        //Données
        $i = 0;
        foreach( $tab['data'] as $k => $v ){
            if($this->limit>0 && ($i+1) <= $this->limit ) $row[$k] = $v;
            
            $i++;
        }
        
            
        $fluid = t3lib_div::makeInstance('Tx_Fluid_View_StandaloneView'); // instanciate standalone view
        // Emplacement du template
        

        //on spécifie l'emplacement du template
        $fluid->setTemplatePathAndFilename($this->template);

                

        //on assigne une variable
        $fluid->assign('data', $row );
        $fluid->assign('date', $date );
         

        return $fluid->render();
    }
                


         function preInit() {
		$flexformTyposcript = $this->flexFormValue('myTS', 's_template');
		if ($flexformTyposcript) {
			require_once(PATH_t3lib.'class.t3lib_tsparser.php');
			$tsparser = t3lib_div::makeInstance('t3lib_tsparser');
			// Copy conf into existing setup
			$tsparser->setup = $this->conf;
			// Parse the new Typoscript
			$tsparser->parse($flexformTyposcript);
			// Copy the resulting setup back into conf
			$this->conf = $tsparser->setup;
		}
	}
        /**
	 * Loads a variable from the flexform
	 *
	 * @param	string		name of variable
	 * @param	string		name of sheet
	 * @return	string		value of var
	 */
    function flexFormValue($var, $sheet) {
            return $this->pi_getFFvalue($this->cObj->data['pi_flexform'], $var,$sheet);
    }
       

       
}

                

?>