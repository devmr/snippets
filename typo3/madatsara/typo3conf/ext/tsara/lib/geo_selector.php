<?php
/***************************************************************
*  Copyright notice
*
*  (c) 1999-2008 Kasper Skaarhoj (kasperYYYY@typo3.com)
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*  A copy is found in the textfile GPL.txt and important notices to the license
*  from the author is found in LICENSE.txt distributed with these scripts.
*
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * Dummy document - displays nothing but background color.
 *
 * $Id: dummy.php 3439 2008-03-16 19:16:51Z flyguide $
 * Revised for TYPO3 3.6 2/2003 by Kasper Skaarhoj
 * XHTML compliant content
 *
 * @author	Kasper Skaarhoj <kasperYYYY@typo3.com>
 */
/**
 * [CLASS/FUNCTION INDEX of SCRIPT]
 *
 *
 *
 *   68: class SC_dummy
 *   76:     function main()
 *   92:     function printContent()
 *
 * TOTAL FUNCTIONS: 2
 * (This index is automatically created/updated by the extension "extdeveval")
 *
 */

if (!preg_match('/typo3conf/', $_SERVER['PHP_SELF'])) {
  $BACK_PATH = '../../';
  define('TYPO3_MOD_PATH', 'ext/tsara/lib/');
} else {
  $BACK_PATH = '../../../../typo3/';
  define('TYPO3_MOD_PATH', '../typo3conf/ext/tsara/lib/');
}
 
require ($BACK_PATH.'init.php');
require ($BACK_PATH.'template.php');
require_once (PATH_t3lib.'class.t3lib_page.php');
require_once (PATH_t3lib.'class.t3lib_tstemplate.php');
require_once (PATH_t3lib.'class.t3lib_tsparser_ext.php');
$LANG->includeLLFile('EXT:tsara/lib/locallang_wizard.xml');


/**
 * Script Class, creating the content for the dummy script - which is just blank output.
 *
 * @author	Kasper Skaarhoj <kasperYYYY@typo3.com>
 * @package TYPO3
 * @subpackage core
 */
class ql_googlemap_selector {
		// GET vars:
	var $P;				// Wizard parameters, coming from TCEforms linking to the wizard.
	var $currGeoDat;
	var $geoValue;	// Value of the current color picked.
	var $fieldChangeFunc;	// Serialized functions for changing the field... Necessary to call when the value is transferred to the TCEform since the form might need to do internal processing. Otherwise the value is simply not be saved.
	var $fieldName;		// Form name (from opener script)
	var $formName;		// Field name (from opener script)
	var $md5ID;			// ID of element in opener script for which to set color.
	var $conf;
	var $apiKey;

	/**
	 * document template object
	 *
	 * @var smallDoc
	 */
	var $doc;
	var $content;				// Accumulated content.


	function loadTS($pageUid) {
		$sysPageObj = t3lib_div::makeInstance('t3lib_pageSelect');
		$rootLine = $sysPageObj->getRootLine($pageUid);
		$TSObj = t3lib_div::makeInstance('t3lib_tsparser_ext');
		$TSObj->tt_track = 0;
		$TSObj->init();
		$TSObj->runThroughTemplates($rootLine);
		$TSObj->generateConfig();
		$this->conf = $TSObj->setup['plugin.']['tsara.']['gmap.'];
                 
	}


	function getApiKey($conf) {
		$currHost = $this->getHost( $_SERVER['HTTP_HOST'] );

		$apiArr = array();
		$apiConfArr = t3lib_div::trimExplode(',', $conf['apiKey'] );

		if( is_array($apiArr) ) {
			foreach ($apiConfArr as $key => $val) {
				$apiEntry = t3lib_div::trimExplode('|', $val );
				$apiArr[$this->getHost($apiEntry[0])] = $apiEntry[1];
			}
			$this->apiKey = $apiArr[$currHost];
		}
	}


	function getHost($host) {
		$currHost = str_replace( "http://", "", $host );
		$currHost = str_replace( "https://", "", $currHost );

		return $currHost;
	}


	function getGeoDat($geoData) {
		
		if(strlen($geoData) > 0 && strpos($geoData, ',')) 
			$currData = t3lib_div::trimExplode(',', $geoData );
		else
			$currData = array(-19.01019029439606, 47.5543212890625);
					
		if( !is_array($currData) && $this->conf['defaultLatLng'] ) {
			$currData = t3lib_div::trimExplode('|', $this->conf['defaultLatLng']);
		}
		
		if( is_array($currData) && ($currData[0] && $currData[1]) ) {
			$returnArray =  array( 'lat' => $currData[0], 'lng' => $currData[1] );
		}
		else {
			$returnArray = array( 'lat' => 0, 'lng' => 0 );
		}

		return $returnArray;
	}


	/**
	 * Create content for the dummy script - outputting a blank page.
	 *
	 * @return	void
	 */
	function main()	{
		global $LANG, $BACK_PATH;

			// Setting GET vars (used in frameset script):
		$this->P = t3lib_div::_GP('P',1);
		$this->loadTS($this->P['pid']);

		// Set API Key
		$this->getApiKey($this->conf);

		// Set start GEO data
		$this->currGeoDat = $this->getGeoDat( $this->P["currentValue"] );


			// Setting GET vars:
		$this->geoValue = $this->P['geoValue'];
		$this->fieldChangeFunc = $this->P['fieldChangeFunc'];
		#$this->fieldName = $this->P['fieldName'];
		$this->fieldName = $this->P['itemName'];
		$this->formName = $this->P['formName'];
		$this->md5ID = $this->P['md5ID'];
		$this->exampleImg = $this->P['exampleImg'];

			// Setting field-change functions:
		$fieldChangeFuncArr = $this->fieldChangeFunc;
		$update = '';
		if (is_array($fieldChangeFuncArr))	{
			unset($fieldChangeFuncArr['alert']);
			foreach($fieldChangeFuncArr as $v)	{
				$update.= '
				parent.opener.'.$v;
			}
		}

			// Initialize document object:
		$this->doc = t3lib_div::makeInstance('template');
		$this->doc->backPath = $BACK_PATH;
		$this->doc->docType = 'html5';
		$this->doc->JScodeLibArray = array('<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>');
		$this->doc->postCode = $this->doc->wrapScriptTags('
			var geocoder = new google.maps.Geocoder();
var markersArray = [];
var markers = [];
function geocodePosition(pos) {
  geocoder.geocode({
    latLng: pos
  }, function(responses) {
    if (responses && responses.length > 0) {
      //updateMarkerAddress(responses[0].formatted_address);
    } else {
      //updateMarkerAddress(\'Cannot determine address at this location.\');
    }
  });
}

function updateMarkerStatus(str) {
  document.getElementById(\'markerStatus\').innerHTML = str;
}

function updateMarkerPosition(latLng) {
  /*document.getElementById(\'info\').innerHTML = [
    latLng.lat(),
    latLng.lng()
  ].join(\', \');*/
  document.lgeodatform.lgeodat.value = [
    latLng.lat(),
    latLng.lng()
  ].join(\', \');
}

function updateMarkerAddress(str) {
  document.getElementById(\'address\').innerHTML = str;
}

function initialize() {
  var latLng = new google.maps.LatLng('.$this->currGeoDat['lat'].','.$this->currGeoDat['lng'].');
  var map = new google.maps.Map(document.getElementById(\'mapCanvas\'), {
    zoom: '.($this->P["currentValue"]!=''?'18':'5').',
    center: latLng,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });
  var marker = new google.maps.Marker({
    position: latLng,
    title: \'Point A\',
    map: map,
    draggable: true
  });
  markers.push(marker);
  
  // Update current position info.
  updateMarkerPosition(latLng);
  geocodePosition(latLng);
  
  // Add dragging event listeners.
  google.maps.event.addListener(marker, \'dragstart\', function() {
    //updateMarkerAddress(\'Dragging...\');
  });
  
   
  
  google.maps.event.addListener(marker, \'drag\', function() {
    //updateMarkerStatus(\'Dragging...\');
	//alert(marker.getPosition());
    updateMarkerPosition(marker.getPosition());
  });
  
  google.maps.event.addListener(marker, \'dragend\', function() {
    //updateMarkerStatus(\'Drag ended\');
		infoWindow.close();

    geocodePosition(marker.getPosition());
  });

  google.maps.event.addListener(map, \'click\', function(event) {
		deleteOverlays();
		updateMarkerPosition(event.latLng);
		var marker = new google.maps.Marker({
			position: event.latLng,
			title: \'Point A\',
			map: map,
			draggable: true
		});
		markers.push(marker);
  });
    google.maps.event.addListener(marker, \'click\', function() {
		alert(\'ok\');
	});

}
 // Sets the map on all markers in the array.
      function setAllMap(map) {
        for (var i = 0; i < markers.length; i++) {
          markers[i].setMap(map);
        }
      }
	  // Removes the overlays from the map, but keeps them in the array.
      function clearOverlays() {
        setAllMap(null);
      }
	  // Deletes all markers in the array by removing references to them.
      function deleteOverlays() {
        clearOverlays();
        markers = [];
      }
// Onload handler to fire off the app.
google.maps.event.addDomListener(window, \'load\', initialize);
function checkReference()	{	//
				if (parent.opener && parent.opener.document && parent.opener.document.'.$this->P['formName'].' && parent.opener.document.'.$this->P['formName'].'["'.$this->P['itemName'].'"])	{
					return parent.opener.document.'.$this->P['formName'].'["'.$this->P['itemName'].'"];
				} else {
					close();
				}
			}
			function updateValueInMainForm(input)	{	//
				var field = checkReference();
				if (field)	{
					field.value = input;
						window.opener.'.$this->P['fieldChangeFunc']['typo3form.fieldGet'].'
						window.opener.'.$this->P['fieldChangeFunc']['TBE_EDITOR_fieldChanged'].'
				}
			}
function setValue(input)	{	//
				updateValueInMainForm(input);
				close();
				return false;
			}
                        
window.onresize = function(event) {
    var style = document.getElementById(\'mapCanvas\').style;
    style.width = \'100%\';
    style.height = \'100%\';
    style.position = \'absolute\';
    style.top = \'42px\';
    style.left = \'0\';
    style.right = \'0\';
    style.bottom = \'0\';
    style.z-index = \'0\';
    style.overflow = \'hidden\';
    google.maps.event.trigger(map, \'resize\');
    map.setZoom(map.getZoom());
}
			
		');
		//$this->doc->docType = 'xhtml_trans';
		//$this->doc->bodyTagAdditions = ' onload="initialize()" onunload="GUnload()"';
		/*$this->doc->JScodeLibArray = array('<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key='.$this->apiKey.'" type="text/javascript"></script>');
		$this->doc->JScode = $this->doc->wrapScriptTags('



			function checkReference()	{	//
				if (parent.opener && parent.opener.document && parent.opener.document.'.$this->P['formName'].' && parent.opener.document.'.$this->P['formName'].'["'.$this->P['itemName'].'"])	{
					return parent.opener.document.'.$this->P['formName'].'["'.$this->P['itemName'].'"];
				} else {
					close();
				}
			}
			function updateValueInMainForm(input)	{	//
				var field = checkReference();
				if (field)	{
					field.value = input;
						window.opener.'.$this->P['fieldChangeFunc']['typo3form.fieldGet'].'
						window.opener.'.$this->P['fieldChangeFunc']['TBE_EDITOR_fieldChanged'].'
				}
			}
			function setValue(input)	{	//
				updateValueInMainForm(input);
				close();
				return false;
			}

			var map = null;
		    var geocoder = null;

		    function initialize() {
		      if (GBrowserIsCompatible()) {
		        map = new GMap2(document.getElementById("map_canvas"));
		        var center = new GLatLng('.$this->currGeoDat['lat'].', '.$this->currGeoDat['lng'].');

				map.addControl(new GLargeMapControl());
				map.addControl(new GMapTypeControl());
				map.disableDoubleClickZoom();
				map.setCenter(center, 13);

				setMarker(center);

		        geocoder = new GClientGeocoder();



		      }
		    }

		    function showAddress(address) {
		      if (geocoder) {
		        geocoder.getLatLng(
		          address,
		          function(point) {
		            if (!point) {
		              alert(address + " not found");
		            } else {
		              map.setCenter(point, 13);

		              setMarker(point);

		            }
		          }
		        );
		      }
		    }

		    function setMarker(point) {
		    	map.clearOverlays();
		    	map.closeInfoWindow();
		    	var marker = new GMarker(point, {draggable: true});
		    	map.addOverlay(marker);

				GEvent.addListener( marker, "dragstart", function() {
					map.closeInfoWindow();
				});

				GEvent.addListener( marker, "dragend", function() {
					markerPos = marker.getLatLng();
					map.panTo(markerPos);
					marker.openInfoWindowHtml("<span style=\"font-size: 10px;font-family:Arial,Helvetica,Sans-Serif;\">'.$LANG->getLL('lat').': "+markerPos.lat()+"<br />"+"'.$LANG->getLL('lng').': "+markerPos.lng()+"</span>");
					setGeoDatField(markerPos.lat(),markerPos.lng());
				});

				GEvent.addListener( map, "dblclick", function(overlay, point){
					map.closeInfoWindow();
					if (point) {
						marker.setLatLng(point);
						map.panTo(point);
						marker.openInfoWindowHtml("<span style=\"font-size: 10px;font-family:Arial,Helvetica,Sans-Serif;\">'.$LANG->getLL('lat').': "+point.lat()+"<br />"+"'.$LANG->getLL('lng').': "+point.lng()+"</span>");
						setGeoDatField(point.lat(),point.lng());
					}
				});

		    	markerPos = marker.getLatLng();
				marker.openInfoWindowHtml("<span style=\"font-size: 10px;font-family:Arial,Helvetica,Sans-Serif;\">'.$LANG->getLL('lat').': "+markerPos.lat()+"<br />"+"'.$LANG->getLL('lng').': "+markerPos.lng()+"</span>");
				setGeoDatField(markerPos.lat(),markerPos.lng());
		    }

		    function setGeoDatField(lat,lng) {
		    	document.lgeodatform.lgeodat.value = lat+";"+lng;
		    }
		');*/

		$this->doc->inDocStyles = '
			body {
				padding: 0px;
				margin: 0px;
				height: 100%;
				width: 100%;
			}

			#formContainer {
				padding: 5px 10px;
				background-color: '.$this->doc->bgColor2.'
			}
			#formContainer strong {
				color: #ffffff;
			}
			#formContainer table {
				width: 100%;
			}
			
			  #mapCanvas {
    width: 500px;
    height: 400px;
    float: left;
  }
  #infoPanel {
    float: left;
    margin-left: 10px;
  }
  #infoPanel div {
    margin-bottom: 5px;
  }
		';
			// Start page:
		$this->content.=$this->doc->startPage($LANG->getLL('geoselector_title'));

		$content = '
		<div id="formContainer">
			<table id="formTable" cellpadding="0" cellspacing="0">
				<tr>
					
					<td width="90" align="right"><form action="" name="lgeodatform" id="lgeodatform"><input size="50" type="text" name="lgeodat" value="popupVAl" /><input value="'.$LANG->getLL('set_data').'" onclick="setValue(document.lgeodatform.lgeodat.value);" type="submit"></form></td>
				</tr>
			</table>
		</div>
		';
		//$content .= '<!--<div id="map_canvas" style="width: 100%; height: 324px;"></div>--><div id="mapCanvas"></div>';
		$content .= '<div id="mapCanvas"></div><div id="infoPanel"><div id="info"></div><div id="address"></div><div id="markerStatus"></div></div>';

			// If the save/close button is clicked, then close:
		if(t3lib_div::_GP('save_close')) {
			$content.=$this->doc->wrapScriptTags('
				setValue(\''.$this->geoValue.'\');
				parent.close();
			');
		}

			// Output:
		$this->content.=$this->doc->section('', $content, 0,1);
	}

	/**
	 * Outputting the accumulated content to screen
	 *
	 * @return	void
	 */
	function printContent()	{
		$this->content.= $this->doc->endPage();
		$this->content = $this->doc->insertStylesAndJS($this->content);
		echo $this->content;
	}
}

// Include extension?
if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/ql_googlemap_selector/geo_selector.php'])	{
	include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/ql_googlemap_selector/geo_selector.php']);
}




// Make instance:
$SOBE = t3lib_div::makeInstance('ql_googlemap_selector');
$SOBE->main();
$SOBE->printContent();
?>
