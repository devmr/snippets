<?php
/*
* Copyright notice
*
*   (c) 1999-2004 Kasper Skaarhoj (kasper@typo3.com)
*   All rights reserved
*
*   This script is part of the TYPO3 project. The TYPO3 project is
*   free software; you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation; either version 2 of the License, or
*   (at your option) any later version.
*
*   The GNU General Public License can be found at
*   http://www.gnu.org/copyleft/gpl.html.
*   A copy is found in the textfile GPL.txt and important notices to the license
*   from the author is found in LICENSE.txt distributed with these scripts.
*
*
*   This script is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   This copyright notice MUST APPEAR in all copies of the script!
*/
/**
* This example shows, how you can substitute the pageBrowser from tt_news with your own pagebrowser script
* it uses the function userPageBrowserFunc() from the tt_news class
*
* $Id: example_userPageBrowserFunc.php 2944 2005-05-15 19:18:18Z rupi $
*
* @author Rupert Germann <rupi@gmx.li>
*/


/*
* This is a changed version of the pagebrowser function from class.pi_base (in TYPO3 version below 3.8.0).
* The differences are:
* 1. the values from get_LL are not parsed through htmlspecialchars(). So you can use
*    html-code for the "next" and "previous" links.
* 2. the caching behaviour of the pagebrowser links is now configurable with the TS-parameter "allowCaching"
*
*
*
* Example Configuration (add this to your TS setup):

# include the php script for the pageBrowser userfunction
includeLibs.userPageBrowserFunc = EXT:tt_news/res/example_userPageBrowserFunc.php
# call user function
plugin.tt_news.userPageBrowserFunc = user_substPageBrowser

plugin.tt_news {
	# Pagebrowser settings
	pageBrowser {
	maxPages = 20
	# set this to '0' if you want the pagebrowser to display only numbers
	showPBrowserText = 1
	tableParams = cellpadding=2
	showResultCount = 1
	}
	# Example for overriding values from locallang.php with html-code that displays images
	_LOCAL_LANG.default {
	pi_list_browseresults_prev = <img src="typo3/gfx/pil2left.gif" border="0" height="12" width="7" alt="previous" title="previous">
	pi_list_browseresults_next = <img src="typo3/gfx/pil2right.gif" border="0" height="12" width="7" alt="next" title="next">
	}
}


*/

/**
 * Alternative pagebrowser function
 *
 * @param	array		$markerArray
 * @param	array
 * @return	array		$markerArray with filled in pagebrowser marker
 */
function user_substPageBrowser($markerArray, $conf) {
	$pObj = &$conf['parentObj']; // make a reference to the parent object

        

	// Initializing variables:
	$showResultCount = $pObj->conf['pageBrowser.']['showResultCount'];
	$tableParams = $pObj->conf['pageBrowser.']['tableParams'];
	$pointer = $pObj->piVars['pointer'];
	$count = $pObj->internal['res_count'];
	$results_at_a_time = t3lib_utility_Math::forceIntegerInRange($pObj->internal['results_at_a_time'], 1, 1000);
	$maxPages = t3lib_utility_Math::forceIntegerInRange($pObj->internal['maxPages'], 1, 100);
	$max = t3lib_utility_Math::forceIntegerInRange(ceil($count / $results_at_a_time), 1, $maxPages);
	$pointer = intval($pointer);
	$links = $linkslist = array();
	$pageCount = ceil($count/$results_at_a_time);

	// Make browse-table/links:
	if ($pObj->pi_alwaysPrev >= 0) {
            
                $links[] = '<a   href="'.$pObj->pi_linkTP_keepPIvars_url (array( 'pointer' => 0  ), 1 ).'">«</a>';

		if ($pointer > 0) {
			$links[] = '<a    href="' . $pObj->pi_linkTP_keepPIvars_url( array('pointer' => ($pointer-1?$pointer-1:'')), $pObj->allowCaching) . '">‹</a> ';
		} elseif ($pObj->pi_alwaysPrev) {
			$links[] = '
					' . $pObj->pi_getLL('pi_list_browseresults_prev', '< Previous') . '';
		}
	}

	if($pointer<$maxPages) $debut = 0;
	else $debut = ($pointer+$maxPages);

        $links[] = ' ';

	for($a=0;$a<$pageCount;$a++)    {
		$min = max(0, $pointer+1-ceil($maxPages/2));
		$max = $min+$maxPages;
		if($max>$pageCount)    {
		    $min = $min - ($max-$pageCount);
		}

		if($a >= $min && $a < $max)    {
		    $linkslist[] =  (
				 $a==$pointer ?
					'<span class="current">'.($a+1).'</span> '
					 :
					'<a  href="'.$pObj->pi_linkTP_keepPIvars_url( array(  'pointer'  => ( $a ? $a : '' ) ), $pObj->allowCaching ).'" >'.trim( ( $a + 1) ).'</a>'

				);
		}
	}
        $links[] = implode(' ', $linkslist );
        $links[] = ' ';

	/*

	for($a = 0;$a < $max;$a++) {
		$links[] = ($a==$pointer ? '<span>'.($a+1).'</span>' : $pObj->pi_linkTP_keepPIvars(trim($pObj->pi_getLL('pi_list_browseresults_page', 'Page') . ' ' . ($a + 1)), array('pointer' => ($a?$a:'')), $pObj->allowCaching));
	}

	*/


	if ($pointer < ceil($count / $results_at_a_time)-1) {
		$links[] = '<a  href="'.$pObj->pi_linkTP_keepPIvars_url( array('pointer' => $pointer + 1), $pObj->allowCaching).'">›</a>';
	
                $links[] = '<a   href="'.$pObj->pi_linkTP_keepPIvars_url (array(   'pointer' => (ceil( $count/$results_at_a_time ) - 1)), 1 ).'">»</a>' ;
        }

	$pR1 = $pointer * $results_at_a_time + 1;
	$pR2 = $pointer * $results_at_a_time + $results_at_a_time;


	$sTables = '

		<!--
			List browsing box:
		-->
                
		' .
	($showResultCount ? '
			<div id="paginationliste"><p class="align-center">' .
		($pObj->internal['res_count'] ?
			sprintf(
				str_replace('###SPAN_BEGIN###', '', $pObj->pi_getLL('pi_list_browseresults_displays', 'Displaying results ###SPAN_BEGIN###%s to %s</span> out of ###SPAN_BEGIN###%s</span>')),
				$pObj->internal['res_count'] > 0 ? $pR1 : 0,
				min(array($pObj->internal['res_count'], $pR2)),
				$pObj->internal['res_count']
				) :
			$pObj->pi_getLL('pi_list_browseresults_noResults', 'Sorry, no items were found.')) . '</p><div class="section group " ><div class="col span_1_of_6">Aller à la page</div>':''
		) . '



					<div class="col span_5_of_6"><div class="pagination-wrapper clearfix"><div class="pagination clearfix">' . implode('', $links) . '</div></div></div>

</div></div>
		';

	$markerArray['###BROWSE_LINKS###'] = ''.$sTables.'';

	return $markerArray;
}
 


?>