<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2007 Dmitry Dulepov <dmitry@typo3.org>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

/**
 * [CLASS/FUNCTION INDEX of SCRIPT]
 *
 *
 *
 *   43: class tx_album3x_realurl
 *   55:     function main($params, $ref)
 *   65:     function id2alias($value)
 *   99:     function alias2id($value)
 *  117:     function addAlbum3xConfig($params, &$pObj)
 *
 * TOTAL FUNCTIONS: 4
 * (This index is automatically created/updated by the extension "extdeveval")
 *
 */

require_once(t3lib_extMgm::extPath('realurl', 'class.tx_realurl_advanced.php'));
require_once(t3lib_extMgm::extPath("tsara").'lib/class.tx_tsara_util.php');

class tx_tsara_realurl {

	private	$uidToAliasMap = array();
	private	$aliasToUidMap = array();

	/**
	 * Main etry point
	 *
	 * @param	array		$params	Paramters passed by realurl
	 * @param	tx_realurl		$pObj	Reference to tx_realurl intance
	 * @return	mixed		Conversion result
	 */
	function main($params, $ref)    {
                //t3lib_div::writeFile(PATH_site.'logpowermail.txt',($params['decodeAlias'] ? 'alias2id('.$params['value'].')' : 'id2alias('.$params['value'].')'));
                return ($params['decodeAlias'] ? $this->alias2id($params['value']) : $this->id2alias($params['value']));
	}
        
        
        
        
	/**
	 * COnverts uid of the record to readable alias
	 *
	 * @param	int		$value	uid of the record
	 * @return	string		Created alias
	 */
	function id2alias($value) {
                $util = t3lib_div::makeInstance('tx_tsara_util');
		$value = intval($value);
		if (!($result = $this->uidToAliasMap[$value])) {
                        //Get titre in PGSQL
                        $row = $util->getUid( $value );
                        $title = ($row[$util->getConf('c32')]!=''?$row[$util->getConf('c32')]:$row[$util->getConf('c31')]) ;
                        
                        
                        
			$realurl = t3lib_div::makeInstance('tx_realurl_advanced');
                        $result = $realurl->encodeTitle($title);
                        
                        $result = str_replace('_','-',$result);
                        
                        
                        
                        //Check uniqueness
                        $nb = $GLOBALS['TYPO3_DB']->exec_SELECTcountRows('*', 'tx_tsara_realurl', 'hidden=0 AND deleted=0  AND alias2id = ' . $value  );
                        if( $nb > 0 ){
                            $result = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow('id2alias','tx_tsara_realurl', 'hidden=0 AND deleted=0  AND alias2id = ' . $value);
                            $result = $result['id2alias'];
                             
                           

                        }
                        else{
                            //INSERT
                            $GLOBALS['TYPO3_DB']->exec_INSERTquery(
                                    'tx_tsara_realurl',
                                    array(
                                        'id2alias' => $result,
                                        'alias2id' => $value,
                                    )
                                    );
                            
                        }
                        
                        $result = $this->getDoublons($result, $value, $row);
                        
                        $this->uidToAliasMap[$value] = $result;
                        
			
		}
		return $result;
	}
        
        

	/**
	 * Converts alias of the record to its uid
	 *
	 * @param	string		$value	Alias of the record
	 * @return	mixed		uid of the record or <code>false</code> if could not be resolved
	 */
	function alias2id($value) {
            
                
                
		if (!($result = $this->aliasToUidMap[$value])) {
			
                    $result = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow('alias2id','tx_tsara_realurl', 'hidden=0 AND deleted=0 AND id2alias = ' . $GLOBALS['TYPO3_DB']->fullQuoteStr($value, 'tx_tsara_realurl'));
                    $result = $result['alias2id'];
                    $this->aliasToUidMap[$value] = $result;
			 
		}
		return $result;
	}
        /**
	 * Chercher doublons et ajouter champ supplementaire si doublon
	 *
	 * @param	string		$value	Alias of the record
	 * @return	mixed		uid of the record or <code>false</code> if could not be resolved
	 */
        function getDoublons( $title, $value, $row ){
            $util = t3lib_div::makeInstance('tx_tsara_util');
            $nb = $GLOBALS['TYPO3_DB']->exec_SELECTcountRows('*', 'tx_tsara_realurl', 'hidden=0 AND deleted=0  AND id2alias = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($title, 'tx_tsara_realurl')  );
            //t3lib_div::writeFile(PATH_site.'logpowermail.txt',  $GLOBALS['TYPO3_DB']->SELECTquery('*', 'tx_tsara_realurl', 'hidden=0 AND deleted=0  AND id2alias = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($title, 'tx_tsara_realurl')  ) );
            
            if( $nb > 1 ){
                //Ajouter le lieu c46
                $title .= '-'.($row[$util->getConf('c46')]!=''?$row[$util->getConf('c46')]:'-1') ;
                $realurl = t3lib_div::makeInstance('tx_realurl_advanced');
                $title = $realurl->encodeTitle($title);
                $title = str_replace('_','-',$title);
                
                $GLOBALS['TYPO3_DB']->exec_UPDATEquery(
                        'tx_tsara_realurl',
                        'alias2id = '.$value ,
                        array(
                            'id2alias' => $title
                        )
                 );
                
                
                 //t3lib_div::writeFile(PATH_site.'logpowermail.txt',  $title.' / '.$value       );
                 return $title;
                
            }
            
            return $title;
            
        }

	/**
	 * Generates additional RealURL configuration and merges it with provided configuration
	 *
	 * @param	array		$params	Default configuration
	 * @param	tx_realurl_autoconfgen		$pObj	Parent object
	 * @return	array		Updated configuration
	 */
	function addCartesnsConfig($params, &$pObj) {
            
            return array_merge_recursive($params['config'], array(
				    'postVarSets' => array(
						'_DEFAULT' => array(
							'date' => array(
								array(
									'GETvar' => 'tx_tsara_pi3[date]',
								),
							),
                                                        'thematique' => array(
								array(
									'GETvar' => 'tx_tsara_pi3[categoriegroup]',
								),
							),
                                                        'page' => array(
								array(
									'GETvar' => 'tx_tsara_pi3[pointer]',
								),
							),
                                                        'date-entre-le' => array(
								array(
									'GETvar' => 'tx_tsara_pi3[date_entrele]',
								),
							),
                                                        'date-et-le' => array(
								array(
									'GETvar' => 'tx_tsara_pi3[date_etle]',
								),
							),
                                                        'departement' => array(
								array(
									'GETvar' => 'tx_tsara_pi3[departementmap]',
								),
							),
                                                        'recherche-reseau' => array(
								array(
									'GETvar' => 'tx_tsara_pi3[search]',
								),
							),
							'carte-cadeau' => array(
								array(
									'GETvar' => 'tx_tsara_pi3[uid]',
									'userFunc' => 'EXT:tsara/lib/class.tx_tsara_realurl.php:&tx_tsara_realurl->main',
								),
							),
					))));
            
	}
}

 

?>