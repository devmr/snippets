<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2005-2011 Rupert Germann (rupi@gmx.li)
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*  A copy is found in the textfile GPL.txt and important notices to the license
*  from the author is found in LICENSE.txt distributed with these scripts.
*
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

/**
 * Class 'tx_ttnews_tcemain' for the tt_news extension.
 *
 * $Id: class.tx_ttnews_tcemain.php 44502 2011-03-02 16:30:55Z rupi $
 *
 * @author     Rupert Germann <rupi@gmx.li>
 */
/**
 * [CLASS/FUNCTION INDEX of SCRIPT]
 *
 *
 *
 *   64: class tx_ttnews_tcemain
 *   73:     function getSubCategories($catlist, $cc = 0)
 *  105:     function processDatamap_preProcessFieldArray(&$fieldArray, $table, $id, &$pObj)
 *
 *
 *  187: class tx_ttnews_tcemain_cmdmap
 *  201:     function processCmdmap_preProcess($command, &$table, &$id, $value, &$pObj)
 *  263:     function processCmdmap_postProcess($command, $table, $srcId, $destId, &$pObj)
 *  310:     function int_recordTreeInfo($CPtable, $srcId, $counter, $rootID, $table, &$pObj)
 *
 * TOTAL FUNCTIONS: 5
 * (This index is automatically created/updated by the extension "extdeveval")
 *
 */

require_once(t3lib_extMgm::extPath('realurl', 'class.tx_realurl_advanced.php'));
/**
 * Class being included by TCEmain using a hook
 *
 * @author	Rupert Germann <rupi@gmx.li>
 * @package TYPO3
 * @subpackage tt_news
 */
// Inclusion des librairies nécessaires

class tx_tsara_tcemain {

	 
        
        
	/**
	 * This method is called by a hook in the TYPO3 Core Engine (TCEmain) when a record is saved. We use it to disable saving of the current record if it has categories assigned that are not allowed for the BE user.
	 *
	 * @param	array		$fieldArray: The field names and their values to be processed (passed by reference)
	 * @param	string		$table: The table TCEmain is currently processing
	 * @param	string		$id: The records id (if any)
	 * @param	object		$pObj: Reference to the parent object (TCEmain)
	 * @return	void
	 * @access public
	 */
	function processDatamap_preProcessFieldArray(&$fieldArray, $table, $id, &$pObj) {
                 
             //1
            
            //SI on est sur la table tx_tsara_une
            if($fieldArray['title'] && $table == 'tt_news' ){
                
                $confArr = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['tsara']);
                
                //Get pid
                $befunc = t3lib_div::makeInstance('t3lib_befunc'); 
                $record = $befunc->getRecord($table,$id,'pid');
                
                if( $confArr['pidevenement'] == $record['pid'] ){
                     t3lib_div::writeFile(PATH_site.'typo3temp/logtcemain.txt',print_r($fieldArray,1).' ID : '.$id);
                     //Si le champ tx_tsara_event_date_end est vide
                     if( $fieldArray['tx_tsara_event_date_end']==0 && $fieldArray['tx_tsara_event_date'] > 0){
                         $fieldArray['tx_tsara_event_date_end'] = $fieldArray['tx_tsara_event_date'];
                     }
                }
                
                
                $charset = 'utf-8';
                $space = '_';
                
                $title = $fieldArray['title'];
                
                $cs = t3lib_div::makeInstance('t3lib_cs');                
                $title = $cs->conv_case($charset, $title, 'toLower'  );
                $title = strip_tags($title);
                $title = preg_replace('/[ \-+_]+/', $space, $title);
                $title = $cs->specCharsToASCII($charset, $title);
                $title = preg_replace('/[^a-zA-Z0-9' . ($space ? preg_quote($space) : '') . ']/', '', $title);
                $title = preg_replace('/\\' . $space . '{2,}/', $space, $title); // Convert multiple 'spaces' to a single one
		$title = trim($title, $space);                
                $fieldArray['tx_tsarattnews_slug'] = $title;
                
                
                 
                
                
                 
                
            }
            
            
            return true;
                
	}
        protected function initTSFE() {
                require_once(PATH_t3lib . 'class.t3lib_timetracknull.php');
                $TT = new t3lib_timeTrackNull();
                $GLOBALS['TT'] = $TT;
		if (version_compare(TYPO3_version, '4.3.0', '<')) {
			$tsfeClassName = t3lib_div::makeInstanceClassName('tslib_fe');
			$GLOBALS['TSFE'] = new $tsfeClassName($GLOBALS['TYPO3_CONF_VARS'], t3lib_div::_GP('id'), '');
		}
		else {
			$GLOBALS['TSFE'] = t3lib_div::makeInstance('tslib_fe', $GLOBALS['TYPO3_CONF_VARS'], t3lib_div::_GP('id'), '');
		}
		$GLOBALS['TSFE']->connectToDB();
		$GLOBALS['TSFE']->initFEuser();
		$GLOBALS['TSFE']->determineId();
		$GLOBALS['TSFE']->getCompressedTCarray();
		$GLOBALS['TSFE']->initTemplate();
		$GLOBALS['TSFE']->getConfigArray();

		// Get linkVars, absRefPrefix, etc
		TSpagegen::pagegenInit();
	}
        function processDatamap_postProcessFieldArray($status, $table, $id, $fieldArray, &$pObj){
                
                $confArr = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['tsara']);
                
                
                
            return true;
        }
        function processDatamap_afterDatabaseOperations($status, $table, $id, $fieldArray, $pObj) {
            
             
            //3
           
                
          
            
            
            return true;
		
	}


}
