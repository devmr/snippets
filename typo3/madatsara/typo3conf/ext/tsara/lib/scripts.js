/*!
 * Ext JS Library 3.4.0
 * Copyright(c) 2006-2011 Sencha Inc.
 * licensing@sencha.com
 * http://www.sencha.com/license
 */
Ext.onReady(function(){
     var ds = new Ext.data.Store({
         //url: '../index.php?eID=cartesns&'
        proxy: new Ext.data.ScriptTagProxy({
             
            url: '../index.php?eID=cartesns&extkey=cartesns&task=search'
        }),
        reader: new Ext.data.JsonReader({
            root: 'topics',
            totalProperty: 'totalCount',
            id: 'id_element'
        }, [
            {name: 'title', mapping: 'organisateur'} 
            ,{name: 'titre3', mapping: 'titre'} 
            ,{name: 'id', mapping: 'id_element'} 
            ,{name: 'description', mapping: 'description'} 
            ,{name: 'id_titre3', mapping: 'id_titre3'} 
        ])
    });

    // Custom rendering Template
    var resultTpl = new Ext.XTemplate(
        '<tpl for="."><div class="search-item">',
            '<h3>(ID : {id}) / {titre3}</h3>',
            '<p><strong>Organisateur</strong> : {title}</p>',
            '<p><strong>Descriptif</strong> : {description}</p>',
        '</div></tpl>'
    );
    
    var search = new Ext.form.ComboBox({
        store: ds,
        displayField:'id_titre3',
        typeAhead: false,
        loadingText: 'Searching...',
        width: 570,
        pageSize:10,
        hideTrigger:false,
        tpl: resultTpl,
        applyTo: 'search',
        itemSelector: 'div.search-item'
        ,listeners:{
            select:{
                fn:function(combo, value) {
                    
                    
                }
            }
        }
     }
     );
    

});