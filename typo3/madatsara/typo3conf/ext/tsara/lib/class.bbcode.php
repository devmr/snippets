<?php

 class bbcode {
    
    protected $subject = ''; //string that will be parsed
    
    //array of bbcode patterns
    protected $patterns = array
    (
        '/\[b\](.+)\[\/b\]/Uis',
        '/\[i\](.+)\[\/i\]/Uis',
        '/\[u\](.+)\[\/u\]/Uis',
        '/\[s\](.+)\[\/s\]/Uis',
        '/\[url=(.+)\](.+)\[\/url\]/Ui',
        '/\[img\](.+)\[\/img\]/Ui',    
        '/\[code\](.+)\[\/code\]/Uis',
        '/\[color=(\#[0-9a-f]{6}|[a-z]+)\](.+)\[\/color\]/Ui',
        '/\[color=(\#[0-9a-f]{6}|[a-z]+)\](.+)\[\/color\]/Uis'
        ,'/\[quote=([^\]]+)\](.+)\[\/quote\]/Uis'
    );
    
    //array of HTML tags that correspond to bbcode patterns
    protected $replacements = array
    (
        '<b>\1</b>',
        '<i>\1</i>',
        '<u>\1</u>',
        '<s>\1</s>',
        '<a href = "\1" target = "_blank">\2</a>',
        '<img src = "\1" alt = "Image" />',
        '<pre>\1</pre>',
        '<span style = "color: \1;">\2</span>',
        '<div style = "color: \1;">\2</div>'
        ,'<div  class="quote">\1 a &eacute;crit : \2</div>'
    );
    
    /**
    * Constructor
    *
    * @params string
    */
    public function BBCodeParser($subject){
        __construct($subject);
    }
    
    public function __construct($subject){
        $this->subject = $subject;
    }
    
    /**
    * Convert bbcode to (x)HTML tags
    *
    * @return string
    */
    public function bbc2html(){
        $this->subject = nl2br($this->subject);
        
        $this->subject = preg_replace($this->patterns, $this->replacements, $this->subject);
            
        return $this->subject;
    }
}