<?php
require_once t3lib_extMgm::extPath("tsara").'lib/class.datetimeconverter.php';
class Tx_Tsara_Lib_PowermailBeforeRenderView extends Tx_Extbase_MVC_Controller_ActionController {
    public function __construct() {
        $this->confArr = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['tsara']);
        $this->cs = t3lib_div::makeInstance('t3lib_cs');
        $this->fileFunc = t3lib_div::makeInstance('t3lib_basicFileFunctions');
        $this->datetimeconverter = t3lib_div::makeInstance('Date_Time_Converter');
    }

    public function savepowermail($field, $form, $mail, $pObj ){
        $tab_kw = array();
        //S'il s'agit du formulaire d'ajout de site
        switch( t3lib_utility_Math::convertToPositiveInteger( $form )  ){
			//Site
            case '3' :
                $this->save_site($field);
            break;
			//Evenement
            case '5' :
                $this->save_event($field);
            break;
			//livre
            case '6' :
                $this->save_book($field);
            break;
        }
        return true;
    }
    function save_book($field){
        $tlog = array();
        
       //variables
       $date_parution = t3lib_div::removeXSS( $field[58] ); 
       $nom = t3lib_div::removeXSS( $field[56] ); 
       $description = t3lib_div::removeXSS( $field[60] ); 
       $keywords =  t3lib_div::removeXSS( t3lib_div::_GP('keywords') ); 
         
       $nbpages = t3lib_div::removeXSS( $field[59] );        
       
       $author = t3lib_div::removeXSS( $field[61] );
       $author_email = t3lib_div::removeXSS( $field[62] );
       
       $pid = $this->confArr['pidlivres']; 
       $parent_category_livre = $this->confArr['parent_category_livre']; 
       
       $image = '';
       $charset = 'utf-8';
       $space = '_';
       
       $file = $_FILES['tx_powermail_pi1'] ;
       $tlog[] = 'Line '.__LINE__.' - '.json_encode($_FILES);
       $powermailSession = $GLOBALS['TSFE']->fe_user->getKey('ses', 'powermail');
       $tlog[] = 'Line '.__LINE__.' - '. $powermailSession['upload'][0] ;
        
       
       if( $file['error']['field'][57] == 0 && $file['name']['field'][57]!=''){
           
            $filesrc = $powermailSession['upload'][0];
            $filename = str_ireplace(PATH_site.'uploads/tx_powermail/', '', $filesrc );
           
            $tlog[] = 'Line '.__LINE__.' - '.$filename;
            $title = $this->cs->conv_case($charset, $nom, 'toLower'  );
                        
            $title = strip_tags($title);
            $title = preg_replace('/[ \-+_]+/', $space, $title);
            $title = $this->cs->specCharsToASCII($charset, $title);
            $title = preg_replace('/[^a-zA-Z0-9' . ($space ? preg_quote($space) : '') . ']/', '', $title);
            $title = preg_replace('/\\' . $space . '{2,}/', $space, $title); // Convert multiple 'spaces' to a single one
            $title = trim($title, $space);


            $tab = t3lib_div::split_fileref( $filename );
            
            if( t3lib_div::inList('jpg,jpeg,png',$this->cs->conv_case($charset, $tab['fileext'], 'toLower'  ) ) ){
                
                $image =  $title.'.'.$tab['fileext'];
                $tlog[] = 'Line '.__LINE__.' - '.$image ;
                 

                $fichier = $this->fileFunc->cleanFileName( basename( $image ) );
                $chemin = PATH_site.'uploads/pics' ;
                $myfilepath = $this->getUniqueName($fichier, $chemin);
                $tlog[] = 'Line '.__LINE__.' - Sauvegarde de '.$filesrc.' vers '.$myfilepath ;
                if (t3lib_div::upload_copy_move( $filesrc  , $myfilepath  ) ) { 
                    
                    $tlog[] = 'Line '.__LINE__.' - Sauvegarde OK de '.$myfilepath ;
                    
                }
            }
            
       }
       
        //Enregistrer
       $sqli = $GLOBALS['TYPO3_DB']->INSERTquery(
                            'tt_news'
                            ,array(
                                'pid' => $pid
                                ,'tstamp' => time()
                                ,'crdate' => time()
                                ,'title' => $nom 
                                ,'datetime' => time()
                                ,'bodytext' => $description
                                ,'author' => $author
                                ,'author_email' => $author_email 
                                ,'type' => 0 
                                ,'hidden' => 1 
                                ,'image' => $image 
                                ,'keywords' => $keywords
                                ,'tx_tsara_event_prix' => $nbpages
                                
                                 
                            )
                        );
       $tlog[] = 'Line '.__LINE__.' - '.$sqli;
       $GLOBALS['TYPO3_DB']->sql_query( $sqli );
       $uid_local = $GLOBALS['TYPO3_DB']->sql_insert_id();
       
        
        
        if( t3lib_utility_Math::convertToPositiveInteger( $parent_category_livre ) ){
            $sqli = $GLOBALS['TYPO3_DB']->INSERTquery(
                        'tt_news_cat_mm'
                        ,array(
                            'uid_local' => $uid_local
                            ,'uid_foreign' => $parent_category_livre 
                        )
                    );
            $tlog[] = 'Line '.__LINE__.' - '.$sqli;
            $GLOBALS['TYPO3_DB']->sql_query( $sqli ); 
        }
        t3lib_div::writeFile(PATH_site.'typo3temp/log_'.__FUNCTION__.'.txt', implode("\n",$tlog ) );
        return true;
        
    }
    function save_event($field){
        $tlog = array();
        
       //variables
       $date_le = t3lib_div::removeXSS( $field[31] ); 
       $date_entrele = t3lib_div::removeXSS( $field[44] ); 
       $date_etle = t3lib_div::removeXSS( $field[45] ); 
       $recurrence = t3lib_div::removeXSS( $field[53] ); 
       $nom = t3lib_div::removeXSS( $field[27] ); 
       $type = t3lib_div::removeXSS( $field[46] ); 
       $description = t3lib_div::removeXSS( $field[28] ); 
       $keywords =  t3lib_div::removeXSS( t3lib_div::_GP('keywords') ); 
         
       $ville = t3lib_div::removeXSS( $field[34] ); 
       
       $ville_mada = t3lib_div::removeXSS( $field[48] ); //157
       $ville_oi = t3lib_div::removeXSS( $field[49] ); //158
       $ville_europe = t3lib_div::removeXSS( $field[50] ); //europe
       $ville_amerique = t3lib_div::removeXSS( $field[52] ); //161
       $ville_asie = t3lib_div::removeXSS( $field[51] ); //162
       
       
       $adresse = t3lib_div::removeXSS( $field[30] ); 
       $contacts = t3lib_div::removeXSS( $field[41] ); 
       $acces = t3lib_div::removeXSS( $field[36] ); 
       //Payant
       $prix = t3lib_div::removeXSS( $field[37] ); 
       $location = t3lib_div::removeXSS( $field[40] ); 
       $details_reservation = t3lib_div::removeXSS( $field[38] ); 
       $prevente = t3lib_div::removeXSS( $field[39] ); 
       
       $author = t3lib_div::removeXSS( $field[54] );
       $author_email = t3lib_div::removeXSS( $field[55] );
       
       $pid = $this->confArr['pidevenement'];
       
       //Traitement
       $tx_tsara_event_date = ($date_le!=''?$this->datetimeconverter->_date_to_timestamp($date_le,'d/m/Y H:i'):$this->datetimeconverter->_date_to_timestamp($date_entrele,'d/m/Y'));
       $tx_tsara_event_date_end = ($date_etle!=''?$this->datetimeconverter->_date_to_timestamp($date_etle,'d/m/Y'):$this->datetimeconverter->_date_to_timestamp($date_le,'d/m/Y'));
       $tx_tsara_event_lieu = $adresse;
       $tx_tsara_event_contacts = $contacts;
       
       $tx_tsara_event_prix = ($acces=='Payant'?$prix:'');
       $tx_tsara_event_reservation = ($acces=='Payant'?$details_reservation:'');
       $tx_tsara_event_prevente = ($acces=='Payant'?$prevente:'');
       $tx_tsara_event_location = ($acces=='Payant'?$location:'');
       
       $image = '';
       $charset = 'utf-8';
       $space = '_';
       
       $file = $_FILES['tx_powermail_pi1'] ;
       $tlog[] = 'Line '.__LINE__.' - '.json_encode($_FILES);
       $powermailSession = $GLOBALS['TSFE']->fe_user->getKey('ses', 'powermail');
       $tlog[] = 'Line '.__LINE__.' - '. $powermailSession['upload'][0] ;
        
       
       if( $file['error']['field'][29] == 0 && $file['name']['field'][29]!=''){
           
            $filesrc = $powermailSession['upload'][0];
            $filename = str_ireplace(PATH_site.'uploads/tx_powermail/', '', $filesrc );
           
            $tlog[] = 'Line '.__LINE__.' - '.$filename;
            $title = $this->cs->conv_case($charset, $nom, 'toLower'  );
                        
            $title = strip_tags($title);
            $title = preg_replace('/[ \-+_]+/', $space, $title);
            $title = $this->cs->specCharsToASCII($charset, $title);
            $title = preg_replace('/[^a-zA-Z0-9' . ($space ? preg_quote($space) : '') . ']/', '', $title);
            $title = preg_replace('/\\' . $space . '{2,}/', $space, $title); // Convert multiple 'spaces' to a single one
            $title = trim($title, $space);


            $tab = t3lib_div::split_fileref( $filename );
            
            if( t3lib_div::inList('jpg,jpeg,png',$this->cs->conv_case($charset, $tab['fileext'], 'toLower'  ) ) ){
                
                $image =  $title.'.'.$tab['fileext'];
                $tlog[] = 'Line '.__LINE__.' - '.$image ;
                 

                $fichier = $this->fileFunc->cleanFileName( basename( $image ) );
                $chemin = PATH_site.'uploads/pics' ;
                $myfilepath = $this->getUniqueName($fichier, $chemin);
                $tlog[] = 'Line '.__LINE__.' - Sauvegarde de '.$filesrc.' vers '.$myfilepath ;
                if (t3lib_div::upload_copy_move( $filesrc  , $myfilepath  ) ) { 
                    
                    $tlog[] = 'Line '.__LINE__.' - Sauvegarde OK de '.$myfilepath ;
                    
                }
            }
            
       }
        
                            
        
       //Enregistrer
       $sqli = $GLOBALS['TYPO3_DB']->INSERTquery(
                            'tt_news'
                            ,array(
                                'pid' => $pid
                                ,'tstamp' => time()
                                ,'crdate' => time()
                                ,'title' => $nom 
                                ,'datetime' => time()
                                ,'bodytext' => $description
                                ,'author' => $author
                                ,'author_email' => $author_email 
                                ,'type' => 0 
                                ,'hidden' => 1 
                                ,'image' => $image 
                                ,'keywords' => $keywords
                                
                                ,'tx_tsara_event_date' => $tx_tsara_event_date 
                                ,'tx_tsara_event_date_end' => $tx_tsara_event_date_end 
                                ,'tx_tsara_event_lieu' => $tx_tsara_event_lieu 
                                ,'tx_tsara_event_prix' => $tx_tsara_event_prix 
                                ,'tx_tsara_event_reservation' => $tx_tsara_event_reservation 
                                ,'tx_tsara_event_prevente' => $tx_tsara_event_prevente 
                                ,'tx_tsara_event_location' => $tx_tsara_event_location 
                                ,'tx_tsara_event_contacts' => $tx_tsara_event_contacts 
                            )
                        );
       $tlog[] = 'Line '.__LINE__.' - '.$sqli;
       $GLOBALS['TYPO3_DB']->sql_query( $sqli );
       $uid_local = $GLOBALS['TYPO3_DB']->sql_insert_id();
       
       //Insert category
       if( $type != '' ){
            $tab_type = t3lib_div::trimExplode(',',$type );
            if( count( $tab_type )> 0){
                foreach( $tab_type as $th ){
                    $sqli = $GLOBALS['TYPO3_DB']->INSERTquery(
                        'tt_news_cat_mm'
                        ,array(
                            'uid_local' => $uid_local
                            ,'uid_foreign' => $th 
                        )
                    );
                    $tlog[] = 'Line '.__LINE__.' - '.$sqli;
                    $GLOBALS['TYPO3_DB']->sql_query( $sqli );
                }
            }
        }
        
        if( t3lib_utility_Math::convertToPositiveInteger( $ville ) ){
            $sqli = $GLOBALS['TYPO3_DB']->INSERTquery(
                        'tt_news_cat_mm'
                        ,array(
                            'uid_local' => $uid_local
                            ,'uid_foreign' => $ville 
                        )
                    );
            $tlog[] = 'Line '.__LINE__.' - '.$sqli;
            $GLOBALS['TYPO3_DB']->sql_query( $sqli );
            
            switch($ville){
                case '157' :
                    //Madagascar
                    $uid_foreign = $ville_mada;
                break;
                case '158' :
                    //Ocean Indien
                    $uid_foreign = $ville_oi;
                break;
                case '161' :
                    //Amerique
                    $uid_foreign = $ville_amerique;
                break;
                case '162' :
                    //Asie
                    $uid_foreign = $ville_asie;
                break;
                case '160' :
                    //Europe
                    $uid_foreign = $ville_europe;
                break;
            }
            
            if( t3lib_div::inList('157,158,161,162,160',$ville)){
                $sqli = $GLOBALS['TYPO3_DB']->INSERTquery(
                        'tt_news_cat_mm'
                        ,array(
                            'uid_local' => $uid_local
                            ,'uid_foreign' => $uid_foreign 
                        )
                    );
                    $tlog[] = 'Line '.__LINE__.' - '.$sqli;
                    $GLOBALS['TYPO3_DB']->sql_query( $sqli );
            }
            
            
        }
        
        t3lib_div::writeFile(PATH_site.'typo3temp/log_'.__CLASS__.'.txt', implode("\n",$tlog ) );
        return true;
    }
    function save_site($field){
        
            
              
            //variables
            $url = t3lib_div::removeXSS( $field[13] );
            $nomsite = t3lib_div::removeXSS( $field[17] );
            $short = t3lib_div::removeXSS( $field[18] );
            $author = t3lib_div::removeXSS( $field[23] );
            $author_email = t3lib_div::removeXSS( $field[24] );
            $thematique = t3lib_div::removeXSS( $field[22] );
             
            $pid = $this->confArr['pidsites'];
            
            //Save in ttnews if not in DB
            $tab_url = parse_url($url);
            $host = $tab_url['host'];
            
            $host = $this->cs->conv_case('utf-8',$host,'toLower');
            $scheme = $this->cs->conv_case('utf-8',$tab_url['scheme'],'toLower');
            $host = trim($host);
            $www = $this->cs->substr('utf-8',$host,0,4);
            $url_without_www = 'http://'.($www=='www.'?$this->cs->substr('utf-8',$host,4):$host).'/';
            $urls_without_www = 'https://'.($www=='www.'?$this->cs->substr('utf-8',$host,4):$host).'/';
            $url_with_www = 'http://'.($www=='www.'?$host:'www.'.$host).'/';
            $urls_with_www = 'https://'.($www=='www.'?$host:'www.'.$host).'/';
            
            $report = array();
            $tlog = array();
            
            $str = t3lib_div::getUrl($url,1,FALSE,$report);
             
            
            
            
            $nb = $GLOBALS['TYPO3_DB']->exec_SELECTcountRows(
                    'uid',
                    'tt_news',
                    ' ext_url IN('.$GLOBALS['TYPO3_DB']->fullQuoteStr($url_without_www,'tt_news').','.$GLOBALS['TYPO3_DB']->fullQuoteStr($url_with_www,'tt_news').','.$GLOBALS['TYPO3_DB']->fullQuoteStr($urls_with_www,'tt_news').','.$GLOBALS['TYPO3_DB']->fullQuoteStr($urls_without_www,'tt_news').')  AND deleted=0 AND pid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($pidsites,'tt_news')
                    );
            $tlog[] = 'Line '.__LINE__.' - '.$nb;
            if( $nb <= 0 ){
                $hidden = 1;
                //Si le title ou le short contient mada > valider
                $kw_sitevalide = $this->confArr['keywords_sitevalide'];
                $tlog[] = 'Line '.__LINE__.' - '.$kw_sitevalide;
                if( trim( $kw_sitevalide)!=''){
                    $kw_sitevalide = str_replace("/","\/",$kw_sitevalide);
                    $tab_kw = t3lib_div::trimExplode(',',$kw_sitevalide);
                    $tlog[] = 'Line '.__LINE__.' - '.implode('|',$tab_kw);
                    if( preg_match('/'.implode('|',$tab_kw).'/iU',$nomsite ) || preg_match('/'.implode('|',$tab_kw).'/iU',$short ) ) {
                        $hidden = 0;
                    }
                        
                    
                }
                
                $tlog[] = 'Line '.__LINE__.' - '.$hidden;
                
                
                
                
                $sqli = $GLOBALS['TYPO3_DB']->INSERTquery(
                            'tt_news'
                            ,array(
                                'pid' => $pid
                                ,'tstamp' => time()
                                ,'crdate' => time()
                                ,'title' => $nomsite
                                ,'datetime' => time()
                                ,'short' => $short
                                ,'author' => $author
                                ,'author_email' => $author_email
                                ,'ext_url' => $scheme.'://'.$host.'/' 
                                ,'hidden' => $hidden 
                                ,'type' => 2 
                            )
                        );
                $tlog[] = 'Line '.__LINE__.' - '.$sqli;
                
                 $GLOBALS['TYPO3_DB']->sql_query( $sqli );
                
                $uid = $GLOBALS['TYPO3_DB']->sql_insert_id();
                //INsert in ttnews_cat_mm
                
                if( $thematique != '' ){
                    $tab_thematique = t3lib_div::trimExplode(',',$thematique);
                    if( count( $tab_thematique )> 0){
                        foreach( $tab_thematique as $th ){
                            $sqli = $GLOBALS['TYPO3_DB']->INSERTquery(
                                'tt_news_cat_mm'
                                ,array(
                                    'uid_local' => $uid
                                    ,'uid_foreign' => $th 
                                )
                            );
                            $tlog[] = 'Line '.__LINE__.' - '.$sqli;
                            $GLOBALS['TYPO3_DB']->sql_query( $sqli );
                        }
                    }
                }
                
               
                
            }
            return true;
    }
    function getUniqueName($theFile, $theDest, $dontCheckForUnique = 0) {
        $getUniqueNamePrefix = '';
        $maxNumber = 99;
        $uniquePrecision = 6;

        $origFileInfo = t3lib_div::split_fileref($theFile); // Fetches info about path, name, extention of $theFile
        if ($theDest) {
            if ($getUniqueNamePrefix) { // Adds prefix
                    $origFileInfo['file'] = $getUniqueNamePrefix . $origFileInfo['file'];
                    $origFileInfo['filebody'] = $getUniqueNamePrefix . $origFileInfo['filebody'];
            }

                    // Check if the file exists and if not - return the filename...
            $fileInfo = $origFileInfo;
            $theDestFile = $theDest . '/' . $fileInfo['file']; // The destinations file
            if (!file_exists($theDestFile) || $dontCheckForUnique) { // If the file does NOT exist we return this filename
                    return $theDestFile;
            }

                    // Well the filename in its pure form existed. Now we try to append numbers / unique-strings and see if we can find an available filename...
            $theTempFileBody = preg_replace('/_[0-9][0-9]$/', '', $origFileInfo['filebody']); // This removes _xx if appended to the file
            $theOrigExt = $origFileInfo['realFileext'] ? '.' . $origFileInfo['realFileext'] : '';

            for ($a = 1; $a <= ($maxNumber + 1); $a++) {
                    if ($a <= $maxNumber) { // First we try to append numbers
                            $insert = '_' . sprintf('%02d', $a);
                    } else { // .. then we try unique-strings...
                            $insert = '_' . substr(md5(uniqId('')), 0, $uniquePrecision);
                    }
                    $theTestFile = $theTempFileBody . $insert . $theOrigExt;
                    $theDestFile = $theDest . '/' . $theTestFile; // The destinations file
                    if (!file_exists($theDestFile)) { // If the file does NOT exist we return this filename
                            return $theDestFile;
                    }
            }
        }
}
    }

?>
