<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2009 Alexander Kellner <alexander.kellner@einpraegsam.net>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

require_once(t3lib_extMgm::extPath("tsara").'lib/class.tx_tsara_util.php');


class user_tca {
        public function __construct() {
            $this->util = t3lib_div::makeInstance('tx_tsara_util');
            $this->confArr = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['tsara']);
            $this->cs = t3lib_div::makeInstance('t3lib_cs');
        }
        function gettest($content,$conf){
             
            $ttnews = t3lib_div::_GP('tx_ttnews');
            
            return $ttnews['swords'];
            
            
            
        }
        function getThematiquesite($content,$conf){
            $variables = t3lib_div::_GP('tx_powermail_pi1');
            $thematique = t3lib_div::removeXSS( $variables['field'][22] );
            $tab = array();
            $tab_thematique = t3lib_div::trimExplode(',',$thematique);
            foreach($tab_thematique as $uid ){
                $tab[] = $this->util->getfieldincat($uid,'title');
            }
            return implode(', ',$tab);
                    
        }
        function getUidevent($content,$conf){
            
            $uid = 0;
            $keywords =  t3lib_div::removeXSS( t3lib_div::_GP('keywords') ); 
             
            $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow(
                    'uid',
                    'tt_news',
                    'keywords = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($keywords,'tt_news').'
                        AND deleted = 0 AND 
                        pid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($this->confArr['pidevenement'],'tt_news')
                    );
            $uid = $row['uid'];
            return $uid;
        }
        function get_image($content,$conf){
            $image = '';
            $uid = 0;            
            
            
            $opt = $conf['opt']; 
            
            switch ( $opt ){
                case 'photo' :
                case 'video' :
                    $ttnews = t3lib_div::_GP('tx_ttnews');
                    if(isset($ttnews) &&  t3lib_utility_Math::convertToPositiveInteger( $ttnews['tt_news'] )  ){
                        $uid = $ttnews['tt_news'];
                        $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow(
                        'tx_tsara_youtube_thumbnail',
                        'tt_news',
                        'uid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($uid,'tt_news').'
                            AND deleted = 0 ' 
                        );
                        $image = $row['tx_tsara_youtube_thumbnail'];
                        $image = $GLOBALS['TSFE']->baseUrl.'thbx_300__1_'.str_replace('http://','',$image);
                        return $image;
                    }
                break;
                case 'artiste' :
                    $ttnews = t3lib_div::_GP('tx_ttnews');
                    if(isset($ttnews) &&  t3lib_utility_Math::convertToPositiveInteger( $ttnews['artiste'][0] )  ){
                        $uid = $ttnews['artiste'][0];
                        $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow(
                        'image',
                        'tt_news_cat',
                        'uid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($uid,'tt_news_cat').'
                            AND deleted = 0 ' 
                        );
                        $image = $row['image'];
                        $image = $GLOBALS['TSFE']->baseUrl.'thb_300__1_/uploads/pics/'.($image);
                        return $image;
                    }
                break;
                case 'author' :
                    $ttnews = t3lib_div::_GP('tx_ttnews');
                    if(isset($ttnews) &&  t3lib_utility_Math::convertToPositiveInteger( $ttnews['author']  )  ){
                        $uid = $ttnews['author'] ;
                        $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow(
                        'image',
                        'fe_users',
                        'uid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($uid,'fe_users').'
                            AND deleted = 0 ' 
                        );
                        $image = $row['image'];
                        $image = $GLOBALS['TSFE']->baseUrl.'thb_300__1_/uploads/pics/'.($image);
                        return $image;
                    }
                break;
               
            }
            return $title;
        }
		function  get_metadescription($content,$conf){
			$description = $GLOBALS['TSFE']->page['description'];
			$ttnews = t3lib_div::_GP('tx_ttnews');
			if(isset($ttnews) &&  t3lib_utility_Math::convertToPositiveInteger( $ttnews['tt_news'] )  ){
				$pattern = 
		  '\\['                              // Opening bracket
		. '(\\[?)'                           // 1: Optional second opening bracket for escaping shortcodes: [[tag]]
		. "($tagregexp)"                     // 2: Shortcode name
		. '\\b'                              // Word boundary
		. '('                                // 3: Unroll the loop: Inside the opening shortcode tag
		.     '[^\\]\\/]*'                   // Not a closing bracket or forward slash
		.     '(?:'
		.         '\\/(?!\\])'               // A forward slash not followed by a closing bracket
		.         '[^\\]\\/]*'               // Not a closing bracket or forward slash
		.     ')*?'
		. ')'
		. '(?:'
		.     '(\\/)'                        // 4: Self closing tag ...
		.     '\\]'                          // ... and closing bracket
		. '|'
		.     '\\]'                          // Closing bracket
		.     '(?:'
		.         '('                        // 5: Unroll the loop: Optionally, anything between the opening and closing shortcode tags
		.             '[^\\[]*+'             // Not an opening bracket
		.             '(?:'
		.                 '\\[(?!\\/\\2\\])' // An opening bracket not followed by the closing shortcode tag
		.                 '[^\\[]*+'         // Not an opening bracket
		.             ')*+'
		.         ')'
		.         '\\[\\/\\2\\]'             // Closing shortcode tag
		.     ')?'
		. ')'
		. '(\\]?)';
				$uid = $ttnews['tt_news'];
				$row = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow(
				'bodytext',
				'tt_news',
				'uid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($uid,'tt_news').'
					AND deleted = 0 ' 
				);
				$description = trim( strip_tags( ( $row['bodytext'] ) ) );
				$description = preg_replace( "/$pattern/s", '', $description );
				
				$description = $this->cs->entities_to_utf8( $description, 1 );
				$description = $this->cs->crop('utf-8',$description,200 );
				$description = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", " ", $description);
				 
			}
			
			return $description;
		}
        function get_title($content,$conf){
            $title = $GLOBALS['TSFE']->page['tx_tsara_metatitle'];
            $uid = 0;            
            
            
            $opt = $conf['opt']; 
            
            switch ( $opt ){
                case 'author' :
                    $ttnews = t3lib_div::_GP('tx_ttnews');
                    if(isset($ttnews) &&  t3lib_utility_Math::convertToPositiveInteger( $ttnews['author']  )  ){
                        $uid = $ttnews['author'] ;
                        $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow(
                        'name',
                        'fe_users',
                        'uid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($uid,'fe_users').'
                            AND deleted = 0 ' 
                        );
                        $name = $row['name'];
                
                        return 'Tous les articles de '.$name;
                    }
                break;
                case 'video' :
                    $ttnews = t3lib_div::_GP('tx_ttnews');
                    $tx_tsara_pi5 = t3lib_div::_GP('tx_tsara_pi5');
                    if( isset($tx_tsara_pi5) ) $artistegroupe =  $tx_tsara_pi5['artistegroupe'];
                    if(isset($ttnews) &&  t3lib_utility_Math::convertToPositiveInteger( $ttnews['tt_news'] )  ){                
                        $uid = $ttnews['tt_news'];
                        $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow(
                        'title',
                        'tt_news',
                        'uid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($uid,'tt_news').'
                            AND deleted = 0 ' 
                        );
                        $title = $row['title'];
                        $artiste = $this->util->get_cat( $uid );
                        $title = $title.' de '.$artiste[0]['name'].(count($artiste)>1?'...':'');
                    }
                    if(isset($ttnews) &&  is_array( $ttnews['artiste'] )  ){                
                        $artiste = $ttnews['artiste'][0];
                        $titleartiste = $this->util->getfieldincat( $artiste, 'title' );
                        $title = 'Tous les clips vidéos de '.$titleartiste.(count($ttnews['artiste'])>1?'...':'');
                    }
                    if(isset($ttnews) &&  is_array( $ttnews['genre'] )  ){                
                        $artiste = $ttnews['genre'][0];
                        $titleartiste = $this->util->getfieldincat( $artiste, 'title' );
                        $title = 'Tous les clips vidéos du genre musical '.$titleartiste.(count($ttnews['genre'])>1?'...':'');
                    }
                    if(isset($ttnews) &&  $ttnews['filter'] == 'duo'  ){                
                        $title = 'Clips Malgaches - Tous les Duo ' ;
                    }
                    if(isset($ttnews) &&  $ttnews['filter'] == 'multiple'  ){                
                        $title = 'Clips Malgaches avec plusieurs artistes' ;
                    }
                    if(isset($ttnews) &&  $ttnews['order'] == 'numcomments'  ){                
                        $title = 'Clips Malgaches les plus commentés' ;
                    }
                    if(isset($ttnews) &&  $ttnews['order'] == 'countview'  ){                
                        $title = 'Clips Malgaches les plus vus' ;
                    }
                    if(isset($ttnews) &&  $ttnews['order'] == 'numlikes'  ){                
                        $title = 'Clips Malgaches les plus aimés' ;
                    }
                    if(isset($ttnews) &&  $ttnews['order'] == 'duration'  ){                
                        $title = 'Clips Malgaches les plus longs' ;
                    }
                    if(isset($artistegroupe) && t3lib_utility_Math::convertToPositiveInteger($artistegroupe)   ){                
                        $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow(
                        'title',
                        'tt_news_cat',
                        'uid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($artistegroupe,'tt_news_cat').'
                            AND deleted = 0 ' 
                        );
                        $title = $row['title'];
                        $title = 'Autres artistes ayant chantés avec '.$title ;
                    }
                break; 
               
            }
            return $title;
        }
        function encodeTitlenews($content,$conf){
            $ttnews = t3lib_div::_GP('tx_ttnews');
            $uid = $ttnews['tt_news'];
            $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow(
                    'title',
                    'tt_news',
                    'uid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($uid,'tt_news').'
                        AND deleted = 0 ' 
                    );
            $space = '_';
            $title = $row['title'];
            $title = $this->cs->conv_case('utf-8',$title,'toLower');
            $title = preg_replace('/[ \-+_]+/', $space, $title);
            $title = $this->cs->specCharsToASCII($charset, $title);
            $title = preg_replace('/[^a-zA-Z0-9' . ($space ? preg_quote($space) : '') . ']/', '', $title);
            $title = preg_replace('/\\' . $space . '{2,}/', $space, $title); // Convert multiple 'spaces' to a single one
            $title = trim($title, $space);
            
            return $title;
        }
        function getUidlivre($content,$conf){
            
            $uid = 0;
            $keywords =  t3lib_div::removeXSS( t3lib_div::_GP('keywords') ); 
             
            $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow(
                    'uid',
                    'tt_news',
                    'keywords = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($keywords,'tt_news').'
                        AND deleted = 0 AND 
                        pid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($this->confArr['pidlivres'],'tt_news')
                    );
            $uid = $row['uid'];
            return $uid;
        }
        function ip_isadmin(){
            $isadmin = false;
            
            $ipadmin = $this->confArr['ipadmin'];
            $ip = t3lib_div::getIndpEnv('REMOTE_ADDR');
            
            if( t3lib_div::inList($ipadmin, $ip) ) $isadmin = true;
            
            
            return $isadmin;
        }
        function isadmin(){
            $isadmin = false;
            if( trim( $_COOKIE['be_typo_user'] ) != '' ){
                    $be_typo_user = t3lib_div::removeXSS($_COOKIE['be_typo_user']);                
                    $rowsession = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow(
                            'a.admin',
                            'be_users AS a LEFT JOIN be_sessions AS b ON b.ses_userid = a.uid ',
                            'a.disable = 0 AND b.ses_id = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($be_typo_user ,'be_sessions') 
                            );                
                    $isadmin = ($rowsession['admin']==1?true:false) ;
                }
            return $isadmin;
        }
        function create_field_beconnected($content,$conf){
            if( $this->isadmin() || $this->ip_isadmin() ){
                return '<input type="hidden" id="beconnected" value="1" />';
            }
            return '';
        }
        function get_recapmodifsite($content,$conf){
            $variables = t3lib_div::_GP('tx_powermail_pi1');
            //return '<pre>'.print_r($variables,1).'</pre>';
            $tlog = array(); 
                    
                    
                    
            $uidsite = t3lib_utility_Math::convertToPositiveInteger( $variables['field']['uidsite'] );
            $opt = t3lib_utility_Math::convertToPositiveInteger( $variables['field']['opt'] );
            $site_existant = t3lib_div::removeXSS( $variables['field'][82] );
            $nouvelleurldusite = t3lib_div::removeXSS( $variables['field'][83] );
            $design = t3lib_div::removeXSS( $variables['field'][84] );
            $description = t3lib_div::removeXSS( $variables['field'][85] );
            $nom = t3lib_div::removeXSS( $variables['field'][87] );
            $email = t3lib_div::removeXSS( $variables['field'][88] );
            $theme = t3lib_div::removeXSS( $variables['field'][22] );
            
            if( $uidsite > 0 ){
                $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow(
                        '*',
                        'tt_news',
                        'uid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($uidsite,'tt_news')
                        );
                $tlog[] = 'Infos sur le site dans la BDD ';
                $tlog[] = '-----------'  ;
                $tlog[] = 'Nom : '.$row['title'];
                $tlog[] = 'Date ajout : '.date('d-m-Y H:i:s',$row['datetime']);
                $tlog[] = 'Image : '.$GLOBALS['TSFE']->baseUrl.'uploads/pics/'.$row['image'];
                $tlog[] = 'URL : '.$row['ext_url'];
                $tlog[] = 'Auteur : '.$row['author'].' ('.str_replace('madatsara.com','',$row['author_email']).')';
                $tlog[] = 'Description : '.$row['short'] ;
                $tlog[] = '-----------'  ;
            }
            $tlog[] = 'Infos remplie par l\'internaute et actions ';
            switch( $opt ){
                case '1' :
                    $lien_desactiver = $GLOBALS['TSFE']->baseUrl.'index.php?eID=tsara&task=valid&opt=3&uid='.$uidsite;
                    $tlog[] = 'Ce site n\'existe plus ? : '.$site_existant;
                    $tlog[] =  $site_existant=='Oui'?'Désactiver sur Madatsara > '.$lien_desactiver:'Aucune action à faire';
                break;
                case '2' :
                    $lien_desactiver = $GLOBALS['TSFE']->baseUrl.'index.php?eID=tsara&task=majsite&opt=1&uid='.$uidsite.'&newurl='.$nouvelleurldusite;
                    $tlog[] = 'Nouvelle URL du site : '.$nouvelleurldusite;
                    $tlog[] =  'Mettre à jour  sur Madatsara ?  > '.$lien_desactiver ;
                break;
                case '3' :
                    $lien_desactiver = $GLOBALS['TSFE']->baseUrl.'index.php?eID=tsara&task=majsite&opt=2&uid='.$uidsite ;
                    $tlog[] = 'Ce site a changé de design ? : '.$design;
                    $tlog[] =  $design=='Oui'?'Mettre à jour  sur Madatsara ?  > '.$lien_desactiver:'Aucune action à faire' ;
                break;
                case '4' :
                    $time = time();
                    $lien_desactiver = $GLOBALS['TSFE']->baseUrl.'index.php?eID=tsara&task=majsite&opt=3&uid='.$uidsite.'&jsonfile='.rawurlencode(PATH_site.'typo3temp/json/'.$time.'.txt');
                    $tlog[] = 'Nom : '.$nom;
                    $tlog[] = 'Thematique : '.$this->getThematiquesite($content,$conf).' (ID : '.$theme.') ';
                    $tlog[] = 'Email : '.$email;
                    $tlog[] = 'Description : '.'<hr />'.($description).'<hr />';
                    $tlog[] =  'Mettre à jour  sur Madatsara ?  > '.$lien_desactiver ;
                    t3lib_div::writeFile(PATH_site.'typo3temp/json/'.$time.'.txt',json_encode(array('author' => $nom, 'email_author' => $email, 'short' => $description, 'cat' => t3lib_div::trimExplode(',',$theme) )));
                break;
            }
            $tlog[] = '-----------'  ;
            
            return nl2br( implode("\r\n", $tlog));
        }
        
        function get_author($content,$conf){
            $variables = t3lib_div::_GP('tx_powermail_pi1');
            $uid = t3lib_div::removeXSS( $variables['field'][80] );
            
            $field = ($conf['field']!=''?$conf['field']:'email');
            
            $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow(
                    $field,
                    'fe_users',
                    'uid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($uid,'fe_users').'
                        AND deleted = 0 ' 
                    );
            if( $row[$field] != '' ){
                return $row[$field];
            }
            else return 'madatsara@gmail.com';
        }
        
        function getUidsite($content,$conf){
            
            $uid = 0;
            $variables = t3lib_div::_GP('tx_powermail_pi1');
            $url = t3lib_div::removeXSS( $variables['field'][13] );
            $tab_url = parse_url($url);
            $host = $tab_url['host'];
            $host = $this->cs->conv_case('utf-8',$host,'toLower');
            $scheme = $this->cs->conv_case('utf-8',$tab_url['scheme'],'toLower');
            $url = $scheme.'://'.$host.'/';
            $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow(
                    'uid',
                    'tt_news',
                    'ext_url = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($url,'tt_news').'
                        AND deleted = 0 AND 
                        pid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($this->confArr['pidsites'],'tt_news')
                    );
            $uid = $row['uid'];
            return $uid;
        }
        function get_thematique($content,$conf){
             
            
            
            $menuArr = array();
            $lConf = $conf["userFunc."];
            //$res = $this->cObj->exec_getQuery($lConf["table"],$lConf["select."]);
            
            $sql = $GLOBALS['TYPO3_DB']->SELECTquery(
                    '*',
                    $lConf["table"],
                    '1=1 '.$this->cObj->enableFields($lConf["table"]).' AND pid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($lConf["select."]['pid'],$lConf["table"]).' AND parent_category = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($lConf["select."]['parentcategory'],$lConf["table"])
                    ,''
                    ,'title ASC'
                    );
            $res = $GLOBALS['TYPO3_DB']->sql_query( $sql );
            
            while ($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
                $menuArr[] = $row;
            }
            
            return $menuArr;
            
            
            
        }
	 
         
        
        
        function user_showwizardpartenaire($PA, $fobj) {
                $row = $toption = array();
                
                //Value du TCA
                $value = $PA['itemFormElValue']; 
                $html = '';
                
                
                $html .= '
                    <input type="hidden" id="uidpartenaire" name="'.$PA['itemFormElName'].'" value="'.$value.'" />
                        <input type="text" size="40" name="'.$PA['itemFormElName'].'" id="search" value="('.$value.') '.$this->getUid($value,'c32').'" />
                    
                    <script type="text/javascript" src="../typo3conf/ext/tsara/lib/scripts.js"></script>
                    
                ';
            
                return $html;
	}
        
        
        function user_showfieldfeuser($config) {
            $row = $toption = array();
            //Value du TCA
            $value = $PA['itemFormElValue'];
            
            $row = $GLOBALS['TYPO3_DB']->admin_get_fields('fe_users');
             
            
            $i=0;
            foreach( array_keys($row) as $field ){
                $optionList[$i] = array(0 => $field, 1 => $field);
                $i++;
            }
                
            $config['items'] = array_merge($config['items'],$optionList);
            return $config;
             
        }
	
         
        	function user_showforumliste($PA, $fobj) {
                $row = $toption = array();
                
                //Value du TCA
                $value = $PA['itemFormElValue'];
                
                $listfields = ' f.id,f.forum_name,f.forum_desc,f.num_posts,f.num_topics ';
                
                $sql = "SELECT".$listfields."FROM ".$this->confArr['prefixtable']."forums f 
LEFT JOIN ".$this->confArr['prefixtable']."forum_perms perms ON perms.forum_id = f.id
WHERE perms.read_forum > 0  GROUP BY f.id";
                   
                   
                $sql .= " ORDER BY f.forum_name ASC";
                
                
                   
                
                //Get Row
                $res = $GLOBALS['TYPO3_DB']->sql_query( $sql );
                
                if( $res ){
                    while( $ligne = $GLOBALS['TYPO3_DB']->sql_fetch_assoc( $res ) ) {
                        $toption[] = '<option value="'.$ligne['id'].'"'.($ligne['id']==$value?' selected="selected"':'').'>'.$ligne['forum_name'].'</option>';
                    }
                }
                
                 
                
                
                
                
                $html = '';
                
                $html .= '<select name="'.$PA['itemFormElName'].'" id="'.$PA['itemFormElID'].'">';
                
                $html .= '<option value=""></option>';
                
                $html .= implode("\n",$toption);
                
                $html .= '</select>';
            
                return $html;
	}
        

}
?>