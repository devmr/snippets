<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of class
 *
 * @author mrabehasy
 */
class tx_tsara_realurlspurl {
    function encodeSpURL_postProc(&$params, &$ref) {
        
        
        $url = $params['URL'];
         
        
        $url =  str_replace('actualites/detail/article/', 'article_', $url);
        $url =  str_replace('actualites/auteur/', 'tous-les-articles-de_', $url);
        $url =  str_replace('culture/livres/detail/article/', 'livre_', $url);
        $url =  str_replace('culture/cinemas/detail/article/', 'cinema_', $url);
        $url =  str_replace('culture/traditions/detail/article/', 'tradition_', $url);
        $url =  str_replace('evenements/detail/article/', 'evenement_', $url);
        $url =  str_replace('mediatheque/photos/detail/article/', 'album-photo_', $url);
        $url =  str_replace('mediatheque/videos/detail/article/', 'video_', $url);
        $url =  str_replace('actualites-dans-la-presse-locale/detail/article/', 'actualite_', $url);
        
        $url =  str_replace('/page/', ',page_', $url);
        $url =  str_replace('/pagecomment/', ',pagec_', $url);
        $url =  str_replace('videos/artiste/', 'clips-chansons_', $url);
        $url =  str_replace('videos/genre-musical/', 'clips-genre_', $url);
        $url =  str_replace('/order/numcomments', 'les-plus-commentes', $url);
        $url =  str_replace('/order/countview', 'les-plus-vus', $url);
        $url =  str_replace('/order/numlikes', 'les-plus-aimes', $url);
        $url =  str_replace('/order/duration', 'les-plus-longs', $url);
        $url =  str_replace('/filtre/duo', ',les-duo,', $url);
        $url =  str_replace('/filtre/multiple', ',avec-plusieurs-artistes,', $url);
        
        $url =  str_replace('culture/livres/ecrit-par/', 'livres-rediges-par_', $url);
        $url =  str_replace('culture/cinemas/ecrit-par/', 'films-ecrits-par_', $url);
        $url =  str_replace('culture/livres/realise-par/', 'livres-edites-par_', $url);
        $url =  str_replace('culture/cinemas/realise-par/', 'films-realises-par_', $url);
        $url =  str_replace('culture/cinemas/photo-dirigee-par/', 'film-directeur-photographie-par_', $url);
        $url =  str_replace('culture/cinemas/produits-par/', 'film-produits-par_', $url);
        $url =  str_replace('culture/cinemas/scenarise-par/', 'film-scenarise-par_', $url);
        $url =  str_replace('culture/cinemas/son-monte-par/', 'film-montage-son-par_', $url);
        $url =  str_replace('culture/cinemas/perchman-par/', 'film-perchman-par_', $url);
        $url =  str_replace('culture/cinemas/effets-visuels-par/', 'film-effets-visuels-par_', $url);
        $url =  str_replace('culture/cinemas/genre-musical/', 'genre-film_', $url);
        $url =  str_replace('mediatheque/videos/tous-les-artistes-ayant-chante-avec/artistechante/', 'artistes-chantes-avec_', $url);
        
        $url =  str_replace('/motscles/', '__', $url);
        
        
        //$url = $this->encodeUrl( $url );
        
        
        
        
        
        $params['URL'] = $url;
    }
    function decodeSpURL_preProc(&$params, &$ref) {
        $url = $params['URL'];
        
        $url =  str_replace('tous-les-articles-de_', 'actualites/auteur/', $url);
        $url =  str_replace('article_', 'actualites/detail/article/', $url);
        $url =  str_replace('livre_', 'culture/livres/detail/article/', $url);
        $url =  str_replace('cinema_', 'culture/cinemas/detail/article/', $url);
        $url =  str_replace('tradition_', 'culture/traditions/detail/article/', $url);
        $url =  str_replace('evenement_', 'evenements/detail/article/', $url);
        $url =  str_replace('album-photo_', 'mediatheque/photos/detail/article/', $url);
        $url =  str_replace('video_', 'mediatheque/videos/detail/article/', $url);
        $url =  str_replace('actualite_', 'actualites-dans-la-presse-locale/detail/article/', $url);
        $url =  str_replace(',page_', '/page/', $url);
        $url =  str_replace(',pagec_', '/pagecomment/', $url);
        $url =  str_replace('clips-chansons_', 'videos/artiste/', $url);
        $url =  str_replace('clips-genre_', 'videos/genre-musical/', $url);
        $url =  str_replace('les-plus-commentes', '/order/numcomments', $url);
        $url =  str_replace('les-plus-vus', '/order/countview', $url);
        $url =  str_replace('les-plus-aimes', '/order/numlikes', $url);
        $url =  str_replace('les-plus-longs', '/order/duration', $url);
        $url =  str_replace(',les-duo,','/filtre/duo', $url);
        $url =  str_replace(',avec-plusieurs-artistes,','/filtre/multiple', $url);
        
        $url =  str_replace('livres-rediges-par_', 'culture/livres/ecrit-par/', $url);
        $url =  str_replace('films-ecrits-par_', 'culture/cinemas/ecrit-par/', $url);
        $url =  str_replace('livres-edites-par_', 'culture/livres/realise-par/', $url);
        $url =  str_replace('films-realises-par_', 'culture/cinemas/realise-par/', $url);
        
        $url =  str_replace('film-directeur-photographie-par_', 'culture/cinemas/photo-dirigee-par/', $url);
        $url =  str_replace('film-produits-par_', 'culture/cinemas/produits-par/', $url);
        $url =  str_replace('film-scenarise-par_', 'culture/cinemas/scenarise-par/', $url);
        $url =  str_replace('film-montage-son-par_', 'culture/cinemas/son-monte-par/', $url);
        $url =  str_replace('film-perchman-par_', 'culture/cinemas/perchman-par/', $url);
        $url =  str_replace('film-effets-visuels-par_', 'culture/cinemas/effets-visuels-par/', $url);
        $url =  str_replace('genre-film_', 'culture/cinemas/genre-musical/', $url);
        $url =  str_replace('artistes-chantes-avec_', 'mediatheque/videos/tous-les-artistes-ayant-chante-avec/artistechante/', $url);

        
        $url =  str_replace('__', '/motscles/', $url);
//        $url = $this->decodeUrl( $url );
        
                        
         
        $params['URL'] = $url;
        
    }
    
    function encodeUrl( $url ){
        
        /*
         * Thematique seule - liste-cadeau-culturel/thematique/49/recherche-reseau/1
         * Departement seul - liste-cadeau-culturel/departement/18/recherche-reseau/1
         * Thematiques combinees - liste-cadeau-culturel/thematique/24-25-35/recherche-reseau/1
         * Departements combines - liste-cadeau-culturel/departement/41-63-88/recherche-reseau/1
         * Departement et thematique combines - liste-cadeau-culturel/thematique/49-51/departement/13-38/recherche-reseau/1
         * Localite - liste-cadeau-culturel/recherche-reseau/1/localite/manche
         * Localite + thematique - liste-cadeau-culturel/thematique/24/recherche-reseau/1/localite/paris.html
         */
        
        $tab = $tab2 = $tabliste = $tab_dept = $tab_thematique = $tab3 = array();
        $thematique = 0;
        $departement = 0;
        
        
        $tab = t3lib_div::trimExplode('/', $url );
        if( count( $tab ) > 0 ){
           
            if( $tab[0] == 'liste-cadeau-culturel' ){
               
                
                    
                    switch( $tab[3] ){
                        case 'recherche-reseau' :
                             //Thematique ou departement
                             switch( $tab[1] ){
                                case 'thematique' :
                                    $thematique = $tab[2];
                                    $tab2 = t3lib_div::trimExplode('-', $thematique );
                                    if( count( $tab2 ) > 0 ){
                                        
                                        //Plusieurs thematiques                                        
                                        foreach( $tab2 as $liste ){
                                            $tabliste[] = $this->getThematique($liste); 
                                        }
                                        
                                        if( isset( $tab[5] ) && $tab[5] == 'localite' ){
                                            $localite = $tab[6];
                                            $localite = substr( $localite,0,strpos($localite,'.html') );
                                            return 'carte-cadeau-'.implode('_',$tabliste).','.$localite.'__.html';
                                        }
                                         
                                        return 'carte-cadeau-'.implode('_',$tabliste).'.html';
                                        unset($tabliste);
                                        
                                    }
                                    else{
                                        //Une seule thematique
                                        if( isset( $tab[5] ) && $tab[5] == 'localite' ){
                                            $localite = $tab[6];
                                            $localite = substr( $localite,0,strpos($localite,'.html') );
                                            return 'carte-cadeau-'.$this->getThematique($thematique).','.$localite.'__.html';
                                        }
                                        return 'carte-cadeau-'.$this->getThematique($thematique).'.html';
                                    }
                                    
                                break;
                                case 'departement' :
                                    $departement = $tab[2];
                                    $tab2 = t3lib_div::trimExplode('-', $departement );
                                    if( count( $tab2 ) > 0 ){
                                        //PLusieurs departements
                                        foreach( $tab2 as $liste ){
                                            $tabliste[] = $this->getDepartement($liste); 
                                        }
                                        
                                        return 'carte-cadeau-'.implode('_',$tabliste).'.html';
                                        unset($tabliste);
                                    }
                                    else{
                                        //Un  seul departement
                                        return 'carte-cadeau-'.$this->getDepartement( $departement ).'.html';
                                    }
                                break;
                             }
                            
                        
                        break;
                        case 'departement' :
                            $url = '';
                            //Thematique seule ou combinees
                            $departement = $tab[4];
                            $thematique = $tab[2];
                            
                            $tab_dept = t3lib_div::trimExplode('-', $departement );
                            $tab_thematique = t3lib_div::trimExplode('-', $thematique );
                            
                            if( count( $tab_thematique ) > 0 ){
                                
                                //PLusieurs departements
                                foreach( $tab_thematique as $liste ){
                                    $tabliste[] = $this->getThematique($liste); 
                                }

                                $url .= implode('_',$tabliste);
                                unset($tabliste);
                            }
                            else{
                                //Un  seul departement
                                
                                $url .= $this->getThematique( $departement );
                            }
                            if( count( $tab_dept ) > 0 ){
                                $url .= ',';
                                //PLusieurs departements
                                foreach( $tab_dept as $liste ){
                                    $tabliste[] = $this->getDepartement($liste); 
                                }

                                $url .= implode('_',$tabliste);
                                unset($tabliste);
                            }
                            else{
                                //Un  seul departement
                                
                                $url .= ','.$this->getDepartement( $departement );
                            }
                            
                            
                            
                            return 'carte-cadeau-'.$url.'.html';
                            
                        break;
                        case 'localite' :
                            $lasturl = substr( $tab[4],0,strpos($tab[4],'.html') );
                             
                            return 'carte-cadeau-'.$lasturl.'__.html';
                        break;
                    }
                                        
                
                
            }
            
        }
        
        return $url;
        
    }
    
    function decodeUrl( $url ){
        
        /*
         * Thematique seule - carte-cadeau-ateliers-formations > liste-cadeau-culturel/thematique/49/recherche-reseau/1
         * Departement seul - carte-cadeau-lozere > liste-cadeau-culturel/departement/18/recherche-reseau/1
         * Thematiques combinees - carte-cadeau-ateliers-formations_cinemas_cabaret-diners-spectacles > liste-cadeau-culturel/thematique/24-25-35/recherche-reseau/1
         * Departements combines - carte-cadeau-charente_indre_meurthe-et-moselle > liste-cadeau-culturel/departement/41-63-88/recherche-reseau/1
         * Departement et thematique combines - carte-cadeau-cinemas_cirques_concerts,eure_indre_lozere > liste-cadeau-culturel/thematique/49-51/departement/13-38/recherche-reseau/1
         * Localite - carte-cadeau-lyon__.html > liste-cadeau-culturel/recherche-reseau/1/localite/lyon
         * Localite + Thematique - carte-cadeau-lyon__.html > liste-cadeau-culturel/thematique/24/recherche-reseau/1/localite/paris
         */
        
        $tab = $tab2 = $tabliste = $tab_dept = $tab_thematique = array();
        $thematique = 0;
        $departement = 0;
        
       
        
        //Exception
        if( in_array($url, array(
			'carte-cadeau-original.html',
			'carte-cadeau-en-ligne.html'
			,'carte-cadeau-en-ligne/carte-cadeau-noel.html'
			,'carte-cadeau-en-ligne/carte-cadeau-anniversaire.html'
			,'carte-cadeau-en-ligne/carte-cadeau-mariage.html'
			,'carte-cadeau-en-ligne/carte-cadeau-naissance.html'
			,'carte-cadeau-en-ligne/achat-carte-cadeau.html'
			,'carte-cadeau-en-ligne/cadeau-ce-comite-entreprise.html'
			,'carte-cadeau-en-ligne/activite-ce-comite-entreprise.html'
			,'carte-cadeau-en-ligne/idee-sortie-entreprise.html'
			,'carte-cadeau-en-ligne/oeuvres-sociales-comite-entreprise-ce.html'
			 
			
			) ) ||
                preg_match('/carte-cadeau-original\.html.*?/',$url )
                || preg_match('/carte-cadeau-en-ligne.*?/',$url )
                ){
            return $url;
        }
        
        //Si url commence par idees-sorties_carte-cadeau-activee_.html
        if( preg_match('/idees-sorties-carte-cadeau-activee\.html.*?/',$url ) ){
            return $url;
        }
        
        $tab = t3lib_div::trimExplode('carte-cadeau-', $url );
        
        
       
        if( count( $tab ) == 2 ){
           
             $aprescartecadeau = $tab[1];
             $aprescartecadeau = str_replace('.html','',$aprescartecadeau);
             
             
             
             //S'il y a une virgule
             $tab2 = t3lib_div::trimExplode(',', $aprescartecadeau );
             
             
             if( count( $tab2 ) > 1 ){
                 
                 //1ere partie Thematique - 2eme partie Departement > cinemas_cirques_concerts,eure_indre_lozere
                 $url = 'liste-cadeau-culturel/thematique/';
                 $thematique = $tab2[0];                 
                 $departement = $tab2[1];
                 
                 //Plusieurs thematique?
                 $tab_thematique = t3lib_div::trimExplode('_', $thematique );
                 //PLusieurs departements ?
                 if( substr($aprescartecadeau,-2) != '__' ){                      
                     $tab_dept = t3lib_div::trimExplode('_', $departement );
                 }
                 
                 
                  
                 
                 if( count( $tab_thematique ) > 1 ){
                     //Plusieurs thematique
                     foreach( $tab_thematique  as $liste ){
                         $tabliste[] = $this->getThematiqueID($liste);
                     }
                     
                     $url .= implode('-',$tabliste);
                     unset($tabliste);
                 }
                 else{
                     //Une seule thematique > Prendre l'ID dans la BDD
                     $url.=$this->getThematiqueID($thematique);                     
                 }
                 
                 if( substr($aprescartecadeau,-2) != '__' ){                      
                     $url .= '/departement/';
                 }
                 
                 
                 if( count( $tab_dept ) > 1 ){
                     //Plusieurs dept
                     foreach( $tab_dept  as $liste ){
                         $tabliste[] = $this->getDepartementId($liste);
                     }
                     
                     $url .= implode('-',$tabliste);
                     unset($tabliste);
                 }
                 else{
                     //Un seul dept > Prendre l'IDle code dans la BDD
                     if( substr($aprescartecadeau,-2) != '__' ){                      
                        $url.=$this->getDepartementId($departement); 
                     }
                     
                                         
                 }
                 
                 $url .= '/recherche-reseau/1';
                 
                 if( substr($aprescartecadeau,-2) == '__' ){                      
                     $url .= '/localite/'.str_replace('__','',$departement);
                 }
                  
                 return $url;
                 
             }
             else{
                 //Departement ou Thematique combines ou non
                 $url = 'liste-cadeau-culturel/';
                 
                 //SI il y a 2 underscores a la fin
                 if( substr( $aprescartecadeau, -2 ) == '__' ){
                     return $url.'recherche-reseau/1/localite/'.str_replace('__','',$aprescartecadeau);
                 }
                 
                 //PLusieurs thematique ou departement existent
                 $tab3 = t3lib_div::trimExplode('_', $aprescartecadeau ) ;
                 if( count( $tab3)>1){
                     $dept = 0;
                     foreach( $tab3 as $liste ){
                         if( $this->isDepartement( $liste ) ){
                             $dept = 1;
                             $tabliste[] = $this->getDepartementId($liste) ;
                         }
                         else{
                             $tabliste[] = $this->getThematiqueId($liste) ;
                         }
                         
                     }
                     
                     if( $dept > 0 ){
                         $url .= 'departement/'.implode('-',$tabliste ) ;
                         
                     }
                     else{
                          $url .= 'thematique/'.implode('-',$tabliste ) ;
                     }
                     unset($tabliste);
                     
                     
                 }
                 else{
                     //Une seule thematique ou un seul dept
                     if( $this->isDepartement( $aprescartecadeau ) ){
                         $url .= 'departement/'.$this->getDepartementId($aprescartecadeau) ;
                     }
                     else{
                         $url .= 'thematique/'.$this->getThematiqueId($aprescartecadeau) ;
                     }
                 }
                 
                 $url .= '/recherche-reseau/1';
                  
                 return $url;
                 
             }
            
        }
        
        return $url;
        
    }
    function isDepartement( $urlrewrite ){
        $count = $GLOBALS['TYPO3_DB']->exec_SELECTcountRows(
                    '*',
                    'tx_tsara_departement'
                    ,'1=1 AND deleted=0 AND hidden=0 AND urlrewrite LIKE '.$GLOBALS['TYPO3_DB']->fullQuoteStr($urlrewrite,'tx_tsara_departement') 
                );
        return ($count>0?true:false);
    }
    function getThematiqueId( $uid ) {
         
        
        $row = array();
        $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow(
                    'id_champrubrique',
                    'tx_tsara_corresthematiquesearch'
                    ,'1=1 AND deleted=0 AND hidden=0 AND urlrewrite LIKE '.$GLOBALS['TYPO3_DB']->fullQuoteStr($uid,'tx_tsara_corresthematiquesearch') 
                );
         
        return $row['id_champrubrique'];
    }
    function getThematique( $uid ) {
        if( !t3lib_div::intval_positive($uid)) return '';
        
        $row = array();
        $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow(
                    'urlrewrite',
                    'tx_tsara_corresthematiquesearch'
                    ,'1=1 AND deleted=0 AND hidden=0 AND id_champrubrique = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($uid,'tx_tsara_corresthematiquesearch') 
                );
         
        return $row['urlrewrite'];
    }
    function getDepartementId( $uid ) {
        $row = array();
        $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow(
                    'substring(code,2,3) AS code2',
                    'tx_tsara_departement'
                    ,'1=1 AND deleted=0 AND hidden=0 AND urlrewrite LIKE '.$GLOBALS['TYPO3_DB']->fullQuoteStr($uid,'tx_tsara_departement') 
                );
        
        return $row['code2'];
    }
    function getDepartement( $uid ) {
        $row = array();
        $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow(
                    'urlrewrite',
                    'tx_tsara_departement'
                    ,'1=1 AND deleted=0 AND hidden=0 AND substring(code,2,3) = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($uid,'tx_tsara_departement') 
                );
        
        return $row['urlrewrite'];
    }
    
}
?>
