<?php
class Tx_Tsara_Lib_Powermailslotvalid extends Tx_Extbase_MVC_Controller_ActionController {

    public function addInformation($params, $obj) {
        
        //Verif field 13 - URL du site
        if( isset( $params[13]) && trim( $params[13] ) != '' ){
            $url = trim( $params[13] );
            $url = t3lib_div::removeXSS( $url );
            $tab_url = parse_url($url);
            $host = $tab_url['host'];
            $cs = t3lib_div::makeInstance('t3lib_cs');
            $host = $cs->conv_case('utf-8',$host,'toLower');
            $host = trim($host);
            $www = $cs->substr('utf-8',$host,0,4);
            $url_without_www = 'http://'.($www=='www.'?$cs->substr('utf-8',$host,4):$host).'/';
            $urls_without_www = 'https://'.($www=='www.'?$cs->substr('utf-8',$host,4):$host).'/';
            $url_with_www = 'http://'.($www=='www.'?$host:'www.'.$host).'/';
            $urls_with_www = 'https://'.($www=='www.'?$host:'www.'.$host).'/';
            
            $nb = $GLOBALS['TYPO3_DB']->exec_SELECTcountRows(
                    'uid',
                    'tt_news',
                    ' ext_url IN('.$GLOBALS['TYPO3_DB']->fullQuoteStr($url_without_www,'tt_news').','.$GLOBALS['TYPO3_DB']->fullQuoteStr($url_with_www,'tt_news').','.$GLOBALS['TYPO3_DB']->fullQuoteStr($urls_with_www,'tt_news').','.$GLOBALS['TYPO3_DB']->fullQuoteStr($urls_without_www,'tt_news').')  AND deleted=0 AND pid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($pidsites,'tt_news')
                    );
            if( $nb > 0 ){
                $obj->isValid = false;
                $obj->setError('error13', 13);
                 
            }
            
            
        }
        
    }

}
?>
