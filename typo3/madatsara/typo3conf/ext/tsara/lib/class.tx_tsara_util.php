<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of class
 *
 * @author mrabehasy
 */
require_once (t3lib_extMgm :: extPath('adodb') . 'adodb/adodb.inc.php');
//require_once(PATH_tslib . 'class.tslib_pibase.php'); // get pibase


//class tx_tsara_util extends tslib_pibase {
class tx_tsara_util extends \TYPO3\CMS\Frontend\Plugin\AbstractPlugin {
 	
        public $prefixId      = 'tx_tsara_pi1'; // Same as class name
	public $scriptRelPath = 'lib/class.tx_tsara_util.php';	// Path to this script relative to the extension dir.
	public $extKey        = 'tsara'; // The extension key.
	public $pi_checkCHash = true;
        
        var $fieldform = array(
            'departement'
            ,'date_le'
            ,'date_entrele'
            ,'date_etle'
            ,'date'
            ,'departementmap'
            ,'datetstample'
            ,'datetstampentrele'
            ,'datetstampetle'
            ,'affichemois'
            ,'offreprivilege'
            ,'resultat_etendu_region'
            ,'resultat_etendu_dateproches'
            ,'resultat_etendu_categorierelatives'
        );
        
        /**
	 * Constructeur
	 */
	public function __construct() {
                $this->confArr = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['tsara']);
                $this->cs = t3lib_div::makeInstance('t3lib_cs'); 
	}
        public function clear_cache() {
		$GLOBALS['BE_USER']->user['admin'] = 1;
		$tce = t3lib_div::makeInstance('t3lib_TCEmain');
		$tce->start(Array(), Array());
		$tce->clear_cacheCmd('all');
		return TRUE;
	}
        function get_date_event($row, $pObj){
            $date =  $date_end = 0;

            if( t3lib_utility_Math::convertToPositiveInteger($row['tx_tsara_event_date']) ){
                $date = $row['tx_tsara_event_date'];
            }
             
            if( t3lib_utility_Math::convertToPositiveInteger($row['tx_tsara_event_date_end']) ){
                $date_end = $row['tx_tsara_event_date_end'];
            }
            
             

            if( 
                     $date > 0  && $date_end > 0  && $date!=$date_end
              ){
                $date =  sprintf($pObj->conf['tsara.']['sprintfdate_double'], $pObj->cObj->stdWrap( $date ,$pObj->conf['tsara.']['dateformat_stdWrap.'] ), $pObj->cObj->stdWrap($date_end,$pObj->conf['tsara.']['dateformat_stdWrap.'] ) ) ;
            
                return $date;
            }

            if( 
                      $date == $date_end  

               ){
                 
                 



                $date =  sprintf($pObj->conf['tsara.']['sprintfdate_unique'], $pObj->cObj->stdWrap($date ,$pObj->conf['tsara.']['dateuniqueformat_stdWrap.'] ) ) ;
                return $date;
            }

            
            return false;


        }
        function get_between($inputstr,$delimeterLeft,$delimeterRight) {
            $posLeft  = stripos($inputstr,$delimeterLeft)+strlen($delimeterLeft);
            $posRight = stripos($inputstr,$delimeterRight,$posLeft+1);
            return  substr($inputstr,$posLeft,$posRight-$posLeft);
         }
         function oteraccents($str){
            $charset = 'utf-8';
            $str = $this->cs->conv_case($charset, $str, 'toLower'  );
            return $this->cs->specCharsToASCII($charset, $str );
        }
        function array_delete($array, $remove_null_number = true)
        {
	$new_array = array();

	$null_exceptions = array();

	foreach ($array as $key => $value)
	{
		$value = trim($value);

	        if($remove_null_number)
			{
		        $null_exceptions[] = '0';
			}

	        if(!in_array($value, $null_exceptions) && $value != "")
			{
	            $new_array[] = $value;
	        }
    		}
    		return $new_array;
	}
        function is_similartodept($str){
            $str = trim( $str );
            if(strlen($str)==2 && preg_match('/^(([\d]{2} )|(2[abAB] ))*(([\d]{2})|(2[abAB]))$/', $str ) ) return true;
            return false;
        }
        function is_similartocp($str){
            $str = trim( $str );
            if(strlen($str)==5 && preg_match('/^\d{5}$/', $str ) ) return true;
            return false;
        }
        public function encodeTitle($title) {

		// Fetch character set:
		$charset = $GLOBALS['TYPO3_CONF_VARS']['BE']['forceCharset'] ? $GLOBALS['TYPO3_CONF_VARS']['BE']['forceCharset'] : $GLOBALS['TSFE']->defaultCharSet;

		// Convert to lowercase:
		$processedTitle = $GLOBALS['TSFE']->csConvObj->conv_case($charset, $title, 'toLower');

		// Strip tags
		$processedTitle = strip_tags($processedTitle);

		// Convert some special tokens to the space character
		//$space = isset($this->conf['spaceCharacter']) ? $this->conf['spaceCharacter'] : '_';
		//$processedTitle = preg_replace('/[ \-+_]+/', $space, $processedTitle); // convert spaces

		// Convert extended letters to ascii equivalents
		$processedTitle = $GLOBALS['TSFE']->csConvObj->specCharsToASCII($charset, $processedTitle);

		// Strip the rest
		/*if ($this->extConf['init']['enableAllUnicodeLetters']) {
			// Warning: slow!!!
			$processedTitle = preg_replace('/[^\p{L}0-9' . ($space ? preg_quote($space) : '') . ']/u', '', $processedTitle);
		}
		else {
			$processedTitle = preg_replace('/[^a-zA-Z0-9' . ($space ? preg_quote($space) : '') . ']/', '', $processedTitle);
		}*/
		//$processedTitle = preg_replace('/\\' . $space . '{2,}/', $space, $processedTitle); // Convert multiple 'spaces' to a single one
		$processedTitle = trim($processedTitle);

		

		// Return encoded URL:
		return rawurlencode(strtolower($processedTitle));
	}
         
        function getDepartementId( $uid, $field = 'departement' ) {
            $row = array();
            $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow(
                        $field,
                        'tx_tsara_departement'
                        ,'1=1 AND deleted=0 AND hidden=0 AND urlrewrite LIKE '.$GLOBALS['TYPO3_DB']->fullQuoteStr($uid,'tx_tsara_departement') 
                    );

            return $row[$field];
        }
        
          
        function resize_img($image, $alt='', $maxH, $maxW,  $title='')
        {
            $img['file'] = $image;
            $lConf['file.']['maxH']=$maxH;
            $lConf['file.']['maxW']=$maxW;
            $lConf['altText']=$alt;
            $lConf['titleText']=$title;
            $lConf['emptyTitleHandling']='removeAttr';
            
             

            // Si on veut forcer une taille d'image sans conserver l'homothétie,
            // (par exemple toujours afficher une image carrée quelle que soit l'image d'origine) on utilise un "crop" sur l'image :

            $lConf['file.']['height']=$maxH.'c';
            $lConf['file.']['width']=$maxW.'c';

            return $GLOBALS['TSFE']->cObj->cImage($img["file"], $lConf);        
        }
         
        function addHttp( $str, $addhttp=1 ){
            $str = trim( $str );
            $str = mb_strtolower($str);
            if( mb_substr($str,0,7) != 'http://' && $addhttp==1 ) $str = 'http://'.$str;
            return $str;
            
        }
        function addMailto( $str, $format = 'html' ){
            $str = trim( $str );
            $str = mb_strtolower($str);
            if( mb_substr($str,0,7) != 'mailto:' && $format == 'html' ) $str = $GLOBALS['TSFE']->encryptEmail('mailto:'.$str);
            return $str;
            
        }
       function getnews_byuid( $uid, $retour = 0 ){
           $data = array();
           $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                   'tt_news.*',
                   'tt_news',
                   '1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news').' AND tt_news.uid IN('.$uid.')'
				   ,''
				   ,'FIELD(uid,'.$uid.')'
                   );
           $nb = $GLOBALS['TYPO3_DB']->exec_SELECTcountRows(
                   'tt_news.uid',
                   'tt_news',
                   '1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news').' AND tt_news.uid IN('.$uid.')'
                   );
           if( $retour == 1 ) return $nb;
           if( $nb > 0 ){
               $i = 0;
               foreach( $row as $ligne ){
                   $data[$i]['uid'] = $ligne['uid'];
                   $data[$i]['title'] = $ligne['title'];
                   $data[$i]['tx_tsara_youtubeid'] = $ligne['tx_tsara_youtubeid'];
                   $data[$i]['artiste'] = $this->get_cat($ligne['uid']);
                   $i++;
               }
           }
           return $data;
           
       }
       
       function get_cat($uid){
           $data = array();
           $row = $this->getcategoriesofarticle( $uid, $this->confArr['parent_category_artiste'] );
           if( count( $row ) > 0 ){
               $i = 0;
               foreach( $row as $ligne ){
                   $data[$i]['uid'] = $ligne['uid_foreign'];
                   $data[$i]['name'] = $this->getfieldincat( $ligne['uid_foreign'], 'title' );
                   $i++;
               }
           }
           return $data;
       }
       function getfieldincat( $uid, $field = 'tx_tsara_pagelist' ){
           $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows('tt_news_cat.'.$field,'tt_news_cat','1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news_cat').' AND tt_news_cat.uid IN ('. $uid .')');
           //echo $GLOBALS['TYPO3_DB']->SELECTquery('tt_news_cat.'.$field,'tt_news_cat','1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news_cat').' AND tt_news_cat.uid IN ('. $uid .')');
           //print_r($row);
           if(  t3lib_utility_Math::convertToPositiveInteger($uid) ) return $row[0][$field];
           else return $row;
           
       } 
       function get_fieldintable( $uid, $table, $field  ){
           if( $table == '' ) return '';
           if( !t3lib_utility_Math::convertToPositiveInteger($uid) ) return '';
           
           $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow($table.'.'.$field,$table,'1=1 '.$GLOBALS['TSFE']->cObj->enableFields($table).' AND '.$table.'.uid IN ('. $uid .')');
                                
           if(  $field != '*' ) return $row[$field];
           else return $row;
           
       }
       function getfieldin_ttaddress( $uid, $field = 'name' ){
           $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows('tt_address.'.$field,'tt_address','1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_address').' AND tt_address.uid IN ('. $uid .')');
            
           if(  t3lib_utility_Math::convertToPositiveInteger($uid) ) return $row[0][$field];
           else return $row;
           
       }
       function getauthor( $uid  ){
           $row =  $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                    'fe_users.souhaitecontact, fe_users.name,fe_users.image,fe_users.description',
                    'fe_users',
                    '1=1 '.$GLOBALS['TSFE']->cObj->enableFields('fe_users').' AND fe_users.uid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr( $uid,'fe_users')
                    );
           
           return $row[0];
           
       }
        
       function getcategoriesofarticle($uid, $parent_category = 0, $eid = 0){
           
           if( $eid != 1 ) $enable = $GLOBALS['TSFE']->cObj->enableFields('tt_news_cat').$GLOBALS['TSFE']->cObj->enableFields('tt_news');
           else $enable = 'tt_news_cat.deleted = 0 AND tt_news_cat.hidden = 0 AND tt_news.deleted = 0 AND tt_news.hidden = 0';
               
           $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                        'uid_foreign',
                        'tt_news_cat_mm
                            LEFT JOIN tt_news_cat ON tt_news_cat.uid = tt_news_cat_mm.uid_foreign
                            LEFT JOIN tt_news ON tt_news.uid = tt_news_cat_mm.uid_local
                            ',
                        '1=1  '.$enable.'
                            AND tt_news_cat_mm.uid_local = '.$GLOBALS['TYPO3_DB']->fullQuoteStr( $uid,'tt_news_cat_mm').' 
                                '.(!is_string($parent_category) || $parent_category=='0'?'':' AND tt_news_cat.parent_category  IN('.$parent_category.')') 
                        );
                                
            
           return $row;
       }
       function getothercategoriesofarticle( $uid, $offset = '', $parent = 0, $retour = 0  ){
           $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                        'd.uid AS uid_foreign',
                        'tt_news_cat_mm a
                            LEFT JOIN tt_news_cat_mm b ON b.uid_local = a.uid_local 
                            LEFT JOIN tt_news  c ON c.uid = b.uid_local 
                            LEFT JOIN tt_news_cat  d ON d.uid = b.uid_foreign
                            LEFT JOIN tt_news_cat_mm e ON e.uid_foreign = b.uid_foreign
                        ',
                        '1=1  AND d.deleted = 0 AND c.deleted = 0  
                          AND  a.uid_foreign IN('.$uid.') AND d.parent_category = '.$parent.' AND b.uid_foreign NOT IN('.$uid.')'
                          ,'b.uid_foreign'
                          ,''
                          ,($offset!=''?$offset:'')
                        );
           $rowc = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                        'd.uid AS uid_foreign',
                        'tt_news_cat_mm a
                            LEFT JOIN tt_news_cat_mm b ON b.uid_local = a.uid_local 
                            LEFT JOIN tt_news  c ON c.uid = b.uid_local 
                            LEFT JOIN tt_news_cat  d ON d.uid = b.uid_foreign
                            LEFT JOIN tt_news_cat_mm e ON e.uid_foreign = b.uid_foreign
                        ',
                        '1=1  AND d.deleted = 0 AND c.deleted = 0  
                          AND  a.uid_foreign IN('.$uid.') AND d.parent_category = '.$parent.' AND b.uid_foreign NOT IN('.$uid.')'
                          ,'b.uid_foreign'
                          
                        );
            if( $retour == 1 ) return count($rowc);
           return $row;
       }
       function getlisteauthor( $authorexclude = 0, $pobj ){
           $where = array();
           $limit = '';
           if(  trim($authorexclude) != ''){ 
               
               $where[] = 'fe_users.uid NOT IN('.trim($authorexclude).')';
               
           }
           
           if( t3lib_utility_Math::convertToPositiveInteger( $pobj->flexFormValue('limitotherauthor', 's_template') ) ){
               $limit = $pobj->flexFormValue('limitotherauthor', 's_template');
           }
           $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                        'fe_users.uid,fe_users.image,fe_users.name,COUNT(*) as totalarticles',
                        'fe_users LEFT JOIN tt_news ON tt_news.tx_tsara_author = fe_users.uid ',
                        '1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news').$GLOBALS['TSFE']->cObj->enableFields('fe_users').(count($where)>0?' AND '.implode(' AND ',$where):'')
                        ,'fe_users.uid'                        
                        ,'totalarticles DESC'
                        ,$limit
                        );
           /*echo $GLOBALS['TYPO3_DB']->SELECTquery(
                        'fe_users.uid,fe_users.image,fe_users.name,COUNT(*) as totalarticles',
                        'fe_users LEFT JOIN tt_news ON tt_news.tx_tsara_author = fe_users.uid ',
                        '1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news').$GLOBALS['TSFE']->cObj->enableFields('fe_users').(count($where)>0?' AND '.implode(' AND ',$where):'')
                        ,'fe_users.uid'                        
                        ,'totalarticles DESC'
                        ,$limit
                        );*/
           return $row;
       }
       function getarticleincategory($uid,$tabuidf,$pobj){
           $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                        'tt_news.uid,tt_news.title,tt_news.image,tt_news.datetime,tt_news.author',
                        'tt_news LEFT JOIN tt_news_cat_mm ON tt_news_cat_mm.uid_local = tt_news.uid',
                        '1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news').' AND tt_news.uid != '.$GLOBALS['TYPO3_DB']->fullQuoteStr( $uid,'tt_news').' AND tt_news_cat_mm.uid_foreign IN('.implode(',',$tabuidf).')'
                        ,'tt_news.uid'
                        ,($pobj->flexFormValue('listOrderBy', 's_template')!=''?$pobj->flexFormValue('listOrderBy', 's_template').($pobj->flexFormValue('ascDesc', 's_template')!=''?' '.$pobj->flexFormValue('ascDesc', 's_template'):'ASC'):'')
                        ,(t3lib_utility_Math::convertToPositiveInteger( $pobj->flexFormValue('listLimit', 's_template') )?$pobj->flexFormValue('listLimit', 's_template'):'')
                );
           
           return $row;
       }
       
       function getarticlepluscommente($uid = 0,$pobj,$pid, $swords = '',$author = 0,$categorySelection = ''){
           $where = array();
           $where[] = 'tt_news.pid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr( $pid,'tt_news');
           $from = 'tt_news';
           if(  t3lib_utility_Math::convertToPositiveInteger( $uid )) $where[] = 'tt_news.uid != '.$GLOBALS['TYPO3_DB']->fullQuoteStr( $uid,'tt_news');
           if(  trim($swords) != '') $where[] = '(tt_news.short LIKE '.$GLOBALS['TYPO3_DB']->fullQuoteStr('%'.trim($swords).'%','tt_news').' OR tt_news.bodytext LIKE '.$GLOBALS['TYPO3_DB']->fullQuoteStr('%'.trim($swords).'%','tt_news').' OR tt_news.author LIKE '.$GLOBALS['TYPO3_DB']->fullQuoteStr('%'.trim($swords).'%','tt_news').' OR tt_news.keywords LIKE '.$GLOBALS['TYPO3_DB']->fullQuoteStr('%'.trim($swords).'%','tt_news').' OR tt_news.links LIKE '.$GLOBALS['TYPO3_DB']->fullQuoteStr('%'.trim($swords).'%','tt_news').' OR tt_news.imagecaption LIKE '.$GLOBALS['TYPO3_DB']->fullQuoteStr('%'.trim($swords).'%','tt_news').' OR tt_news.title LIKE '.$GLOBALS['TYPO3_DB']->fullQuoteStr('%'.trim($swords).'%','tt_news').')';               
           if(  t3lib_utility_Math::convertToPositiveInteger( $author )) $where[] = 'tt_news.tx_tsara_author = '.$GLOBALS['TYPO3_DB']->fullQuoteStr( $author,'tt_news');
           if(  trim($categorySelection) != ''){
               $from .= ' LEFT JOIN tt_news_cat_mm ON tt_news_cat_mm.uid_local = tt_news.uid ';
               $where[] = 'tt_news_cat_mm.uid_foreign IN('.trim($categorySelection).')';
           }
               
           $row =  $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                        'tt_news.uid,tt_news.title,tt_news.image,tt_news.datetime,tt_news.author',
                        $from,
                        '1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news').(count($where)>0?' AND '.implode(' AND ',$where):'') 
                        ,''
                        ,($pobj->flexFormValue('view', 's_template')=='pluslus'?'tx_newsreadedcount_readedcounter':'tx_tsarattnews_nbcomments').' DESC'
                        ,(t3lib_utility_Math::convertToPositiveInteger( $pobj->flexFormValue('listLimit', 's_template') )?$pobj->flexFormValue('listLimit', 's_template'):'')
                );
           
           /*echo $GLOBALS['TYPO3_DB']->SELECTquery(
                        'tt_news.uid,tt_news.title,tt_news.image,tt_news.datetime',
                        $from,
                        '1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news').(count($where)>0?' AND '.implode(' AND ',$where):'') 
                        ,''
                        ,($pobj->flexFormValue('view', 's_template')=='pluslus'?'tx_newsreadedcount_readedcounter':'tx_tsarattnews_nbcomments').' DESC'
                        ,(t3lib_utility_Math::convertToPositiveInteger( $pobj->flexFormValue('listLimit', 's_template') )?$pobj->flexFormValue('listLimit', 's_template'):'')
                );*/
           return $row;
       }
	   function get_event( $pobj ){
			$where = array();
			$now = time();
			$where[] = 'tt_news.pid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr( $pobj->flexFormValue('pages', 's_template'),'tt_news');
			$from = 'tt_news';
			switch( $pobj->flexFormValue('view', 's_template') ){
				case 'event_aujourdhui':
					$where[] = "(FROM_UNIXTIME(`tt_news`.`tx_tsara_event_date`,'%Y-%m-%d') = ".$GLOBALS['TYPO3_DB']->fullQuoteStr( date('Y-m-d'),'tt_news')." OR ('".date('Y-m-d')."' BETWEEN FROM_UNIXTIME(`tt_news`.`tx_tsara_event_date`,'%Y-%m-%d') AND FROM_UNIXTIME(`tt_news`.`tx_tsara_event_date_end`,'%Y-%m-%d')))";
				break;
				case 'event_cettesemaine':					
					$bow = strtotime('last Monday', $now); // Gives you the time at the BEGINNING of the week
					$eow = strtotime('next Sunday', $now) + 86400;
					$where[] = "((FROM_UNIXTIME(`tx_tsara_event_date`,'%Y-%m-%d')>='".date('Y-m-d',$bow)."' AND FROM_UNIXTIME(`tx_tsara_event_date_end`,'%Y-%m-%d')<='".date('Y-m-d',$eow)."') OR ('".date('Y-m-d',$bow)."' BETWEEN FROM_UNIXTIME(`tx_tsara_event_date`,'%Y-%m-%d') AND FROM_UNIXTIME(`tx_tsara_event_date_end`,'%Y-%m-%d')) OR ('".date('Y-m-d',$bow)."' BETWEEN FROM_UNIXTIME(`tx_tsara_event_date`,'%Y-%m-%d') AND FROM_UNIXTIME(`tx_tsara_event_date_end`,'%Y-%m-%d')) )";
                break;
				case 'event_cemois':					
					$month = date('n');
					$year  = date('Y');
					$bow = mktime(0,0,0,$month,1,$year);
					$eow = mktime(23,59,00,$month+1,0,$year);					
					$where[] = "((FROM_UNIXTIME(`tx_tsara_event_date`,'%Y-%m-%d')>='".date('Y-m-d',$bow)."' AND FROM_UNIXTIME(`tx_tsara_event_date_end`,'%Y-%m-%d')<='".date('Y-m-d',$eow)."') OR ('".date('Y-m-d',$bow)."' BETWEEN FROM_UNIXTIME(`tx_tsara_event_date`,'%Y-%m-%d') AND FROM_UNIXTIME(`tx_tsara_event_date_end`,'%Y-%m-%d')) OR ('".date('Y-m-d',$eow)."' BETWEEN FROM_UNIXTIME(`tx_tsara_event_date`,'%Y-%m-%d') AND FROM_UNIXTIME(`tx_tsara_event_date_end`,'%Y-%m-%d')) )";
                break;
				case 'event_cetteannee':
					$year  = date('Y');
					$bow = mktime(0,0,0,1,1,$year);
					$eow = mktime(23,59,00,12,31,$year);
					
					$where[] = "((FROM_UNIXTIME(`tx_tsara_event_date`,'%Y-%m-%d')>='".date('Y-m-d',$bow)."' AND FROM_UNIXTIME(`tx_tsara_event_date_end`,'%Y-%m-%d')<='".date('Y-m-d',$eow)."') OR ('".date('Y-m-d',$bow)."' BETWEEN FROM_UNIXTIME(`tx_tsara_event_date`,'%Y-%m-%d') AND FROM_UNIXTIME(`tx_tsara_event_date_end`,'%Y-%m-%d')) OR ('".date('Y-m-d',$eow)."' BETWEEN FROM_UNIXTIME(`tx_tsara_event_date`,'%Y-%m-%d') AND FROM_UNIXTIME(`tx_tsara_event_date_end`,'%Y-%m-%d')) )";
                break;
			}
			$row =  $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                        'tt_news.*',
                        $from,
                        '1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news').(count($where)>0?' AND '.implode(' AND ',$where):'') 
                        ,''
                        ,'tx_tsara_event_date DESC'
                        ,(t3lib_utility_Math::convertToPositiveInteger( $pobj->flexFormValue('listLimit', 's_template') )?$pobj->flexFormValue('listLimit', 's_template'):'')
                );
			/*echo $GLOBALS['TYPO3_DB']->SELECTquery(
                        'tt_news.uid,tt_news.title,tt_news.image,tt_news.datetime,tt_news.author',
                        $from,
                        '1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news').(count($where)>0?' AND '.implode(' AND ',$where):'') 
                        ,''
                        ,'tx_tsara_event_date DESC'
                        ,(t3lib_utility_Math::convertToPositiveInteger( $pobj->flexFormValue('listLimit', 's_template') )?$pobj->flexFormValue('listLimit', 's_template'):'')
                );*/
			return $row;
	   }
           function get_video_commented_recently( $pobj ){
			$where = $tabuid = $row = array();
                        $limit = (t3lib_utility_Math::convertToPositiveInteger( $pobj->flexFormValue('listLimit', 's_template') )?$pobj->flexFormValue('listLimit', 's_template'):'');
                        
                        
                        $select = "REPLACE(tx_comments_comments.external_ref,'tt_news_','') as uidnews";        
                        
                        $from = "`tx_comments_comments`";        
                        
			 //$where[] = 'FROM_UNIXTIME(tx_comments_comments.crdate,\'%Y-%m-%d\') = '.$GLOBALS['TYPO3_DB']->fullQuoteStr( date('Y-m-d'),'tt_news');
			 //$where[] = 'tx_tsara_youtube_published != \'\'';
                        $where[] = 'pid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr( $pobj->flexFormValue('pages', 's_template'),'tt_news');
			
                                
                        
                                
                        $nb = $GLOBALS['TYPO3_DB']->exec_SELECTcountRows(
                            '*',
                            $from,
                            '1=1 '.
                                $GLOBALS['TSFE']->cObj->enableFields('tx_comments_comments').
                                    (count($where )>0?' AND '.implode(' AND ',$where ):'') 
                         );
                        
                                
                        
                        
                        
                        if( $limit > 0 && $nb > $limit ){
                            $limit = $limit;
                        }
                        else $limit = '';
                        
                       $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                                $select,
                                $from,
                                '1=1 '.
                                $GLOBALS['TSFE']->cObj->enableFields('tx_comments_comments').
                                        (count($where)>0?' AND '.implode(' AND ',$where):'') 
                                ,''
                                ,'tx_comments_comments.tx_tsara_youtube_published DESC'
                               ,$limit
                                
                             );
                        
                        if( count($row)>0){
                            foreach($row as $ligne){
                                $tabuid[] = $ligne['uidnews'];
                            }
                        }
                        
                        $tabuid = array_unique($tabuid);
                        
                        $uidnews = implode(',',$tabuid);
                        
                        
                        
                         $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                                'tt_news.*',
                                'tt_news',
                                '1=1 '.
                                $GLOBALS['TSFE']->cObj->enableFields('tt_news').
                                        ( ' AND uid IN('.$uidnews.')') 
                                ,''
                                ,'FIELD(uid,'.$uidnews.')'
                                ,$limit
                                
                             );
                                
                        return $row; 
                                
	   }
           function get_video_commented_today( $pobj ){
			$where = array();
                        $limit = (t3lib_utility_Math::convertToPositiveInteger( $pobj->flexFormValue('listLimit', 's_template') )?$pobj->flexFormValue('listLimit', 's_template'):'');
                        
                        
                        $select = "tt_news.*";        
                        
                        $from = "`tx_comments_comments`
                            LEFT JOIN tt_news ON tt_news.uid = REPLACE(tx_comments_comments.external_ref,'tt_news_','')
                            ";        
                        
			 $where[] = 'FROM_UNIXTIME(tx_comments_comments.crdate,\'%Y-%m-%d\') = '.$GLOBALS['TYPO3_DB']->fullQuoteStr( date('Y-m-d'),'tt_news');
			 //$where[] = 'tx_tsara_youtube_published != \'\'';
                        $where[] = 'tt_news.pid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr( $pobj->flexFormValue('pages', 's_template'),'tt_news');
			
                                
                        
                                
                        $nb = $GLOBALS['TYPO3_DB']->exec_SELECTcountRows(
                            '*',
                            $from,
                            '1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news').
                                $GLOBALS['TSFE']->cObj->enableFields('tx_comments_comments').
                                    (count($where )>0?' AND '.implode(' AND ',$where ):'') 
                         );
                        
                                
                        
                        
                        
                        if( $limit > 0 && $nb > $limit ){
                            $limit = $limit;
                        }
                        else $limit = '';
                        
                        return $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                                $select,
                                $from,
                                '1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news').
                                $GLOBALS['TSFE']->cObj->enableFields('tx_comments_comments').
                                        (count($where)>0?' AND '.implode(' AND ',$where):'') 
                                ,'tt_news.uid'
                                ,'tx_comments_comments.tx_tsara_youtube_published DESC'
                                ,$limit
                             );
                                
                                
                                
                        
                        
                        
                        return false;
                                
	   }
           function get_event_memelieu( $pobj ){
               
                    $where = $trow =  $row = array();
                    
                    $varttnews = t3lib_div::_GP('tx_ttnews');
                    $uid = $varttnews['tt_news'];
                    
                                
                    
                    if( !t3lib_utility_Math::convertToPositiveInteger( $uid ) ) return $row;
                    
                    //$row = $this->getcategoriesofarticle( $uid  );
                    
                    $event_endroit = $this->get_fieldintable($uid, 'tt_news', 'tx_tsara_event_endroit' );
                    
                    if( !t3lib_utility_Math::convertToPositiveInteger( $event_endroit ) ) return $row;    
                    
                    $where[] = 'tt_news.tx_tsara_event_endroit = '.$GLOBALS['TYPO3_DB']->fullQuoteStr( $event_endroit,'tt_news').' AND tt_news.uid != '.$uid;
                    
                    
                    $limit = (t3lib_utility_Math::convertToPositiveInteger( $pobj->flexFormValue('listLimit', 's_template') )?$pobj->flexFormValue('listLimit', 's_template'):'');

                    $where[] = 'tt_news.pid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr( $pobj->flexFormValue('pages', 's_template'),'tt_news');

                    $from = 'tt_news';

                    $where[] = " (FROM_UNIXTIME(`tt_news`.`tx_tsara_event_date`,'%Y-%m-%d') >= ".$GLOBALS['TYPO3_DB']->fullQuoteStr( date('Y-m-d'),'tt_news')." OR ('".date('Y-m-d')."' BETWEEN FROM_UNIXTIME(`tt_news`.`tx_tsara_event_date`,'%Y-%m-%d') AND FROM_UNIXTIME(`tt_news`.`tx_tsara_event_date_end`,'%Y-%m-%d')))";

                    
                    
                    
                    $nb  = $GLOBALS['TYPO3_DB']->exec_SELECTcountRows(
                        '*',
                        $from,
                        '1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news').
                                (count($where )>0?' AND '.implode(' AND ',$where ):'') 
                     );


                                


                    if(  $nb  > 0 ){
                        

                        $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                            'tt_news.*',
                            $from,
                            '1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news').
                                    (count($where)>0?' AND '.implode(' AND ',$where):'') 
                            ,''
                            ,'tx_tsara_event_date ASC'
                            ,$limit
                         );
                        
                        return $row;
                    }
                                





                    return false;
                                
	   }
           function get_event_memethematique( $pobj ){
               
                    $where = $trow =  $row = array();
                    
                    $varttnews = t3lib_div::_GP('tx_ttnews');
                    $uid = $varttnews['tt_news'];
                    
                                
                    
                    if( !t3lib_utility_Math::convertToPositiveInteger( $uid ) ) return $row;
                    
                    $row = $this->getcategoriesofarticle( $uid  );
                    
                   // print_r($row);
                    
                    foreach($row as $ligne ){
                        $tabuid[] = $ligne['uid_foreign'];
                    }
                    
                    
                    
                    
                    
                    $limit = (t3lib_utility_Math::convertToPositiveInteger( $pobj->flexFormValue('listLimit', 's_template') )?$pobj->flexFormValue('listLimit', 's_template'):'');

                    $where[] = 'tt_news.pid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr( $pobj->flexFormValue('pages', 's_template'),'tt_news');
                    $where[] = 'tt_news_cat.parent_category = '.$GLOBALS['TYPO3_DB']->fullQuoteStr( $this->confArr['parent_category_type'],'tt_news_cat');

                    $from = 'tt_news_cat_mm 
                        LEFT JOIN tt_news ON tt_news.uid = tt_news_cat_mm.uid_local
                        LEFT JOIN tt_news_cat  ON tt_news_cat.uid = tt_news_cat_mm.uid_foreign
                        ';

                    $where[] = " (FROM_UNIXTIME(`tt_news`.`tx_tsara_event_date`,'%Y-%m-%d') >= ".$GLOBALS['TYPO3_DB']->fullQuoteStr( date('Y-m-d'),'tt_news')." OR ('".date('Y-m-d')."' BETWEEN FROM_UNIXTIME(`tt_news`.`tx_tsara_event_date`,'%Y-%m-%d') AND FROM_UNIXTIME(`tt_news`.`tx_tsara_event_date_end`,'%Y-%m-%d')))";
                    
                    $where[] = " tt_news_cat_mm.uid_foreign in(".implode(',',array_unique($tabuid)).")";
                    $where[] = " tt_news.uid NOT IN(".$uid.")";

                    
                    
                    
                    $nb  = $GLOBALS['TYPO3_DB']->exec_SELECTcountRows(
                        '*',
                        $from,
                        '1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news').
                                (count($where )>0?' AND '.implode(' AND ',$where ):'') 
                     );


                                


                    if(  $nb  > 0 ){
                        

                        $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                            'tt_news.*',
                            $from,
                            '1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news').
                                    (count($where)>0?' AND '.implode(' AND ',$where):'') 
                            ,'tt_news.uid'
                            ,'tx_tsara_event_date ASC'
                            ,$limit
                         );
                        
                        return $row;
                    }
                                





                    return false;
                                
	   }
           function get_event_memeregion( $pobj ){
               
                    $where = $trow =  $row = array();
                    
                    $varttnews = t3lib_div::_GP('tx_ttnews');
                    $uid = $varttnews['tt_news'];
                    
                                
                    
                    if( !t3lib_utility_Math::convertToPositiveInteger( $uid ) ) return $row;
                    
                    $row = $this->getcategoriesofarticle( $uid  );
                    
                   // print_r($row);
                    
                    foreach($row as $ligne ){
                        $tabuid[] = $ligne['uid_foreign'];
                    }
                    
                    //Get all children of region parent
                    $row = $this->get_child_recursive($this->confArr['parent_category_region']);
                    
                    $row = array_keys( $row ) ;
                    
                    
                    
                    
                    
                    $limit = (t3lib_utility_Math::convertToPositiveInteger( $pobj->flexFormValue('listLimit', 's_template') )?$pobj->flexFormValue('listLimit', 's_template'):'');

                    $where[] = 'tt_news.pid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr( $pobj->flexFormValue('pages', 's_template'),'tt_news');
                    $where[] = 'tt_news_cat.parent_category IN('.implode(',',$row).')';

                    $from = 'tt_news_cat_mm 
                        LEFT JOIN tt_news ON tt_news.uid = tt_news_cat_mm.uid_local
                        LEFT JOIN tt_news_cat  ON tt_news_cat.uid = tt_news_cat_mm.uid_foreign
                        ';

                    $where[] = " (FROM_UNIXTIME(`tt_news`.`tx_tsara_event_date`,'%Y-%m-%d') >= ".$GLOBALS['TYPO3_DB']->fullQuoteStr( date('Y-m-d'),'tt_news')." OR ('".date('Y-m-d')."' BETWEEN FROM_UNIXTIME(`tt_news`.`tx_tsara_event_date`,'%Y-%m-%d') AND FROM_UNIXTIME(`tt_news`.`tx_tsara_event_date_end`,'%Y-%m-%d')))";
                    
                    $where[] = " tt_news_cat_mm.uid_foreign in(".implode(',',array_unique($tabuid)).")";
                    $where[] = " tt_news.uid NOT IN(".$uid.")";

                    
                    
                    
                    $nb  = $GLOBALS['TYPO3_DB']->exec_SELECTcountRows(
                        '*',
                        $from,
                        '1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news').
                                (count($where )>0?' AND '.implode(' AND ',$where ):'') 
                     );


                                


                    if(  $nb  > 0 ){
                        

                        $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                            'tt_news.*',
                            $from,
                            '1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news').
                                    (count($where)>0?' AND '.implode(' AND ',$where):'') 
                            ,'tt_news.uid'
                            ,'tx_tsara_event_date ASC'
                            ,$limit
                         );
                        
                        return $row;
                    }
                                





                    return false;
                                
	   }
           function get_child_recursive($value_id, $fielduid = 'uid', $table = 'tt_news_cat', $fieldparentname = 'parent_category') {
                $s = "SELECT ".$fielduid." FROM ".$table." WHERE ".$fieldparentname." = ".$value_id;
                $r = $GLOBALS['TYPO3_DB']->sql_query($s);

                $children = array();

                if($GLOBALS['TYPO3_DB']->sql_num_rows($r) > 0) {
                    # It has children, let's get them.
                    while($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($r)) {
                        # Add the child to the list of children, and get its subchildren
                        $children[$row[$fielduid]] = $this->get_child_recursive($row[$fielduid]);
                    }
                }

                return $children;
            }
           function get_event_complete( $pobj ){
			$where = $trow = $where_now = $where_week = $where_month = $where_year = $row_now = $row_week = $row_month = $row_year = array();
                        $limit = (t3lib_utility_Math::convertToPositiveInteger( $pobj->flexFormValue('listLimit', 's_template') )?$pobj->flexFormValue('listLimit', 's_template'):'');
                        
                        
			
			$now = time();
                        $bow = strtotime('last Monday', $now); // Gives you the time at the BEGINNING of the week
			$eow = strtotime('next Sunday', $now) + 86400;
                        
			$where[] = 'tt_news.pid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr( $pobj->flexFormValue('pages', 's_template'),'tt_news');
			
                        $from = 'tt_news';
                        
                        $where_now[] = implode(' AND ',$where).(count($where)>0?' AND ':'')."(FROM_UNIXTIME(`tt_news`.`tx_tsara_event_date`,'%Y-%m-%d') = ".$GLOBALS['TYPO3_DB']->fullQuoteStr( date('Y-m-d'),'tt_news')." OR ('".date('Y-m-d')."' BETWEEN FROM_UNIXTIME(`tt_news`.`tx_tsara_event_date`,'%Y-%m-%d') AND FROM_UNIXTIME(`tt_news`.`tx_tsara_event_date_end`,'%Y-%m-%d')))";
                        
                        $where_week[] = implode(' AND ',$where).(count($where)>0?' AND ':'')."((FROM_UNIXTIME(`tx_tsara_event_date`,'%Y-%m-%d')>='".date('Y-m-d',$bow)."' AND FROM_UNIXTIME(`tx_tsara_event_date_end`,'%Y-%m-%d')<='".date('Y-m-d',$eow)."') OR ('".date('Y-m-d',$bow)."' BETWEEN FROM_UNIXTIME(`tx_tsara_event_date`,'%Y-%m-%d') AND FROM_UNIXTIME(`tx_tsara_event_date_end`,'%Y-%m-%d')) OR ('".date('Y-m-d',$bow)."' BETWEEN FROM_UNIXTIME(`tx_tsara_event_date`,'%Y-%m-%d') AND FROM_UNIXTIME(`tx_tsara_event_date_end`,'%Y-%m-%d')) ) AND (FROM_UNIXTIME(`tt_news`.`tx_tsara_event_date`,'%Y-%m-%d') >= ".$GLOBALS['TYPO3_DB']->fullQuoteStr( date('Y-m-d'),'tt_news')." OR ('".date('Y-m-d')."' BETWEEN FROM_UNIXTIME(`tt_news`.`tx_tsara_event_date`,'%Y-%m-%d') AND FROM_UNIXTIME(`tt_news`.`tx_tsara_event_date_end`,'%Y-%m-%d')))";
                        
                        $month = date('n');
			$year  = date('Y');
			$bow = mktime(0,0,0,$month,1,$year);
			$eow = mktime(23,59,00,$month+1,0,$year);					
			$where_month[] = implode(' AND ',$where).(count($where)>0?' AND ':'')."((FROM_UNIXTIME(`tx_tsara_event_date`,'%Y-%m-%d')>='".date('Y-m-d',$bow)."' AND FROM_UNIXTIME(`tx_tsara_event_date_end`,'%Y-%m-%d')<='".date('Y-m-d',$eow)."') OR ('".date('Y-m-d',$bow)."' BETWEEN FROM_UNIXTIME(`tx_tsara_event_date`,'%Y-%m-%d') AND FROM_UNIXTIME(`tx_tsara_event_date_end`,'%Y-%m-%d')) OR ('".date('Y-m-d',$eow)."' BETWEEN FROM_UNIXTIME(`tx_tsara_event_date`,'%Y-%m-%d') AND FROM_UNIXTIME(`tx_tsara_event_date_end`,'%Y-%m-%d')) ) AND (FROM_UNIXTIME(`tt_news`.`tx_tsara_event_date`,'%Y-%m-%d') >= ".$GLOBALS['TYPO3_DB']->fullQuoteStr( date('Y-m-d'),'tt_news')." OR ('".date('Y-m-d')."' BETWEEN FROM_UNIXTIME(`tt_news`.`tx_tsara_event_date`,'%Y-%m-%d') AND FROM_UNIXTIME(`tt_news`.`tx_tsara_event_date_end`,'%Y-%m-%d')))";

                        $year  = date('Y');
                        $bow = mktime(0,0,0,1,1,$year);
                        $eow = mktime(23,59,00,12,31,$year);

                        $where_year[] = implode(' AND ',$where).(count($where)>0?' AND ':'')."((FROM_UNIXTIME(`tx_tsara_event_date`,'%Y-%m-%d')>='".date('Y-m-d',$bow)."' AND FROM_UNIXTIME(`tx_tsara_event_date_end`,'%Y-%m-%d')<='".date('Y-m-d',$eow)."') OR ('".date('Y-m-d',$bow)."' BETWEEN FROM_UNIXTIME(`tx_tsara_event_date`,'%Y-%m-%d') AND FROM_UNIXTIME(`tx_tsara_event_date_end`,'%Y-%m-%d')) OR ('".date('Y-m-d',$eow)."' BETWEEN FROM_UNIXTIME(`tx_tsara_event_date`,'%Y-%m-%d') AND FROM_UNIXTIME(`tx_tsara_event_date_end`,'%Y-%m-%d')) ) AND (FROM_UNIXTIME(`tt_news`.`tx_tsara_event_date`,'%Y-%m-%d') >= ".$GLOBALS['TYPO3_DB']->fullQuoteStr( date('Y-m-d'),'tt_news')." OR ('".date('Y-m-d')."' BETWEEN FROM_UNIXTIME(`tt_news`.`tx_tsara_event_date`,'%Y-%m-%d') AND FROM_UNIXTIME(`tt_news`.`tx_tsara_event_date_end`,'%Y-%m-%d')))";
                        $where_all[] = implode(' AND ',$where).(count($where)>0?' AND ':'')."(FROM_UNIXTIME(`tt_news`.`tx_tsara_event_date`,'%Y-%m-%d') >= ".$GLOBALS['TYPO3_DB']->fullQuoteStr( date('Y-m-d'),'tt_news')." OR ('".date('Y-m-d')."' BETWEEN FROM_UNIXTIME(`tt_news`.`tx_tsara_event_date`,'%Y-%m-%d') AND FROM_UNIXTIME(`tt_news`.`tx_tsara_event_date_end`,'%Y-%m-%d')))";
                        
                        $nb_now = $GLOBALS['TYPO3_DB']->exec_SELECTcountRows(
                            '*',
                            $from,
                            '1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news').
                                    (count($where_now)>0?' AND '.implode(' AND ',$where_now):'') 
                         );
                        
                        $nb_week = $GLOBALS['TYPO3_DB']->exec_SELECTcountRows(
                            '*',
                            $from,
                            '1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news').
                                    (count($where_week)>0?' AND '.implode(' AND ',$where_week):'') 
                         );
                        
                        $nb_month = $GLOBALS['TYPO3_DB']->exec_SELECTcountRows(
                            '*',
                            $from,
                            '1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news').
                                    (count($where_month)>0?' AND '.implode(' AND ',$where_month):'') 
                         );
                        
                        $nb_year = $GLOBALS['TYPO3_DB']->exec_SELECTcountRows(
                            '*',
                            $from,
                            '1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news').
                                    (count($where_year)>0?' AND '.implode(' AND ',$where_year):'') 
                         );
                        
                        
                        
                        if( $limit > 0 && $nb_now > $limit ){
                                
                            return $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                                'tt_news.*',
                                $from,
                                '1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news').
                                        (count($where_now)>0?' AND '.implode(' AND ',$where_now):'') 
                                ,''
                                ,'tx_tsara_event_date DESC'
                                ,$limit
                             );
                        }
                        if($nb_now>0){
                            $row_now = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                                'tt_news.uid',
                                $from,
                                '1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news').
                                        (count($where_now)>0?' AND '.implode(' AND ',$where_now):'') 
                                ,''
                                ,'tx_tsara_event_date ASC'
                             );
                        }
                        if($nb_week>0){
                            $row_week = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                                'tt_news.uid',
                                $from,
                                '1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news').
                                        (count($where_week)>0?' AND '.implode(' AND ',$where_week):'') 
                                ,''
                                ,'tx_tsara_event_date ASC'
                             );
                        }
                        if($nb_month>0){
                            $row_month = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                                'tt_news.uid',
                                $from,
                                '1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news').
                                        (count($where_month)>0?' AND '.implode(' AND ',$where_month):'') 
                                ,''
                                ,'tx_tsara_event_date ASC'
                             );
                        }
                        if($nb_year>0){
                            $row_year = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                                'tt_news.uid',
                                $from,
                                '1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news').
                                        (count($where_year)>0?' AND '.implode(' AND ',$where_year):'') 
                                ,''
                                ,'tx_tsara_event_date ASC'
                             );
                        }
                        
                        if( count($row_month)>0 || count($row_year)>0 || count($row_week)>0 || count($row_now)>0){
                            //if( t3lib_div::getIndpEnv('REMOTE_ADDR') == '82.230.218.34' ){
                              
                                $row = array_merge_recursive($row_now,$row_week,$row_month,$row_year ) ;
                                foreach($row as $ligne ){
                                    $trow[] = $ligne['uid'];
                                }
                                $uid = implode(',',array_slice(array_unique($trow),0,$limit)) ;
                                return $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                                    'tt_news.*',
                                    $from,
                                    '1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news').
                                            'AND tt_news.uid IN('.$uid.')' 
                                    ,''
                                    ,'tx_tsara_event_date ASC'
                                );
                           // }
                        }
                         else{
                                
                            return $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                                'tt_news.*',
                                $from,
                                '1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news').
                                        (count($where_all)>0?' AND '.implode(' AND ',$where_all):'') 
                                ,''
                                ,'tx_tsara_event_date DESC'
                                ,$limit
                             );
                        } 
                        
                        
                        
                        return false;
                                
	   }
                                
       function getcomptesbyauthor($uid_author){
           $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                    'title,typecompte,url,showinfront',
                    'tx_tsara_compteauthor', 
                    '1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tx_tsara_compteauthor').' AND tx_tsara_compteauthor.showinfront=1 AND tx_tsara_compteauthor.comptes = '.$GLOBALS['TYPO3_DB']->fullQuoteStr( $uid_author,'tx_tsara_compteauthor')
                    );
           return $row;
       }
       function getauthorbyarticle( $article_uid  ){
           $row =  $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                    'tt_news.tx_tsara_author,fe_users.souhaitecontact, fe_users.name,fe_users.image,fe_users.description',
                    'tt_news LEFT JOIN fe_users ON fe_users.uid = tt_news.tx_tsara_author',
                    '1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news').' AND tt_news.uid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr( $article_uid,'tt_news')
                    );
           if( !t3lib_utility_Math::convertToPositiveInteger( $row[0]['tx_tsara_author'] ) ) return array();
           return $row[0];
           
       }
       function getcount_newsbycat( $uidcat ){
              
           
           return $GLOBALS['TYPO3_DB']->exec_SELECTcountRows(
                   'uid_local',
                   'tt_news_cat_mm
                       LEFT JOIN tt_news ON tt_news.uid = tt_news_cat_mm.uid_local ',
                   '1=1 AND tt_news.deleted = 0 AND tt_news.hidden = 0 AND uid_foreign = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($uidcat,'tt_news_cat') 
                    
                   ); 
           
       }
       function get_ttaddress_name( $uid, $table = 'tx_tsara_cine_auteurs_mm'   ) {
           $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                        'uid_foreign',
                        $table.' AS a
                            LEFT JOIN tt_news ON tt_news.uid = a.uid_local
                            LEFT JOIN tt_address ON tt_address.uid = a.uid_foreign
                            ',
                        '1=1  '.$GLOBALS['TSFE']->cObj->enableFields('tt_news').$GLOBALS['TSFE']->cObj->enableFields('tt_address').'
                            AND a.uid_local = '.$GLOBALS['TYPO3_DB']->fullQuoteStr( $uid,$table)  
                        );
           if( count( $row ) > 0 ){
               $i = 0;
               foreach( $row as $ligne ){
                   $data[$i]['uid'] = $ligne['uid_foreign'];
                   $data[$i]['name'] = $this->getfieldin_ttaddress( $ligne['uid_foreign'], 'name' );
                   $i++;
               }
           } 
           
            
           return $data;
            
       }
       function getarticlebyauthor( $authoruid, $limit = 3, $order = ' tt_news.datetime DESC' ){
           $uid = 0;
           $varttnews = t3lib_div::_GP('tx_ttnews');
           $where = array();
        
           if( is_array( $varttnews ) && t3lib_utility_Math::convertToPositiveInteger($varttnews['tt_news'])){ 
               $uid = $varttnews['tt_news'];
               $where[] = 'tt_news.uid NOT IN('.$uid.')';
           }
           
           if(!t3lib_utility_Math::convertToPositiveInteger($authoruid)) return array(); 
           
           $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                   'tt_news.*',
                   'tt_news',
                   '1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news').' AND tt_news.tx_tsara_author = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($authoruid,'tt_news').(count($where)>0?' AND '.implode(' AND ',$where):'')
                   ,''
                   ,$order
                   ,$limit
                   );
           /*echo $GLOBALS['TYPO3_DB']->SELECTquery(
                   'tt_news.*',
                   'tt_news',
                   '1=1 '.$GLOBALS['TSFE']->cObj->enableFields('tt_news').' AND tt_news.tx_tsara_author = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($authoruid,'tt_news').(count($where)>0?' AND '.implode(' AND ',$where):'')
                   ,''
                   ,$order
                   ,$limit
                   );*/
           return $row;
           
       }
       
       function getforum( $pobj ){
           $sql = '';
           $row = array();
           switch( $pobj->flexFormValue('view', 's_template') ){
               case 'listsposts' :
                   $listfields = ' f.forum_name,t.subject,p.id,p.poster,p.message,p.posted ';
                   $sql = "SELECT".$listfields."FROM ".$this->confArr['prefixtable']."posts p 
LEFT JOIN ".$this->confArr['prefixtable']."topics t ON t.id = p.topic_id
LEFT JOIN ".$this->confArr['prefixtable']."forums f ON f.id = t.forum_id
LEFT JOIN ".$this->confArr['prefixtable']."forum_perms perms ON perms.forum_id = f.id
WHERE perms.read_forum > 0 ";

		if( t3lib_utility_Math::convertToPositiveInteger( $pobj->flexFormValue('uidforum', 's_template') )  ){
                       $sql .= " AND f.id = ".t3lib_utility_Math::convertToPositiveInteger( $pobj->flexFormValue('uidforum', 's_template') );
                }
                
                //else $sql .= " GROUP BY f.id";
                   
                   if( $pobj->flexFormValue('order', 's_template') != '' && in_array( $pobj->flexFormValue('order', 's_template'), t3lib_div::trimExplode(',',$this->confArr['listsposts_order'] ) ) ){
                       $sql .= " ORDER BY p.".$pobj->flexFormValue('order', 's_template').($pobj->flexFormValue('ascdesc', 's_template')!=''?' '.$pobj->flexFormValue('ascdesc', 's_template'):'');
                   }
                   
                   if( t3lib_utility_Math::convertToPositiveInteger( $pobj->flexFormValue('listLimit', 's_template') ) ){
                       $sql .= " LIMIT ".$pobj->flexFormValue('listLimit', 's_template');
                   }
               break;
               case 'listsforum' :
                   $listfields = ' f.id,f.forum_name,f.forum_desc,f.num_posts,f.num_topics ';
                   $sql = "SELECT".$listfields."FROM ".$this->confArr['prefixtable']."forums f 
LEFT JOIN ".$this->confArr['prefixtable']."forum_perms perms ON perms.forum_id = f.id
WHERE perms.read_forum > 0  ";
		
		$sql .= " GROUP BY f.id";
                   
                   if( $pobj->flexFormValue('order', 's_template') != '' && in_array( $pobj->flexFormValue('order', 's_template'), t3lib_div::trimExplode(',',$this->confArr['listsforums_order'] ) ) ){
                       $sql .= " ORDER BY f.".$pobj->flexFormValue('order', 's_template').($pobj->flexFormValue('ascdesc', 's_template')!=''?' '.$pobj->flexFormValue('ascdesc', 's_template'):'');
                   }
                   
                   
                   
                   //si tri par popularité
                   if( $pobj->flexFormValue('order', 's_template') == 'num_views'  ){
                       $sql = "SELECT ".$listfields.",sum(t.num_views) sommevues  FROM ".$this->confArr['prefixtable']."topics t
LEFT JOIN ".$this->confArr['prefixtable']."forums f ON f.id = t.forum_id
LEFT JOIN ".$this->confArr['prefixtable']."forum_perms perms ON perms.forum_id = f.id
WHERE perms.read_forum > 0  GROUP BY f.id ORDER BY sommevues ".($pobj->flexFormValue('ascdesc', 's_template')!=''?' '.$pobj->flexFormValue('ascdesc', 's_template'):'');
                   }
                   
                   if( t3lib_utility_Math::convertToPositiveInteger( $pobj->flexFormValue('listLimit', 's_template') ) ){
                       $sql .= " LIMIT ".$pobj->flexFormValue('listLimit', 's_template');
                   }
               break;
               case 'liststopics' :
                   $listfields = ' t.num_replies,t.posted, t.id, t.num_views, t.subject, t.poster, f.forum_name, f.id as forum_id ';
                   $sql = "SELECT".$listfields."FROM ".$this->confArr['prefixtable']."topics t
LEFT JOIN ".$this->confArr['prefixtable']."forums f ON f.id = t.forum_id
LEFT JOIN ".$this->confArr['prefixtable']."forum_perms perms ON perms.forum_id = f.id
WHERE perms.read_forum > 0  ";

		if( t3lib_utility_Math::convertToPositiveInteger( $pobj->flexFormValue('uidforum', 's_template') )  ){
                       $sql .= " AND f.id = ".t3lib_utility_Math::convertToPositiveInteger( $pobj->flexFormValue('uidforum', 's_template') );
                }

		//else $sql .= " GROUP BY f.id";
                   
                   if( $pobj->flexFormValue('order', 's_template') != ''  && in_array( $pobj->flexFormValue('order', 's_template'), t3lib_div::trimExplode(',',$this->confArr['liststopics_order'] ) ) ){
                       $sql .= " ORDER BY t.".$pobj->flexFormValue('order', 's_template').($pobj->flexFormValue('ascdesc', 's_template')!=''?' '.$pobj->flexFormValue('ascdesc', 's_template'):'');
                   }
                   
                   if( t3lib_utility_Math::convertToPositiveInteger( $pobj->flexFormValue('listLimit', 's_template') ) ){
                       $sql .= " LIMIT ".$pobj->flexFormValue('listLimit', 's_template');
                   }
               break;
           }
           
           if( $sql != '' ){
           
           // echo $sql;
                
               $res = $GLOBALS['TYPO3_DB']->sql_query( $sql );
               while( $ligne = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res) ){
                   $row[] = $ligne;
               }
               
               //print_r( $row );
           }
           
           return $row;
       }
       
       public function setSession($varname,$varcontent) {
            $GLOBALS['TSFE']->fe_user->setKey('ses',$varname,$varcontent);
            $GLOBALS['TSFE']->storeSessionData(); // validate the session
        }

        function get_random_photos( $pObj ){
            
            $row = $rowcat = $rowune = array();
            //$pObj contains $this objet of class 
            $pid = $pObj->pidList;
            $limit = $pObj->limit;
            if( TYPO3\CMS\Core\Utility\GeneralUtility::getIndpEnv('REMOTE_ADDR') == '82.230.218.34' ){
                echo __LINE__.' - '.__CLASS__;exit;
            }
            
            //if pid <=0 return empty
            if( !t3lib_utility_Math::convertToPositiveInteger($pid) ) return '';
            
            // 
            $nb  = $GLOBALS['TYPO3_DB']->exec_SELECTcountRows(
                    'uid',
                    'tx_tsara_videosfilm',
                    '1=1 AND tx_tsara_videosfilm.deleted = 0 AND tx_tsara_videosfilm.hidden=0 AND pid='.$GLOBALS['TYPO3_DB']->fullQuoteStr( $pid,'tx_tsara_videosfilm') 
                    );
            
            
            if( $nb > 0 ){
               
                //Récupérer la une qui est la carte dans ces categories
                $row  = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                        't.title as titlealbum,p.title,p.url,p.tx_tsara_cine_videosfilm AS uidnews',
                        'tx_tsara_videosfilm p
                            LEFT JOIN tt_news t ON t.uid = p.tx_tsara_cine_videosfilm
                            ',
                        '1=1 AND p.hidden = 0 AND p.deleted = 0 AND p.pid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr( $pid,'tx_tsara_videosfilm')  
                        ,'p.tx_tsara_cine_videosfilm'
                        ,'RAND()'
                        ,$limit
                    );
                
                 
                
                 
                 
                //Grouper les champs dans un tableau
                $i  = 0;
                 
                foreach( $row  as $ligne ){
               
                    $data[$i]['title'] = $ligne['title'];
                    $data[$i]['titlealbum'] = $ligne['titlealbum'];
                    $data[$i]['url'] = str_replace('http://','',str_replace('s288','s'.$pObj->conf['width'],$ligne['url']));
                    $data[$i]['uidnews'] = $ligne['uidnews'];
                    $data[$i]['linkpage'] = $GLOBALS['TSFE']->cObj->typoLink('', array(
                                                                'parameter' => $pObj->page_listephotos,
                                                                'additionalParams' => '&tx_ttnews[tt_news]='.$ligne['uidnews'],
                                                                'useCacheHash' => 1,
                                                                'returnLast' => 'url',
                        )                                                   );
                     
                    
                    $i++;
                }
                 
                return $data;
               
            }
            
            return false;
            
        }

        public function getSession ($varname="") {
            if($varname!="") {
                    return $GLOBALS['TSFE']->fe_user->getKey('ses',$varname);
            } else {
                    return $GLOBALS['TSFE']->fe_user->sesData;
            }
        }
        
       
         
        
        
}
?>