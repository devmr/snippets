<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2012 Miary <rabehasy@gmail.com>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * [CLASS/FUNCTION INDEX of SCRIPT]
 *
 * Hint: use extdeveval to insert/update function index above.
 */
require_once(t3lib_extMgm::extPath("tsara").'lib/class.tx_tsara_util.php');



/**
 * Plugin 'La une' for the 'tsara' extension.
 *
 * @author	Miary <rabehasy@gmail.com>
 * @package	TYPO3
 * @subpackage	tx_tsara
 */
class tx_tsara_pi5 extends  tslib_pibase {
        var $prefixId      = 'tx_tsara_pi5';        // Same as class name
        var $scriptRelPath = 'pi5/class.tx_tsara_pi5.php';    // Path to this script relative to the extension dir.
        var $extKey        = 'tsara';    // The extension key.
        var $pi_checkCHash = true;
        var $totalresult = 0;
         
	
    /**
     * The main method of the PlugIn
     *
     * @param    string        $content: The PlugIn content
     * @param    array        $conf: The PlugIn configuration
     * @return    The content that is displayed on the website
     */
    function main($content, $conf) {
        $this->confArr = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['tsara']); 
        $this->conf = $conf;
        $this->pi_setPiVarDefaults();
        $this->util = t3lib_div::makeInstance('tx_tsara_util');
        // Init FlexForm configuration for plugin:
        $this->pi_initPIflexForm();
        $this->preInit();
        
        $this->pi_loadLL();
        
        $tabuid = array();
        $artistegroupe = '';
        
        $templateFile = $this->flexFormValue('templateFile', 's_template') ? $this->flexFormValue('templateFile', 's_template') : 'EXT:tsara/pi3/template.html';
        $this->template = $this->cObj->fileResource($templateFile);
        //var ttnews
        $tt_news = t3lib_div::_GP('tx_ttnews');
        $uid_news = $tt_news['tt_news'];
        if( !t3lib_utility_Math::convertToPositiveInteger($uid_news) && $this->piVars['artistegroupe'] != '' ) {
            $tabuid = t3lib_div::trimExplode(',',t3lib_div::removeXSS($this->piVars['artistegroupe']));
        }
        
        $view = $this->flexFormValue('view', 's_template');
        $order = $this->flexFormValue('order', 's_template');
        $ascdesc = $this->flexFormValue('ascdesc', 's_template');
		
        $limit = (t3lib_utility_Math::convertToPositiveInteger( $this->flexFormValue('limit', 's_template') ) ? $this->flexFormValue('limit', 's_template') : (t3lib_utility_Math::convertToPositiveInteger($this->conf['pageBrowser.']['limit'])?$this->conf['pageBrowser.']['limit']:15));
        
        switch( $view ){
            case 'artiste_maxv' :
            case 'genre_maxv' :
                $sql = 'SELECT a.uid AS uid_foreign,a.title,a.image, (SELECT COUNT(*) FROM tt_news_cat_mm b WHERE b.uid_foreign=a.uid) AS nb 
FROM tt_news_cat AS a
WHERE (SELECT COUNT(*) FROM tt_news_cat_mm b WHERE b.uid_foreign=a.uid)>0 AND a.parent_category = '.$this->flexFormValue('parent_category', 's_template').' AND a.hidden = 0 AND a.deleted = 0';
				$sql .= ' ORDER BY ';
				if( $order!='' && $ascdesc!='' ){
					$sql .= $order.' '.$ascdesc.',';
				}
				$sql .= 'a.title ASC, a.image DESC ';

$sqlc = 'SELECT COUNT(a.uid) FROM tt_news_cat AS a
WHERE a.parent_category = '.$this->flexFormValue('parent_category', 's_template').' AND a.hidden = 0 AND a.deleted = 0
';

				$offset = 0;
				if( t3lib_div::removeXSS( $this->piVars['pointer'] )>0 ){
					$offset = t3lib_div::removeXSS( $this->piVars['pointer'] )*$limit;
				}
                $sqlimit = (t3lib_utility_Math::convertToPositiveInteger($this->flexFormValue('limit', 's_template'))?' LIMIT '.$offset.','.$this->flexFormValue('limit', 's_template'):'');
                $res = $GLOBALS['TYPO3_DB']->sql_query( $sql.$sqlimit  );
                while ($row[] = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
                        ;
                }
                array_pop($row);
                $GLOBALS['TYPO3_DB']->sql_free_result($res);
                
                $res = $GLOBALS['TYPO3_DB']->sql_query( $sqlc );
                list($count) = $GLOBALS['TYPO3_DB']->sql_fetch_row($res);
				$count = intval($count);
                $GLOBALS['TYPO3_DB']->sql_free_result($res);
                
                $this->totalresult = $count;
				// echo $count;
                
                 
            break;
            case 'selection' :
            case 'playliste' :    
                 $uidliste = $this->util->getSession('uidliste');
                 $row = $this->util->getnews_byuid( $uidliste );
                 $this->totalresult = $this->util->getnews_byuid( $uidliste, 1 );
                 
            break;
            case 'otherartistes' :
				$offset = 0;
            
				if( t3lib_div::removeXSS( $this->piVars['pointer'] )>0 ){
					$offset = t3lib_div::removeXSS( $this->piVars['pointer'] )*$limit;
				}
                if( count($tabuid)<=0 ) $row = $this->util->getcategoriesofarticle( $uid_news, $this->flexFormValue('parent_category', 's_template') );
				
                 
                if( count($row) > 0 && count($tabuid)<=0){
                    
                    foreach( $row as $ligne ){
                        $tabuid[] = $ligne['uid_foreign'];
                    }
                    
                    $artistegroupe = implode(',',$tabuid);
                    
                    $row = $this->util->getothercategoriesofarticle( implode(',',$tabuid), $offset.','.$limit, $this->flexFormValue('parent_category', 's_template')  );
                    
                    $this->totalresult = $this->util->getothercategoriesofarticle( implode(',',$tabuid), '', $this->flexFormValue('parent_category', 's_template'), 1  );
					
                    
                }
                elseif(count($tabuid)>0){ 
                    $artistegroupe = implode(',',$tabuid);
                    $row = $this->util->getothercategoriesofarticle( implode(',',$tabuid), $offset.','.$limit, $this->flexFormValue('parent_category', 's_template')  );
                    $this->totalresult = $this->util->getothercategoriesofarticle( implode(',',$tabuid), '', $this->flexFormValue('parent_category', 's_template'), 1  );
					//echo __LINE__.' - '.$this->totalresult.',,';
                    
                }
            break;
            default :
                   if(count($tabuid)>0){
                       $i = 0;
                       foreach( $tabuid as $uid ){
                           $row[$i] = array('uid_foreign' => $uid );
                           $i++;
                       }
                   }
                   else $row = $this->util->getcategoriesofarticle( $uid_news, $this->flexFormValue('parent_category', 's_template') );
            break;
        }
        
       
        
        
        $w = (t3lib_utility_Math::convertToPositiveInteger($this->flexFormValue('catImageMaxWidth', 's_template'))?$this->flexFormValue('catImageMaxWidth', 's_template'):'120');
        $h = (t3lib_utility_Math::convertToPositiveInteger($this->flexFormValue('catImageMaxHeight', 's_template'))?$this->flexFormValue('catImageMaxHeight', 's_template'):'92');
        
        
        if( count( $row ) > 0 ){
            
            $subpart  = $this->cObj->getSubpart($this->template, '###TEMPLATE###');
            
            $i = 0;
            $firstvideoid = '';
            foreach( $row as $ligne ){
                
                if( $i == 0 ){
                    $firstvideoid = (isset($ligne['tx_tsara_youtubeid'])? $ligne['tx_tsara_youtubeid'] :'');
                }
                
                $image = (isset($ligne['image'])?$ligne['image']:$this->util->getfieldincat( $ligne['uid_foreign'], 'image' ));
                
                $data['CONTENT'][$i]['TITLE'] = (isset($ligne['title'])?$ligne['title']:$this->util->getfieldincat( $ligne['uid_foreign'], 'title' ));
                $data['CONTENT2'][$i]['TITLE'] = (isset($ligne['title'])?$ligne['title']:$this->util->getfieldincat( $ligne['uid_foreign'], 'title' ));
                $data['CONTENT'][$i]['NB'] = $this->cObj->stdWrap( (isset($ligne['nb'])?$ligne['nb']:$this->util->getcount_newsbycat($ligne['uid_foreign'])) , $this->conf['nbvideos_stdWrap.'] );;
                $data['CONTENT'][$i]['IMAGE'] = ($image!='' && file_exists(PATH_site.'uploads/pics/'.$image)?'<img src="thb_'.$w.'_'.$h.'_1_/uploads/pics/'.$image.'" alt="" />':'<img src="thb_'.$w.'_'.$h.'_1_/fileadmin/templates/maquettes/images/empty.jpg" alt="" />');
                $data['CONTENT'][$i]['LINK'] = $GLOBALS['TSFE']->cObj->typoLink('', array(
                                                                'parameter' => $this->flexFormValue('backPid', 's_template'),
                                                                'additionalParams' => '&tx_ttnews['.($this->flexFormValue('parent_category', 's_template')==$this->confArr['parent_category_artiste']?'artiste':'genre').'][0]='.($ligne['uid_foreign']) ,
                                                                'useCacheHash' => 1,
                                                                'returnLast' => 'url',
                        )                                                   );
                $data['CONTENT'][$i]['BEGIN_DIV'] = ($i==0 ? '<div>' : '');
                $data['CONTENT'][$i]['END_DIV'] = ($i+1==count( $row ) || ($i+1)%4==0 ? '</div><div>' : '');
                $data['CONTENT'][$i]['TITLEVIDEO'] = (isset($ligne['title'])?$ligne['title']:'');
                $data['CONTENT'][$i]['IMAGEVIDEO'] = (isset($ligne['tx_tsara_youtubeid'])?'<img src="thbx_'.$w.'_'.$h.'_1_i.ytimg.com/vi/'.$ligne['tx_tsara_youtubeid'].'/hqdefault.jpg" alt="" />':'');
                $data['CONTENT'][$i]['NEWSUID'] = (isset($ligne['uid'])?$ligne['uid']:'');
                $data['CONTENT'][$i]['YTID'] = (isset($ligne['tx_tsara_youtubeid'])? $ligne['tx_tsara_youtubeid'] :'');
                $data['CONTENT'][$i]['LINKPID'] = $GLOBALS['TSFE']->cObj->typoLink('', array(
                                                                'parameter' => $GLOBALS['TSFE']->id,
                                                                'useCacheHash' => 1,
                                                                'returnLast' => 'url',
                        )                                                   );
                $data['CONTENT'][$i]['LINKVIDEO'] = $GLOBALS['TSFE']->cObj->typoLink('', array(
                                                                'parameter' => $this->flexFormValue('PIDitemDisplay', 's_template'),
                                                                'additionalParams' => '&tx_ttnews[tt_news]='.$ligne['uid'],
                                                                'useCacheHash' => 1,
                                                                'returnLast' => 'url',
                        )                                                   );
                if( is_array( $ligne['artiste'] ) ){
                    $j = 0;
                    foreach($ligne['artiste'] as $artiste){
                        $data['CONTENT'][$i]['ARTISTE'][$j]['NAME'] = $artiste['name'];
                        $data['CONTENT'][$i]['ARTISTE'][$j]['UID'] = $artiste['uid'];
                        $data['CONTENT'][$i]['ARTISTE'][$j]['SEPARATOR'] = (($j+1)!=count($ligne['artiste']) && count($ligne['artiste'])>1?', ':'');
                        $j++;
                    }
                }
                
                $i++;
            }
            
            

        }
        else{
            $subpart  = $this->cObj->getSubpart($this->template, '###TEMPLATE_EMPTY###');
        }
        $data['TEXTE'] =  $this->pi_RTEcssText( $this->flexFormValue('texte', 's_template') );
        $data['NEWSUID'] =  $uid_news;
		 
        $data['JSON'] =  $this->showjson( $row );
        $data['HIDDENARTISTE'] =  $this->showhidden( $row );
		
        $data['emptymessage'] =  ($this->flexFormValue('texteempty', 's_template')!=''?$this->pi_RTEcssText( $this->flexFormValue('texteempty', 's_template') ):$this->pi_getLL('texteempty'));
        $data['LINK_ALLARTISTES'] =  $GLOBALS['TSFE']->cObj->typoLink('', array(
                                                                'parameter' => $this->flexFormValue('pid_artistegroupe', 's_template'),
                                                                'additionalParams' => '&'.$this->prefixId.'[artistegroupe]='.$artistegroupe,
                                                                'useCacheHash' => 1,
                                                                'returnLast' => 'url',
                        )                                                   );
        $data['LINK_PLAYLISTE'] =  $GLOBALS['TSFE']->cObj->typoLink('', array(
                                                                'parameter' => $this->flexFormValue('pid_playliste', 's_template'),
                                                                
                                                                'useCacheHash' => 1,
                                                                'returnLast' => 'url',
                        )                                                   ).'#?watch='.$firstvideoid;
        $data['NBSELECTION'] = count( $row );
        $data['PAGINATION'] = (is_array($row) && $this->totalresult > $this->conf['pageBrowser.']['limit'] ? $this->afficherPagination( 'pointer' ): '');
        
        $content = $this->cObj->substituteMarkerAndSubpartArrayRecursive($subpart, $data, '###|###' ); 
	
                
	$content = $this->replace( $content  );
        return $this->conf['wrapContentInBaseClass'] ? $this->pi_wrapInBaseClass($content) : $content;
    }
	function showhidden( $row = array() ){
		if( count( $row ) == 0 ) return '';
			$data = array(); 
            foreach( $row as $ligne ){                        
                $data[] = $ligne['uid_foreign'];                 
                 
            }
		 
		
		return implode(',',$data );
		
	}
	function showjson( $row = array() ){
		if( count( $row ) == 0 ) return 'null';
			$data = array();
			$i = 0;
            foreach( $row as $ligne ){                        
                $data[$i]['name'] = (isset($ligne['title'])?$ligne['title']:$this->util->getfieldincat( $ligne['uid_foreign'], 'title' ));                                                    
                $data[$i]['id'] = $ligne['uid_foreign'];                 
                $i++;
            }
		 
		
		return json_encode( $data );
		
	}
    function afficherPagination( $pointerName = 'pointer' )  {
	$pObj = $this; // make a reference to the parent object
	//echo $this->totalresult.',';
    if( $this->totalresult > $this->conf['pageBrowser.']['limit'] ) {


		    $this->internal['res_count'] = $this->totalresult;
		    $this->internal['results_at_a_time'] = ($this->conf['pageBrowser.']['limit'] > 0 ? $this->conf['pageBrowser.']['limit'] : '');
		    $this->internal['maxPages'] = ($this->conf['pageBrowser.']['maxPages'] > 0 ? $this->conf['pageBrowser.']['maxPages'] : '5');

		} else {
		    return  '';
		}    

	// Initializing variables:
	$showResultCount = $pObj->conf['pageBrowser.']['showResultCount'];
	$tableParams = $pObj->conf['pageBrowser.']['tableParams'];
	$pointer = $pObj->piVars['pointer'];
	$count = $pObj->internal['res_count'];
	$results_at_a_time = t3lib_utility_Math::forceIntegerInRange($pObj->internal['results_at_a_time'], 1, 1000);
	$maxPages = t3lib_utility_Math::forceIntegerInRange($pObj->internal['maxPages'], 1, 100);
	$max = t3lib_utility_Math::forceIntegerInRange(ceil($count / $results_at_a_time), 1, $maxPages);
	$pointer = intval($pointer);
	$links = $linkslist = array();
	$pageCount = ceil($count/$results_at_a_time);

	// Make browse-table/links:
	if ($pObj->pi_alwaysPrev >= 0) {
            
                $links[] = '<a   href="'.$pObj->pi_linkTP_keepPIvars_url (array( 'pointer' => 0  ), 1 ).'">«</a>';

		if ($pointer > 0) {
			$links[] = '<a    href="' . $pObj->pi_linkTP_keepPIvars_url( array('pointer' => ($pointer-1?$pointer-1:'')), $pObj->allowCaching) . '">‹</a> ';
		} elseif ($pObj->pi_alwaysPrev) {
			$links[] = '
					' . $pObj->pi_getLL('pi_list_browseresults_prev', '< Previous') . '';
		}
	}

	if($pointer<$maxPages) $debut = 0;
	else $debut = ($pointer+$maxPages);

        $links[] = ' ';

	for($a=0;$a<$pageCount;$a++)    {
		$min = max(0, $pointer+1-ceil($maxPages/2));
		$max = $min+$maxPages;
		if($max>$pageCount)    {
		    $min = $min - ($max-$pageCount);
		}

		if($a >= $min && $a < $max)    {
		    $linkslist[] =  (
				 $a==$pointer ?
					'<span class="current">'.($a+1).'</span> '
					 :
					'<a  href="'.$pObj->pi_linkTP_keepPIvars_url( array(  'pointer'  => ( $a ? $a : '' ) ), $pObj->allowCaching ).'" >'.trim( ( $a + 1) ).'</a>'

				);
		}
	}
        $links[] = implode(' ', $linkslist );
        $links[] = ' ';

	/*

	for($a = 0;$a < $max;$a++) {
		$links[] = ($a==$pointer ? '<span>'.($a+1).'</span>' : $pObj->pi_linkTP_keepPIvars(trim($pObj->pi_getLL('pi_list_browseresults_page', 'Page') . ' ' . ($a + 1)), array('pointer' => ($a?$a:'')), $pObj->allowCaching));
	}

	*/


	if ($pointer < ceil($count / $results_at_a_time)-1) {
		$links[] = '<a  href="'.$pObj->pi_linkTP_keepPIvars_url( array('pointer' => $pointer + 1), $pObj->allowCaching).'">›</a>';
	
                $links[] = '<a   href="'.$pObj->pi_linkTP_keepPIvars_url (array(   'pointer' => (ceil( $count/$results_at_a_time ) - 1)), 1 ).'">»</a>' ;
        }

	$pR1 = $pointer * $results_at_a_time + 1;
	$pR2 = $pointer * $results_at_a_time + $results_at_a_time;


	$sTables = '

		<!--
			List browsing box:
		-->
                
		' .
	($showResultCount ? '
			<div id="paginationliste"><p class="align-center">' .
		($pObj->internal['res_count'] ?
			sprintf(
				str_replace('###SPAN_BEGIN###', '<span' . $pObj->pi_classParam('browsebox-strong') . '>', $pObj->pi_getLL('pi_list_browseresults_displays', 'Displaying results ###SPAN_BEGIN###%s to %s</span> out of ###SPAN_BEGIN###%s</span>')),
				$pObj->internal['res_count'] > 0 ? $pR1 : 0,
				min(array($pObj->internal['res_count'], $pR2)),
				$pObj->internal['res_count']
				) :
			$pObj->pi_getLL('pi_list_browseresults_noResults', 'Sorry, no items were found.')) . '</p><div class="section group " ><div class="col span_1_of_6">Aller à la page</div>':''
		) . '



					<div class="col span_5_of_6"><div class="pagination-wrapper clearfix"><div class="pagination clearfix">' . implode('', $links) . '</div></div></div>



<!--
			End List browsing box:
		-->
		';


	return $sTables;
}
    
    
    function preInit() {
                $flexformTyposcript = $this->flexFormValue('myTS', 's_template');
                if ($flexformTyposcript) {
                        require_once(PATH_t3lib.'class.t3lib_tsparser.php');
                        $tsparser = t3lib_div::makeInstance('t3lib_tsparser');
                        // Copy conf into existing setup
                        $tsparser->setup = $this->conf;
                        // Parse the new Typoscript
                        $tsparser->parse($flexformTyposcript);
                        // Copy the resulting setup back into conf
                        $this->conf = $tsparser->setup;
                }


    }
    function replace( $content ){
            // 1. replace ll markers
            $content = preg_replace_callback ( // Automaticly fill locallangmarkers with fitting value of locallang.xml
                    '$###LL_(.*)###$Uis', // regulare expression
                    array($this, 'DynamicLocalLangMarker'), // open function
                    $content // current content
            );
            $content = preg_replace_callback ( // Automaticly fill locallangmarkers with fitting value of locallang.xml
                    '$###CONF_(.*)###$Uis', // regulare expression
                    array($this, 'DynamicConfMarker'), // open function
                    $content // current content
            );
            $content = preg_replace_callback ( // Automaticly fill locallangmarkers with fitting value of locallang.xml
                    '$###PID_(.*)###$Uis', // regulare expression
                    array($this, 'DynamicPidMarker'), // open function
                    $content // current content
            );
            $content = preg_replace_callback ( // Automaticly fill locallangmarkers with fitting value of locallang.xml
                    '$###DATA_(.*)###$Uis', // regulare expression
                    array($this, 'DynamicDataMarker'), // open function
                    $content // current content
            );

            return $content;
        }
        function DynamicLocalLangMarker($array) {
            if ($this->pi_getLL(strtolower($array[1]))) { // if there is an entry in locallang.xml
                $string = $this->pi_getLL(strtolower($array[1])); // search for a fitting entry in locallang.xml or typoscript
            }
            if (!empty($string)) return $string;
        }
        function DynamicDataMarker($array) {
            if ($this->cObj->data[(strtolower($array[1]))]) { // if there is an entry in locallang.xml
                $string = $this->cObj->data[(strtolower($array[1]))]; // search for a fitting entry in locallang.xml or typoscript
            }
            if (!empty($string)) return $string;
        }
        function DynamicConfMarker($array) {
            if ($this->conf[(strtolower($array[1]))]) { // if there is an entry in locallang.xml
                $string = $this->conf[(strtolower($array[1]))]; // search for a fitting entry in locallang.xml or typoscript
            }
            if (!empty($string)) return $string;
        }
        function DynamicPidMarker($array) {
            if ($this->conf['pid_'.(strtolower($array[1]))]) { // if there is an entry in locallang.xml
                $string = $GLOBALS['TSFE']->cObj->typoLink('', array(
                                                                'parameter' => $this->conf['pid_'.(strtolower($array[1]))],
                                                                'useCacheHash' => 1,
                                                                'returnLast' => 'url',
                                                                )
                                                            );
            }
            if (!empty($string)) return $string;
        }
        /**
	 * Loads a variable from the flexform
	 *
	 * @param	string		name of variable
	 * @param	string		name of sheet
	 * @return	string		value of var
	 */
    function flexFormValue($var, $sheet) {
            return $this->pi_getFFvalue($this->cObj->data['pi_flexform'], $var,$sheet);
    }
 
}



if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/tsara/pi5/class.tx_tsara_pi5.php'])	{
	include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/tsara/pi5/class.tx_tsara_pi5.php']);
}

?>