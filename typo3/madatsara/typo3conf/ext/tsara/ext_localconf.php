<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}


//Hook felogin
//$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['felogin']['forgotPasswordMail'][] = 'EXT:tsara/hook/class.tx_tsara_felogin.php:tx_tsara_felogin->forgotPasswordMail' ;
//$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['felogin']['login_confirmed'][] = 'EXT:tsara/hook/class.tx_tsara_felogin.php:tx_tsara_felogin->login_confirmed' ;


//Hook tcemain
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processDatamapClass']['tsara'] = 'EXT:tsara/lib/class.tx_tsara_tcemain.php:tx_tsara_tcemain';

//Hook realurl
/*$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/realurl/class.tx_realurl_autoconfgen.php']['extensionConfiguration']['tsara'] = 'EXT:tsara/lib/class.tx_tsara_realurl.php:tx_tsara_realurl->addCartesnsConfig';
*/
$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl']['encodeSpURL_postProc']['tsara'] = 'EXT:tsara/lib/class.tx_tsara_realurlspurl.php:tx_tsara_realurlspurl->encodeSpURL_postProc';
$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl']['decodeSpURL_preProc']['tsara'] = 'EXT:tsara/lib/class.tx_tsara_realurlspurl.php:tx_tsara_realurlspurl->decodeSpURL_preProc';
/**/ 

 
//eID
$GLOBALS['TYPO3_CONF_VARS']['FE']['eID_include']['tsara'] = 'EXT:tsara/eid/class.tx_tsara_eID.php';


$TYPO3_CONF_VARS['SC_OPTIONS']['tslib/class.tslib_fe.php']['hook_eofe'][] = t3lib_extMgm::extPath($_EXTKEY).'hook/class.tx_tsara_tslibfe.php:tx_tsara_tslibfe->main';

t3lib_extMgm::addPItoST43($_EXTKEY,'hook/class.tx_newsreadedcount_pi1.php','_pi1','',1);

//$TYPO3_CONF_VARS['EXTCONF']['tt_news']['extraItemMarkerHook'][] = 'EXT:tsara/hook/class.tx_newsreadedcount_pi1.php:tx_newsreadedcount_pi1';
$TYPO3_CONF_VARS['EXTCONF']['tt_news']['extraItemMarkerHook'][] = 'EXT:tsara/hook/class.tx_tsara_ttnews.php:tx_tsara_ttnews';
$TYPO3_CONF_VARS['EXTCONF']['tt_news']['extraGlobalMarkerHook'][] = 'EXT:tsara/hook/class.tx_tsara_ttnews.php:tx_tsara_ttnews';
$TYPO3_CONF_VARS['EXTCONF']['tt_news']['selectConfHook'][] = 'EXT:tsara/hook/class.tx_tsara_ttnews.php:tx_tsara_ttnews';
$TYPO3_CONF_VARS['EXTCONF']['tt_news']['additionalFormSearchFields'][] = 'EXT:tsara/hook/class.tx_tsara_ttnews.php:tx_tsara_ttnews';


t3lib_extMgm::addPItoST43($_EXTKEY, 'pi1/class.tx_tsara_pi1.php', '_pi1', 'list_type', 1);
t3lib_extMgm::addPItoST43($_EXTKEY, 'pi2/class.tx_tsara_pi2.php', '_pi2', 'list_type', 1);
t3lib_extMgm::addPItoST43($_EXTKEY, 'pi3/class.tx_tsara_pi3.php', '_pi3', 'list_type', 1);
t3lib_extMgm::addPItoST43($_EXTKEY, 'pi4/class.tx_tsara_pi4.php', '_pi4', 'list_type', 1);
t3lib_extMgm::addPItoST43($_EXTKEY, 'pi5/class.tx_tsara_pi5.php', '_pi5', 'list_type', 1);
t3lib_extMgm::addPItoST43($_EXTKEY, 'pi6/class.tx_tsara_pi6.php', '_pi6', 'list_type', 1);

	// Register information for the task
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks'] = array(
    'tx_tsara_Test' => array(
	'extension'        => $_EXTKEY,
	'title'            => 'Nom de la tache (tsara scheduler)',
	'description'      => 'Description de la tache (tsara scheduler)'/*,
	'additionalFields' => 'tx_smscheddemo_hidecontent_additionalfieldprovider'*/
    ),
    'tx_tsara_disqus_listpopular' => array(
	'extension'        => $_EXTKEY,
	'title'            => '(tsara)::Disqus maj post populaires::',
	'description'      => '(tsara)::Disqus maj post populaires::'/*,
	'additionalFields' => 'tx_smscheddemo_hidecontent_additionalfieldprovider'*/
    )
    ,
    'tx_tsara_fetchsite_midi' => array(
	'extension'        => $_EXTKEY,
	'title'            => '(tsara)::Fetch site Midi::',
	'description'      => '(tsara)::Fetch site Midi::',
	'additionalFields' => 'tx_tsara_fetchsite_midi_fields'
    )
    ,'tx_tsara_fetchsite_express' => array(
	'extension'        => $_EXTKEY,
	'title'            => '(tsara)::Fetch site Express::',
	'description'      => '(tsara)::Fetch site Express::',
	'additionalFields' => 'tx_tsara_fetchsite_express_fields'
    )
    ,'tx_tsara_fetchsite_gazetiko' => array(
	'extension'        => $_EXTKEY,
	'title'            => '(tsara)::Fetch site Gazetiko::',
	'description'      => '(tsara)::Fetch site Gazetiko::',
	'additionalFields' => 'tx_tsara_fetchsite_gazetiko_fields'
    )
    ,'tx_tsara_fetchsite_inovaovao' => array(
	'extension'        => $_EXTKEY,
	'title'            => '(tsara)::Fetch site inovaovao::',
	'description'      => '(tsara)::Fetch site inovaovao::',
	'additionalFields' => 'tx_tsara_fetchsite_inovaovao_fields'
    )
    ,'tx_tsara_fetchsite_lanation' => array(
	'extension'        => $_EXTKEY,
	'title'            => '(tsara)::Fetch site lanation::',
	'description'      => '(tsara)::Fetch site lanation::',
	'additionalFields' => 'tx_tsara_fetchsite_lanation_fields'
    )
    ,'tx_tsara_fetchsite_madapro' => array(
	'extension'        => $_EXTKEY,
	'title'            => '(tsara)::Fetch site madapro::',
	'description'      => '(tsara)::Fetch site madapro::',
	'additionalFields' => 'tx_tsara_fetchsite_madapro_fields'
    )
    ,'tx_tsara_fetchsite_rss' => array(
	'extension'        => $_EXTKEY,
	'title'            => '(tsara)::Fetch RSS:: tx_tsara_fetchsite_rss',
	'description'      => '(tsara)::Fetch RSS::',
	'additionalFields' => 'tx_tsara_fetchsite_rss_fields'
    )
    ,'tx_tsara_fetchpicasa_album' => array(
	'extension'        => $_EXTKEY,
	'title'            => '(tsara)::Fetch Picasa album:: tx_tsara_fetchpicasa',
	'description'      => '(tsara)::Fetch Picasa album::',
	'additionalFields' => 'tx_tsara_fetchpicasa_fields'
    )
,'tx_tsara_fetchpicasa_photos' => array(
	'extension'        => $_EXTKEY,
	'title'            => '(tsara)::Fetch Picasa Photos:: tx_tsara_fetchpicasa',
	'description'      => '(tsara)::Fetch Picasa photos::' ,
	'additionalFields' => 'tx_tsara_fetchpicasa_fields'
    )
    ,'tx_tsara_fetch_ytpl' => array(
	'extension'        => $_EXTKEY,
	'title'            => '(tsara)::Fetch Youtube Playliste:: tx_tsara_fetch_ytpl',
	'description'      => '(tsara)::Fetch Youtube Playliste::',
	'additionalFields' => 'tx_tsara_fetch_ytpl_fields'
    )
    ,'tx_tsara_fetch_ytvideoinpl' => array(
	'extension'        => $_EXTKEY,
	'title'            => '(tsara)::Fetch Youtube Video dans une Playliste:: tx_tsara_fetch_ytvideoinpl',
	'description'      => '(tsara)::Fetch Youtube Video dans une Playliste::',
	'additionalFields' => 'tx_tsara_fetch_ytvideoinpl_fields'
    )
    ,'tx_tsara_fetch_yt' => array(
	'extension'        => $_EXTKEY,
	'title'            => '(tsara)::Fetch Youtube:: tx_tsara_fetch_yt',
	'description'      => '(tsara)::Fetch Youtube::',
	'additionalFields' => 'tx_tsara_fetch_yt_fields'
    )
    ,'tx_tsara_fetch_ytmaj' => array(
	'extension'        => $_EXTKEY,
	'title'            => '(tsara)::Fetch Youtube MAJ:: tx_tsara_fetch_ytmaj',
	'description'      => '(tsara)::Fetch Youtube MAJ videos::',
	'additionalFields' => 'tx_tsara_fetch_ytmaj_fields'
    )
    ,'tx_tsara_blugawebsite' => array(
	'extension'        => $_EXTKEY,
	'title'            => '(tsara)::Maj capture website (blugawebsite)::  ',
	'description'      => '(tsara)::Maj capture website (blugawebsite)::',
	//'additionalFields' => 'tx_tsara_blugawebsite_fields'
    )
    ,
    'tx_tsara_fetchsite_bcrm' => array(
	'extension'        => $_EXTKEY,
	'title'            => '(tsara)::Fetch site BCRM::',
	'description'      => '(tsara)::Fetch site BCRM::',
	'additionalFields' => 'tx_tsara_fetchsite_bcrm_fields'
    )
    ,
    'tx_tsara_ytupdate' => array(
	'extension'        => $_EXTKEY,
	'title'            => '(tsara)::Update Video Youtube::',
	'description'      => '(tsara)::Update Video Youtube::'
    )
);
if( !function_exists('user_Isswords') ){
	function user_Isswords(){
		$ttnews = t3lib_div::_GP('tx_ttnews');
		if( is_array($ttnews)  && $ttnews['swords']!='' ) return true;
		return false;
	}
}
if( !function_exists('user_isMobile') ){
	function user_isMobile(){
		$useragent = $_SERVER['HTTP_USER_AGENT'];
		if( preg_match('/android|avantgo|blackberry|blazer|elaine|hiptop|ip(hone|od)|kindle|midp|mmp|mobile|o2|opera mini|palm( os)?|pda|plucker|pocket|psp|smartphone|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce; (iemobile|ppc)|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|e\-|e\/|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(\-|2|g)|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)) ) {
				
				return true;
			
		}
		return false;
	}
}
//slots signals
$signalSlotDispatcher = t3lib_div::makeInstance('Tx_Extbase_SignalSlot_Dispatcher');

$signalSlotDispatcher->connect('Tx_Powermail_Domain_Validator_CustomValidator', 'isValid', 'Tx_Tsara_Lib_Powermailslotvalid', 'addInformation', FALSE);
$signalSlotDispatcher->connect('Tx_Powermail_Controller_FormsController', 'createActionBeforeRenderView', 'Tx_Tsara_Lib_PowermailBeforeRenderView', 'savepowermail', FALSE);

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['GLOBAL']['cliKeys']['getmail'] = array('EXT:tsara/scheduler/getmail.phpsh', '_CLI_lowlevel');
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['GLOBAL']['cliKeys']['truncate_table'] = array('EXT:tsara/scheduler/truncate_table.phpsh', '_CLI_lowlevel');

$GLOBALS['TYPO3_CONF_VARS']['FE']['pageNotFoundOnCHashError'] = 0;
?>