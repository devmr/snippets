<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of class
 *
 * @author mrabehasy
 */
require_once (t3lib_extMgm :: extPath('adodb') . 'adodb/adodb.inc.php');
class ux_tx_ddgooglesitemap_pages  extends tx_ddgooglesitemap_pages {
    
     	protected function generateSitemapForPages() {
            
                $this->pgadodb = NewADOConnection("postgres");
                $rs  = $this->pgadodb->Connect( $GLOBALS['POSTGRECONF']['host'], $GLOBALS['POSTGRECONF']['username'], $GLOBALS['POSTGRECONF']['password'], $GLOBALS['POSTGRECONF']['dbname'] );
                
                $arrFields = array(
                    'DISTINCT evenement_script.id_element'
                );
            
                $arrfrom = array(
                    'evenement_script  LEFT JOIN champ_element ON champ_element.id_element = evenement_script.id_element '
                );
                $arrwhere =  $arrOrder = array();
                
                $arrwhere[] = "1=1 AND champ_element.id_champ=179 AND champ_element.contenu LIKE 'true' AND (( evenement_script.c34 >= '".date('YmdHi',mktime(0,0,0,date('n'),date('j'),date('Y')))."'   AND evenement_script.c113 = '40' )  OR evenement_script.c113 = '41' ) ";

                $pgsql = "SELECT ".implode(',',$arrFields)." FROM ".implode($arrfrom)." WHERE ".implode(' AND ',$arrwhere );
                
                $this->row = $this->pgadodb->GetAll( $pgsql );
                
                $pgsql = "SELECT DISTINCT evenement_script.c46 as localite FROM ".implode($arrfrom)." WHERE ".implode(' AND ',$arrwhere ).' AND evenement_script.c46!=\'\' ORDER BY evenement_script.c46 ASC';
                
                $this->rowlocalite = $this->pgadodb->GetAll( $pgsql );
            
		while (count($this->pageList) > 0) {
			$pageInfo = array_shift($this->pageList);
			/*$this->writeSingleUrl($pageInfo);

			// Add subpages of this page to the end of the page list. This way
			// we get top level pages in the sitemap first, then subpages of the
			// first, second, etc pages of the top level pages and so on.
			//
			// Notice: no sorting (for speed)!
			$this->pageList += $GLOBALS['TSFE']->sys_page->getMenu($pageInfo['uid'],
					'uid,doktype,no_search,SYS_LASTCHANGED,tx_ddgooglesitemap_lastmod',
					'', '', false);
                        */
                        
                        //fiche evenement
                        if( $pageInfo['uid'] == '137' ){
                            //print_r( $pageInfo ) ;
                             
                            $this->showfiche($pageInfo);
                            
                            
                        }
                        else{
                            $this->writeSingleUrl($pageInfo);
                            $this->pageList += $GLOBALS['TSFE']->sys_page->getMenu($pageInfo['uid'],
					'uid,doktype,no_search,SYS_LASTCHANGED,tx_ddgooglesitemap_lastmod',
					'', '', false);
                        }
                        
                        
		}
                
                
                 
	}
        
          function showfiche($pageInfo){
              
            if( count( $this->row ) > 0 ){
                
                foreach( $this->row as $row ){
                    $conf = array(
                                'parameter' => $pageInfo['uid'],
                                'additionalParams' => '&tx_tsara_pi3[uid]='.$row['id_element'], 'useCacheHash' => 1, 'returnLast' => 'url' 
                                ,'returnLast' => 'url',
                            );
                    $link = htmlspecialchars($this->cObj->typoLink('', $conf));
                    //$link = htmlspecialchars( 'index.php?id='.$conf['parameter'].$conf['additionalParams'] );
                    $url = t3lib_div::locationHeaderUrl($link);
                    if( strlen(trim($url)) < 160 ){
                        echo $this->renderer->renderEntry($url, $pageinfo['title'],
                        $pageInfo['SYS_LASTCHANGED'] > 24*60*60 ? $pageInfo['SYS_LASTCHANGED'] : 0,
                        $this->getChangeFrequency($pageInfo));
                    }
                    
                     
                    
                    unset($conf);
                }
            }
            
            //Afficher liens des activites
            $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                    'id_champrubrique',
                    'tx_tsara_corresthematiquesearch',
                    '1=1 '.$this->cObj->enableFields('tx_tsara_corresthematiquesearch')
                    ) ;
            if( count( $row ) > 0 ){
                foreach( $row as $ligne ){
                    $conf = array(
                                'parameter' => '99',
                                'additionalParams' => '&tx_tsara_pi3[search]=1&tx_tsara_pi3[categoriegroup]='.$ligne['id_champrubrique']
                                ,'returnLast' => 'url',
                            );
                    $link = htmlspecialchars($this->cObj->typoLink('', $conf));
                    //$link = htmlspecialchars( 'index.php?id='.$conf['parameter'].$conf['additionalParams'] );
                    $url = t3lib_div::locationHeaderUrl($link);
                    if( strlen(trim($url)) < 160 ){
                        echo $this->renderer->renderEntry($url, $pageinfo['title'],
                        $pageInfo['SYS_LASTCHANGED'] > 24*60*60 ? $pageInfo['SYS_LASTCHANGED'] : 0,
                        $this->getChangeFrequency($pageInfo));
                    }
                    
                     
                    
                    unset($conf);
                }
            }
            $rowactivite = $row;
            unset($row);
            
            //Afficher liens des departements
            $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                    'substr(code,2,3) as code',
                    'tx_tsara_departement',
                    '1=1 '.$this->cObj->enableFields('tx_tsara_departement').' AND uid<102'
                    ) ;
            if( count( $row ) > 0 ){
                foreach( $row as $ligne ){
                    $conf = array(
                                'parameter' => '99',
                                'additionalParams' => '&tx_tsara_pi3[search]=1&tx_tsara_pi3[departementmap]='.$ligne['code']
                                ,'returnLast' => 'url',
                            );
                    $link = htmlspecialchars($this->cObj->typoLink('', $conf));
                    //$link = htmlspecialchars( 'index.php?id='.$conf['parameter'].$conf['additionalParams'] );
                    $url = t3lib_div::locationHeaderUrl($link);
                    if( strlen(trim($url)) < 160 ){
                        echo $this->renderer->renderEntry($url, $pageinfo['title'],
                        $pageInfo['SYS_LASTCHANGED'] > 24*60*60 ? $pageInfo['SYS_LASTCHANGED'] : 0,
                        $this->getChangeFrequency($pageInfo));
                    }
                    
                    
                    //Pour chaque activite
                    if( count( $rowactivite ) > 0 ){
                        foreach( $rowactivite as $lignea ){
                            $conf = array(
                                        'parameter' => '99',
                                        'additionalParams' => '&tx_tsara_pi3[search]=1&tx_tsara_pi3[departementmap]='.$ligne['code'].'&tx_tsara_pi3[categoriegroup]='.$lignea['id_champrubrique']
                                        ,'returnLast' => 'url',
                                    );
                            $link = htmlspecialchars($this->cObj->typoLink('', $conf));
                            //$link = htmlspecialchars( 'index.php?id='.$conf['parameter'].$conf['additionalParams'] );
                            $url = t3lib_div::locationHeaderUrl($link);
                            if( strlen(trim($url)) < 160 ){
                                echo $this->renderer->renderEntry($url, $pageinfo['title'],
                                $pageInfo['SYS_LASTCHANGED'] > 24*60*60 ? $pageInfo['SYS_LASTCHANGED'] : 0,
                                $this->getChangeFrequency($pageInfo));
                            }



                            unset($conf);
                            unset($lignea);
                        }
                         
                    }
                     
                    
                    unset($conf);
                }
            }
            unset($row);  
            
            //Afficher liens des localites
            if( count( $this->rowlocalite ) > 0 ){
                
                foreach( $this->rowlocalite as $row ){
                    $conf = array(
                                'parameter' => '99',
                                'additionalParams' => '&tx_tsara_pi3[search]=1&tx_tsara_pi3[departement]='.$row['localite'] 
                                ,'returnLast' => 'url',
                            );
                    $link = htmlspecialchars($this->cObj->typoLink('', $conf));
                    //$link = htmlspecialchars( 'index.php?id='.$conf['parameter'].$conf['additionalParams'] );
                    $url = t3lib_div::locationHeaderUrl($link);
                    if( strlen(trim($url)) < 160 ){
                        echo $this->renderer->renderEntry($url, $pageinfo['title'],
                        $pageInfo['SYS_LASTCHANGED'] > 24*60*60 ? $pageInfo['SYS_LASTCHANGED'] : 0,
                        $this->getChangeFrequency($pageInfo));
                    }
                    
                    //Pour chaque activite
                    if( count( $rowactivite ) > 0 ){
                        foreach( $rowactivite as $lignea ){
                            $conf = array(
                                        'parameter' => '99',
                                        'additionalParams' => '&tx_tsara_pi3[search]=1&tx_tsara_pi3[departement]='.$row['localite'].'&tx_tsara_pi3[categoriegroup]='.$lignea['id_champrubrique']
                                        ,'returnLast' => 'url',
                                    );
                            $link = htmlspecialchars($this->cObj->typoLink('', $conf));
                            //$link = htmlspecialchars( 'index.php?id='.$conf['parameter'].$conf['additionalParams'] );
                            $url = t3lib_div::locationHeaderUrl($link);
                            if( strlen(trim($url)) < 160 ){
                                echo $this->renderer->renderEntry($url, $pageinfo['title'],
                                $pageInfo['SYS_LASTCHANGED'] > 24*60*60 ? $pageInfo['SYS_LASTCHANGED'] : 0,
                                $this->getChangeFrequency($pageInfo));
                            }

                            

                            unset($conf);
                            unset($lignea);
                        }
                         
                    }
                    
                     
                    
                    unset($conf);
                    
                }
                 
            }
             
            
            return true;
        }   
}
?>
