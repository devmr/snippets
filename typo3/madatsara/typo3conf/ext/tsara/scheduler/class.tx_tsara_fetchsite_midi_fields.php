<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of class
 *
 * @author mrabehasy
 */
class tx_tsara_fetchsite_midi_fields implements tx_scheduler_AdditionalFieldProvider  {

	/**
	 * Field generation.
	 * This method is used to define new fields for adding or editing a task
	 * In this case, it adds a page ID field
	 *
	 * @param	array			$taskInfo: reference to the array containing the info used in the add/edit form
	 * @param	object			$task: when editing, reference to the current task object. Null when adding.
	 * @param	tx_scheduler_Module	$parentObject: reference to the calling object (Scheduler's BE module)
	 * @return	array			Array containg all the information pertaining to the additional fields
	 *					The array is multidimensional, keyed to the task class name and each field's id
	 *					For each field it provides an associative sub-array with the following:
	 *						['code']		=> The HTML code for the field
	 *						['label']		=> The label of the field (possibly localized)
	 *						['cshKey']		=> The CSH key for the field
	 *						['cshLabel']		=> The code of the CSH label
	 */
	public function getAdditionalFields(array &$taskInfo, $task, tx_scheduler_Module $parentObject) {

			// Initialize extra field value
		if (empty($taskInfo['prefixe'])) {
			if ($parentObject->CMD == 'edit') {
				$taskInfo['prefixe'] = $task->prefixe;
			} else {
				$taskInfo['prefixe'] = '';
			}
		}
		
		
                
		
		if (empty($taskInfo['uidpresse'])) {
			if ($parentObject->CMD == 'edit') {
				$taskInfo['uidpresse'] = $task->uidpresse;
			} else {
				$taskInfo['uidpresse'] = '';
			}
		}
                if (empty($taskInfo['author'])) {
			if ($parentObject->CMD == 'edit') {
				$taskInfo['author'] = $task->author;
			} else {
				$taskInfo['author'] = '';
			}
		}
                 

			// Generate the additional field
		 $additionalFields = array();
                
                 $fieldID = 'task_prefixe';
		$fieldCode = '<input type="text" name="tx_scheduler[prefixe]" id="' . $fieldID . '" value="' . $taskInfo['prefixe'] . '" size="50" />';
		$additionalFields[$fieldID] = array(
			'code' => $fieldCode,
			'label' => 'Prefixe nom fichier',
			'cshKey' => 'tsara',
			'cshLabel' => $fieldID
		);
		
		
                
		
		$fieldID = 'task_uidpresse';
		$fieldCode = '<input type="text" name="tx_scheduler[uidpresse]" id="' . $fieldID . '" value="' . $taskInfo['uidpresse'] . '" size="50" />';
		$additionalFields[$fieldID] = array(
			'code' => $fieldCode,
			'label' => 'UID presse locale (tt_news_cat)',
			'cshKey' => 'tsara',
			'cshLabel' => $fieldID
		);
		$fieldID = 'task_author';
		$fieldCode = '<input type="text" name="tx_scheduler[author]" id="' . $fieldID . '" value="' . $taskInfo['author'] . '" size="50" />';
		$additionalFields[$fieldID] = array(
			'code' => $fieldCode,
			'label' => 'Auteur (tt_news)',
			'cshKey' => 'tsara',
			'cshLabel' => $fieldID
		);
                 

		return $additionalFields;
	}

	/**
	 * Field validation.
	 * This method checks if page id given in the 'Hide content' specific task is int+
	 * If the task class is not relevant, the method is expected to return true
	 *
	 * @param	array			$submittedData: reference to the array containing the data submitted by the user
	 * @param	tx_scheduler_Module	$parentObject: reference to the calling object (Scheduler's BE module)
	 * @return	boolean			True if validation was ok (or selected class is not relevant), false otherwise
	 */
	public function validateAdditionalFields(array &$submittedData, tx_scheduler_Module $parentObject) {
		 $result = TRUE;

		return $result;
	}

	/**
	 * Store field.
	 * This method is used to save any additional input into the current task object
	 * if the task class matches
	 *
	 * @param	array			$submittedData: array containing the data submitted by the user
	 * @param	tx_scheduler_Task	$task: reference to the current task object
	 * @return	void
	 */
	public function saveAdditionalFields(array $submittedData, tx_scheduler_Task $task) {
            
		
            
		$task->prefixe = $submittedData['prefixe'];
		$task->uidpresse = $submittedData['uidpresse'];
		$task->author= $submittedData['author'];
	}
}
?>
