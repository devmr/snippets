<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2009 Steffen Müller (typo3@t3node.com)
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

/**
 * Class "tx_smscheddemo_HideContent" provides task procedures
 *
 * @author		Steffen Müller <typo3@t3node.com>
 * @package		TYPO3
 * @subpackage		tx_smscheddemo
 *
 */
require_once(t3lib_extMgm::extPath("tsara").'lib/class.tx_tsara_util.php');
class tx_tsara_fetchsite_bcrm extends tx_scheduler_Task {

	 

	/**
	 * Function executed from the Scheduler.
	 * Hides all content elements of a page
	 *
	 * @return	boolean	TRUE if success, otherwise FALSE
	 */
	public function execute() {
		$success = FALSE;
		
                $prefixe = $this->prefixe;
                $uidpresse = $this->uidpresse;
                $categoryuid = $this->categoryuid;
                $tlog = $tab = array();
                //Timestamp du jour
                $tstamp_time = mktime(12, 0, 0, date('n'), date('d'), date('Y'));
                
                //Si le fichier existe
                if(file_exists(PATH_site.'fileadmin/json/bcrm/bcrm_'.$tstamp_time.'.json')){
                    return true;
                }
                
                require_once t3lib_extMgm::extPath("tsara").'lib/simple_html_dom.php';
                require_once t3lib_extMgm::extPath("tsara").'lib/class.datetimeconverter.php';

                $dtc = t3lib_div::makeInstance('Date_Time_Converter');
                $content = t3lib_div::getUrl( 'http://www.banque-centrale.mg/' );
                 
                 $html = str_get_html( $content );
                 
                //liste
                $date = $html->find("td.lienGauche table",1)->find('td',0)->find('strong',0)->plaintext;
                $date = str_replace('&nbsp;','',$date);
                $date = str_replace('MGA','',$date);
                $date = str_replace('Du','',$date);
                $date = trim($date);

                $datetstamp = $dtc->_date_to_timestamp($date,'d/m/y');
                $tlog[] = '<p>Date sur http://www.banque-centrale.mg/ : <b>'.date('d-m-Y',$datetstamp).'</b> - Timestamp : <i>'.$datetstamp.'</i></p><table cellpadding="4" cellspacing="0" border="1">';

                //Si le fichier existe
                if(file_exists(PATH_site.'fileadmin/json/bcrm/bcrm_'.$datetstamp.'.json')){
                    return true;
                }

                foreach( $html->find("td.lienGauche table",2)->find('tr') as $tr ){

                                        $devise = trim( $tr->find('td',0)->plaintext );
                                        $ariary = trim( $tr->find('td',1)->plaintext );

                                        $tab[$devise] = $ariary;
										
										$tlog[] = '<tr><td>'.$devise.'</td><td>'.$ariary.' Ar</td></tr>';

                //                        echo $ariary.'-'.$devise."\r\n";

                        $i++;
                }
                
                $tlog[] = '</table><p>Nombre de données : <b>'.count($tab).'</b></p>';

                $tabd = array(
                    'date' => array(
                        'datestr' => date('d-m-Y',$datetstamp)
                        ,'datetstamp' => $datetstamp
                        )
                    ,'data' => $tab

                    );

//                echo json_encode($tabd);

                t3lib_div::writeFile(PATH_site.'fileadmin/json/bcrm/bcrm_'.$datetstamp.'.json',json_encode($tabd));

                unset($tab);
                $d = opendir(PATH_site.'fileadmin/json/bcrm/');
                while( $f = readdir( $d )){
                    if( $f != '.' && $f!='..' ){
                        $tab[filemtime(PATH_site.'fileadmin/json/bcrm/'.$f)] = $f;
                    }
                }
                
                
                $tlog[] = '<p>Fichier à prendre : <b>'.max($tab).'</b></p>';
                //max($tab);
                $tstamp = mktime(0, 0, 0, date('n'), date('d')-8, date('Y'));
                $tlog[] = '<p>Supprimer les enregistrements avant le : <b>'.date('d-m-Y',$tstamp).'</b></p><ul>';
                 
                //Supprimer les enregistrements vieux de 1 semaine
                $n = 0;
                foreach($tab as $k => $f ){
                    if( $k < $tstamp ){
                        $tlog[] = '<li>Fichier à supprimer : '.$f.'</li>';
                        if( file_exists(PATH_site.'fileadmin/json/bcrm/'.$f) ) unlink(PATH_site.'fileadmin/json/bcrm/'.$f);
                        $n++;
                    }
                } 
                
                $tlog[] = '</ul><p>Nombre total de fichiers supprimés : '.$n.'</p>';
                
                //Envoi mail
                    $mail = t3lib_div::makeInstance('t3lib_mail_Message');
                    $mail->setTo(array('madatsara@gmail.com'))
                        ->setFrom(array($GLOBALS['TYPO3_CONF_VARS']['MAIL']['defaultMailFromAddress'] => $GLOBALS['TYPO3_CONF_VARS']['MAIL']['defaultMailFromName'] ))
                        ->setSubject( 'MADATSARA CRON Récup. Cours du change - le '.date('d-m-Y H:i:s') )
                        ->setCharset('utf-8');
                    //$mail->setBody( nl2br( implode("\n",$tlog ) ) , 'text/html');
                    $mail->setBody( implode(" ",$tlog )   , 'text/html');
                    $mail->send();
                    /*$this->util = t3lib_div::makeInstance('tx_tsara_util');
                    $this->util->clear_cache();*/ 
                $success = true;
		return $success;
	}
        
        
        
        
        

}

if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/tsara/scheduler/class.tx_tsara_fetchsite_bcrm.php'])	{
	include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/tsara/scheduler/class.tx_tsara_fetchsite_bcrm.php']);
}

?>