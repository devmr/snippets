<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2009 Steffen Müller (typo3@t3node.com)
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

/**
 * Class "tx_smscheddemo_HideContent" provides task procedures
 *
 * @author		Steffen Müller <typo3@t3node.com>
 * @package		TYPO3
 * @subpackage		tx_smscheddemo
 *
 */
require_once(t3lib_extMgm::extPath("tsara").'lib/class.tx_tsara_util.php');

class tx_tsara_fetch_ytpl extends tx_scheduler_Task {

	 

	/**
	 * Function executed from the Scheduler.
	 * Hides all content elements of a page
	 *
	 * @return	boolean	TRUE if success, otherwise FALSE
	 */
	public function execute() {
		$success = FALSE;
		$tlog = array();
                $user = $this->user;
                $pid = $this->pid;
                $parent_category = $this->parent_category;
                $parent_category_r = $this->parent_category_r;
                $idexclude = $this->idexclude;
				
                 
                $url = 'http://gdata.youtube.com/feeds/api/users/'.$user.'/playlists?max-results=50&alt=json&orderby=published';
                
                $tlog[] = ' URL : <a href="'.$url.'" target="_blank">'.$url.'</a>';

                $data = t3lib_div::getUrl( $url );
                $data = json_decode( $data , 1 );

                $nbresults = $data['feed']['openSearch$totalResults']['$t'];
                $tlog[] = 'Nombre de r&eacute;sultats : '.$nbresults;

 

                $pages = ceil($nbresults / 50);
                $tlog[] = 'Nombre de pages : '.$pages;



                for($i=0;$i<$pages;$i++){

                    $url = 'http://gdata.youtube.com/feeds/api/users/'.$user.'/playlists?max-results=50'.($i>0?'&start-index='.($i*50):'').'&alt=json&orderby=published';
                    $tlog[] = ' URL : <a href="'.$url.'" target="_blank">'.$url.'</a>';
                    $data = t3lib_div::getUrl( $url );
                    $data = json_decode( $data , 1 );
                    $boucle = $data['feed']['entry'];

                    foreach( $boucle as $row ){
                            $id = $row['yt$playlistId']['$t'];
                            $title = $row['title']['$t'];
                            $tligne[] = array(
                                    'title' => $row['title']['$t']
                                    ,'id' => $row['yt$playlistId']['$t']
                            );
                    }
                }



                //print_r( $tligne  );exit;
                $n = 0;
                foreach( $tligne as $row ){

                        $id = $row['id'];
                        $rythme = '';
                        $tlog[] = $row['title'];
                         


                        $sqli = $GLOBALS['TYPO3_DB']->INSERTquery(
                        'tt_news_cat'
                        ,array(
                                'pid' => $pid
                                ,'parent_category' => (substr(trim($row['title']),-7)=='_Rythme'?$parent_category_r:$parent_category)
                                ,'title' => $row['title']
                                ,'tstamp' => time()
                                ,'crdate' => time()
                                ,'tx_tsara_youtubeid' => $id
                        )
                        );



                         

                        $count = $GLOBALS['TYPO3_DB']->exec_SELECTcountRows('uid','tt_news_cat','1=1 AND tt_news_cat.tx_tsara_youtubeid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr( $id, 'tt_news_cat' ).' AND tt_news_cat.pid = '.$pid);
                        
                        $tlog[] = $GLOBALS['TYPO3_DB']->SELECTquery('COUNT(uid)','tt_news_cat','1=1 AND tt_news_cat.tx_tsara_youtubeid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr( $id, 'tt_news_cat' ).' AND tt_news_cat.pid = '.$pid);
                        $tlog[] = 'Count : '.$count;
                        if( $count == 0 ) {
                                $GLOBALS['TYPO3_DB']->sql_query( $sqli );
                                $tlog[] = $sqli;
                                $n++;
                        }


                }
                
                $tlog[] =  $n.' playlists ajout&eacute;s ';

		$sqlu = $GLOBALS['TYPO3_DB']->UPDATEquery('tt_news_cat','title LIKE '.$GLOBALS['TYPO3_DB']->fullQuoteStr( '%Rythme', 'tt_news_cat' ),array('parent_category' => 12333));
                 $GLOBALS['TYPO3_DB']->sql_query( $sqlu );
 
                $tlog[] = $sqlu;
                t3lib_div::writeFile(PATH_site.'typo3temp/log_'.__CLASS__.$tstamp.'.html', nl2br( implode("\n",$tlog ) ) );
					
                $to = 'madatsara@gmail.com';
                $subjectmail = 'Cron '.__CLASS__.' %s';                
                $mail = t3lib_div::makeInstance('t3lib_mail_Message');
                $mail->setTo(array($to))
                    ->setFrom(array($GLOBALS['TYPO3_CONF_VARS']['MAIL']['defaultMailFromAddress'] => $GLOBALS['TYPO3_CONF_VARS']['MAIL']['defaultMailFromName'] ))
                    ->setSubject(sprintf( $subjectmail, date('d-m-Y H:i:s') ))
                    ->setCharset('utf-8');

                $mail->setBody('http://vvv.madatsara.com/typo3temp/log_'.__CLASS__.$tstamp.'.html'  , 'text/html');
                $mail->send();
                 
                $success = true;
		return $success;
	}
	
	 
        
         	
        
 
        
 

}

if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/tsara/scheduler/class.tx_tsara_fetchsite_inovaovao.php'])	{
	include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/tsara/scheduler/class.tx_tsara_fetchsite_inovaovao.php']);
}

?>