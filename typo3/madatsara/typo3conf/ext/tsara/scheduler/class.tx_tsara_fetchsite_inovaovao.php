<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2009 Steffen Müller (typo3@t3node.com)
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

/**
 * Class "tx_smscheddemo_HideContent" provides task procedures
 *
 * @author		Steffen Müller <typo3@t3node.com>
 * @package		TYPO3
 * @subpackage		tx_smscheddemo
 *
 */
require_once(t3lib_extMgm::extPath("tsara").'lib/class.tx_tsara_util.php');
class tx_tsara_fetchsite_inovaovao extends tx_scheduler_Task {

	 

	/**
	 * Function executed from the Scheduler.
	 * Hides all content elements of a page
	 *
	 * @return	boolean	TRUE if success, otherwise FALSE
	 */
	public function execute() {
		$success = FALSE;
		
                $prefixe = $this->prefixe;
                $uidpresse = $this->uidpresse;
                $categoryuid = $this->categoryuid;
                
                require_once t3lib_extMgm::extPath("tsara").'lib/simple_html_dom.php';
                $this->util = t3lib_div::makeInstance('tx_tsara_util');
                $tlog = array();
                $tabTitre = array();
                $tabUrl = array();
                $tabImg = array();
                $tabCorps = array();
                $data = array();
                $tabmc = array();
                $tuidcat = array();
                $tabUidcat = array();
                $metahttp = 'utf-8';
                $this->extensionok = 'jpeg,jpg,png,gif';
                
                $fileFunc = t3lib_div::makeInstance('t3lib_basicFileFunctions');
                $this->confArr = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['tsara']);
                $this->cs = t3lib_div::makeInstance('t3lib_cs');
                $content = t3lib_div::getUrl( 'http://www.inovaovao.mg/' );
                $html = str_get_html( $content );
                
                foreach( $html->find('meta[http-equiv*=content-type]') as $ct ) { 
                    $metahttp = $ct->content;
                    
                    $tabpointv = t3lib_div::trimExplode(';',$metahttp );
                    if( count( $tabpointv ) == 2 ){
                        $tcharset = t3lib_div::trimExplode('=',$tabpointv[1] );
                        $metahttp = $tcharset[1];
                    }
                    $metahttp = trim( preg_replace('/^[\pZ\pC]+|[\pZ\pC]+$/u', '', $metahttp ) );
                    $metahttp = mb_strtolower( $metahttp );
                }
                
                $metahttp = 'utf-8';
                  
                foreach( $html->find("div.bloc_c") as $p ){
                    foreach( $p->find('a') as $link ){

                        $url = 'http://www.inovaovao.mg/'.$link->href;

                        $body = t3lib_div::getUrl( $url);
                        $html = str_get_html($body);

                        $titre = $html->find('div.txt_rub h2',0)->plaintext;
                         
                        foreach( $html->find('div.txt_rub p') as $p ){
                            $corps .= $p->plaintext."\r\n";
                        }
                        $img = 'http://www.inovaovao.mg/'.$html->find('div.txt_rub img',0)->src;

                        $titre = ($metahttp!='utf-8'?$this->cs->conv( trim( $titre ), $metahttp,'utf-8' ):trim($titre));
                        $corps = ($metahttp!='utf-8'?$this->cs->conv( trim( $corps ), $metahttp,'utf-8' ):trim($corps));

                        $tabUrl[] = $url;
                        $tabTitre[] = $titre;
                        $tabCorps[] = $corps;
                        $tabImg[] = $img;
                        
                        unset($corps);
                    }

                }
   

                if(is_array($tabTitre) && count($tabTitre)>0){
                   
		$tabTitre = $this->util->array_delete( $tabTitre );
		$tabCorps = $this->util->array_delete( $tabCorps );
		$tabUrl = $this->util->array_delete( $tabUrl );
		$tabImg = $this->util->array_delete( $tabImg );

                   /*print_r($tabTitre);
                    print_r($tabCorps);
                    print_r($tabUrl);
                    print_r($tabImg);

                    exit;*/
                    
                    //Liste des mots clés
                    $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                            'uid,tx_tsara_keywords',
                            'tt_news_cat',
                            '1=1 AND deleted=0 AND hidden=0 AND tx_tsara_keywords IS NOT NULL AND pid IN('.$this->confArr['pidcat'].') AND parent_category IN('.$this->confArr['parent_category'].')'
                            );
                    $tlog[] = $GLOBALS['TYPO3_DB']->SELECTquery(
                            'uid,tx_tsara_keywords',
                            'tt_news_cat',
                            '1=1 AND deleted=0 AND hidden=0 AND tx_tsara_keywords IS NOT NULL AND  pid IN('.$this->confArr['pidcat'].') AND parent_category IN('.$this->confArr['parent_category'].')'
                            );;
                    if( count( $row ) > 0 ){
                        //Construire les regex
                        foreach( $row as $ligne ){
                            $mc = $this->util->oteraccents( trim($ligne['tx_tsara_keywords'] ) );
                            
                            //recreer les valeurs
                            $tabmc = t3lib_div::trimExplode(',', $mc );
                            $data[$ligne['uid']] = '/'.implode('|',$tabmc ).'/iU';
                            unset( $tabmc );
                        }
                    }
                    
                    
                    for($i=0;$i<count($tabTitre);$i++){

                                $titre = ( $tabTitre[$i] ); 
                                $corps = trim( $tabCorps[$i] ); 
                                $corps = preg_replace('/^[\pZ\pC]+|[\pZ\pC]+$/u', '', $corps );
                                $corps = trim( $corps );
                                $url = $tabUrl[$i];
                                $img = $tabImg[$i];
                                
                                $titre = $this->cs->entities_to_utf8($titre,1);
                                $corps = $this->cs->entities_to_utf8($corps,1);

                                if( $img!=''){
                                    $fichier = $prefixe.$fileFunc->cleanFileName( basename( $img ) );
                                    $chemin = PATH_site.'uploads/pics' ;
                                    $myfilepath = $fileFunc->getUniqueName($fichier, $chemin);
                                    $tab = t3lib_div::split_fileref( $myfilepath );
                                    $img  = ($tab['file']!='_01' && $tab['fileext']!=''?$tab['file']:'');
                                    if(  t3lib_div::inList($this->extensionok,mb_strtolower( $tab['fileext'] ))){
                                        t3lib_div::writeFile( $myfilepath, t3lib_div::getUrl($tabImg[$i]));
                                    }
                                    else{
                                        $img = "";
                                    }
                                    unset($tab);
                                }
                                $count = $GLOBALS['TYPO3_DB']->exec_SELECTcountRows('uid','tt_news','1=1 AND deleted=0 AND hidden=0 AND title = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($titre,'tt_news').' AND tx_tsara_exturl2 = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($url,'tt_news') );
                                $tlog[] =   $GLOBALS['TYPO3_DB']->SELECTquery('COUNT(uid)','tt_news','1=1 AND deleted=0 AND hidden=0 AND title = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($titre,'tt_news').' AND tx_tsara_exturl2 = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($url,'tt_news') ); 

                                if($titre!='' && $count == 0 && $url!='' && trim( $corps ) != '' ){

                                    $sql = $GLOBALS['TYPO3_DB']->INSERTquery(
                                            'tt_news',
                                            array(
                                                'pid' => $this->confArr['pidsave']
                                                ,'tstamp' => time()
                                                ,'crdate' => time()
                                                ,'cruser_id' => 1
                                                ,'title' => $titre
                                                ,'datetime' => time()
                                                ,'image' => $img
                                                ,'bodytext' => trim($corps)
                                                ,'author' => $this->author
                                                ,'tx_tsara_exturl2' => $url
                                            )
                                    );
                                    $GLOBALS['TYPO3_DB']->sql_query( $sql );
                                    $uid = $GLOBALS['TYPO3_DB']->sql_insert_id();
                                    
                                    $tlog[] = $sql;
                                    
                                    $sql = $GLOBALS['TYPO3_DB']->INSERTquery(
                                                        'tt_news_cat_mm',
                                                        array(
                                                            'uid_local' => $uid
                                                            ,'uid_foreign' => $uidpresse 
                                                        )
                                                );
                                                
                                    $GLOBALS['TYPO3_DB']->sql_query( $sql );
                                    $tlog[] = $sql;             
                                                
                                    //Attribuer les categories en fonction des mot cles
                                    if( count( $data ) > 0 ){
                                        
                                        //oter les accents dans le corps et le titre
                                        $corps = $this->util->oteraccents( $corps );
                                        foreach( $data as $key => $regex ){
                                            if( preg_match($regex,$corps) || preg_match($regex,$titre) ) {
                                                 
                                                
                                                $sql = $GLOBALS['TYPO3_DB']->INSERTquery(
                                                        'tt_news_cat_mm',
                                                        array(
                                                            'uid_local' => $uid
                                                            ,'uid_foreign' => $key 
                                                        )
                                                );
                                                
                                                $GLOBALS['TYPO3_DB']->sql_query( $sql );
                                                
                                                $tlog[] = $sql;
                                                
                                                
                                            }
                                            else{
                                                $tlog[] = $regex;
                                            }
                                        }
                                        
                                         
                                        unset( $tuidcat );
                                    }

                                    



                                }
                                else $tlog[] = 'Count : '.$count;



                    }

                }

                t3lib_div::writeFile( PATH_site.'typo3temp/'.__CLASS__.'.txt',implode(";\n", $tlog ));
                $success = true;
		return $success;
	}
        
 
        
 

}

if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/tsara/scheduler/class.tx_tsara_fetchsite_inovaovao.php'])	{
	include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/tsara/scheduler/class.tx_tsara_fetchsite_inovaovao.php']);
}

?>