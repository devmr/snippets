<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2009 Steffen M?ller (typo3@t3node.com)
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

/**
 * Class "tx_smscheddemo_HideContent" provides task procedures
 *
 * @author		Steffen M?ller <typo3@t3node.com>
 * @package		TYPO3
 * @subpackage		tx_smscheddemo
 *
 */
require_once(t3lib_extMgm::extPath("tsara").'lib/class.tx_tsara_util.php');
require_once(t3lib_extMgm::extPath("tsara").'lib/encoding.php');
class tx_tsara_fetchpicasa_album extends tx_scheduler_Task {

	 

	/**
	 * Function executed from the Scheduler.
	 * Hides all content elements of a page
	 *
	 * @return	boolean	TRUE if success, otherwise FALSE
	 */
	public function execute() {
		$success = FALSE;
		
                $user = $this->user;
                $json = $this->json;
                $pid = $this->pid;
                
                 
                $str = t3lib_div::getUrl('http://picasaweb.google.com/data/feed/base/user/'.$user.'?alt=json&start-index=1&max-results=1000&kind=album&hl=en_US');
                
                $js = json_decode( $str, true ) ;
				//t3lib_div::writeFile( PATH_site.'typo3temp/logpicasa/line_'.__LINE__.'.txt', '' );
                //print_r( $js );
                $title = $js['feed']['title']['$t'];
                $subtitle = $js['feed']['subtitle']['$t'];
                $icon = $js['feed']['icon']['$t'];
                $link = $js['feed']['link'][1]['href'];
                $author = $js['feed']['author'][0]['name']['$t'];
                $nbalbum = $js['feed']['openSearch$totalResults']['$t'];
                
		//DELETE ALL ttnews in $pid
                
                
                
                $i = 0;
                foreach( $js['feed']['entry'] as $ligne ){
					
					try{
						$id = $ligne['id']['$t'];

					
						$id = str_replace('http://picasaweb.google.com/data/entry/base/user/'.$user.'/albumid/','',str_replace('?alt=json&hl=en_US','',$id));

						
						
                    
						
						$fp = fopen( PATH_site.'typo3temp/logpicasa/lineboucle_'.__LINE__.'.txt', ($i==0?'w':'a') );
						fwrite( $fp, ($i>0?"\n":"").$ligne['title']['$t'].' - '.sprintf('http://picasaweb.google.com/data/feed/base/user/'.$user.'/albumid/%s?alt=json&hl=en_US',$id) );
						fclose($fp);
						
						//$jsinner = json_decode( t3lib_div::getUrl( sprintf('http://picasaweb.google.com/data/feed/base/user/'.$user.'/albumid/%s?alt=json&hl=en_US',$id) ), true ) ;
					
					
						$j = 0;
						/*foreach( $jsinner['feed']['entry'] as $liste ){
						
							try{
										$tliste[$id][] = array(
												'published' => $liste['published']['$t'],
												'updated' => $liste['updated']['$t'],
												'title' => $liste['title']['$t'],
												'content' => $liste['content']['src'],
												'thumbnail' => $this->replacethumbnail( $liste['media$group']['media$thumbnail'][2]['url'] )
										);
									$fp = fopen( PATH_site.'typo3temp/logpicasa/lineboucle_'.$i.'_'.__LINE__.'.txt', ($j==0?'w':'a') );
									fwrite( $fp, ($j>0?"\n":"").$ligne['title']['$t']  );
									fclose($fp);
							}
							catch(Exception $e){
									$fp = fopen( PATH_site.'typo3temp/logpicasa/linebouclerror_'.$i.'_'.__LINE__.'.txt', ($j==0?'w':'a') );
									fwrite( $fp, ($j>0?"\n":"").$e->getMessage()  );
									fclose($fp);
							}
									$j++;
						}*/
					
						$tphotos[] = array(
								'nbphotos' => $jsinner['feed']['openSearch$totalResults']['$t'],
								'liste' => $tliste[$id]
						);

						$tligne[] = array(
								'id' => $id,
								'date' => $ligne['published']['$t'],
								'maj' => $ligne['updated']['$t'],
								'title' => $ligne['title']['$t'],
								'description' => $ligne['media$group']['media$description']['$t'],
								'thumbnail' => $this->replacethumbnail( $ligne['media$group']['media$thumbnail'][0]['url'] ),
								'photos' => $tphotos,
						);
                                                
                                                //Monter en BDD
                                                $nb = $GLOBALS['TYPO3_DB']->exec_SELECTcountRows(
                                                        'uid',
                                                        'tt_news',
                                                        'deleted=0 AND pid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($pid,'tt_news').' AND tx_tsara_youtubeid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($id,'tt_news'));
                                                
                                                $sqli = $GLOBALS['TYPO3_DB']->INSERTquery(
                                                        'tt_news',
                                                        array(
                                                            'pid' => $pid
                                                            ,'title' => $ligne['title']['$t']
                                                            ,'tstamp' => time()
                                                            ,'crdate' => time()
                                                            ,'tx_tsara_youtube_published' => $ligne['published']['$t']
                                                            ,'short' => $ligne['media$group']['media$description']['$t']
                                                            ,'type' => 2
                                                            ,'tx_tsara_youtube_thumbnail' => $this->replacethumbnail( $ligne['media$group']['media$thumbnail'][0]['url'] )
                                                            ,'tx_tsara_youtubeid' => $id
                                                        ));
                                                if( $nb <= 0  ) $GLOBALS['TYPO3_DB']->sql_query( $sqli );
					 

						unset($tphotos);
					}
					catch(Exception $e){
						$fp = fopen( PATH_site.'typo3temp/logpicasa/linebouclerror_'.__LINE__.'.txt', ($i==0?'w':'a') );
						fwrite( $fp, ($i>0?"\n":"").$e->getMessage()  );
						fclose($fp);
					}
					$i++;
				}
                                //Maj Date                  
                                $GLOBALS['TYPO3_DB']->sql_query( "UPDATE `tt_news` SET datetime =  UNIX_TIMESTAMP(`tx_tsara_youtube_published`) WHERE `pid` = ".$pid );
            
            /*$tableau =  array('infocompte' => array(
                'title' => $title,
                'subtitle' => $subtitle,
                'icon' => $icon,
                'link' => $link,
                'author' => $author,
                'Nombre album' => $nbalbum	
                ),
                'detailcompte' => $tligne 
                ) ;
                
                t3lib_div::writeFile( PATH_site.$json, json_encode( $tableau ) ); 

                */
                

                 
                $success = true;
		return $success;
	}
	
	function replacethumbnail( $src ){
		if( $this->sizethumbnails == '' ) $this->sizethumbnails = 's300-c';
		$src = str_replace('s160-c',$this->sizethumbnails,$src);
		return $src;
	}
        
         	
        
 
        
 

}

if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/tsara/scheduler/class.tx_tsara_fetchpicasa_album.php'])	{
	include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/tsara/scheduler/class.tx_tsara_fetchpicasa_album.php']);
}

?>