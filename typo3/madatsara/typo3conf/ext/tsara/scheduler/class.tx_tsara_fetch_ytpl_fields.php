<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of class
 *
 * @author mrabehasy
 */
class tx_tsara_fetch_ytpl_fields implements tx_scheduler_AdditionalFieldProvider  {

	/**
	 * Field generation.
	 * This method is used to define new fields for adding or editing a task
	 * In this case, it adds a page ID field
	 *
	 * @param	array			$taskInfo: reference to the array containing the info used in the add/edit form
	 * @param	object			$task: when editing, reference to the current task object. Null when adding.
	 * @param	tx_scheduler_Module	$parentObject: reference to the calling object (Scheduler's BE module)
	 * @return	array			Array containg all the information pertaining to the additional fields
	 *					The array is multidimensional, keyed to the task class name and each field's id
	 *					For each field it provides an associative sub-array with the following:
	 *						['code']		=> The HTML code for the field
	 *						['label']		=> The label of the field (possibly localized)
	 *						['cshKey']		=> The CSH key for the field
	 *						['cshLabel']		=> The code of the CSH label
	 */
	public function getAdditionalFields(array &$taskInfo, $task, tx_scheduler_Module $parentObject) {

			// Initialize extra field value
		if (empty($taskInfo['idexclude'])) {
			if ($parentObject->CMD == 'edit') {
				$taskInfo['idexclude'] = $task->idexclude;
			} else {
				$taskInfo['idexclude'] = '';
			}
		}
		
		
		
		if (empty($taskInfo['user'])) {
			if ($parentObject->CMD == 'edit') {
				$taskInfo['user'] = $task->user;
			} else {
				$taskInfo['user'] = '';
			}
		}
		
		if (empty($taskInfo['pid'])) {
			if ($parentObject->CMD == 'edit') {
				$taskInfo['pid'] = $task->pid;
			} else {
				$taskInfo['pid'] = '';
			}
		}
		if (empty($taskInfo['parent_category'])) {
			if ($parentObject->CMD == 'edit') {
				$taskInfo['parent_category'] = $task->parent_category;
			} else {
				$taskInfo['parent_category'] = '';
			}
		}
                if (empty($taskInfo['parent_category_r'])) {
			if ($parentObject->CMD == 'edit') {
				$taskInfo['parent_category_r'] = $task->parent_category_r;
			} else {
				$taskInfo['parent_category_r'] = '';
			}
		}
		
		 
		
		 
                
                 

			// Generate the additional field
		 $additionalFields = array();
                
                 $fieldID = 'task_idexclude';
		$fieldCode = '<input type="text" name="tx_scheduler[idexclude]" id="' . $fieldID . '" value="' . $taskInfo['idexclude'] . '" size="50" />';
		$additionalFields[$fieldID] = array(
			'code' => $fieldCode,
			'label' => 'ID exclude',
			'cshKey' => 'tsara',
			'cshLabel' => $fieldID
		);
		
		
                 
		
		$fieldID = 'task_pid';
		$fieldCode = '<input type="text" name="tx_scheduler[pid]" id="' . $fieldID . '" value="' . $taskInfo['pid'] . '" size="50" />';
		$additionalFields[$fieldID] = array(
			'code' => $fieldCode,
			'label' => 'PID',
			'cshKey' => 'tsara',
			'cshLabel' => $fieldID
		);
		
		$fieldID = 'task_parent_category';
		$fieldCode = '<input type="text" name="tx_scheduler[parent_category]" id="' . $fieldID . '" value="' . $taskInfo['parent_category'] . '" size="50" />';
		$additionalFields[$fieldID] = array(
			'code' => $fieldCode,
			'label' => 'UID parent_category',
			'cshKey' => 'tsara',
			'cshLabel' => $fieldID
		);
                
                $fieldID = 'task_parent_category_r';
		$fieldCode = '<input type="text" name="tx_scheduler[parent_category_r]" id="' . $fieldID . '" value="' . $taskInfo['parent_category_r'] . '" size="50" />';
		$additionalFields[$fieldID] = array(
			'code' => $fieldCode,
			'label' => 'UID parent_category Rythme',
			'cshKey' => 'tsara',
			'cshLabel' => $fieldID
		);
		
		$fieldID = 'task_user';
		$fieldCode = '<input type="text" name="tx_scheduler[user]" id="' . $fieldID . '" value="' . $taskInfo['user'] . '" size="50" />';
		$additionalFields[$fieldID] = array(
			'code' => $fieldCode,
			'label' => 'User ID',
			'cshKey' => 'tsara',
			'cshLabel' => $fieldID
		);
                 

		return $additionalFields;
	}

	/**
	 * Field validation.
	 * This method checks if page id given in the 'Hide content' specific task is int+
	 * If the task class is not relevant, the method is expected to return true
	 *
	 * @param	array			$submittedData: reference to the array containing the data submitted by the user
	 * @param	tx_scheduler_Module	$parentObject: reference to the calling object (Scheduler's BE module)
	 * @return	boolean			True if validation was ok (or selected class is not relevant), false otherwise
	 */
	public function validateAdditionalFields(array &$submittedData, tx_scheduler_Module $parentObject) {
		 $result = TRUE;

		return $result;
	}

	/**
	 * Store field.
	 * This method is used to save any additional input into the current task object
	 * if the task class matches
	 *
	 * @param	array			$submittedData: array containing the data submitted by the user
	 * @param	tx_scheduler_Task	$task: reference to the current task object
	 * @return	void
	 */
	public function saveAdditionalFields(array $submittedData, tx_scheduler_Task $task) {
            
		
		$task->user = $submittedData['user'];
		$task->idexclude = $submittedData['idexclude'];
		$task->pid = $submittedData['pid'];
		$task->parent_category = $submittedData['parent_category'];
		$task->parent_category_r = $submittedData['parent_category_r'];
	}
}
?>