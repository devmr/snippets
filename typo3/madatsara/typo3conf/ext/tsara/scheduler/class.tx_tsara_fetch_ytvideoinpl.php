<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2009 Steffen Müller (typo3@t3node.com)
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
//t
/**
 * Class "tx_smscheddemo_HideContent" provides task procedures
 *
 * @author		Steffen Müller <typo3@t3node.com>
 * @package		TYPO3
 * @subpackage		tx_smscheddemo
 *
 */
require_once(t3lib_extMgm::extPath("tsara").'lib/class.tx_tsara_util.php');

class tx_tsara_fetch_ytvideoinpl extends tx_scheduler_Task {

	 

	/**
	 * Function executed from the Scheduler.
	 * Hides all content elements of a page
	 *
	 * @return	boolean	TRUE if success, otherwise FALSE
	 */
	public function execute() {
		$success = FALSE;
		
               
                $pid = $this->pid;
                $limit = $this->limit;
                $dbug = $this->dbug;
                
                
                $tstamp = mktime(0,0,0);
                 $trow = array();
                 
                $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                        'uid,title,tx_tsara_youtubeid',
                        'tt_news_cat',
                        'pid='.$pid.' AND tx_tsara_youtubeid !=\'\' AND deleted=0 AND tx_tsara_tstamp != '.$tstamp,
                        '',
                        'uid ASC',
                        $limit 
                        );
                
                if( count( $row ) > 0 ){
                    foreach( $row as $ligne ){
                       
                       $uid_foreign = $ligne['uid'];
                        
                       $data = $this->get_playliste ( $ligne['tx_tsara_youtubeid'] );
                       
                       $totalitems = $data['totalItems'];
                       /*print_r( $data );
                       exit;
                       */
                       $pages = ceil( $totalitems / 50);
                       
                        
                       
                       for($i=0;$i<$pages;$i++){
                              $trow[] = $this->get_items($ligne['tx_tsara_youtubeid'], ($i*50) ); 
                       };
                       
                       $trow = $this->flatten_array( $trow );
                       if( $dbug == 1 ) print_r( $trow );
                       if( count( $trow ) > 0 ){
                           
                           
                           foreach( $trow as $ligne2 ){
                               
                               
                               //Chercher uid dans tt_news
                               $uid_local = $this->get_uidnews( $ligne2 );
							   
							   
                               
                               if( $uid_local['uid'] > 0 ){
                                   //inserer dans tt_news_cat_mm7
                                    /*$nb = $GLOBALS['TYPO3_DB']->exec_SELECTcountRows('uid_local','tt_news_cat_mm','1=1 AND tt_news_cat_mm.uid_local = '.$GLOBALS['TYPO3_DB']->fullQuoteStr( $uid_local['uid'], 'tt_news_cat_mm' ).' AND tt_news_cat_mm.uid_foreign = '.$GLOBALS['TYPO3_DB']->fullQuoteStr( $uid_foreign, 'tt_news_cat_mm' ));
                                    */
					$GLOBALS['TYPO3_DB']->exec_DELETEquery('tt_news_cat_mm','uid_local = '.$GLOBALS['TYPO3_DB']->fullQuoteStr( $uid_local['uid'], 'tt_news_cat_mm' ) );
                                        if( !$dbug || $dbug != 1 ){
$GLOBALS['TYPO3_DB']->exec_INSERTquery(
                                            'tt_news_cat_mm',
                                            array(
                                                'uid_local' => $uid_local['uid']
                                                ,'uid_foreign' => $uid_foreign
                                            )
                                        );
}
if( $dbug == 1 ){
                                        echo $GLOBALS['TYPO3_DB']->INSERTquery(
                                            'tt_news_cat_mm',
                                            array(
                                                'uid_local' => $uid_local['uid']
                                                ,'uid_foreign' => $uid_foreign
                                            )
                                        )."\n";
}
                                    
                                    
                                    //Forcer le pid
                                    if( $uid_local['pid'] != $pid && $this->forcepid == 1 ){
                                        $sqlu = $GLOBALS['TYPO3_DB']->UPDATEquery(
                                                'tt_news',
                                                'uid = '.$uid_local['uid']
                                                ,array(
                                                    'pid' => $pid
                                                ));
                                        if(  !$dbug || $dbug != 1 ) $GLOBALS['TYPO3_DB']->sql_query( $sqlu );
if( $dbug == 1 ) echo $sqlu."\n";
                                    }
                               }
                               
                           }
                                   
                       }
					   
					   unset($trow);
                       
                        
                        


                                           $sqlu = $GLOBALS['TYPO3_DB']->UPDATEquery(
                                            'tt_news_cat',
                                            'uid = '.$ligne['uid'],
                                            array(
                                                'tx_tsara_tstamp' => $tstamp 
                                            )
                                        );
                                           
                                          $GLOBALS['TYPO3_DB']->sql_query($sqlu);


                    }
					unset($row );
                }
 

                if( $dbug == 1 )  exit;
                $success = true;
		return $success;
	}
	function flatten_array($array, $preserve_keys = 0, &$out = array()) {
            # Flatten a multidimensional array to one dimension, optionally preserving keys.
            #
            # $array - the array to flatten
            # $preserve_keys - 0 (default) to not preserve keys, 1 to preserve string keys only, 2 to preserve all keys
            # $out - internal use argument for recursion
            foreach($array as $key => $child)
                if(is_array($child))
                    $out = $this->flatten_array($child, $preserve_keys, $out);
                elseif($preserve_keys + is_string($key) > 1)
                    $out[$key] = $child;
                else
                    $out[] = $child;
            return $out;
        }
	function get_playliste( $id  ){
            $tligne = array();
            if( $id == '' ) return '';

            $id = str_replace('PL','',$id);

            $url = 'http://gdata.youtube.com/feeds/api/playlists/'.$id.'?v=2&max-results=50&orderby=published&alt=jsonc';
            $data = t3lib_div::getUrl( $url );
            $data = json_decode( $data , 1 );

            return $data['data'];


        }
        
        function get_items($id, $page = 0 ){
            $tliste = array();
            
            $url = 'http://gdata.youtube.com/feeds/api/playlists/'.$id.'?v=2&max-results=50&orderby=published&alt=jsonc';
            if( $page > 0 ){
                $url .= '&start-index='.$page;
            }
            $data = t3lib_div::getUrl( $url );
            $data = json_decode( $data , 1 );
            
             
            //Boucle
            foreach( $data['data']['items'] as $row ){
                $tliste[] = $row['video']['id'];
            }
            
            return ( $tliste );
        }
        
        function get_uidnews( $ytid ){
            $count = $GLOBALS['TYPO3_DB']->exec_SELECTcountRows('uid','tt_news','1=1  AND tt_news.tx_tsara_youtubeid LIKE '.$GLOBALS['TYPO3_DB']->fullQuoteStr( $ytid, 'tt_news' ) );
        
            if( $count > 0 ){
                $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow('pid,uid','tt_news','1=1  AND tt_news.tx_tsara_youtubeid LIKE '.$GLOBALS['TYPO3_DB']->fullQuoteStr( $ytid, 'tt_news' ) );
                return $row;
                
            }
            
            return '0';
        }
        
         	
        
 
        
 

}

if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/tsara/scheduler/class.tx_tsara_fetchsite_inovaovao.php'])	{
	include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/tsara/scheduler/class.tx_tsara_fetchsite_inovaovao.php']);
}

?>