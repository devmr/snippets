<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2009 Steffen Müller (typo3@t3node.com)
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

/**
 * Class "tx_smscheddemo_HideContent" provides task procedures
 *
 * @author		Steffen Müller <typo3@t3node.com>
 * @package		TYPO3
 * @subpackage		tx_smscheddemo
 *
 */
require_once(PATH_site.'fileadmin/batch/imap.inc.php');
require_once(PATH_site.'fileadmin/batch/mimedecode.inc.php');

require_once(t3lib_extMgm::extPath("tsara").'lib/class.tx_tsara_util.php');
require_once t3lib_extMgm::extPath("tsara").'lib/class.datetimeconverter.php';

class tx_tsara_fetch_ytmaj extends tx_scheduler_Task {

	 

	/**
	 * Function executed from the Scheduler.
	 * Hides all content elements of a page
	 *
	 * @return	boolean	TRUE if success, otherwise FALSE
	 */
	public function execute() {
		$success = FALSE;
                
                $tlog = array();
		
                $user = $this->user;
                $pid = $this->pid;
                $limit = $this->limit;
                $subjectmail = $this->subjectmail;
                $parent_category = $this->parent_category;
                $to = $this->to;
                $sendmail = intval($this->sendmail);
                
                $this->datetimeconverter = t3lib_div::makeInstance('Date_Time_Converter');
                
                $tligne = $datacomm = array();
                $tstamp = mktime(0,0,0);
                
                $error = 0;
                 
                //Fetch mail in mailbox
                $imap = new IMAPMAIL;
                //$tlog[] = 'Ligne '.__LINE__.' - Connexion ';
                //if(!$imap->open("mail.sarimihetsika.com","143")) 
                //if(!$imap->open("ml.madatsara.com","143")) 
                if(!$imap->open("imap.free.fr","143")) 
                { 
                    $tlog[] = 'Ligne '.__LINE__.' - Connexion impossible ';
                    t3lib_div::writeFile(PATH_site.'typo3temp/logstatus_ytmaj.txt',  implode("\n", $tlog ) );
                    return false;
                }
                //$tlog[] = 'Ligne '.__LINE__.' - Login pwd ';
                //if( !$imap->login("youtube@sarimihetsika.com","y0utu63") ){
                //if( !$imap->login("youtube@madatsara.com","y0utu63t54r4") ){
                if( !$imap->login("youtubetsara","4xntmj60") ){
                    $tlog[] = 'Ligne '.__LINE__.' - '.$imap->error;
                    t3lib_div::writeFile(PATH_site.'typo3temp/logstatus_ytmaj.txt',  implode("\n", $tlog ) );
                    return false;
                }
                //$tlog[] = 'Ligne '.__LINE__.' - Open mailbox ';
                if( !$response=$imap->open_mailbox("INBOX") ){
                    $tlog[] = 'Ligne '.__LINE__.' - '.$imap->error;
                    t3lib_div::writeFile(PATH_site.'typo3temp/logstatus_ytmaj.txt',  implode("\n", $tlog ) );
                    return false;
                }
                $dontupdate = 0;
                //$tlog[] = 'Ligne '.__LINE__.' - Get nb de messages ';
                $nresponse = $imap->get_msglist();
                $tlog[] = 'Ligne '.__LINE__.' -  '.$nresponse.' message(s) dans la BAL';
                t3lib_div::writeFile(PATH_site.'typo3temp/logstatus_ytmaj.txt',  implode("\n", $tlog ) );
                if( $nresponse > 0 ){
                    $tlog[] = 'Ligne '.__LINE__.' - Get message 1 ';
                    $response = $imap->get_message(1);
                    //$tlog[] = 'Ligne '.__LINE__.' - Decode message 1 ';
                    $mimedecoder = new MIMEDECODE($response,"\r\n");
                    $msg = $mimedecoder->decode(); 
                    //$tlog[] = 'Ligne '.__LINE__.' - Get message 1 ';
                    $subject = imap_mime_header_decode($msg->headers['subject'] );
                    $subject = $subject[0]->text;
                    $tlog[] = 'Ligne '.__LINE__.' - objet : '.$subject ;
                    
                    if( preg_match('/[Comment posted on|Reply from]/', $subject ) ){
                        
                        $tlog[] = 'Ligne '.__LINE__.' - objet : '.$subject ;
                        $bodytext = $msg->parts[0]->body;
						$tlog[] = 'Ligne '.__LINE__.' - Corps du message : '.$bodytext ;
                        $idvideo = substr($bodytext,strpos($bodytext,'http://www.youtube.com/watch?v=')+strlen('http://www.youtube.com/watch?v='));
                        $idvideo = substr($idvideo,0,strpos($idvideo,'&'));
                        
                        $tlog[] = 'Ligne '.__LINE__.' - IDVIDEO : '.$idvideo;
                        
                        t3lib_div::writeFile(PATH_site.'typo3temp/logstatus_ytmaj.txt',  implode("\n", $tlog ) );

                        $data = $this->get_detail( $idvideo );
                        $row_artistes = $this->get_artistes( $data['media$group']['media$title']['$t'] );
                        $title = $data['title']['$t'];
                        $duration = $data['media$group']['yt$duration']['seconds'];
                        //$published = $this->datetimeconverter->_date_to_timestamp( $this->convertdate( $data['published']['$t'] ), 'Y-m-d H:i:s' );
                        $publishedo = $data['published']['$t'];
                        $numLikes = $data['yt$rating']['numLikes'];
                        $numDislikes = $data['yt$rating']['numDislikes'];
                        $favoriteCount = $data['yt$statistics']['favoriteCount'];
                        $viewCount = $data['yt$statistics']['viewCount'];
                        $nbcomments = $data['gd$comments']['gd$feedLink']['countHint'];
                        
                        $tlog[] = 'Ligne '.__LINE__.' - Nombre de commentaires : '.$nbcomments;
                        //Ajout
                        $sqli = $GLOBALS['TYPO3_DB']->INSERTquery(
                        'tt_news'
                        ,array(
                                'pid' => $this->pid
                                ,'title' => $this->get_title( $title )
                                ,'tstamp' => time()
                                ,'crdate' => time()
                                ,'tx_tsara_youtubeid' => $idvideo
                                ,'tx_tsara_youtube_thumbnail' => $data['media$group']['media$thumbnail'][0]['url']
                                ,'tx_tsara_youtube_duration' => $duration
                                ,'tx_tsara_youtube_viewcount' => $viewCount
                                ,'tx_tsara_youtube_favoritecount' => $favoriteCount
                                ,'tx_tsara_youtube_published' =>  $publishedo
                        )
                        );
                        $sqlu = $GLOBALS['TYPO3_DB']->UPDATEquery(
                            'tt_news'
                            ,'tx_tsara_youtubeid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr( $idvideo, 'tt_news' )
                            ,array(
                                     'title' => $this->get_title( $title )
                                    ,'tx_tsara_youtube_thumbnail' => $data['media$group']['media$thumbnail'][0]['url']
                                    ,'tx_tsara_youtube_duration' => $duration
                                    ,'tx_tsara_youtube_viewcount' => $viewCount
                                    ,'tx_tsara_youtube_favoritecount' => $favoriteCount
                                    ,'tx_tsara_youtube_published' => $publishedo
                            )
                        );

                        $count = $GLOBALS['TYPO3_DB']->exec_SELECTcountRows('uid','tt_news','1=1 AND tt_news.tx_tsara_youtubeid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr( $idvideo, 'tt_news' ).' AND tt_news.pid = '.$this->pid);
                        $tlog[] = 'Ligne '.__LINE__.' - Video deja existante '.($count>0?'oui':'non');
                        if( $count == 0 && trim($this->get_title( $title ))!='' ) {
                            $GLOBALS['TYPO3_DB']->sql_query( $sqli ); 
                            $uid_local = $GLOBALS['TYPO3_DB']->sql_insert_id(); 
                            $n++;
                        }
                        else{
                             
                             $GLOBALS['TYPO3_DB']->sql_query( $sqlu );
                            $row_uidlocal = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow('uid','tt_news','tx_tsara_youtubeid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr( $idvideo, 'tt_news' ));
                            $uid_local = $row_uidlocal['uid']; 
                        }
                        $tlog[] = 'Ligne '.__LINE__.' - UID video : '.$uid_local;
                        
                        foreach( $row_artistes as $artist ){
                            //Si a un uid
                            if( t3lib_div::intval_positive( $artist['uid'] ) ){
                                $uid_foreign = $artist['uid'];
                            }
                            //Sinon insere dans la table ttnews_cat
                            else{
                                $tlog[] = 'Ligne '.__LINE__.' - Ajout artiste non existant : '.$artist['name'];
                                $sqli = $GLOBALS['TYPO3_DB']->INSERTquery(
                                    'tt_news_cat'
                                    ,array(
                                            'pid' => 138
                                            ,'parent_category' => $parent_category
                                            ,'title' => $artist['name']
                                            ,'tstamp' => time()
                                            ,'crdate' => time()

                                    )
                                 );
                                $GLOBALS['TYPO3_DB']->sql_query( $sqli );
                                $uid_foreign = $GLOBALS['TYPO3_DB']->sql_insert_id();                        
                            }

                            $counta = $GLOBALS['TYPO3_DB']->exec_SELECTcountRows('*','tt_news_cat_mm','1=1 AND uid_local = '.$GLOBALS['TYPO3_DB']->fullQuoteStr( $uid_local, 'tt_news' ).' AND uid_foreign = '.$GLOBALS['TYPO3_DB']->fullQuoteStr( $uid_foreign, 'tt_news' ) );
                             
                            //inserer dans tt_news_cat_mm
                            if( $counta == 0 ){
                                $sqli = $GLOBALS['TYPO3_DB']->INSERTquery(
                                    'tt_news_cat_mm',
                                    array(
                                        'uid_local' => $uid_local
                                        ,'uid_foreign' => $uid_foreign
                                    )
                                );
                                                               
                                $GLOBALS['TYPO3_DB']->sql_query( $sqli );
                            }
                             

                        }
                        
                        
                    
                        
                        if( in_array(strip_tags( $this->get_detail( $idvideo, 1 ) ),array('yt:quotatoo_many_recent_calls','GDataResourceNotFoundExceptionVideo not found' ))) {
                            $contentyt =strip_tags( $this->get_detail( $idvideo, 1 ) );

                            if( $contentyt == 'GDataResourceNotFoundExceptionVideo not found' ){
                                 $tlog[] = 'Ligne '.__LINE__.' - Erreur Youtube GDataResourceNotFoundExceptionVideo not found ' ;

                            }
                            $dontupdate = 1;
                        }
                        if( $nbcomments > 0 ){
                            $pages = ceil( $nbcomments / 50);
                            $tlog[] = 'Ligne '.__LINE__.' - Nb de pages (commentaires) : '.$pages ;

                            for($i=0;$i<$pages;$i++){
                                $datacomm[$uid_local][] = $this->get_commentaires($idvideo , ($i*50) );
                            };

                        }
                       
                       
                       if($dontupdate == 0 && count( $datacomm[$uid_local] ) > 0 ){
                           $nboldcom = $GLOBALS['TYPO3_DB']->exec_SELECTcountRows(
                                   'uid',
								   'tx_comments_comments',
                                   'external_ref = '.$GLOBALS['TYPO3_DB']->fullQuoteStr('tt_news_'.$uid_local,'tx_comments_comments')
                                   );
                           //DELETE all comment in tx_comments_comments
                            $sqld = $GLOBALS['TYPO3_DB']->DELETEquery(
                                   'tx_comments_comments',
                                   'external_ref = '.$GLOBALS['TYPO3_DB']->fullQuoteStr('tt_news_'.$uid_local,'tx_comments_comments')
                                   );
                           if($dontupdate == 0 )  $GLOBALS['TYPO3_DB']->sql_query( $sqld );

                           foreach( $datacomm[$uid_local] as $commentaires ){
						   
								$commentaires = array_reverse( $commentaires );

                               foreach( $commentaires as $commrow ){

                                   //Insert comment in tx_comments_comments
                                    $sqli = $GLOBALS['TYPO3_DB']->INSERTquery(
                                            'tx_comments_comments' 
                                            ,array(
                                                'pid' => $this->pid
                                                ,'approved' => 1
                                                ,'external_prefix' => 'tx_ttnews'
                                                ,'external_ref' => 'tt_news_'.$uid_local
                                                ,'firstname' => $commrow['author']
                                                ,'content' => $commrow['content']
                                                ,'remote_addr' => '127.0.0.1'
                                                ,'tstamp' => 'UNIX_TIMESTAMP("'.$commrow['published'].'")'
                                                ,'crdate' => 'UNIX_TIMESTAMP("'.$commrow['published'].'")'
                                                ,'tx_tsara_youtube_published' =>  $commrow['published'] 
                                            )
                                            );
                                   $GLOBALS['TYPO3_DB']->sql_query( $sqli );
                                   
                                   //echo $sqli."\n";

                               }

                           }
						   
                           $GLOBALS['TYPO3_DB']->sql_query( 'UPDATE tx_comments_comments SET crdate = UNIX_TIMESTAMP(tx_tsara_youtube_published), tstamp = UNIX_TIMESTAMP(tx_tsara_youtube_published) WHERE tx_tsara_youtube_published > 0' );

                       }
                       
                       
                         
                    }
                    
                    $GLOBALS['TYPO3_DB']->sql_query( "UPDATE `tt_news` SET datetime =  UNIX_TIMESTAMP(`tx_tsara_youtube_published`) WHERE `pid` = ".$pid );
                     $GLOBALS['TYPO3_DB']->sql_query( "UPDATE tt_news SET tx_tsara_artistecount = (SELECT COUNT(*) FROM tt_news_cat_mm LEFT JOIN tt_news_cat ON tt_news_cat.uid = tt_news_cat_mm.uid_foreign where tt_news_cat.parent_category = ".$parent_category." AND tt_news_cat_mm.uid_local = tt_news.uid AND tt_news.pid = ".$pid.")");
                     $tlog[] = 'Ligne '.__LINE__.' - Suppression du msg dans IMAP ' ;
                     $imap->delete_message(1);
                     $imap->close();
                     
                     //Envoi mail
                     $mail = t3lib_div::makeInstance('t3lib_mail_Message');
                    $mail->setTo(array('madatsara@gmail.com'))
                        ->setFrom(array($GLOBALS['TYPO3_CONF_VARS']['MAIL']['defaultMailFromAddress'] => $GLOBALS['TYPO3_CONF_VARS']['MAIL']['defaultMailFromName'] ))
                        ->setSubject( 'MADATSARA CRON Youtube - '.$this->get_title( $title ).' le '.date('d-m-Y H:i:s') )
                        ->setCharset('utf-8');
                    $mail->setBody( nl2br( implode("\n",$tlog ) ) , 'text/html');
                    $mail->send();
                    
                    /*$this->util = t3lib_div::makeInstance('tx_tsara_util');
                    $this->util->clear_cache();
                    */ 
                }
                  
                $success = true;
		return $success;
	}
	function conv_tstamp( $str ){
            $y = $m = $d = $h = $i = $s = 0;
            //2009-06-01T21:54:40.000Z
            $str = str_replace('T','_',$str );
            $tabdate = t3lib_div::trimExplode('.',$str);
            if( count( $tabdate ) == 2){
                $date = $tabdate[0]; //2009-06-01_21:54:40
                unset($tabdate);
                $tabdate = t3lib_div::trimExplode('_',$date);
                if( count( $tabdate ) == 2 ){
                    $ymd = $tabdate[0];
                    $hms = $tabdate[1];
                    unset($tabdate);
                    $tabdate = t3lib_div::trimExplode('-',$ymd);
                    if( count( $tabdate ) == 3 ){
                        $y = intval( $tabdate[0] );
                        $m = intval( $tabdate[1] );
                        $d = intval( $tabdate[1] );
                    }
                    unset($tabdate);
                    $tabdate = t3lib_div::trimExplode(':',$his);
                    if( count( $tabdate ) == 3 ){
                        $h = intval( $tabdate[0] );
                        $i = intval( $tabdate[1] );
                        $s = intval( $tabdate[1] );
                    }
                    unset($tabdate);
                }
            }
            return mktime($h,$i,$s,$m,$d,$y);
            
        }
	 function convertdate( $str ){
             $str = str_replace('T',' ',$str);
             $str = str_replace('.000Z','',$str);
             return trim( $str );
         }
        function get_commentaires( $id, $startindex ){
            $url = 'https://gdata.youtube.com/feeds/api/videos/'.$id.'/comments?v=2&alt=json&max-results=50';

            if( $startindex > 0 ) $url .= '&start-index='.($startindex);

            
            $data = t3lib_div::getUrl( $url );
            $data = json_decode( $data , 1 );
            $boucle = $data['feed']['entry'];
            foreach( $boucle as $row ){
                $tligne[] = array(

                     'author' => $row['author'][0]['name']['$t']
                    ,'content' => $row['content']['$t']
                    ,'published' => $row['published']['$t'] 
                );
            }
            return $tligne;
        }
        function get_title( $str ){
            $texplode = t3lib_div::trimExplode('::',$str);
            return trim( $texplode[0] );
        }
        function get_detail( $id , $out = 0  ){
            $tligne = array();
            if( $id == '' ) return '';



            $url = 'https://gdata.youtube.com/feeds/api/videos/'.$id.'?v=2&alt=json&';
            $data = t3lib_div::getUrl( $url );
            if( $out == 1 ) return $data;
            $data = json_decode( $data , 1 );

            return $data['entry'];


        }
        function get_artistes( $str ){
            $data = array();
            $uid = 0;
            $tx_tsara_youtubeid = '';

            $texplode = t3lib_div::trimExplode('::',$str);
            $ta = trim( $texplode[1] );
            $taba = t3lib_div::trimExplode(',',$ta);
            foreach( $taba as $artiste ){
                //get playliste of artiste
                $nb = $GLOBALS['TYPO3_DB']->exec_SELECTcountRows('uid','tt_news_cat','1=1 AND pid='.$this->pid.' AND title LIKE '.$GLOBALS['TYPO3_DB']->fullQuoteStr('%'.$artiste.'%','tt_news_cat'));
                if( $nb > 0 ){

                    $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows('uid,tx_tsara_youtubeid','tt_news_cat','1=1 AND pid='.$this->pid.' AND title LIKE '.$GLOBALS['TYPO3_DB']->fullQuoteStr('%'.$artiste.'%','tt_news_cat'),'','',1);
                    $uid = $row[0]['uid'];
                    $tx_tsara_youtubeid = $row[0]['tx_tsara_youtubeid'];

                }

                $data[] = array(
                    'name' => $artiste
                    ,'uid' => $uid
                    ,'tx_tsara_youtubeid' => $tx_tsara_youtubeid
                );
            }

            return $data;   
            } 	
        
 
        
 

}

if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/tsara/scheduler/class.tx_tsara_fetchsite_inovaovao.php'])	{
	include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/tsara/scheduler/class.tx_tsara_fetchsite_inovaovao.php']);
}

?>