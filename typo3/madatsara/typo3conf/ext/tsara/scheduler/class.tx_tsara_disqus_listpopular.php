<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2009 Steffen Müller (typo3@t3node.com)
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

/**
 * Class "tx_smscheddemo_HideContent" provides task procedures
 *
 * @author		Steffen Müller <typo3@t3node.com>
 * @package		TYPO3
 * @subpackage		tx_smscheddemo
 *
 */
class tx_tsara_disqus_listpopular extends tx_scheduler_Task {

	 

	/**
	 * Function executed from the Scheduler.
	 * Hides all content elements of a page
	 *
	 * @return	boolean	TRUE if success, otherwise FALSE
	 */
	public function execute() {
		$success = FALSE;
		$data = array();
		$tlog = array();
		
		$this->confArr = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['tsara']);
                //Get 
                $urldisqus = 'https://disqus.com/api/3.0/threads/listPopular.json?api_secret=%s&forum=madatsara';
                
                $urldisqus = sprintf( $urldisqus, $this->confArr['disqus_apisecret'] );
                
                $content = t3lib_div::getUrl( $urldisqus );
                
                //t3lib_div::writeFile(PATH_site.'typo3temp/sch_'.__FUNCTION__.'.txt','Cron executé le '.date('d-m-Y H:i:s').' à partir du fichier '.__FILE__.' Dans la fonction '.__FUNCTION__.' à la ligne '.__LINE__.' de la classe '.__CLASS__);
                
                
                $data = json_decode( $content, 1);
                $tlog[] = 'Code return '.$data['code'];
                
                if( $data['code'] == 0 ){
                    $response = (array) $data['response'];
                    $tlog[] = 'Response count '.count( $response );
                    foreach( $response as $ligne ){
                        $slug = $ligne['slug'];
                        $nbposts = $ligne['posts'];
                        
                        //Regarder l'UID correspondant au slug dans la table ttnews
                        $count = $GLOBALS['TYPO3_DB']->exec_SELECTcountRows('uid','tt_news','1=1 AND tx_tsarattnews_slug = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($slug,'tt_news'));
                         
                        
                        //Si UID trouve maj table ttnews avec nbpost
                        if( t3lib_div::intval_positive( $count ) ){
                            $tlog[] = 'SQL '.$GLOBALS['TYPO3_DB']->UPDATEquery('tt_news','tx_tsarattnews_slug = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($slug,'tt_news'),array('tx_tsarattnews_nbcomments' => $nbposts));;
                            $GLOBALS['TYPO3_DB']->exec_UPDATEquery('tt_news','tx_tsarattnews_slug = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($slug,'tt_news'),array('tx_tsarattnews_nbcomments' => $nbposts));
                        }
                        
                    }
                }
                
                t3lib_div::writeFile(PATH_site.'typo3temp/sch_'.str_replace('_','',__CLASS__).'.txt', implode("\n", $tlog ) );
                
		return $success;
	}

}

if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/tsara/scheduler/class.tx_tsara_disqus_listpopular.php'])	{
	include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/tsara/scheduler/class.tx_tsara_disqus_listpopular.php']);
}

?>
