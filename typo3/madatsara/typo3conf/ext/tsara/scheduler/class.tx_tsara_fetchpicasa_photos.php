<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2009 Steffen M?ller (typo3@t3node.com)
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

/**
 * Class "tx_smscheddemo_HideContent" provides task procedures
 *
 * @author		Steffen M?ller <typo3@t3node.com>
 * @package		TYPO3
 * @subpackage		tx_smscheddemo
 *
 */
require_once(t3lib_extMgm::extPath("tsara").'lib/class.tx_tsara_util.php');
require_once(t3lib_extMgm::extPath("tsara").'lib/encoding.php');
class tx_tsara_fetchpicasa_photos extends tx_scheduler_Task {

	 

	/**
	 * Function executed from the Scheduler.
	 * Hides all content elements of a page
	 *
	 * @return	boolean	TRUE if success, otherwise FALSE
	 */
	public function execute() {
		$success = FALSE;
		
                $user = $this->user;
                $json = $this->json;
                $pid = $this->pid;
                
                $tlog = array();
                
                //Select ttnews
                $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                        'uid,tx_tsara_youtubeid'
                        ,'tt_news'
                        ,'1=1 AND deleted = 0 AND tx_tsara_youtube_viewcount = 0 AND pid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($pid,'tt_news'),
                        '',
                        'uid ASC',
                        '3'
                        );
                
                $tlog[] = 'Ligne '.__LINE__.' - '.$GLOBALS['TYPO3_DB']->SELECTquery(
                        'uid,tx_tsara_youtubeid'
                        ,'tt_news'
                        ,'1=1 AND deleted = 0 AND tx_tsara_youtube_viewcount = 0 AND pid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($pid,'tt_news'),
                        '',
                        'uid ASC',
                        '3'
                        );
                
                $nb = $GLOBALS['TYPO3_DB']->exec_SELECTcountRows(
                        'uid'
                        ,'tt_news'
                        ,'1=1 AND deleted = 0 AND tx_tsara_youtube_viewcount = 0 AND pid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($pid,'tt_news')                        
                       );
                
                $tlog[] = 'Ligne '.__LINE__.' - '.$nb;
                    
                
                if( $nb > 0 ){
                    foreach( $row as $ligne ){
                        
                        $id = $ligne['tx_tsara_youtubeid'];
                        $uid = $ligne['uid'];
                        
                        //DELETE ALL tx_tsara_videosfilm in $pid and $uid
                        $sqld = $GLOBALS['TYPO3_DB']->DELETEquery('tx_tsara_videosfilm','tx_tsara_cine_videosfilm = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($uid,'tx_tsara_videosfilm').' AND pid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($pid,'tx_tsara_videosfilm'));
                        $GLOBALS['TYPO3_DB']->sql_query( $sqld );
                
                        $tlog[] = 'Ligne '.__LINE__.' - '.$sqld;
                        
                        $jsinner = json_decode( t3lib_div::getUrl( sprintf('http://picasaweb.google.com/data/feed/base/user/'.$user.'/albumid/%s?alt=json&hl=en_US',$id) ), true ) ;
					
                        $j = 0;
                        foreach( $jsinner['feed']['entry'] as $liste ){

                                try{
                                    /*
                                    $tliste[$id][] = array(
                                                    'published' => $liste['published']['$t'],
                                                    'updated' => $liste['updated']['$t'],
                                                    'title' => $liste['title']['$t'],
                                                    'content' => $liste['content']['src'],
                                                    'thumbnail' => $this->replacethumbnail( $liste['media$group']['media$thumbnail'][2]['url'] )
                                    );
                                    */
                                    //Monter en BDD
                                    $sqli = $GLOBALS['TYPO3_DB']->INSERTquery(
                                            'tx_tsara_videosfilm',
                                            array(
                                                'pid' => $pid
                                                ,'title' => $liste['title']['$t']
                                                ,'description' => $liste['content']['src']
                                                ,'tstamp' => time()
                                                ,'typecompte' => 'extrait'
                                                ,'crdate' => time()
                                                ,'url' => $this->replacethumbnail( $liste['media$group']['media$thumbnail'][2]['url'] )
                                                ,'tx_tsara_cine_videosfilm' => $uid 
                                                //,'tx_tsara_youtube_viewcount' => $jsinner['feed']['openSearch$totalResults']['$t'] 
                                            ));
                                    $GLOBALS['TYPO3_DB']->sql_query( $sqli );
                                    $tlog[] = 'Ligne '.__LINE__.' - '.$sqli;
                                                 
                                }
                                catch(Exception $e){
                                                 
                                }
                                                $j++;
                        }
                        
                        $sqlu = $GLOBALS['TYPO3_DB']->UPDATEquery(
                                            'tt_news',
                                            'uid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($uid,'tt_news').' AND pid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($pid,'tt_news')
                                            ,array( 
                                                'tx_tsara_youtube_viewcount' => $jsinner['feed']['openSearch$totalResults']['$t']
                                                ,'tx_tsara_cine_videosfilm' =>  $jsinner['feed']['openSearch$totalResults']['$t']
                                            ));
                        $GLOBALS['TYPO3_DB']->sql_query( $sqlu );
                        $tlog[] = 'Ligne '.__LINE__.' - '.$sqlu;
                    }
                }
                
                t3lib_div::writeFile(PATH_site.'typo3temp/log_'.__CLASS__.'.txt', implode("\n",$tlog ) );
                
                 
             
                 
                $success = true;
		return $success;
	}
        function replacethumbnail( $src ){
		if( $this->sizethumbnails == '' ) $this->sizethumbnails = 's300-c';
		$src = str_replace('s160-c',$this->sizethumbnails,$src);
		return $src;
	}
	
	 
        
         	
        
 
        
 

}

if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/tsara/scheduler/class.tx_tsara_fetchpicasa_photos.php'])	{
	include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/tsara/scheduler/class.tx_tsara_fetchpicasa_photos.php']);
}

?>