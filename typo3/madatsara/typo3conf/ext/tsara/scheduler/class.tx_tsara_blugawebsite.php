<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2009 Steffen Müller (typo3@t3node.com)
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

/**
 * Class "tx_smscheddemo_HideContent" provides task procedures
 *
 * @author		Steffen Müller <typo3@t3node.com>
 * @package		TYPO3
 * @subpackage		tx_smscheddemo
 *
 */
require_once(PATH_typo3conf.'ext/tsara/lib/class.tx_tsara_util.php');

class tx_tsara_blugawebsite extends tx_scheduler_Task {

	 

	/**
	 * Function executed from the Scheduler.
	 * Hides all content elements of a page
	 *
	 * @return	boolean	TRUE if success, otherwise FALSE
	 */
	public function execute() {
		$success = FALSE;
		$data = array();
		$tlog = array();
		
		$this->confArr = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['tsara']);
		$this->util = t3lib_div::makeInstance('tx_tsara_util');
                
                $cs = t3lib_div::makeInstance('t3lib_cs');
                $fileFunc = t3lib_div::makeInstance('t3lib_basicFileFunctions');
                //Bluga
                require_once t3lib_extMgm::extPath('tsara', 'lib/bluga/config.php');
                
                if( !is_dir(PATH_site.'typo3temp/bluga/' ) ) mkdir(PATH_site.'typo3temp/bluga/',777);
                
                $home = getenv('HOME');
                if (file_exists("$home/.webthumb.php")) {
                        include "$home/.webthumb.php";
                }
                
                $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                        'ext_url,uid',
                        'tt_news',
                        '1=1 AND pid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($this->confArr['pidsites'],'tt_news').' AND deleted = 0 AND hidden = 0 AND image=""'
                        ,''
                        ,''
                        ,'3'
                        );
                $tlog[] = $GLOBALS['TYPO3_DB']->SELECTquery(
                        'ext_url,uid',
                        'tt_news',
                        '1=1 AND pid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($this->confArr['pidsites'],'tt_news').' AND deleted = 0 AND hidden = 0 AND image=""'
                        ,''
                        ,''
                        ,'3'
                        );
                $charset = 'utf-8';
                $space = '_';
                
                if( count( $row ) > 0 ){
                    foreach( $row as $ligne ){
                        
                        $url = $ligne['ext_url'];
                        $uid = $ligne['uid'];
                        
                        $title = $cs->conv_case($charset, str_replace('http://','',str_replace('/','',str_replace('.','',$url))), 'toLower'  );
                        
                        $title = strip_tags($title);
                        $title = preg_replace('/[ \-+_]+/', $space, $title);
                        $title = $cs->specCharsToASCII($charset, $title);
                        $title = preg_replace('/[^a-zA-Z0-9' . ($space ? preg_quote($space) : '') . ']/', '', $title);
                        $title = preg_replace('/\\' . $space . '{2,}/', $space, $title); // Convert multiple 'spaces' to a single one
                        $title = trim($title, $space);
                        $fname = $title.'.jpg';

                        try {
                            $webthumb = new Bluga_Webthumb();
                            $webthumb->setApiKey($this->confArr['bluga_api']);
                            $job = $webthumb->addUrl($url,$THUMB_SIZE, 1024, 768);
                            $tlog[] = 'Line '.__LINE__.' - '.$THUMB_SIZE;
                            $webthumb->submitRequests();

                            while (!$webthumb->readyToDownload()) {
                                sleep(5);
                                //echo "Checking Job Status\n";
                                $webthumb->checkJobStatus();
                            } 

                            $webthumb->fetchToFile($job,$fname,null,$OUTPUT_DIR);
                            //$webthumb->fetchToFile($job );
                            foreach(glob($job->status->id.'*') as $file)
                            {
                                //echo "$file\n";
                                $tlog[] =  'Line '.__LINE__.' - '.$file;
                            }



                            $fichier = $fileFunc->cleanFileName( basename( $fname ) );
                            $chemin = PATH_site.'uploads/pics' ;
                            $myfilepath = $this->getUniqueName($fichier, $chemin);
                            $tab = t3lib_div::split_fileref( $myfilepath );
                            $filename  = ($tab['file']!='_01' &&  $tab['fileext'] !=''?$tab['file']:'');
                            if (t3lib_div::upload_copy_move( PATH_site.'typo3temp/bluga/'.$fname  , $myfilepath  ) ) { }
                            
                            //Update
                            $GLOBALS['TYPO3_DB']->exec_UPDATEquery(
                                    'tt_news',
                                    'uid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($uid,'tt_news')
                                    ,array(
                                        'image' => $filename
                                    ) 
                                    
                           );
                            
                            $tlog[] = 'Line '.__LINE__.' - '.$myfilepath;
                            $tlog[] = 'Line '.__LINE__.' - '.$GLOBALS['TYPO3_DB']->UPDATEquery(
                                    'tt_news',
                                    'uid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($uid,'tt_news')
                                    ,array(
                                        'image' => $filename
                                    )
                           );

                        } catch (Exception $e) {
                            //var_dump($e->getMessage());
                        }
                    }
                    $this->util->clear_cache(); 
                }
                
                t3lib_div::writeFile(PATH_site.'typo3temp/log_'.__CLASS__.'.txt', implode("\n",$tlog ) );
                
                
                
                
		return true;
	}
        function getUniqueName($theFile, $theDest, $dontCheckForUnique = 0) {
        $getUniqueNamePrefix = '';
        $maxNumber = 99;
        $uniquePrecision = 6;

        $origFileInfo = t3lib_div::split_fileref($theFile); // Fetches info about path, name, extention of $theFile
        if ($theDest) {
            if ($getUniqueNamePrefix) { // Adds prefix
                    $origFileInfo['file'] = $getUniqueNamePrefix . $origFileInfo['file'];
                    $origFileInfo['filebody'] = $getUniqueNamePrefix . $origFileInfo['filebody'];
            }

                    // Check if the file exists and if not - return the filename...
            $fileInfo = $origFileInfo;
            $theDestFile = $theDest . '/' . $fileInfo['file']; // The destinations file
            if (!file_exists($theDestFile) || $dontCheckForUnique) { // If the file does NOT exist we return this filename
                    return $theDestFile;
            }

                    // Well the filename in its pure form existed. Now we try to append numbers / unique-strings and see if we can find an available filename...
            $theTempFileBody = preg_replace('/_[0-9][0-9]$/', '', $origFileInfo['filebody']); // This removes _xx if appended to the file
            $theOrigExt = $origFileInfo['realFileext'] ? '.' . $origFileInfo['realFileext'] : '';

            for ($a = 1; $a <= ($maxNumber + 1); $a++) {
                    if ($a <= $maxNumber) { // First we try to append numbers
                            $insert = '_' . sprintf('%02d', $a);
                    } else { // .. then we try unique-strings...
                            $insert = '_' . substr(md5(uniqId('')), 0, $uniquePrecision);
                    }
                    $theTestFile = $theTempFileBody . $insert . $theOrigExt;
                    $theDestFile = $theDest . '/' . $theTestFile; // The destinations file
                    if (!file_exists($theDestFile)) { // If the file does NOT exist we return this filename
                            return $theDestFile;
                    }
            }
        }
}

}

if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/tsara/scheduler/class.tx_tsara_blugawebsite.php'])	{
	include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/tsara/scheduler/class.tx_tsara_blugawebsite.php']);
}

?>
