<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of class
 *
 * @author mrabehasy
 */
class tx_tsara_fetch_ytvideoinpl_fields implements tx_scheduler_AdditionalFieldProvider  {

	/**
	 * Field generation.
	 * This method is used to define new fields for adding or editing a task
	 * In this case, it adds a page ID field
	 *
	 * @param	array			$taskInfo: reference to the array containing the info used in the add/edit form
	 * @param	object			$task: when editing, reference to the current task object. Null when adding.
	 * @param	tx_scheduler_Module	$parentObject: reference to the calling object (Scheduler's BE module)
	 * @return	array			Array containg all the information pertaining to the additional fields
	 *					The array is multidimensional, keyed to the task class name and each field's id
	 *					For each field it provides an associative sub-array with the following:
	 *						['code']		=> The HTML code for the field
	 *						['label']		=> The label of the field (possibly localized)
	 *						['cshKey']		=> The CSH key for the field
	 *						['cshLabel']		=> The code of the CSH label
	 */
	public function getAdditionalFields(array &$taskInfo, $task, tx_scheduler_Module $parentObject) {

		
		
		if (empty($taskInfo['pid'])) {
			if ($parentObject->CMD == 'edit') {
				$taskInfo['pid'] = $task->pid;
			} else {
				$taskInfo['pid'] = '';
			}
		}
		if (empty($taskInfo['limit'])) {
			if ($parentObject->CMD == 'edit') {
				$taskInfo['limit'] = $task->limit;
			} else {
				$taskInfo['limit'] = '';
			}
		}
                if (empty($taskInfo['forcepid'])) {
			if ($parentObject->CMD == 'edit') {
				$taskInfo['forcepid'] = $task->forcepid;
			} else {
				$taskInfo['forcepid'] = '0';
			}
		}
if (empty($taskInfo['dbug'])) {
			if ($parentObject->CMD == 'edit') {
				$taskInfo['dbug'] = $task->dbug;
			} else {
				$taskInfo['dbug'] = '0';
			}
		}
		
		 
		
		 
                
                 

			// Generate the additional field
		 $additionalFields = array();
                 
		
		
                 
		
		$fieldID = 'task_pid';
		$fieldCode = '<input type="text" name="tx_scheduler[pid]" id="' . $fieldID . '" value="' . $taskInfo['pid'] . '" size="50" />';
		$additionalFields[$fieldID] = array(
			'code' => $fieldCode,
			'label' => 'PID',
			'cshKey' => 'tsara',
			'cshLabel' => $fieldID
		);
		
		  $fieldID = 'task_forcepid';
		$fieldCode = '<input type="checkbox" name="tx_scheduler[forcepid]" id="' . $fieldID . '" value="1" '.($taskInfo['forcepid']==1?'checked="checked"':'').' />';
		$additionalFields[$fieldID] = array(
			'code' => $fieldCode,
			'label' => 'Forcer le changement du PID si différent ?',
			'cshKey' => 'tsara',
			'cshLabel' => $fieldID
		);
		
		$fieldID = 'task_limit';
		$fieldCode = '<input type="text" name="tx_scheduler[limit]" id="' . $fieldID . '" value="' . $taskInfo['limit'] . '" size="50" />';
		$additionalFields[$fieldID] = array(
			'code' => $fieldCode,
			'label' => 'Nombre d\'enregistrements',
			'cshKey' => 'tsara',
			'cshLabel' => $fieldID
		);

$fieldID = 'task_dbug';
		$fieldCode = '<input type="checkbox" name="tx_scheduler[dbug]" id="' . $fieldID . '" value="1" '.($taskInfo['dbug']==1?'checked="checked"':'').' />';
		$additionalFields[$fieldID] = array(
			'code' => $fieldCode,
			'label' => 'Afficher dbug?',
			'cshKey' => 'tsara',
			'cshLabel' => $fieldID
		);
                 

		return $additionalFields;
	}

	/**
	 * Field validation.
	 * This method checks if page id given in the 'Hide content' specific task is int+
	 * If the task class is not relevant, the method is expected to return true
	 *
	 * @param	array			$submittedData: reference to the array containing the data submitted by the user
	 * @param	tx_scheduler_Module	$parentObject: reference to the calling object (Scheduler's BE module)
	 * @return	boolean			True if validation was ok (or selected class is not relevant), false otherwise
	 */
	public function validateAdditionalFields(array &$submittedData, tx_scheduler_Module $parentObject) {
		 $result = TRUE;

		return $result;
	}

	/**
	 * Store field.
	 * This method is used to save any additional input into the current task object
	 * if the task class matches
	 *
	 * @param	array			$submittedData: array containing the data submitted by the user
	 * @param	tx_scheduler_Task	$task: reference to the current task object
	 * @return	void
	 */
	public function saveAdditionalFields(array $submittedData, tx_scheduler_Task $task) {
            
		
		$task->limit = $submittedData['limit'];
		$task->pid = $submittedData['pid'];
		$task->forcepid = $submittedData['forcepid'];
$task->dbug = $submittedData['dbug'];
	}
}
?>