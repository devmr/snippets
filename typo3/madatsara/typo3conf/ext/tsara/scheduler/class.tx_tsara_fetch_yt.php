<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2009 Steffen M端ller (typo3@t3node.com)
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

/**
 * Class "tx_smscheddemo_HideContent" provides task procedures
 *
 * @author		Steffen M端ller <typo3@t3node.com>
 * @package		TYPO3
 * @subpackage		tx_smscheddemo
 *
 */
require_once(PATH_typo3conf.'ext/tsara/lib/class.tx_tsara_util.php');



class tx_tsara_fetch_yt extends tx_scheduler_Task {

	 

	/**
	 * Function executed from the Scheduler.
	 * Hides all content elements of a page
	 *
	 * @return	boolean	TRUE if success, otherwise FALSE
	 */
	public function execute() {
		$success = FALSE;
		
                $user = $this->user;
                $pid = $this->pid;
                $parent_category = $this->parent_category;
                
                $tligne = array();
                $tlog = $tlogadd = array();
                $tlisteadd = array();
                $maj = 0;
				 
                $this->util = t3lib_div::makeInstance('tx_tsara_util');
                $url = 'http://gdata.youtube.com/feeds/api/videos?max-results=50&alt=json&orderby=published&author='.$user;
                 
                
                $tlog[] = ' URL : <a href="'.$url.'" target="_blank">'.$url.'</a>';
                
                $data = t3lib_div::getUrl( $url );
               // echo $data;exit;
                $data = json_decode( $data , 1 );

                $nbresults = $data['feed']['openSearch$totalResults']['$t'];
                
                $tlog[] = 'Nombre de r&eacute;sultats : '.$nbresults;
                
                $boucle = $data['feed']['entry'];
                
                foreach( $boucle as $row ){
	
                    $id = $row['id']['$t'];
                    $id = str_replace('http://gdata.youtube.com/feeds/api/videos/','',$id);

                    
                    $tligne[] = array(
                            'id' => $id
                            ,'published' => $row['published']['$t']
                            ,'title' => $this->get_title( $row['title']['$t'] )
                            ,'title_yt' => ( $row['title']['$t'] )
                            ,'artistes' => $this->get_artistes( $row['title']['$t']  )
                            ,'thumbnail' => $row['media$group']['media$thumbnail'][0]['url']
                            ,'duration' => $row['media$group']['yt$duration']['seconds']
                            ,'viewCount' => $row['yt$statistics']['viewCount'] 
                            ,'favoriteCount' => $row['yt$statistics']['favoriteCount'] 

                    );


                }
                
                $pages = ceil($nbresults / 50);

                $tlog[] = 'Nombre de pages : '.$pages;

                
                for($i=1;$i<=20;$i++){
                    $page = ($i*50);
                    if( $i > 19  ) $page = 951;

                    $url = 'http://gdata.youtube.com/feeds/api/videos?max-results=50&start-index='.$page.'&alt=json&orderby=published&author='.$user;

                    $tlog[] = ' URL : <a href="'.$url.'" target="_blank">'.$url.'</a>';

                    $data = t3lib_div::getUrl( $url );
                    $data = json_decode( $data , 1 );
                    $boucle = $data['feed']['entry'];

                    foreach( $boucle as $row ){

                        $id = $row['id']['$t'];
                        $id = str_replace('http://gdata.youtube.com/feeds/api/videos/','',$id);


                        $tligne[] = array(
                                'id' => $id
                                ,'published' => $row['published']['$t']
                                ,'title' => $this->get_title( $row['title']['$t'] )
                                ,'title_yt' => ( $row['title']['$t'] )
                                ,'artistes' => $this->get_artistes( $row['title']['$t']  )
                                ,'thumbnail' => $row['media$group']['media$thumbnail'][0]['url']
                                ,'duration' => $row['media$group']['yt$duration']['seconds']
                                ,'viewCount' => $row['yt$statistics']['viewCount'] 
                                ,'favoriteCount' => $row['yt$statistics']['favoriteCount'] 
                        );


                    }

                }
                
                 $n = 0;
                 $tab_artiste = array();
                foreach( $tligne as $row ){

                        $id = $row['id'];
                        $title_yt = trim( $row['title_yt']);
                        $date_yt = trim( $row['published']);
                        $duration_yt = date('H:i', mktime(0,trim( $row['duration'])));
                        $date = DateTime::createFromFormat('Y-m-d\TH:i:s.000\Z', $date_yt);
                        $ytdate = $date->format('d-m-Y à H:i:s');
                        //$artistes = trim( $row['artistes']);
                        
                        
                        unset($tab_artiste);

                        $sqli = $GLOBALS['TYPO3_DB']->INSERTquery(
                        'tt_news'
                        ,array(
                                'pid' => $this->pid
                                ,'title' => $row['title']
                                ,'tstamp' => time()
                                ,'crdate' => time()
                                ,'tx_tsara_youtubeid' => $id
                                ,'tx_tsara_youtube_thumbnail' => $row['thumbnail']
                                ,'tx_tsara_youtube_duration' => $row['duration']
                                ,'tx_tsara_youtube_viewcount' => $row['viewCount']
                                ,'tx_tsara_youtube_favoritecount' => $row['favoriteCount']
                                ,'tx_tsara_youtube_published' =>  $row['published']
                        )
                        );
                        $sqlu = $GLOBALS['TYPO3_DB']->UPDATEquery(
                            'tt_news'
                            ,'tx_tsara_youtubeid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr( $id, 'tt_news' )
                            ,array(
                                     'title' => $row['title']
                                    ,'tx_tsara_youtube_thumbnail' => $row['thumbnail']
                                    ,'tx_tsara_youtube_duration' => $row['duration']
                                    ,'tx_tsara_youtube_viewcount' => $row['viewCount']
                                    ,'tx_tsara_youtube_favoritecount' => $row['favoriteCount']
                                    ,'tx_tsara_youtube_published' => $row['published']
                            )
                        );
                        
                        $ythtml = '<table cellpadding="4" cellspacing="2"><tr><td>Titre :</td><td>'.$title_yt.'</td></tr><tr><td>Publié le :</td><td>'.$ytdate.'</td></tr><tr><td>Durée :</td><td>'.str_replace(':',' mn ',$duration_yt).'</td></tr><tr><td>URL :</td><td><a href="http://youtu.be/'.$id.'" target="_blank">'.$id.'</a></td></tr></table>';

                        $count = $GLOBALS['TYPO3_DB']->exec_SELECTcountRows('uid','tt_news','1=1 AND tt_news.tx_tsara_youtubeid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr( $id, 'tt_news' ).' AND tt_news.pid = '.$this->pid);
                        if( $count == 0 && trim($row['title'])!='' ) {
                            $GLOBALS['TYPO3_DB']->sql_query( $sqli );
                            //$tlisteadd[] = $sqli."\n";
                            $tlog[] = $sqli;
                            $uid_local = $GLOBALS['TYPO3_DB']->sql_insert_id();

                            $n++;
                        }
                        else{
							
							$rowold = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow(
							   'tx_tsara_youtube_viewcount,tx_tsara_youtube_favoritecount',
                               'tt_news',
                               'tx_tsara_youtubeid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr( $id, 'tt_news' )
                               );
							 
							 
							 
				if( $rowold['tx_tsara_youtube_viewcount'] != $row['viewCount'] ){$maj += 1; /*$tlogadd[] = 'MAJ viewcount de '.$rowold['tx_tsara_youtube_viewcount'].' &agrave; '.$row['viewCount'].' de <a href="http://youtu.be/'.$id.'" target="_blank">'.$id.'</a>';*/}				
				if( $rowold['tx_tsara_youtube_favoritecount'] != $row['favoriteCount'] ){$maj += 1;  /*$tlogadd[] = 'MAJ favoritecount de '.$rowold['tx_tsara_youtube_favoritecount'].' &agrave; '.$row['favoriteCount'].' <a href="http://youtu.be/'.$id.'" target="_blank">'.$id.'</a>';*/} 						
							 	
							 
							 
                            $tlog[] = $sqlu;
                             $GLOBALS['TYPO3_DB']->sql_query( $sqlu );
                            $row_uidlocal = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow('uid','tt_news','tx_tsara_youtubeid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr( $id, 'tt_news' ));
                            $uid_local = $row_uidlocal['uid'];
							
							

                        }
                        
                        //Insert dans tt_news_cat_mm
                        $tlog[] = 'Artiste '.json_encode($row['artistes']);
                        
                        $tartiste = array();
                        foreach( $row['artistes'] as $artist ){
                            //Si a un uid
                            if( t3lib_utility_Math::convertToPositiveInteger( $artist['uid'] ) ){
                                $uid_foreign = $artist['uid'];
                            }
                            //Sinon insere dans la table ttnews_cat
                            else{
                                $sqli = $GLOBALS['TYPO3_DB']->INSERTquery(
                                    'tt_news_cat'
                                    ,array(
                                            'pid' => 138
                                            ,'parent_category' => $parent_category
                                            ,'title' => $artist['name']
                                            ,'tstamp' => time()
                                            ,'crdate' => time()

                                    )
                                 );
                                $GLOBALS['TYPO3_DB']->sql_query( $sqli );
                                $uid_foreign = $GLOBALS['TYPO3_DB']->sql_insert_id();                        
                            }

                            $counta = $GLOBALS['TYPO3_DB']->exec_SELECTcountRows('*','tt_news_cat_mm','1=1 AND uid_local = '.$GLOBALS['TYPO3_DB']->fullQuoteStr( $uid_local, 'tt_news' ).' AND uid_foreign = '.$GLOBALS['TYPO3_DB']->fullQuoteStr( $uid_foreign, 'tt_news' ) );
                            $tlog[] = $counta;
                            //inserer dans tt_news_cat_mm
                            if( $counta == 0 ){
                                $sqli = $GLOBALS['TYPO3_DB']->INSERTquery(
                                    'tt_news_cat_mm',
                                    array(
                                        'uid_local' => $uid_local
                                        ,'uid_foreign' => $uid_foreign
                                    )
                                );
                                $tlog[] = $sqli;                                
                                $GLOBALS['TYPO3_DB']->sql_query( $sqli );
                            }
                            
                            
                            $tartiste[] = 'Nom : '.$artist['name'].'(ID : '.$uid_foreign.')';
                            
                             
							$tab_artiste[] = '<tr><td>'.$artist['name'].'</td><td>('.$uid_foreign.')</td></tr>';
							 
                        
							
                            
                        }
						
						$artistehtml = '<table cellpadding="2" cellspacing="2">'.implode(' ',$tab_artiste ).'</table>';
						unset( $tab_artiste );
						if( $count == 0 && trim($row['title'])!='' ) {
                            $tlisteadd[] = '<tr><td valign="top">'.trim($row['title']).'</td><td valign="top">'.$artistehtml.'</td><td valign="top">'.$ythtml.'</td></tr>';
                        }
                        
                        //$tlisteadd[] = 'Artiste(s) '.implode("\n",$tartiste);
                        
                        unset($tartiste);
						
						
						

                    }
					
                    if( $n > 0 ){
                                    $tlogadd[] = 'Vid&eacute;os ajout&eacute;es : '.$n;
                                    $tlogadd[] = 'Liste vid&eacute;os : <table cellpadding="4" cellspacing="1" border="1"><tr><th>Titre</th><th>Artiste(s)</th><th>Info Youtube</th></tr>'.implode(' ', $tlisteadd).'</table>';
                    }
                    else{
                            $tlogadd[] = 'Aucun ajout - Nb MAJ : '.$maj ;
                    }
                    
                     $GLOBALS['TYPO3_DB']->sql_query( "UPDATE `tt_news` SET datetime =  UNIX_TIMESTAMP(`tx_tsara_youtube_published`) WHERE `pid` = ".$pid );
                     $GLOBALS['TYPO3_DB']->sql_query( "UPDATE tt_news SET tx_tsara_artistecount = (SELECT COUNT(*) FROM tt_news_cat_mm LEFT JOIN tt_news_cat ON tt_news_cat.uid = tt_news_cat_mm.uid_foreign where tt_news_cat.parent_category = ".$parent_category." AND tt_news_cat_mm.uid_local = tt_news.uid AND tt_news.pid = ".$pid.")");
						
                    unset($tlisteadd);
                    $tstamp = time();
                    t3lib_div::writeFile(PATH_site.'typo3temp/log_'.__CLASS__.'.html', nl2br( implode("\n",$tlogadd ) ).'<br />-----------------------<br />'.nl2br( implode("\n",$tlog ) ) );
				 //   t3lib_div::writeFile(PATH_site.'typo3temp/log_'.__CLASS__.$tstamp.'.html', nl2br( implode("\n",$tlogadd ) ) );
                     if( $n > 0 ){
                        $mail = t3lib_div::makeInstance('t3lib_mail_Message');
                        $mail->setTo(array('madatsara@gmail.com'))
                            ->setFrom(array($GLOBALS['TYPO3_CONF_VARS']['MAIL']['defaultMailFromAddress'] => $GLOBALS['TYPO3_CONF_VARS']['MAIL']['defaultMailFromName'] ))
                            ->setSubject( 'MADATSARA CRON le '.date('d-m-Y H:i:s').' - '.($n>0?$n.' vidéos ajoutées':'') )
                            ->setCharset('utf-8');
                        $p = parse_url(t3lib_div::getIndpEnv('TYPO3_REQUEST_SCRIPT')); // Url of this script
                        $url = str_replace('//', '/', $p['host'] . ($p['port'] ? ':' . $p['port'] : '') );
                        $mail->setBody( nl2br( implode("\n",$tlogadd ) ) , 'text/html');
                        $attachment = Swift_Attachment::fromPath( PATH_site.'typo3temp/log_'.__CLASS__.'.html' , 'text/html');
                        $mail->attach($attachment);
                        $mail->send();
                    }
                     
                $success = true;
		return $success;
	}
	function conv_tstamp( $str ){
            $y = $m = $d = $h = $i = $s = 0;
            //2009-06-01T21:54:40.000Z
            $str = str_replace('T','_',$str );
            $tabdate = t3lib_div::trimExplode('.',$str);
            if( count( $tabdate ) == 2){
                $date = $tabdate[0]; //2009-06-01_21:54:40
                unset($tabdate);
                $tabdate = t3lib_div::trimExplode('_',$date);
                if( count( $tabdate ) == 2 ){
                    $ymd = $tabdate[0];
                    $hms = $tabdate[1];
                    unset($tabdate);
                    $tabdate = t3lib_div::trimExplode('-',$ymd);
                    if( count( $tabdate ) == 3 ){
                        $y = intval( $tabdate[0] );
                        $m = intval( $tabdate[1] );
                        $d = intval( $tabdate[1] );
                    }
                    unset($tabdate);
                    $tabdate = t3lib_div::trimExplode(':',$his);
                    if( count( $tabdate ) == 3 ){
                        $h = intval( $tabdate[0] );
                        $i = intval( $tabdate[1] );
                        $s = intval( $tabdate[1] );
                    }
                    unset($tabdate);
                }
            }
            return mktime($h,$i,$s,$m,$d,$y);
            
        }
	 
        function get_title( $str ){
            $texplode = t3lib_div::trimExplode('::',$str);
            return trim( $texplode[0] );
            }

            function get_artistes( $str ){
            $data = array();
            $uid = 0;
            $tx_tsara_youtubeid = '';

            $texplode = t3lib_div::trimExplode('::',$str);
            $ta = trim( $texplode[1] );
            $taba = t3lib_div::trimExplode(',',$ta);
            foreach( $taba as $artiste ){
                //get playliste of artiste
                $artiste = trim( $artiste );
                $nb = $GLOBALS['TYPO3_DB']->exec_SELECTcountRows('uid','tt_news_cat','1=1 AND pid=138 AND title = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($artiste,'tt_news_cat'));
                if( $nb > 0 ){

                    $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows('uid,tx_tsara_youtubeid','tt_news_cat','1=1 AND pid=138 AND title = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($artiste,'tt_news_cat'),'','',1);
                    $uid = $row[0]['uid'];
                    $tx_tsara_youtubeid = $row[0]['tx_tsara_youtubeid'];

                }

                $data[] = array(
                    'name' => $artiste
                    ,'uid' => $uid
                );
            }

            return $data;   
            }
			 
         	
        
 
        
 

}

if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/tsara/scheduler/class.tx_tsara_fetchsite_inovaovao.php'])	{
	include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/tsara/scheduler/class.tx_tsara_fetchsite_inovaovao.php']);
}

?>