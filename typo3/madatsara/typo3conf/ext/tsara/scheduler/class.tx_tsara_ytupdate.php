<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2009 Steffen M端ller (typo3@t3node.com)
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

/**
 * Class "tx_smscheddemo_HideContent" provides task procedures
 *
 * @author		Steffen M端ller <typo3@t3node.com>
 * @package		TYPO3
 * @subpackage		tx_smscheddemo
 *
 */



class tx_tsara_ytupdate extends tx_scheduler_Task {

	 

	/**
	 * Function executed from the Scheduler.
	 * Hides all content elements of a page
	 *
	 * @return	boolean	TRUE if success, otherwise FALSE
	 */
	public function execute() {
		$success = FALSE;
		
                $user = $this->user;
                $pid = $this->pid;
                $parent_category = $this->parent_category;
                
                $tligne = array();
                $tlog = $tlogadd = array();
                $tlisteadd = array();
                $maj = 0;
				 
                $this->confArr = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['tsara']);
                
                $nb = $GLOBALS['TYPO3_DB']->exec_SELECTcountRows(
                    '*',
                    '`tt_news`
                        LEFT JOIN tx_ytok a ON a.videouid = tt_news.uid',
                    '`tt_news`.`pid` = 138 AND a.videouid IS NULL AND `tt_news`.`deleted` = 0'
                    );

                if( $nb > 0 ){
                    
                    $artiste = $tabligne = array();


                    $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow(
                        'tt_news.uid,tt_news.tx_tsara_youtubeid',
                        '`tt_news`
                            LEFT JOIN tx_ytok a ON a.videouid = tt_news.uid',
                        '`tt_news`.`pid` = 138 AND a.videouid IS NULL AND `tt_news`.`deleted` = 0' 
                        ,''
                        ,'crdate DESC'
                        );
                    $videouid = $row['uid'];
                    $ytuid = $row['tx_tsara_youtubeid'];

                    $artiste = $this->get_cat( $videouid ) ;
                    
                    //print_r($artiste);exit;
                    t3lib_div::writeFile(PATH_site.'typo3temp/logytupdate_'.__LINE__.'.txt', $videouid.' / '.$ytuid);
                    t3lib_div::writeFile(PATH_site.'typo3temp/logytupdate_'.__LINE__.'.txt', print_r($artiste,1));

                    if( count($artiste)>0 ){

                        foreach($artiste as $art ){
                            $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow(
                                'value_alias',
                                '`tx_realurl_uniqalias`',
                                '`tablename` = '.$GLOBALS['TYPO3_DB']->fullQuoteStr('tt_news_cat','tx_realurl_uniqalias').' AND value_id = '.$art['uid'] 
                                ,''
                                ,''
                             );
                            $url = 'http://www.madatsara.com/clips-chansons_'.$row['value_alias'].'.html';
                            $nomartiste = $this->encodeTitle($art['name']);
                            
                            if( trim($row['value_alias'])=='' ){
                                 
                            }

                            $tabligne[] = '- Clips de '.$nomartiste.' sur Madatsara : '.$url;


                        }

                        $description = implode("\n",$tabligne);
                        //echo $description."\n----\n";

                        //maj BDD
                        $sqli = $GLOBALS['TYPO3_DB']->INSERTquery(
                                    'tx_ytok'
                                    ,array(
                                        'videouid' => $videouid
                                    )
                                );
                        $GLOBALS['TYPO3_DB']->sql_query( $sqli );
                        //echo $sqli;
                        //exit;
                        set_include_path(PATH_site.'fileadmin/zend/library/' . PATH_SEPARATOR . get_include_path());

                        require_once 'Zend/Loader.php';
                        Zend_Loader::loadClass('Zend_Gdata_YouTube');
                        Zend_Loader::loadClass('Zend_Gdata_AuthSub');
                        Zend_Loader::loadClass('Zend_Gdata_ClientLogin');
                        Zend_Loader::loadClass('Zend_Gdata_App_Exception');
                        //session_start();

                        $service = Zend_Gdata_Youtube::AUTH_SERVICE_NAME;
                        $email = 'rabehasy@gmail.com';
                        $passwd = 'r4b3h#$y';
                        $httpClient = Zend_Gdata_ClientLogin::getHttpClient($email, $passwd, $service);
                        $yt = new Zend_Gdata_YouTube($httpClient,'Video uploader v1','ytapi-NaivoRABEHASY-Madatsara-du1p2442-0','AI39si597XMqxF0fADET4RwPugEA2PPo41sik78G_NYOR5iwIBzwyG4uftTWTDPX-0kCDWaaKBPjhHekEJP2C9Z-ThIWUv_P_w');


                        $videoEntry = $yt->getVideoEntry($ytuid, null, true);
                        $putUrl = $videoEntry->getEditLink()->getHref();
                        $videoEntry->setVideoDescription($videoEntry->getVideoDescription()."\n----------\n". $description );
                        //echo $videoEntry->getVideoDescription()."\n----------\n". $description ;
                        $yt->updateEntry($videoEntry, $putUrl);
                        //  echo $videoEntry->getVideoDescription()  ;exit;
                        
                        t3lib_div::writeFile(PATH_site.'typo3temp/logytupdate_'.__LINE__.'.txt', $videoEntry->getVideoDescription());
                    }

                }
                /*else{
                    $GLOBALS['TYPO3_DB']->exec_UPDATEquery(
                                'tx_scheduler_task'
                                ,'uid = 34'
                                ,array(
                                    'disable' => 1
                                )
                            );
                }*/

                  
                $success = true;
		return $success;
	}
	function getcategoriesofarticle($uid, $parent_category = 0){
           $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                        'uid_foreign',
                        'tt_news_cat_mm
                            LEFT JOIN tt_news_cat ON tt_news_cat.uid = tt_news_cat_mm.uid_foreign
                            LEFT JOIN tt_news ON tt_news.uid = tt_news_cat_mm.uid_local
                            ',
                        '1=1  AND tt_news_cat_mm.uid_local = '.$GLOBALS['TYPO3_DB']->fullQuoteStr( $uid,'tt_news_cat_mm').(!is_string($parent_category) || $parent_category=='0'?'':' AND tt_news_cat.parent_category IN('.$parent_category.')') 
                        ); 
           t3lib_div::writeFile(PATH_site.'typo3temp/logytupdate_'.__LINE__.'.txt', $GLOBALS['TYPO3_DB']->SELECTquery(
                        'uid_foreign',
                        'tt_news_cat_mm
                            LEFT JOIN tt_news_cat ON tt_news_cat.uid = tt_news_cat_mm.uid_foreign
                            LEFT JOIN tt_news ON tt_news.uid = tt_news_cat_mm.uid_local
                            ',
                        '1=1  AND tt_news_cat_mm.uid_local = '.$GLOBALS['TYPO3_DB']->fullQuoteStr( $uid,'tt_news_cat_mm').(!is_string($parent_category) || $parent_category=='0'?'':' AND tt_news_cat.parent_category IN('.$parent_category.')') 
                        )); 
           return $row;
       }
        function get_cat($uid){
           $data = array();
           $row = $this->getcategoriesofarticle( $uid, $this->confArr['parent_category_artiste'] );
           if( count( $row ) > 0 ){
               $i = 0;
               foreach( $row as $ligne ){
                   $data[$i]['uid'] = $ligne['uid_foreign'];
                   $data[$i]['name'] = $this->getfieldincat( $ligne['uid_foreign'], 'title' );
                   $i++;
               }
           }
           else{
               $row = $this->getcategoriesofarticle( $uid  );
               if( count( $row ) > 0 ){
                    $i = 0;
                    foreach( $row as $ligne ){
                        $data[$i]['uid'] = $ligne['uid_foreign'];
                        $data[$i]['name'] = $this->getfieldincat( $ligne['uid_foreign'], 'title' );
                        $i++;
                        //Update parent_category
                        $GLOBALS['TYPO3_DB']->exec_UPDATEquery(
                                'tt_news_cat'
                                ,'uid = '.$uid
                                ,array(
                                    'parent_category' => $this->confArr['parent_category_artiste']
                                )
                            );
                        t3lib_div::writeFile(PATH_site.'typo3temp/logytupdate_'.__LINE__.'.txt', $GLOBALS['TYPO3_DB']->UPDATEquery(
                                'tt_news_cat'
                                ,'uid = '.$uid
                                ,array(
                                    'parent_category' => $this->confArr['parent_category_artiste']
                                )
                            )); 
                    }
               }
           }
           return $data;
       }
       function getfieldincat( $uid, $field = 'tx_tsara_pagelist' ){
           $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows('tt_news_cat.'.$field,'tt_news_cat','1=1  AND tt_news_cat.uid IN ('. $uid .')');
           //print_r($row);
           if(  t3lib_utility_Math::convertToPositiveInteger($uid) ) return $row[0][$field];
           else return $row;
           
       }
	 
        function encodeTitle($title){
            $cs = t3lib_div::makeInstance('t3lib_cs');         
            $space = '_';
            $charset = 'utf-8'; 
            $title = $cs->specCharsToASCII($charset, $title);
            $title = trim($title, $space);

            return $title;
        }
			 
         	
        
 
        
 

}

if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/tsara/scheduler/class.tx_tsara_fetchsite_inovaovao.php'])	{
	include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/tsara/scheduler/class.tx_tsara_fetchsite_inovaovao.php']);
}

?>