<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2009 Steffen Müller (typo3@t3node.com)
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

/**
 * Class "tx_smscheddemo_HideContent" provides task procedures
 *
 * @author		Steffen Müller <typo3@t3node.com>
 * @package		TYPO3
 * @subpackage		tx_smscheddemo
 *
 */
require_once(t3lib_extMgm::extPath("tsara").'lib/class.tx_tsara_util.php');
require_once(t3lib_extMgm::extPath("tsara").'lib/encoding.php');
class tx_tsara_fetchsite_rss extends tx_scheduler_Task {

	 

	/**
	 * Function executed from the Scheduler.
	 * Hides all content elements of a page
	 *
	 * @return	boolean	TRUE if success, otherwise FALSE
	 */
	public function execute() {
		$success = FALSE;
		
                $prefixe = $this->prefixe;
                $uidpresse = $this->uidpresse;
                $categoryuid = $this->categoryuid;
                $urlrss = $this->urlrss;
                
                require_once t3lib_extMgm::extPath("tsara").'lib/simple_html_dom.php';
                $this->util = t3lib_div::makeInstance('tx_tsara_util');
                $tlog = array();
                $tabTitre = array();
                $tabUrl = array();
                $tabImg = array();
                $tabCorps = array();
                $data = array();
                $tabmc = array();
                $tuidcat = array();
                $tabUidcat = array();
                $metahttp = 'utf-8';
                $this->extensionok = 'jpeg,jpg,png,gif';
                
                $fileFunc = t3lib_div::makeInstance('t3lib_basicFileFunctions');
                $this->confArr = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['tsara']);
                $this->cs = t3lib_div::makeInstance('t3lib_cs');
                
                 
                $yql = 'select * from rss where url="'.$urlrss.'"';
                
                $urlyql = 'http://query.yahooapis.com/v1/public/yql?q=%s&format=json&callback=';
                
                $urlyql = sprintf( $urlyql, urlencode($yql) );
                
                $content = t3lib_div::getUrl( $urlyql );
                $data = json_decode( $content , 1 );
                $img = $corps = '';
                
               // print_r( $urlyql );
                
                if( $data['query']['results']['item'] ){
                	$item = $data['query']['results']['item'];
                	foreach( $item as $row ){
                		$tabTitre[] = $row['title'];
                		$tabUrl[] = $row['link'];
                		$corps = ($row['encoded']!=''?$row['encoded']:$row['description']);
                		
                		$tabCorps[] = strip_tags( $corps );
                		$tabImg[] = $this->getImage($row,$uidpresse);
                		
                		unset( $img );
                		unset( $corps );
                	}
                }
		
                
                
                
                /*
                
   print_r($tabTitre);
                   print_r($tabCorps);
                    print_r($tabUrl);
                    print_r($tabImg);

                    exit;
*/
                if(is_array($tabTitre) && count($tabTitre)>0){
                   
		$tabTitre = $this->util->array_delete( $tabTitre );
		$tabCorps = $this->util->array_delete( $tabCorps );
		$tabUrl = $this->util->array_delete( $tabUrl );
		//$tabImg = $this->util->array_delete( $tabImg );

                   
                    
                    //Liste des mots clés
                    $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows(
                            'uid,tx_tsara_keywords',
                            'tt_news_cat',
                            '1=1 AND deleted=0 AND hidden=0 AND tx_tsara_keywords IS NOT NULL AND pid IN('.$this->confArr['pidcat'].') AND parent_category IN('.$this->confArr['parent_category'].')'
                            );
                    $tlog[] = $GLOBALS['TYPO3_DB']->SELECTquery(
                            'uid,tx_tsara_keywords',
                            'tt_news_cat',
                            '1=1 AND deleted=0 AND hidden=0 AND tx_tsara_keywords IS NOT NULL AND  pid IN('.$this->confArr['pidcat'].') AND parent_category IN('.$this->confArr['parent_category'].')'
                            );;
                    if( count( $row ) > 0 ){
                        //Construire les regex
                        foreach( $row as $ligne ){
                            $mc = $this->util->oteraccents( trim($ligne['tx_tsara_keywords'] ) );
                            
                            //recreer les valeurs
                            $tabmc = t3lib_div::trimExplode(',', $mc );
                            $data[$ligne['uid']] = '/'.implode('|',$tabmc ).'/iU';
                            unset( $tabmc );
                        }
                    }
                    
                    
                    for($i=0;$i<count($tabTitre);$i++){

                                $titre = ( $tabTitre[$i] ); 
                                $corps = trim( $tabCorps[$i] ); 
                                $corps = preg_replace('/^[\pZ\pC]+|[\pZ\pC]+$/u', '', $corps );
                                $corps = trim( $corps );
                                $url = $tabUrl[$i];
                                $img = $tabImg[$i];
                                
                                $titre = $this->cs->entities_to_utf8($titre,1);
                                $corps = $this->cs->entities_to_utf8($corps,1);
                                
                                //$corps = Encoding::fixUTF8( $corps );
                                //$titre = Encoding::fixUTF8( $titre );
                                
                                $corps = str_replace("n?","n'",$corps );
                                $titre = str_replace("n?","n'",$titre );

                                
                                $count = $GLOBALS['TYPO3_DB']->exec_SELECTcountRows('uid','tt_news','1=1 AND deleted=0 AND hidden=0 AND tx_tsara_exturl2 = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($url,'tt_news') );
                                $tlog[] =   $GLOBALS['TYPO3_DB']->SELECTquery('COUNT(uid)','tt_news','1=1 AND deleted=0 AND hidden=0 AND tx_tsara_exturl2 = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($url,'tt_news') ); 

                                if($titre!='' && $count == 0 && $url!='' && trim( $corps ) != '' && strlen($url)<255 ){

                                    $sql = $GLOBALS['TYPO3_DB']->INSERTquery(
                                            'tt_news',
                                            array(
                                                'pid' => $this->confArr['pidsave']
                                                ,'tstamp' => time()
                                                ,'crdate' => time()
                                                ,'cruser_id' => 1
                                                ,'title' => $titre
                                                ,'datetime' => time()
                                                ,'bodytext' => trim($corps)
                                                ,'author' => $this->author
                                                ,'tx_tsara_exturl2' => $url
                                            )
                                    );
                                    $GLOBALS['TYPO3_DB']->sql_query( $sql );
                                    $uid = $GLOBALS['TYPO3_DB']->sql_insert_id();
									$tlog[] = $sql;
									
									if( $img!=''){
										//$fichier = $prefixe.$fileFunc->cleanFileName( basename( $img ) );
										$fichier = $fileFunc->cleanFileName( basename( $img ) );
										$tab = t3lib_div::split_fileref( $fichier );
										$chemin = PATH_site.'uploads/pics' ;
										$fichier = $prefixe.$uid.'.'.$tab['fileext'];
										$myfilepath = $fileFunc->getUniqueName($fichier, $chemin);
										$tab = t3lib_div::split_fileref( $myfilepath );
										$img  = ($tab['file']!='_01' && $tab['fileext']!=''?$tab['file']:'');
										if(  t3lib_div::inList($this->extensionok,mb_strtolower( $tab['fileext'] ))){
											t3lib_div::writeFile( $myfilepath, t3lib_div::getUrl($tabImg[$i]));
										}
										else{
											$img = "";
										}
										unset($tab);
									}									
									//update
									$sql = $GLOBALS['TYPO3_DB']->UPDATEquery(
                                                                                    'tt_news',
                                                                                    'uid = '.$GLOBALS['TYPO3_DB']->fullQuoteStr($uid,'tt_news') 
                                                                                    ,array(                                                
                                                                                        'image' => $img                                                
                                                                                    )
                                                                            );
                                                                            $GLOBALS['TYPO3_DB']->sql_query( $sql );
                                                                            $tlog[] = $sql;
                                    
                                    
                                    $sql = $GLOBALS['TYPO3_DB']->INSERTquery(
                                                        'tt_news_cat_mm',
                                                        array(
                                                            'uid_local' => $uid
                                                            ,'uid_foreign' => $uidpresse 
                                                        )
                                                );
                                                
                                    $GLOBALS['TYPO3_DB']->sql_query( $sql );
                                    $tlog[] = $sql;    
                                    
                                    if( $img!=''){
                                        $sql = $GLOBALS['TYPO3_DB']->INSERTquery(
                                                        'tt_news_cat_mm',
                                                        array(
                                                            'uid_local' => $uid
                                                            ,'uid_foreign' => 16867
                                                        )
                                                );
                                                
                                        $GLOBALS['TYPO3_DB']->sql_query( $sql );
                                        $tlog[] = $sql;
                                    }
                                                
                                    //Attribuer les categories en fonction des mot cles
                                    if( count( $data ) > 0 ){
                                        
                                        //oter les accents dans le corps et le titre
                                        $corps = $this->util->oteraccents( $corps );
                                        foreach( $data as $key => $regex ){
                                            if( preg_match($regex,$corps) || preg_match($regex,$titre) ) {
                                                 
                                                
                                                $sql = $GLOBALS['TYPO3_DB']->INSERTquery(
                                                        'tt_news_cat_mm',
                                                        array(
                                                            'uid_local' => $uid
                                                            ,'uid_foreign' => $key 
                                                        )
                                                );
                                                
                                                $GLOBALS['TYPO3_DB']->sql_query( $sql );
                                                
                                                $tlog[] = $sql;
                                                
                                                
                                            }
                                            else{
                                                $tlog[] = $regex;
                                            }
                                        }
                                        
                                         
                                        unset( $tuidcat );
                                    }

                                    



                                }
                                else $tlog[] = 'Count : '.$count;



                    }
                    /*$this->util = t3lib_div::makeInstance('tx_tsara_util');
                    $this->util->clear_cache();
                    */
                }

                t3lib_div::writeFile( PATH_site.'typo3temp/'.__CLASS__.'.txt',implode(";\n", $tlog ));
                $success = true;
		return $success;
	}
        
        function getImage($row,$uid){
            $content = ($row['encoded']!=''?$row['encoded']:$row['description']);
            $src = '';
            if( trim($content)== '' ) return $src;
            
            $html = str_get_html( $content );
            
            switch( $uid ) {
                //Basyvava
                case '229' :
                    if( $html->find('div.cc-imagewrapper',0) ){
                        $src = $html->find('div.cc-imagewrapper',0)->find('img',0)->src;
                    }
                break;
                //Ao raha 
                case '223' :
                    if( $html->find('div.wp-caption',0) ){
                        $src = $html->find('div.wp-caption',0)->find('img',0)->src;
                    }
                break;
                //TV Mada 
                case '228' :
                    if( $html->find('img.attachment-thumbnail' ) ){
                        foreach( $html->find('img.attachment-thumbnail' ) as $img ){
                            $src = $img->parent()->href;
                        }
                    }   
                break;
                //La vérité 
                case '221' :
                    if( $html->find('img',0) ){
                        $src = $html->find('img',0)->src;
                    }   
                break;
                //Mada matin 
                case '219' :
                    if( $html->find('img.size-full' ) ){
                        foreach( $html->find('img.size-full' ) as $img ){
                            $src = $img->src;
                        }
                    }   
                break;
                //Newsmada
                case '225' :
                    if( $html->find('img',0) ){
                        $src = $html->find('img',0)->src;
                    }  
                break;
                //Ma-laza
                case '230' :
                    if( $html->find('img.aligncenter',0) ){
                        $src = $html->find('img.aligncenter',0)->src;
                        //echo substr($src,0,24);exit;
                        if( substr($src,0,23) == 'data:image/jpeg;base64,'){
                        	//creer l'image dans typo3temp
                        	$contentimg = substr($src,23);
                        	$contentimg = base64_decode( $contentimg );
                        	$fname = $this->generateRandomString(8).'.jpg';
                        	t3lib_div::writeFile( PATH_site.'typo3temp/presse/'.$fname, $contentimg );
                        	
                        	$src = PATH_site.'typo3temp/presse/'.$fname;
                        }
                    }  
                break;
//Mada7sur7
                case '2921' :
                    if( $html->find('img',0) ){
                        $src = $html->find('img',0)->src;
                    }  
                break;
            }
            
            return $src;
        }
        function generateRandomString($length = 10) {    
    		return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
	}	
        
 
        
 

}

if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/tsara/scheduler/class.tx_tsara_fetchsite_inovaovao.php'])	{
	include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/tsara/scheduler/class.tx_tsara_fetchsite_inovaovao.php']);
}

?>