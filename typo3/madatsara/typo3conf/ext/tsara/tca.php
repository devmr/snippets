<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}



#######################################
### TABLE 1: tx_tsara_compteauthor ###
#######################################

$TCA['tx_tsara_compteauthor'] = array (
	'ctrl' => $TCA['tx_tsara_compteauthor']['ctrl'],
	'interface' => array (
		'showRecordFieldList' => 'hidden,nom,email,description,comptes,image'
	),
	'feInterface' => $TCA['tx_tsara_compteauthor']['feInterface'],
	'columns' => array (
		
		'hidden' => array (		
			'exclude' => 1,
			'label'   => 'LLL:EXT:lang/locallang_general.xml:LGL.hidden',
			'config'  => array (
				'type'    => 'check',
				'default' => '0'
			)
		),
		 
		'title' => array (		
			'exclude' => 1,
			'label'   => 'LLL:EXT:tsara/locallang_db.xml:tx_tsara_compteauthor.title',
			'config'  => array (
				'type'    => 'input',
			)
		),
		
		'typecompte' => array (
			'exclude' => 1,		
			'label' => 'LLL:EXT:tsara/locallang_db.xml:tx_tsara_compteauthor.typecompte',		
			'config' => array (
				'type' => 'select',
				'items' => array (
					array('', '',),
					array('LLL:EXT:tsara/locallang_db.xml:tx_tsara_compteauthor.typecompte.I.6', 'ico-tel',),
					array('LLL:EXT:tsara/locallang_db.xml:tx_tsara_compteauthor.typecompte.I.7', 'ico-mail',),
					array('LLL:EXT:tsara/locallang_db.xml:tx_tsara_compteauthor.typecompte.I.1', 'ico-fb',),
					array('LLL:EXT:tsara/locallang_db.xml:tx_tsara_compteauthor.typecompte.I.2', 'ico-twitter',),
					array('LLL:EXT:tsara/locallang_db.xml:tx_tsara_compteauthor.typecompte.I.3', 'ico-googleplus',),
					array('LLL:EXT:tsara/locallang_db.xml:tx_tsara_compteauthor.typecompte.I.4', 'ico-linkedin',),
					array('LLL:EXT:tsara/locallang_db.xml:tx_tsara_compteauthor.typecompte.I.5', 'ico-web',),
					 
				),
				'size' => 1,	
				'maxitems' => 1,
			)
		),
            
                'url' => array (
			'exclude' => 1,		
			'label' => 'LLL:EXT:tsara/locallang_db.xml:tx_tsara_compteauthor.url',		
			'config' => array (
				'type' => 'input',	
				'size' => '30',	
				'eval' => 'required',
			)
		),
                'showinfront' => array (		
			'exclude' => 1,
			'label'   => 'LLL:EXT:tsara/locallang_db.xml:tx_tsara_compteauthor.showinfront',
			'config'  => array (
				'type'    => 'check',
				'default' => '0'
			)
		),
                'comptes' => array (		
			'config' => array (
				'type' => 'passthrough'
			)
		),
	),
	'types' => array (
		'0' => array('showitem' => 'hidden;;1;;1-1-1,title, typecompte, url, showinfront')
	),	
	'palettes' => array (
		'1' => array('showitem' => 'starttime, endtime')
	)
);

#######################################
### TABLE 1: tx_tsara_videosfilm ###
#######################################

$TCA['tx_tsara_videosfilm'] = array (
	'ctrl' => $TCA['tx_tsara_videosfilm']['ctrl'],
	'interface' => array (
		'showRecordFieldList' => 'hidden,title, typecompte, url,description'
	),
	'feInterface' => $TCA['tx_tsara_videosfilm']['feInterface'],
	'columns' => array (
		
		'hidden' => array (		
			'exclude' => 1,
			'label'   => 'LLL:EXT:lang/locallang_general.xml:LGL.hidden',
			'config'  => array (
				'type'    => 'check',
				'default' => '0'
			)
		),
		 
		'title' => array (		
			'exclude' => 1,
			'label'   => 'LLL:EXT:tsara/locallang_db.xml:tx_tsara_videosfilm.title',
			'config'  => array (
				'type'    => 'input',
			)
		),
'description' => array (		
			'exclude' => 1,
			'label'   => 'LLL:EXT:tsara/locallang_db.xml:tx_tsara_videosfilm.description',
			'config'  => array (
				'type'    => 'text',
			)
		),
		
		'typecompte' => array (
			'exclude' => 1,		
			'label' => 'LLL:EXT:tsara/locallang_db.xml:tx_tsara_videosfilm.typecompte',		
			'config' => array (
				'type' => 'select',
				'items' => array (
					array('', '',),
					
					array('LLL:EXT:tsara/locallang_db.xml:tx_tsara_videosfilm.typecompte.I.1', 'ba',),
					array('LLL:EXT:tsara/locallang_db.xml:tx_tsara_videosfilm.typecompte.I.2', 'extrait',)
					 
				),
				'size' => 1,	
				'maxitems' => 1,
			)
		),
            
                'url' => array (
			'exclude' => 1,		
			'label' => 'LLL:EXT:tsara/locallang_db.xml:tx_tsara_videosfilm.url',		
			'config' => array (
				'type' => 'input',	
				'size' => '30',	
				'eval' => 'required',
			)
		),
                 
                'tx_tsara_cine_videosfilm' => array (		
			'config' => array (
				'type' => 'passthrough'
			)
		),
	),
	'types' => array (
		'0' => array('showitem' => 'hidden;;1;;1-1-1,title, typecompte, url,description ')
	),	
	'palettes' => array (
		'1' => array('showitem' => 'starttime, endtime')
	)
);

#######################################
### TABLE 1: tx_tsara_cine_acteurs ###
#######################################

$TCA['tx_tsara_cine_acteurs'] = array (
	'ctrl' => $TCA['tx_tsara_cine_acteurs']['ctrl'],
	'interface' => array (
		'showRecordFieldList' => 'hidden,title,ttaddressuid'
	),
	'feInterface' => $TCA['tx_tsara_cine_acteurs']['feInterface'],
	'columns' => array (
		
		'hidden' => array (		
			'exclude' => 1,
			'label'   => 'LLL:EXT:lang/locallang_general.xml:LGL.hidden',
			'config'  => array (
				'type'    => 'check',
				'default' => '0'
			)
		),
		 
		'title' => array (		
			'exclude' => 1,
			'label'   => 'LLL:EXT:tsara/locallang_db.xml:tx_tsara_cine_acteurs.title',
			'config'  => array (
				'type'    => 'input',
                                'eval' => 'required',
			)
		),
		
		'ttaddressuid' => array (
			'exclude' => 1,		
			'label' => 'LLL:EXT:tsara/locallang_db.xml:tx_tsara_cine_acteurs.typecompte',		
			'config' => array (
				'type' => 'group',
                                'internal_type' => 'db',
                                'allowed' => 'tt_address',
				
				'minitems' => 0,
				'maxitems' => 1
                                ,'wizards' => array(
                                    'suggest' => array(
                                    'type' => 'suggest',
                                    'fe_users' => array(
                                        'maxItemsInResultList' => 5,
                                        'pidList' => '79',
                                    )
                                    )
                                )
				
			)
		),
            
                 
                 
                'tx_tsara_cine_acteurs' => array (		
			'config' => array (
				'type' => 'passthrough'
			)
		),
	),
	'types' => array (
		'0' => array('showitem' => 'hidden;;1;;1-1-1,title,ttaddressuid ')
	),	
	'palettes' => array (
		'1' => array('showitem' => 'starttime, endtime')
	)
);

?>