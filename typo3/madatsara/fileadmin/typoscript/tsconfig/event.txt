mod.web_list {
   allowedNewTables = tt_news,tt_news_cat,pages,tt_address,tt_address_group 
}

# Modification des labels
TCEFORM.tt_news {
	title.label = Titre de l'évènement
	bodytext.label = Description 
	image.label = Visuel
	
}

# Suppression des champs inutiles
TCEFORM.tt_news {
	endtime.disabled = 1
	starttime.disabled = 1
	archivedate.disabled = 0
	category.disabled = 0
	author.disabled = 0
	author_email.disabled = 1
	sys_language_uid.disabled = 1
	type.disabled = 0
	links.disabled = 1
	short.disabled = 1
	bodytext.disabled = 0
	related.disabled = 1
	news_files.disabled = 1
	no_auto_pb.disabled = 1
	editlock.disabled = 1
	keywords.disabled = 1
	imagecaption.disabled = 1
	imagetitletext.disabled = 1	 
	fe_group.disabled = 1
	page.disabled = 0	 
        image.disabled = 0
        datetime.disabled = 1

        tx_tsara_exturl2.disabled = 1
        tx_newsreadedcount_readedcounter.disabled = 0
        tx_tsarattnews_slug.disabled = 1
        tx_tsarattnews_nbcomments.disabled = 1
        tx_tsara_author.disabled = 1

        tx_tsara_youtubeid.disabled = 1
        tx_tsara_youtube_thumbnail.disabled = 1
        tx_tsara_youtube_published.disabled = 1
        tx_tsara_youtube_duration.disabled = 1
        tx_tsara_youtube_viewcount.disabled = 1
        tx_tsara_youtube_favoritecount.disabled = 1
        tx_tsara_youtube_numlikes.disabled = 1
        tx_tsara_youtube_numdislikes.disabled = 1
        tx_tsara_youtube_numcomment.disabled = 1
        tx_tsara_tstamp.disabled = 1
        tx_tsara_yttitle.disabled = 1

        tx_tsara_event_date.disabled = 0
        tx_tsara_event_date_begin.disabled = 0
        tx_tsara_event_date_end.disabled = 0
        tx_tsara_event_lieu.disabled = 0
        tx_tsara_event_prix.disabled = 0
        tx_tsara_event_reservation.disabled = 0
        tx_tsara_event_prevente.disabled = 0
        tx_tsara_event_location.disabled = 0
        tx_tsara_event_contacts.disabled = 0
        

        tx_tsara_cine_acteurs.disabled = 1
        tx_tsara_cine_auteurs.disabled = 1
        tx_tsara_cine_realisateurs.disabled = 1
        tx_tsara_cine_format.disabled = 1
        tx_tsara_cine_duree.disabled = 1
        tx_tsara_cine_anneesortie.disabled = 1
        tx_tsara_cine_siteweb.disabled = 1

        tx_tsara_cine_musique.disabled = 1
        tx_tsara_cine_production.disabled = 1
        tx_tsara_cine_scenario.disabled = 1
        tx_tsara_cine_directeur_photo.disabled = 1
        tx_tsara_cine_montage_son.disabled = 1
        tx_tsara_cine_perchman.disabled = 1
        tx_tsara_cine_effets_visuels.disabled = 1
        tx_tsara_cine_videosfilm.disabled = 1
}
# Suppression des champs inutiles
TCEFORM.tt_address {
    gender.disabled = 1
    first_name.disabled = 1
    last_name.disabled = 1
    middle_name.disabled = 1
    title.disabled = 1
    company.disabled = 1
    birthday.disabled = 1
    building.disabled = 1
    room.disabled = 1
    zip.disabled = 1
    address.disabled = 0
    city.disabled = 1
    country.disabled = 1
    region.disabled = 1
    www.disabled = 0
    phone.disabled = 0
    mobile.disabled = 1
    fax.disabled = 1
    image.disabled = 1
    description.disabled = 1
    email.disabled = 1
}
tx_ttnews.singlePid = 122
#TCEFORM.tt_news.title.config.max = 40
TCAdefaults.tt_news.hidden = 0
TCAdefaults.tt_address.hidden = 0
TCAdefaults.tt_address_group.hidden = 0
#TCEFORM.tt_news.type.removeItems = 2,1
TCEFORM.tt_news.type {
#	altLabels.0 = Détail
}