mod.web_list {
   allowedNewTables = tx_irfaq_q, tx_irfaq_cat,tx_irfaq_expert
}

# Suppression des champs inutiles
TCEFORM.tx_irfaq_q {
	q_from.disabled = 1
	cat.disabled = 1
	expert.disabled = 1
	related.disabled = 1
	related_links.disabled = 1
	faq_files.disabled = 1
}