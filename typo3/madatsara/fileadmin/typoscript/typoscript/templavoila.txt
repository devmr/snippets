

styles.content.loginform.pid = 22

lib.field_logoentete =  RECORDS
lib.field_logoentete {
    source = {$ttcontentid_logoentete}
    tables = tt_content
}

lib.field_leftnav = RECORDS
lib.field_leftnav {
    source = {$ttcontentid_field_leftnav}
    tables = tt_content
}

lib.field_footercontent = COA
lib.field_footercontent {
    1 = RECORDS
    1.source = {$ttcontentid_field_footercontent_1}
    1.tables = tt_content

    2 = RECORDS
    2.source = {$ttcontentid_field_footercontent_2}
    2.tables = tt_content
}



lib.search_box = COA
lib.search_box{
    wrap = |
    1 = TEXT
    1{
        typolink.parameter = {$pidrecherche}
        typolink.returnLast = url
        wrap = <a name="search" id="search"></a><form id="formsearch" action="|" method="post" name="searchform"  ><fieldset>
    }


    2 = COA
    2{
        
        1 = TEXT
        1.data = LLL:fileadmin/tpl/lang.xml:rechercher
        1.wrap = <legend class="blocouter">|</legend>

        2 = TEXT
        2.data = LLL:fileadmin/tpl/lang.xml:rechercher
        2.wrap = <label for="s" class="assistive-text">|</label>

        3 = HTML
        3.value (
                <input type="text" id="s" class="input-search" name="tx_indexedsearch[sword]" 
        )

        4 = TEXT
        4.data = LLL:fileadmin/tpl/lang.xml:rechercher
        4.wrap = placeholder="|" />

        5 = TEXT
        5.data = LLL:fileadmin/tpl/lang.xml:lancer_recherche
        5.wrap = <input class="btnsubmit" name="tx_indexedsearch[submit_button]" type="image" src="fileadmin/templates/images/search.png" value="|"
        

        6 = TEXT
        6.data = LLL:fileadmin/tpl/lang.xml:lancer_recherche
        6.wrap =  alt="|" /></fieldset>
        
        7 = HTML
        7.value (



                <input type="hidden" name="tx_indexedsearch[_sections]" value="0" />
                <input type="hidden" name="tx_indexedsearch[pointer]" value="0" />
                <input type="hidden" name="tx_indexedsearch[ext]" value="0" />
                <input type="hidden" name="tx_indexedsearch[lang]" value="0" />

           </form>


        )

    }

    
}





lib.field_titrepage = TEXT
lib.field_titrepage {
 data = page:subtitle // page:title
 #data = levelfield : -1 , media  , slide
 wrap = <h1>|</h1>
}



lib.logo = COA
lib.logo {
    wrap = <h1 id="site-title" ><span>|</span></h1>
    

    2 = TEXT
    2.typolink.parameter = {$pidhome}
    2.typolink.returnLast = url
    2.wrap = <a href="|" 

    3 = TEXT
    3.data = LLL:fileadmin/tpl/lang.xml:retouraccueil
    3.wrap = title="|" rel="home">

    4 = COA
    4{
        1 = TEXT
        1.data = LLL:fileadmin/tpl/lang.xml:retouraccueil
        1.wrap = <img src="fileadmin/templates/images/andrimaso-adidy-manara-maso-fifidianana.png" alt="|" />

        
        
    } 
    

    


}

[globalVar = GP:L = 1]
lib.logo.5.2.typolink.additionalParams >
lib.logo.5.2.typolink.additionalParams = &L=0
[global]

lib.field_usefulltools = COA
lib.field_usefulltools {
    1 = TEXT
    1.value(
        <div id="Wikio"><a id="wikio" class='st_sharethis_button' displayText="{$partager}"></a></div>
        <script type="text/javascript">var switchTo5x=false;</script><script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script><script type="text/javascript">stLight.options({publisher:'dd06cdb6-c0bb-4132-b391-89405b172c58',onhover: false});</script>
    )

    2 < lib.breadcrumb
}

lib.txttoppage = TEXT
lib.txttoppage.value = <a href="#hautdepage">Top of page</a>



lib.imagedroite = TEXT
lib.imagedroite.value = <img src="uploads/tx_templavoila/info_image_03.gif" alt="" />


TITLE_BROWSER = TEXT
TITLE_BROWSER {
 data = data = page:subtitle // page:title
 wrap = |
}

01_IMAGE_URL = COA
01_IMAGE_URL{
        1 = TEXT
        1.data = TSFE:baseUrl
	2 = TEXT
        2.data = TSFE:page|media
	2.stdWrap.wrap = uploads/media/|
	2.stdWrap.required = 1
}

01_TEXT_DESCRIPTION = TEXT
01_TEXT_DESCRIPTION{
    data = page:description
    wrap = |
}

#-- Ajout info si detail d'une actu---#
[globalVar = GP:tx_ttnews|tt_news > 0]

TITLE_BROWSER = COA
TITLE_BROWSER.1 = RECORDS
TITLE_BROWSER.1 {
    
    source = {GP:tx_ttnews|tt_news}
    source.insertData = 1

    tables = tt_news
    conf.tt_news = TEXT
    conf.tt_news.field = date
    conf.tt_news.field.stdWrap.strftime = %d/%m/%Y
}
TITLE_BROWSER.2 = RECORDS
TITLE_BROWSER.2 {
   source = {GP:tx_ttnews|tt_news}
    source.insertData = 1

    tables = tt_news
    conf.tt_news = TEXT
    conf.tt_news.field = title
}

01_IMAGE_URL{
        2 >
        2 = RECORDS
	2.tables = tt_news
        2.source.data = GP:tx_ttnews|tt_news
        2.conf.tt_news = TEXT
        2.conf.tt_news.field = image
	2.stdWrap.wrap = uploads/pics/|
	2.stdWrap.required = 1
}

01_TEXT_DESCRIPTION = COA
01_TEXT_DESCRIPTION{
    1 = RECORDS
    1 {
        source = {GP:tx_ttnews|tt_news}
        source.insertData = 1

        tables = tt_news
        conf.tt_news = TEXT
      conf.tt_news.field = short
      conf.tt_news.stdWrap.stripHtml=1
    }
    wrap = |
}

[global]


GaUAnalytics = TEXT
GaUAnalytics.value = {$GaUAnalytics}

GaUAnalyticsTsara = TEXT
GaUAnalyticsTsara.value = {$GaUAnalyticsTsara}

01_TEXT_SITENAME = TEXT
01_TEXT_SITENAME.value = {$sitename}

TITLEPLUS = TEXT
TITLEPLUS.value = {$sitename}




01_TEXT_URL = COA
01_TEXT_URL{
      1 = TEXT
      1.data = TSFE:baseUrl

      2 = TEXT
      2.typolink.parameter.data = page:uid
      2.typolink.addQueryString = 1
      2.typolink.addQueryString.exclude = id
      2.typolink.returnLast = url
}

lib.carousel = TEXT
lib.carousel.data = levelfield : -1 , media  , slide
lib.carousel.wrap = <img src="uploads/media/|" width="650" height="246" />


lib.field_header = COA
lib.field_header {
    
    1 < lib.logo
    1.wrap = <hgroup>|</hgroup>
    2 = COA
    2 {
        1 < lib.top
        
    }
    2.wrap = <div id="colb"><nav id="access" role="navigation"><div class="menu-menu-principal-container">|</div></nav>

    3 < lib.search_box

    4 = TEXT
    4.value = </div>

}


lib.field_footer = COA
lib.field_footer {

    1 = TEXT
    1.value = <div class="foot-a"><span>©</span>Bwa Bandé</div>
    2 < lib.footer
    2.wrap = <div class="foot-b">|</div>
    
    #3 < lib.field_footercontent
    3 = COA
    3 {
        1 = TEXT
        1.data = LLL:fileadmin/tpl/lang.xml:retrouvez
        1.wrap = |

        2 = TEXT
        2.value = <a href="{$urlfacebook}" class="fb"><span>Facebook</span></a>&nbsp;&#124;&nbsp;<a href="{$urltwitter}" class="tw"><span>Twitter</span></a>
    }
    3.wrap = <div class="foot-c">|</div>
}


###########################
# POPUP ENVOYER A UNE AMI #
###########################
temp_04.urlEnvoiAmi = COA
temp_04.urlEnvoiAmi {

  10 = TEXT
  10.typolink.parameter = {$idPageEnvoiAmi}
  10.typolink.returnLast = url
  10.typolink.additionalParams.cObject = COA
  10.typolink.additionalParams.cObject {

                
    10 = COA 
    10 {
        10 = TEXT 
        10.data = TSFE:baseUrl 
        20 = TEXT 
        20.typolink.parameter.data = page:uid
        20.typolink.addQueryString = 1
        20.typolink.addQueryString.exclude = id

        20.typolink.returnLast = url
        stdWrap.rawUrlEncode = 1
        
        
    } 
    wrap = &tipUrl=|

    
                
    
  }

        20 < TITLE_BROWSER
        
        #20.stdWrap.rawUrlEncode = 1

        20.wrap = &titre=|

    wrap = <a id="jemail" class="popin" href="|" title="Zarao amin'ny namanao ity pejy ity"><img src="fileadmin/templates/images/email.png" alt="" /></a>
}
page = PAGE
page.10 = USER
page.10.userFunc = tx_templavoila_pi1->main_page
#page.10.disableExplosivePreview = 1


temp.headTemplate = TEMPLATE
temp.headTemplate {
  template = FILE
  template.file = {$tpl}
  workOnSubpart = DOCUMENT_HEAD

  marks{
  	01_TEXT_TITLE < TITLE_BROWSER
  	01_TEXT_TITLE2 < TITLEPLUS
  	01_TEXT_URL < 01_TEXT_URL
  	01_IMAGE_URL < 01_IMAGE_URL
  	01_TEXT_SITENAME < 01_TEXT_SITENAME
  	01_TEXT_DESCRIPTION < 01_TEXT_DESCRIPTION
  	GaUAnalytics < GaUAnalytics
        GaUAnalyticsTsara < GaUAnalyticsTsara

        04_TYPOLINK_RSS = TEXT
        04_TYPOLINK_RSS.value = ?type=100


  }
}

[globalVar = GP:L = 2]
    temp.headTemplate.marks.04_TYPOLINK_RSS.value = ?type=100&L=2
[end]

page.headerData.75 < temp.headTemplate

page.headTag = <head><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" /><meta name="viewport" content="width=device-width, initial-scale=1.0" />


sitemap = PAGE
sitemap {
  typeNum = 200
  10 >
  10 < plugin.tx_weeaargooglesitemap_pi1
  10.pid_list = 1
  10.showLanguages = 0
  10.tt_news.single_page {
    1 = 37
    1.pid_list = 23

  }
  10.tt_news {
    disabledParameter = day,month,year
  }
  10.recursive = 0
  10.allowedDoktypes = 2,1
  10.domain = {$baseURL}
  config {
    disableAllHeaderCode = 1
    additionalHeaders = Content-type:text/xml
    no_cache = 1
    xhtml_cleaning = 0
  }
}




lib.field_topofpage = TEXT
lib.field_topofpage.data = LLL:fileadmin/tpl/lang.xml:txthaut
lib.field_topofpage.wrap = <p class="Ttop"><a href="#top_page">|</a></p>




[globalVar = TSFE:id={$pidhome}] || [globalVar = TSFE:id={$pidforum}]
lib.field_titlebreadcrumb = COA
lib.field_titlebreadcrumb >
lib.field_topofpage  >
lib.field_leftnav >
[global]


