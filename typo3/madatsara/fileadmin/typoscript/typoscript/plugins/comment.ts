/*
 *    Project:	NEW MADATSARA - NEW_MADATSARA
 *    Version:	1.0.0
 *    Date:		1 déc. 2012 10:38:58
 *    Author:	rabehasy 
 *
 *    Coded with Netbeans!
 */
plugin.tx_pagebrowse_pi1 {
    _LOCAL_LANG.default {
            text_first =  «
            text_prev =  ‹
            text_next =  ›
            text_last =  »
     }
}

plugin.tx_comments_pi1 {
    _LOCAL_LANG.default {
            text_no_comments = Aucun commentaire n'a été trouvé pour cette vidéo. Les commentaires affichés ici sont les commentaires de cette vidéo sur la page Youtube de la vidéo.<br /><br />Vous ne pouvez pas commenter directement cette vidéo depuis cette page.<br /><br /> Pour commenter cette vidéo, merci de vous rendre sur la <a id="pagevideoyoutube" title="Aller sur la page Youtube de cette vidéo">page Youtube de cette vidéo</a> en cliquant sur l'icône inférieur à droite "Watch on Youtube" du player.
     }

     advanced.dateFormatMode = strftime
     advanced.dateFormat = %e %B %Y
}


 