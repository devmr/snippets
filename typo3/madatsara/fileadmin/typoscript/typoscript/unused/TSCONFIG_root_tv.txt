## Permissions par defaut sur les nouvelles pages creees
TCEMAIN.permissions.user = show, editcontent, new, edit, delete
TCEMAIN.permissions.group = show, editcontent, new, edit
TCEMAIN.permissions.everybody = show, editcontent, new, edit

##Templavoila
mod.web_txtemplavoilaM1.enableDeleteIconForLocalElements = 1
mod.web_txtemplavoilaM1.sideBarEnable = false
mod.web_txtemplavoilaM1.enableLocalizationLinkForFCEs = 1
mod.web_txtemplavoilaM1.hideCopyForTranslation >


## Suppression automatique des caches a la sauvegarde
TCEMAIN.clearCacheCmd = all
options.clearCache.all = 1



TCAdefaults {
	pages.hidden = 0
	content.hidden = 0
}

TCEMAIN.table.pages {
  disablePrependAtCopy = 1
  disableHideAtCopy = 1
}

TCEMAIN.table.tt_content {
  disablePrependAtCopy = 1
  disableHideAtCopy = 1
}

mod.SHARED.defaultLanguageLabel = French
mod.SHARED.defaultLanguageFlag = fr.gif
mod.web_layout.defLangBinding = 1

TCEMAIN.clearCacheCmd = all
options.uploadFieldsInTopOfEB = 1

#-----------------------------------
# Clean up tt_content
#-----------------------------------


TCEFORM.tt_content.date.disabled = 1
TCEFORM.tt_content.linkToTop.disabled = 1
TCEFORM.tt_content.header_position.disabled = 1
TCEFORM.tt_content.rte_enabled.disabled = 1
TCEFORM.tt_content.longdescURL.disabled = 1
TCEFORM.tt_content.altText.disabled = 1
TCEFORM.tt_content.image_effects.disabled = 1
TCEFORM.tt_content.image_compression.disabled = 1
TCEFORM.tt_content.imagecaption_position.disabled = 1
TCEFORM.tt_content.titleText.disabled = 1
TCEFORM.tt_content.sectionIndex.disabled = 1
TCEFORM.tt_content.spaceBefore.disabled = 1
TCEFORM.tt_content.spaceAfter.disabled = 1
TCEFORM.tt_content.layout.disabled = 1

### TEMPLAVOILA 
TCEFORM.tt_content.colPos.disabled = 1
TCEFORM.tt_content.sys_language_uid.disabled = 0
TCEFORM.tt_content.fe_group.disabled = 1

TCEFORM.tt_content.image_zoom.disabled = 1
TCEFORM.tt_content.imagewidth.disabled = 1
TCEFORM.tt_content.imageheight.disabled = 1

#### Images ###
TCEFORM.tt_content.image_noRows.disabled = 1
TCEFORM.tt_content.imageborder.disabled = 1

[adminUser = 0]
mod.wizards.newContentElement.wizardItems{
	special>

	common.elements{
		bullets>
		table>
	}
	forms.elements{
		search>
		login>
                mailform>
	}
        plugins>

}
[GLOBAL]

#----------#
# Cadres
#----------#
TCEFORM.tt_content.section_frame {
   # on supprime les cadres qui ne nous interressent pas
   removeItems = 1,5,6,10,11,12,20,21
   
   # on ajoute nos cadres perso
   addItems.100 = Header home (hero-unit)
   
   
   


   
}

// MODIFICATION DES DISPOSITIONS (TITRE)
# Customisation des dispositions
 TCEFORM.tt_content {
  header_layout{
    removeItems = 1
  }
  header_layout.altLabels{
		2 = En-tête bloc ouvrant/fermant
                3 = Titre de niveau 3
                4 = Titre de niveau 4
                5 = Titre de niveau 5
	}

}




# ***************************************************************************************
# "Typical" Page TSconfig for htmlArea RTE and Classic RTE
#
# Sets Page TSConfig with most commonly used features representing a good start for typical sites.
#
# @author	Stanislas Rolland <stanislas.rolland(arobas)fructifor.ca>
#
# TYPO3 SVN ID: $Id: pageTSConfig.txt 4140 2008-09-17 02:57:32Z stan $
#
# Il y a aussi des modifs Made in Smart (Imad)
# ***************************************************************************************

	## Define labels and styles to be applied to class selectors in the interface of the RTE
	## The examples included here make partial re-use of color scheme and frame scheme from CSS Styled Content extension
	RTE.classes {
		
		align-justify {
			name = Alignement justifie
			value = text-align: justify;
		}
                align-center {
			name = Alignement centre
			value = text-align: center;
		}
                align-right {
			name = Alignement a droite
			value = text-align: right;
		}

                ico_tel {
			name = Icone telephone
                        value = background:url('../fileadmin/templates/images/ico_mail.png') no-repeat 0% 50%;padding:0 0 0 20px;
		}
		ico_mail {
			name = Icone email
                        value = background:url('../fileadmin/templates/images/ico_tel.png') no-repeat 0% 50%;padding:0 0 0 20px;
		}
                paranormal{
                        name = Paragraphe normal
                }
                clickhere{
                    name = Cliquez ici
                }
		

		
		
		
		warning{
			name = Bloc Alerte
			value = background: #fee; color: #400; border: 2px #844 solid; padding: 10px; margin-bottom: 1em; 
		}
		
		float_left{
			name = Bloc Aligne a gauche
			value = float: left; display:inline; margin-right: 1em; margin-bottom: 0.15em;
		}
		
		float_right{
			name = Bloc Aligne a droite
			value = float: right; display:inline; margin-right: 1em; margin-bottom: 0.15em;
		}
		
		
		

	}

	## Anchor classes configuration for use by the anchor accesibility feature (htmlArea RTE only)
	RTE.classesAnchor {
		externalLink {
			class = external-link
			type = url
			titleText = Lien vers un site externe
			target = _blank
		}
		
		

		download {
			class = download
			type = file
			titleText = Telecharger ce document
			target = _blank
		}

		mail {
			class = mail
			type = mail
			titleText = Envoyer un mail
		}
	}

	## Default RTE configuration
	RTE.default {

		###### CLASSES #######

		## List all class selectors that are allowed on the way to the database
		proc.allowedClasses (
			align-left, align-center, align-right, align-justify,
                        paranormal
		)

		classesParagraph (
			align-left, align-center, align-right,
			paranormal
		)
		classesTable = tableau,full,fixed
		classesTD = align-left, align-center, align-right
		classesLinks = pdf,xls,doc,ppt,zip,clickhere
		classesLinks.default {
			page =
			url = external-link
			file = download
			mail = mail
			oembed = Insertion video
			colorbox = Insertion Zoom image/contenu  
		}
		classesCharacter = tel, mail

		## Configuration of the anchor accessibility feature (htmlArea RTE only)
		## These classes should also be in the list of allowedClasses.
		classesAnchor =  pdf,xls,doc,ppt,zip,html,video, videolightbox, mail,tel,oembed,mp3,lightbox, lien_lightbox
		classesAnchor.default {
			page =
			url = external-link
			file = download
			mail = mail
			oembed = Insertion video
			colorbox = Insertion Zoom image/contenu  
		}

		# Target
		defaultLinkTarget =

		#########################################




		## Markup options (htmlArea RTE only)
		enableWordClean = 1
		removeTrailingBR = 1
		removeComments = 1
                removeTags >
		removeTags = center,  o:p, sdfield, strike,u, div
		removeTagsAndContents = link, meta, script, style, title
		showTagFreeClasses = 1

		showButtons (
			copy,cut,paste,class, blockstylelabel, blockstyle, textstylelabel, textstyle,
			formatblock, bold, italic, textcolor, fontsize, subscript, superscript,
			orderedlist, unorderedlist, outdent, indent,
			insertcharacter, link, unlink, table, findreplace, chMode, undo, redo, about,
			left, center, right,justifyfull, image,
			insertparagraphbefore, insertparagraphafter,
			toggleborders, tableproperties,
			rowproperties, rowinsertabove, rowinsertunder, rowdelete, rowsplit,
			columninsertbefore, columninsertafter, columndelete, columnsplit,
			cellproperties, cellinsertbefore, cellinsertafter, celldelete, cellsplit, cellmerge
		)


		## More toolbar options (htmlArea RTE only)
		keepButtonGroupTogether = 1

		## Afficher la barre d'etat
		showStatusBar =  1

		## Liste des balises  ne pas afficher dans le champ 'Bloc'
		hidePStyleItems = pre,address,h1

		## Permettre l'utilisation d'une CSS externe
		ignoreMainStyleOverride = 1


		## Configuration specific to the TableOperations feature (htmlArea RTE only)
		## Remove the following fieldsets from the table operations dialogs
		disableAlignmentFieldsetInTableOperations = 1
		disableSpacingFieldsetInTableOperations = 1
		disableColorFieldsetInTableOperations = 1
		disableLayoutFieldsetInTableOperations = 1
		disableBordersFieldsetInTableOperations = 1

		## Afficher par defaut les bordures la creation d'un tableau
		buttons.toggleborders.setOnTableCreation = 1

		## Configuration specific to the bold and italic buttons (htmlArea RTE only)
		## Add hotkeys associated with bold and italic buttons
		buttons.bold.hotKey = strong
		buttons.italic.hotKey = em

                proc {
			entryHTMLparser_db {				
				removeTags >
				removeTags = center, font, link, meta, o:p, sdfield
			}
		}
	}

	## front end RTE configuration for the general public (htmlArea RTE only)
	RTE.default.FE < RTE.default
	RTE.default.FE.showStatusBar = 0
	RTE.default.FE.hideButtons = chMode, blockstyle, textstyle, underline, strikethrough, subscript, superscript, lefttoright, righttoleft, table, inserttag, findreplace, removeformat
	RTE.default.FE.FE >
	RTE.default.FE.userElements >
	RTE.default.FE.userLinks >

	## tt_content TCEFORM configuration
	## Let use all the space available for more comfort.
	TCEFORM.tt_content.bodytext.RTEfullScreenWidth= 100%



	#Ajout d'une CSS personnalisee
	RTE.default.contentCSS = fileadmin/templates/css/rte.css
