lib.top = COA

lib.top{

    wrap = <ul class="lavaLamp">|</ul>

    
    1 = HMENU
    #1.entryLevel = 0
    1.special = directory
    1.special.value = 8
    stdWrap.required = 1
    1.1 = TMENU
    1.1{
      noBlur = 1
      NO {
          allWrap = <li> | </li>
          #stdWrap.htmlSpecialChars = 1
          stdWrap.cObject = CASE
          stdWrap.cObject {
            key.field = doktype
            default = TEXT
            default {
              field = title
              typolink {
                parameter.data = field:uid
              }
              #stdWrap.innerWrap = <span>|</span>
              stdWrap.innerWrap = |
            }

            3 < .default
            3 {
              stdWrap.htmlSpecialChars = 1
              typolink {
                parameter {
                  data >
                  dataWrap = http://{field:url}
                }
                extTarget = _top
              }
            }
          }
          
        doNotLinkIt = 1
        }
      ACT = 1
      ACT.stdWrap.htmlSpecialChars = 1
      ACT.allWrap = <li class="current"> | </li>
      #ACT.stdWrap.innerWrap = <span>|</span>
      ACT.stdWrap.innerWrap = |
      ACT.after.cObject < lib.top.1.1.NO.after.cObject
    }
    

   

	
}





lib.breadcrumb = COA
lib.breadcrumb{

	# Wrap general
	wrap = <div id="Breadcrum"><div xmlns:v="http://rdf.data-vocabulary.org/#">|</div></div>
        
        

	# Base du fil d'Ariane
	20 = HMENU
	20{
		entryLevel = 1
                # Configuration generale du fil
		special = rootline

		1 = TMENU
		1.noBlur = 1
		1.NO.ATagParams =  rel="v:url" property="v:title"
		1.NO.linkWrap = <span typeof="v:Breadcrumb">| &gt;&nbsp; </span>

		# Page courante
		1.CUR = 1
		1.CUR.doNotLinkIt = 1
		1.CUR.allWrap = <span typeof="v:Breadcrumb">|</span>
	}
}
#####
# Le titre d'une news
[globalVar = GP:tx_ttnews|tt_news > 0]
lib.breadcrumb.30 = RECORDS
lib.breadcrumb.30{
	dontCheckPid = 1
	tables = tt_news
	source.data = GP:tx_ttnews|tt_news
	conf.tt_news = TEXT
	conf.tt_news.field = title

	wrap = <span typeof="v:Breadcrumb">|</span>
}
[end]

#########################################################################################################
# Menu Gauche #
#########################################################################################################
lib.menugauche  = COA
lib.menugauche{

	#wrap = <dl class="menu_sec">|</dl>
	wrap = |

	#5 = TEXT
	#5.data = leveltitle:1
	#5.wrap = <dt><strong>|</strong></dt>

	15 = HMENU
	15{
		#entryLevel = 1
                special = directory
                special.value = 1
		

		1 = TMENU
		1{
			noBlur = 1
			wrap = <ul id="navlist" class="menu">|</ul>

			NO {
				allWrap = <li>|</li>
				stdWrap.innerWrap = |
			}

			ACTIFSUB = 1
			ACTIFSUB{
				allWrap = <li class="on">|</li>
				doNotLinkIt = 0
				stdWrap.innerWrap = |

				# Wrap des enfants du niveau 2 (= wrapItemAndSub)
				# On construit manuellement les enfants car wrapItemAndSub n'a pas de proprietes stdWrap
				after.cObject = COA
				after.cObject{
					# Menu des enfants de niveau 2
					10 = HMENU
					10{
						special = directory
						special.value.field = uid
						1 = TMENU
						1{
							wrap = <ul>|</ul>
							noBlur = 1

							# Etat NO
							NO{
								allWrap = <li>|</li>
							}
							ACT = 1
							ACT{
								linkWrap = <li class="on">|</li>
							}

						}
					}

				}
			}

			ACT = 1
			ACT{
				doNotLink = 1
				linkWrap = <li class="on">|</li>
				stdWrap.innerWrap = |
				after.cObject < lib.menugauche.15.1.ACTIFSUB.after.cObject
			}
		}
	}
}




lib.footer = COA
lib.footer {
    1 = HMENU
    1.special = directory
    1.special.value = 11
    wrap =  | 
    stdWrap.required = 1
    1.1 = TMENU
    1.1{
        noBlur = 1
      NO.allWrap  =  |*||*| | &nbsp;&#124;&nbsp;   || | 
      NO {
          stdWrap.cObject = CASE
          stdWrap.cObject {
            key.field = doktype
            default = TEXT
            default {
              field = title
              typolink {
                parameter.data = field:uid
              }
            }

            3 < .default
            3 {
              stdWrap.htmlSpecialChars = 1
              typolink {
                parameter {
                  data >
                  dataWrap = http://{field:url}
                }
                extTarget = _top
              }
            }
          }
        doNotLinkIt = 1
        }
      ACT = 1
      ACT.stdWrap.htmlSpecialChars = 1
      ACT.allWrap =  |*||*| | &nbsp;&#124;&nbsp;   || |
    }    
}



lib.footersn = COA
lib.footersn {
    1 = TEXT
    1.value =  <ul>
    stdWrap.required = 1
    2 = HMENU
    2.special = list
    2.special.value = 17
    2.stdWrap.required = 1
    2.1 = TMENU
    2.1{
        noBlur = 1
        NO.allWrap  =  <li>|</li>
        NO {
          ATagParams = class="facebook"
          stdWrap.cObject = CASE
          stdWrap.cObject {
            key.field = doktype
            default = TEXT
            default {
              field = title
              typolink {
                parameter.data = field:uid
                ATagParams = class="facebook"
              }
              stdWrap.innerWrap = <img src="fileadmin/templates/html/images/spacer.gif" width="24" height="20" alt="Facebook" /><span class="horscadre">|</span>
            }

            3 < .default
            3 {
              stdWrap.htmlSpecialChars = 1
              typolink {
                parameter {
                  data >
                  dataWrap = http://{field:url}
                }
                extTarget = _top
              }
            }
          }
          doNotLinkIt = 1
        }
        
        #NO.stdWrap.innerWrap = <img src="fileadmin/templates/html/images/spacer.gif" width="24" height="20" alt="Facebook" /><span class="horscadre">|</span>
    }

    3 = HMENU
    3{
        special = list
        special.value = 18
        stdWrap.required = 1
        1 = TMENU
        1{
            noBlur = 1
            NO.allWrap  =  <li>|</li>
            NO {
              ATagParams = class="twitter"
              stdWrap.cObject = CASE
              stdWrap.cObject {
                key.field = doktype
                default = TEXT
                default {
                  field = title
                  typolink {
                    parameter.data = field:uid
                    ATagParams = class="twitter"
                  }
                  stdWrap.innerWrap = <img src="fileadmin/templates/html/images/spacer.gif" width="24" height="20" alt="Facebook" /><span class="horscadre">|</span>
                }

                3 < .default
                3 {
                  stdWrap.htmlSpecialChars = 1
                  typolink {
                    parameter {
                      data >
                      dataWrap = http://{field:url}
                    }
                    extTarget = _top
                  }
                }
              }
              doNotLinkIt = 1
            }

            #NO.stdWrap.innerWrap = <img src="fileadmin/templates/html/images/spacer.gif" width="24" height="20" alt="Facebook" /><span class="horscadre">|</span>
        }
   }

    4 = HMENU
    4{
        special = list
        special.value = 19
        stdWrap.required = 1
        1 = TMENU
        1{
            noBlur = 1
            NO.allWrap  =  <li>|</li>
            NO {
              ATagParams = class="rss"
              stdWrap.cObject = CASE
              stdWrap.cObject {
                key.field = doktype
                default = TEXT
                default {
                  field = title
                  typolink {
                    parameter.data = field:uid
                    additionalParams = &type=100
                    ATagParams = class="rss"
                  }
                  stdWrap.innerWrap = <img src="fileadmin/templates/html/images/spacer.gif" width="24" height="20" alt="Facebook" /><span class="horscadre">|</span>
                }

                3 < .default
                3 {
                  stdWrap.htmlSpecialChars = 1
                  
                }

                
              }
              doNotLinkIt = 1
            }

            #NO.stdWrap.innerWrap = <img src="fileadmin/templates/html/images/spacer.gif" width="24" height="20" alt="Facebook" /><span class="horscadre">|</span>
        }
   }
   5 = TEXT
   5.value =  </ul>
}



